include 'sys/fool.inc'
videomix:
        Gnode 0,0,1920,1080,\
        .filesystem,\
        .preview,\
        .cliplist,\
        .tools,\
        .timeline;,\

.filesystem:
        Win filesystem,win.SHOW
.preview:
        Win preview,win.SHOW
.cliplist:
        Win cliplist,win.SHOW
.timeline:
        Win timeline,win.SHOW
.tools:
        Win tools,win.SHOW


@@:     db "Files",0
        dd @b
filesystem:
        Gnode 10,10,355,475,.title,next
        Box 0,0,355,475,White
.title: Vtxt 15,0,0,0,Black,@b,1,ugly

@@:     db "Preview",0
        dd @b
preview:
        Gnode 375,10,1000,475,.title,next
        Box 0,0,1000,475,Black
.title: Vtxt 15,0,0,0,White,@b,1,ugly

@@:     db "Clips",0
        dd @b
cliplist:
        Gnode 1385,10,525,475,.title,next
        Box 0,0,530,475,White
.title: Vtxt 15,0,0,0,Black,@b,1,ugly

@@:     db "Timeline",0
        dd @b
timeline:
        Gnode 10,500,1900,440,.title,next
        Node .asm,.time,.tracks,next
        Box 0,0,1900,440,Gray
.title: Vtxt 15,0,0,0,White,@b,1,ugly
.time:  Box 0,420,1900,20,Green
.tracks:
        Tex 0,0,1900,420,0,track
.asm:   Asm next
;        add dword[track.circle+circle.off-1],100
        ret
track:
        Gnode 0,0,1900,100,\
        .frame,\
        .title,\
        .content

.frame: Frame 0,0,1900,100,Red
.title:
        Gnode 0,0,200,100,.box
.box:   Box 0,0,200,100,Maroon

.content:
        Tex 200,0,1700,100,0,next
        Gnode 0,0,100,100,0,next
.circle:Circle 5,5,90,90,White
db 0,0


@@:     db "Tools",0
        dd @b
tools:
        Gnode 10,950,1900,120,.title,next
        Box 0,0,1900,120,Blue
.title: Vtxt 15,0,0,0,White,@b,1,ugly
