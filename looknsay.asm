;FOOLOS:
WINDOWED equ 1
ONTOP equ 1
TRANSPARENCY equ 220

WX = 300
WY = 30
WXL = 1000
WYL = 600

include 'sys/fool.inc'
        APPLICATION 0,0,WXL,WYL,"conway suite"
        Node putbmp,iteration,text1,text2,looknsay,button,keys,.init
.init:
        Init next
        Asm next
        mov eax,'1'
        mov ecx,0x00ff0000
        mov [palettes.usr+eax*4],ecx
        mov eax,'2'
        mov ecx,0x0000ff00
        mov [palettes.usr+eax*4],ecx
        mov eax,'3'
        mov ecx,0x000000ff
        mov [palettes.usr+eax*4],ecx

        ret

keys:   Node .up,.down,.left,.right,.space
.up:    Tmk key.up,1,next
        Asm decdw,putbmp.yl
.down:  Tmk key.down,1,next
        Asm incdw,putbmp.yl
.left:  Tmk key.left,1,next
        Asm decdw,putbmp.xl
.right: Tmk key.right,1,next
        Asm incdw,putbmp.xl
.space: Otk key.spc,button.act

button:
        But 10,70,100,20,Silver,@f,.act
.act:
        Asm next
        xor dword[looknsay],asm
        ret
@@:
        Node .txt,.frame
.frame:
        Frame 0,0,100,20,Blue
.txt:   Txt 20,5,0,0,Maroon,@f
@@:     db "COMPUTE",0
text1:
        Txt 10,10,0,0,Green,result1
text2:
        Txt 10,20,0,0,Blue,result2
iteration:
        Node .txt,.itnum,.sznum
.txt:   Vtxt 10,40,0,0,Black,@f,1,ugly
@@:
        db "iteration "
.it:    db "000",CRLF
        db "size = "
.size:  db "00000000000"
.size_end:
        db 0

.itnum: Fxdec looknsay.n,.it,.it+3
.sznum: Fxdec result1-4,.size,.size_end
MAX = 1'000'000
CHARS = 160
looknsay:
        dd 0,@f
.n      dd 0
.p       dd 0
.c       db 0,0
.result  dd result1,result2
@@:
        push esi edi eax ebx ecx
        inc dword[.n]
        mov byte[result1+MAX],0
        mov byte[result2+MAX],0
        mov al,[.c]
        mov [result1+CHARS],al
        mov al,[.c+1]
        mov [result2+CHARS],al

        mov ebx,[.p]
        mov esi,[.result+ebx*4]
        mov [text1+txt.txt],esi
        xor ebx,1
        mov [.p],ebx

        mov edi,[.result+ebx*4]
        mov [text2+txt.txt],edi
        xor ecx,ecx
        xor edx,edx
.loop0:           ;{
        mov ah,0
.loop1:            ;{
        mov al,[esi+ecx]
        inc ecx
        cmp ecx,MAX
        je .end
        or al,al
        je .end
        inc ah
        cmp al,[esi+ecx]
        je .loop1    ;}
        cmp al,[edi+ecx]
        mov byte[result+ecx],0
        jne @f
        add byte[result+ecx],al
@@:
        add ah,'0'
        mov [edi+edx],ah
        inc edx
        cmp edx,MAX
        je .end
        mov [edi+edx],al
        inc edx
        cmp edx,MAX
        je .end
        jmp .loop0 ;}
.end:
        mov [edi-4],edx
        mov [esi-4],ecx
        mov al,[result1+CHARS]
        mov [.c],al
        mov al,[result2+CHARS]
        mov [.c+1],al
        mov byte[result1+CHARS],0
        mov byte[result2+CHARS],0

        pop ecx ebx eax edi esi
        ret
.stop:
        mov dword[looknsay],0
        jmp .end

putbmp:
        dd bmp,20,100
.xl     dd 950
.yl     dd 400
        dd 0

result:
        rb MAX

        dd 1
result1:
        db '1',0
        rb MAX

        dd 0
result2:
        rb MAX
        db 0

appsize application