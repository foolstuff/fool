do:
.call=0
.it=4
.cc=8
        push esi
        fcall [esi+.it]
        pop esi
        cmp eax,NULL    ;equivalent to break
        je .end
        push esi
        fcall [esi+.cc]
        pop esi
        or eax,eax
        je do
.end:
        ret

macro Do it,cc {
        dd do,it,cc
}
