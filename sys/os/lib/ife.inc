ife:
.call=0
.cc=4
.then=8
.else=12
        push ebx esi
        mov ebx,esi
        fcall [ebx+.cc]
        mov esi,[ebx+.then]
        or eax,eax
        je @f
        mov esi,[ebx+.else]
@@:
        fcall
        pop esi ebx
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro If cc,t,e {
        dd ife,cc,t,e
}
