init:
.call=0
.func=4
.op0=4
.op1=8
.op2=12
.op3=16
.op4=20
.op5=24
.op6=28
        cmp dword[esi+.func],0
        je @f
        fcall dword[esi+.func]
        mov dword[esi+.call],0
@@:
        ret

macro Init f,[a] {
 common
        dd init
 forward
        dd a
 common
}
