tmk:
.call=0
.key=4
.min=8
.func=12
        mov eax,[esi+.key]
        mov al,[key+eax]
        cmp al,[esi+.min]
        jb @f
        fcall [esi+.func]
        ret
@@:
        xor eax,eax
        ret

macro Tmk k,f {
        dd tmk,k,1,f
}