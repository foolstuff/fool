node:
.call=0
.size=4
.list=8
        push ebx
        mov ebx,[esi+.size]
        shr ebx,2
        jl .end
@@:
        push ebx
        fcall [esi+.size+ebx*4]
        pop ebx
        dec ebx
        jg @b
.end:
        pop ebx
        ret

macro Node [a] {
 common
  local .a
        dd node,.a-$-4
 forward
        dd a
 common
  .a:
}
