otk:
.call=0
.key=4
.old=8
.func=12
        mov al,[key+key.cur]
        cmp al,[esi+.key]
        jne @f
        cmp al,[esi+.old]
        je @f
        mov [esi+.old],al
        fcall [esi+.func]
        ret
@@:
        mov [esi+.old],al
        xor eax,eax
        ret

macro Otk k,f {
        dd otk,k,0,f
}
