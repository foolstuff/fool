gptr:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.ptr=20
        push esi edi
        or edi,edi
        je @f
        call addxy
@@:
        mov edi,esi
        fcall [esi+.ptr]
        pop edi esi
        or edi,edi
        je @f
        call subxy
@@:
        ret

macro Gptr x,y,p {
        dd gptr,x,y,0,0,p
}