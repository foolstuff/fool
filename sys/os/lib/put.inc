put:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.cinv=21
.cvar=22
.bmp=24
        cmp dword[esi+.bmp],0
        je .frame
        push dword[esi+.x] dword[esi+.y]
        or edi,edi
        je @f
        call addxy
@@:
        push esi edi
        mov edi,esi
        mov ecx,[esi+.c]
        mov esi,[esi+.bmp]
        push dword[esi+.c]
        add [esi+.c],ecx
        push dword[esi+.xl] dword[esi+.yl]
        pop dword[edi+.yl] dword[edi+.xl]
        fcall
        pop dword[esi+.c]
        pop edi esi
        pop dword[esi+.y] dword[esi+.x]
        ret
.frame:
        call frame
        ret

macro Put x,y,xl,yl,c,b {
        dd put,x,y,xl,yl,c,b
}