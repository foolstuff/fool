include '../bmp/poussoir.inc'
win:
.call=0
.child=4
.show=8
.but=12
.SHOW=0
.HIDE=1
        mov eax,[esi+.show]
        mov dword[titlebar.cross+put.bmp],poussoirh
;        mov [.min],eax
        or eax,eax
        jne @f
        mov dword[titlebar.cross+put.bmp],poussoirl
if defined DRAGABLE
        call grab
else
        fcall [esi+.child]
end if
        call .border
@@:
        call .button
        ret

.button:
        push esi edi
        push esi
        push esi
        mov esi,[esi+.child]
        call addxy
        mov edi,esi
        pop esi

        lea esi,[esi+.but]
        call pixel.setalpha+4
        fcall
        call pixel.notalpha+4

        pop esi
        cmp byte[esi+.show],0
        je @f
        mov esi,[esi+.child]
        mov esi,[esi-4]
        mov [titlebar.str+vtxt.txt],esi
        mov [titlebar.backstr+vtxt.txt],esi
        mov esi,titlebar.title
        call caller
@@:

        pop edi esi

        push esi
        mov esi,[esi+.child]
        call subxy
        pop esi

        ret

.border:
        push esi
        mov esi,[esi+.child]
        call border
;        call frame
        pop esi
        ret

;.min:   dd 0

.hide:
        mov eax,[esi+asm.op1]
        xor dword[eax+.show],1
        ret


titlebar:
.title: Node pixel.notalpha,.str,pixel.setalpha,.backstr
.str:   Vtxt 20,-7,0,0,White,0,2,ugly
.backstr:
        Vtxt 21,-6,0,0,Black,0,2,ugly

.but:
        Node @f,.cross
@@:     Border 2,2,11,11,Gold
.cross: Put 2,2,0,0,200,poussoirx

macro Win child,show {
@@:
if show eq
        dd win,child,1
else
        dd win,child,show
end if
        But 0,0,11,11,0,titlebar.but,next
        Asm win.hide,@b
}