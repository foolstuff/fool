;transparent return
plotc:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.buf=24
.xoff=28
.yoff=32
.xscl=36
.yscl=40
	push eax ebx ecx edx esi ebp
	push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
	mov ecx,[esi+.xl]
	dec ecx
	jle .end
	mov ebp,[esi+.yl]
	dec ebp
	add ebp,[esi+.y]
	add ebp,[esi+.yoff]
	mov edx,[esi+.buf]
	or edx,edx
	je .end
	add edx,[esi+.xoff]
	mov dword[esi+.xl],1
.first:
	push ecx edx
	movzx eax,byte[edx]
	mov ecx,[esi+.xscl]
	dec ecx
	je .s1
@@:
	movzx ebx,byte[edx+ecx]
	add eax,ebx
	loop @b
.s1:
	cdq
	idiv dword[esi+.xscl]
	cdq
	idiv dword[esi+.yscl]
	pop edx ecx
	neg eax
	add eax,ebp
	add edx,[esi+.xscl]
	dec ecx
	je .end
.loop:
	push eax ecx edx
	movzx eax,byte[edx]
	mov ecx,[esi+.xscl]
	dec ecx
	je .s
@@:
	movzx ebx,byte[edx+ecx]
	add eax,ebx
	loop @b
.s:
	cdq
	idiv dword[esi+.xscl]
	cdq
	idiv dword[esi+.yscl]
	mov ebx,eax
	pop edx ecx eax
	neg ebx
	add ebx,ebp
	mov [esi+.y],eax
	mov eax,ebx
	sub ebx,[esi+.y]
	mov [esi+.yl],ebx
	call line
	inc dword[esi+.x]
	add edx,[esi+.xscl]
	loop .loop
.end:
	pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
	pop ebp esi edx ecx ebx eax
	ret