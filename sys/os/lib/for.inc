;For init,until,increment,do
;init var dword 0
;until var equal 120
;add var 1
;stuff to do

for:
.call=0
.init=4
.stop=8
.inc=12
.do=16
        fcall [esi+.init]
.loop:
        fcall [esi+.stop]
        or eax,eax
        je .end
        fcall [esi+.do]
        ;cmp eax,NULL     ;break
        ;je .end
        fcall [esi+.inc]
        jmp .loop
.end:
        ret

macro For i,e,p,d {
        dd for,i,e,p,d
}
