;transparent return
but:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.bmp=24
.event=28
        push eax ebx ecx edx edi
        cmp dword[esi+.bmp],0
        je @f
        mov ecx,[esi+.bmp]
        mov eax,[ecx+.call]
        cmp dword[eax],txt
        jne .nobox
        call box
.nobox:
        test dword[eax+4],1
        je @f
        push dword[ecx+.xl] dword[ecx+.yl]
        pop dword[esi+.yl] dword[esi+.xl]
@@:
        mov ecx,[esi+.c]
        push ecx
;        call onmouse
;        jnc .end
;        mov dword[mouse+.bmp],doigt
        call frame;border
        cmp byte[esi+.c],0
        je @f
        add byte[esi+.c],4
@@:
;        test byte[mouse+int33.status],int33.lclic
;        je @f
;        mov dword[mouse+int33.bmp],clic
@@:
;        mov ecx,[mouse+int33.leftedge]
;        cmp cl,1
;        jne @f
;        mov [mouse+int33.edi],esi
;@@:
;        cmp [mouse+int33.edi],esi
;        jne .end
;        test byte[mouse+int33.status],1
;        je .end
;        mov eax,[esi+.event]
;        mov [mouse+int33.esi],eax
        cmp byte[esi+.c],0
        je .end
        dec byte[esi+.c]
.end:
        cmp dword[esi+.bmp],0
        jne @f
        cmp byte[esi+.c],0
        je .exit
        call box
        call border
        jmp .exit
@@:
        push esi edi
        call addxy
        mov edi,esi
        mov esi,[esi+.bmp]
        call caller
        pop edi esi
        call subxy
.exit:
        pop ecx
        mov [esi+.c],ecx
        pop edi edx ecx ebx eax
        ret

macro But x,y,xl,yl,c,b,e {
        dd but,x,y,xl,yl,c,b,e
}
