;transparent return
pong:
.call=0
.dx=4
.dy=8
        push eax ebx ecx edx
        call loadxy
        add eax,[esi+.dx]
        jge @f
        neg dword [esi+.dx]
        add eax,[esi+.dx]
@@:
        add eax,ecx
        cmp eax,[desk+screen.xl]
        jle @f
        neg dword [esi+.dx]
        add eax,[esi+.dx]
@@:
        sub eax,ecx
        add ebx,[esi+.dy]
        jge @f
        neg dword [esi+.dy]
        add ebx,[esi+.dy]
@@:
        add ebx,edx
        cmp ebx,[desk+screen.yl]
        jle @f
        neg dword [esi+.dy]
        add ebx,[esi+.dy]
@@:
        sub ebx,edx
        mov [edi+item.x],eax
        mov [edi+item.y],ebx
        pop edx ecx ebx eax
        ret

macro Pong x,y {
        dd pong,x,y
}

