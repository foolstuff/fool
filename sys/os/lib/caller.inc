macro fcall o {
;fcall r/m/imm
;this is the master piece in fool
;the fool call macro instruction
;integrate fool calling convention in x86 macro instruction set :)
        if ~ o eq
        if ~ o eq esi
        push esi
        mov esi,o ;o is any operand able to be taken b esi.
        end if
        end if
        call caller ;THE system function to execute a fool call.
        if ~ o eq
        if ~ o eq esi
        pop esi
        end if
        end if
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
caller:
.call=0
        push ebx
if defined FSTACK
        call fstack.push
end if
        or esi,esi
        je @f
        mov ebx,[esi+.call]
        or ebx,ebx
        je @f
;        mov ebx,[ebx]
;        or ebx,ebx
;        je @f
        inc [.tcnt]
        call ebx
@@:
if defined FSTACK
        call fstack.pop
end if
        pop ebx
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.cnt    dd 0
.tcnt   dd 0
        align 16

if defined FSTACK
db 'start of fstack'
        align 16
fstack:
        rd 256
db 'end of fstack'
.push:
        push eax ebx
        mov eax,fstack
        mov ebx,[eax]
        mov [eax+ebx*4],esi
        inc ebx
        mov [eax],ebx
        pop ebx eax
        ret
.pop:
        push eax ebx
        mov eax,fstack
        dec dword[eax]
        pop ebx eax
        ret
end if