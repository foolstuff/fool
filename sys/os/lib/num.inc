;transparent return
num:
.call=0
.num=4
.txt=8
.type=12
.bin:                         ; num.bin, to print variable leng binary strings
        push eax ebx ecx edx esi edi ; save the previous context
        mov eax,[esi+.num]    ; the memory location of bitstring
        or eax,eax
        je .end
        push eax
        mov eax,[eax]         ; the addressing is a second stage one.
        mov ecx,[esi+.type]   ; bit string leng
        mov edx,[esi+.txt]    ; the text buffer, to write convert in ascii
        lea edx,[edx+ecx]     ; point to the last byte of ascii buffer
;        mov byte[edx],0       ; this is a null terminated string
        dec edx               ; point to first bit image
        mov bh,32             ; this is the size of one single stream, a dword
.onebit: 
        mov bl,'0' 
        shr eax,1             ; the lsb is the carry
        adc bl,0              ; add the carry, can be done by a jnc @f, inc bl
        mov [edx],bl          ; fill in ascii buffer
        dec edx               ; point to next bit image
        dec bh 
        jne @f                ; is the end of bit stream?
        mov bh,32             ; reinit the bitstream
        pop eax               ; load the next dword of the bit string
        add eax,4             ; .
        push eax              ; .
        mov eax,[eax]         ; there
@@: 
        dec ecx               ; is it the last bit?
        jne .onebit           ; no, continue
        pop eax 
        inc edx     
        mov [esi+.txt],edx    ; set the text pointer, to know where the string begins
.end:
        pop edi esi edx ecx ebx eax   ; restore the previous context
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.hex:                         ; num.hex, to print variable leng hexadecimal strings
        push eax ebx ecx edx esi edi  ; save the previous context
        mov eax,[esi+.num]    ; the memory location of hexstring
        or eax,eax
        je .end
        push eax
        mov eax,[eax]         ; the addressing is a second stage one.
        mov ecx,[esi+.type]   ; hexa string leng
        mov edx,[esi+.txt]    ; the text buffer, to write convert in ascii
        lea edx,[edx+ecx]     ; point to the last byte of ascii buffer
;        mov byte[edx],0       ; this is a null terminated string
        dec edx               ; point to first nibble image
        mov bh,32             ; this is the size of one single stream, a dword
.onenibble:
        mov bl,al
        and bl,0fh
        add bl,'0'
        cmp bl,'9'
        jle @f
        add bl,'A'-'9'-1
@@:
        shr eax,4
        mov [edx],bl          ; fill in ascii buffer
        dec edx               ; point to next nibble image
        sub bh,4
        jne @f                ; is the end of nibble stream?
        mov bh,32             ; reinit the bitstream
        pop eax               ; load the next dword of the nibble string
        add eax,4             ; .
        push eax              ; .
        mov eax,[eax]         ; there
@@: 
        dec ecx               ; is it the last nibble?
        jne .onenibble        ; no, continue
        pop eax 
        inc edx     
        mov [esi+.txt],edx    ; set the text pointer, to know where the string begins
        pop edi esi edx ecx ebx eax   ; restore the previous context
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
