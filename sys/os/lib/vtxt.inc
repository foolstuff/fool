;transparent return
vtxt:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.txt=24
.zoom=28
.font=32
.dx=0
.dy=1
.w=2

        cmp dword[esi+.zoom],0
        jne @f
        push dword[esi+.font]
        mov dword[esi+.font],font85
        call txt
        pop dword[esi+.font]
        jmp .over
@@:
        push eax edx ebp
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        mov ebp,[esi+.x]
        mov edx,[esi+.txt]
.nextchar:
        mov al,[edx]
        inc edx
        or al,al
        je .endofstring
        cmp al,9
        je .tab
        cmp al,10
        je .cr
        cmp al,13
        je .lf
        cmp al,' '
        je .spc

        call .putc

        jmp .nextchar
.endofstring:
        pop dword [esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop ebp edx eax
.over:
        ret

.cr:
        mov [esi+.x],ebp
        mov eax,0
        mov ebx,0
        jmp @f
.lf:
        mov eax,0
        mov ebx,15
        jmp @f
.tab:
        mov eax,20
        mov ebx,0
        jmp @f
.spc:
        mov eax,4
        mov ebx,0
        jmp @f

@@:
        imul eax,[esi+.zoom]
        imul ebx,[esi+.zoom]
        add [esi+.x],eax
        add [esi+.y],ebx
        jmp .nextchar

.putc:
        push eax ebx ecx edx esi edi
        push dword[esi+.x]  dword[esi+.y]
        push ebp
        movzx eax,al
        xchg ebp,edi
        mov edi,[esi+.font]
        lea edi,[eax*4+edi]
        mov edi,[edi]
        or edi,edi
        je .end
        movzx ecx,byte[edi]
        inc edi
        dec ecx
        jl .end
.next:
        movsx eax,byte[edi]
        imul eax,[esi+.zoom]
        inc edi
        movsx ebx,byte[edi]
        imul ebx,[esi+.zoom]
        inc edi
        mov [esi+.xl],eax
        mov [esi+.yl],ebx
        cmp byte[edi],0
        je @f
        xchg ebp,edi
        call line
        xchg ebp,edi
@@:
        add [esi+.x],eax
        add [esi+.y],ebx
        inc edi
        sub ecx,3
        jnl .next
.end:
        pop ebp
        pop dword[esi+.y] dword[esi+.x]
        movsx eax,byte[edi]
        movsx ebx,byte[edi+1]
        imul eax,[esi+.zoom]
        imul ebx,[esi+.zoom]
        add [esi+.x],eax
        add [esi+.y],ebx
        pop edi esi edx ecx ebx eax
        ret


;.wintext:
;        invoke  TextOut,[hDC],100,100,Hello,[PrinterNameSize]
;        ret
;
;        invoke DrawTextEx,HDC,textptr,textsize(-1),rect,format,extformat(0)

macro Vtxt x,y,xl,yl,c,t,z,f {

        dd vtxt,x,y,xl,yl,c,t
if z eq
        dd 1
else
        dd z
end if
if f eq
        dd ugly
else
        dd f
end if
}
