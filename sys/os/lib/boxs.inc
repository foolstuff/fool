;transparent return
boxs:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.ref=24
.size=28
.str=32
	push eax ebx ecx edx
	push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
	push dword[esi+.c]
	mov eax,[esi+.xl]
	xor ecx,ecx
	mov edx,[esi+.c]
.next:
	mov bl,[esi+.str+ecx]
	or bl,bl
	je .no
	movzx ebx,bl
	add ebx,[esi+.ref]
	mov bl,[ebx]
	add bl,dl
	movzx ebx,bl
	mov ebx,[palettes.usr+ebx*4]
	mov [esi+.c],ebx
	call box
.no:
	add [esi+.x],eax
	inc ecx
	cmp ecx,[esi+.size]
	jne .next
.end:
	pop dword[esi+.c]
	pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
	pop edx ecx ebx eax
	ret
