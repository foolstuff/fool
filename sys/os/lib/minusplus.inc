minus:
.call=0
.a=4
        mov eax,[esi+.a]
        or eax,eax
        je @f
        dec dword[eax]
        mov eax,[eax]
@@:
        ret


macro Minus a {
        dd minus,a
}

plus:
.call=0
.a=4
        mov eax,[esi+.a]
        or eax,eax
        je @f
        inc dword[eax]
        mov eax,[eax]
@@:
        ret

macro Plus a {
        dd plus,a
}