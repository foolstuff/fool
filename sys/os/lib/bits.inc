;transparent return
bits:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.ref=24
.size=28
.str=32
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        push dword[esi+.c]
        mov eax,[esi+.xl]
        mov edx,[esi+.c]
        xor ecx,ecx
.next:
        mov ebx,[esi+.ref]
        mov bl,[ebx]
        mov bh,[esi+.str+ecx]
        or bh,bh
        je .ignore
        test bl,bh
        mov ebx,edx
        je @f
        not ebx
@@:
        mov [esi+.c],ebx
        call box
.ignore:
        add [esi+.x],eax
        inc ecx
        cmp ecx,[esi+.size]
        jne .next
.end:
        pop dword[esi+.c]
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret
