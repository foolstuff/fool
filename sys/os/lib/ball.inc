ball:
.x dd 0
.y dd 0
.xl dd 320
.yl dd 200
.c dd 003243D0h
balling:
        mov eax,[ball.x]
        mov ebx,[ball.y]
        mov ecx,[ball.xl]
        mov edx,[ball.yl]
        add eax,[.dx]
        jge @f
        neg dword [.dx]
        add eax,[.dx]
@@:
        add eax,ecx
        cmp eax,[xres]
        jl @f
        neg dword [.dx]
        add eax,[.dx]
@@:
        sub eax,ecx
        add ebx,[.dy]
        jge @f
        neg dword [.dy]
        add ebx,[.dy]
@@:
        add ebx,edx
        cmp ebx,[yres]
        jl @f
        neg dword [.dy]
        add ebx,[.dy]
@@:
        sub ebx,edx
        mov [ball.x],eax
        mov [ball.y],ebx

        mov eax,[ball.x]
        mov ebx,[ball.y]
        mov ecx,[ball.xl]
        mov edx,[ball.yl]
        mov     [box.x],eax
        mov     [box.y],ebx
        mov     [box.xl],ecx
        mov     [box.yl],edx
        mov eax,[ball.c]
        mov [box.c],eax
        call box
        ret
.dx dd 4
.dy dd 3
