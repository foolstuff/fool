;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
screen:
.call=0
;geometry
.x=4
.y=8
.xl=12;dd 0
.yl=16;dd 0
.count=20
;screen description
.bpp=24; dd 0
.size=28; dd 0

;memory model
.buffer=32; dd sys_vbuff
.frame=36; dd sys_vesa

;pixel methods
.pixel=40; dd pixel32
.getpixel=44; dd pixel32

.child=48

;optional values
.bpsl=52; dd 0
.bptl=56; dd 0


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        push edi esi
        call vsync
        pop esi
        mov edi,esi
        fcall [esi+.child]
        pop edi
        ret


.init:
        push eax edx
;set the screen geometry
        mov eax,[xres]
        mov [esi+.xl],eax
        mov eax,[yres]
        mov [esi+.yl],eax
        xor edx,edx
        imul eax,dword[esi+.xl]
        mov [esi+.count],eax
        mov eax,[bpp]
        mov [esi+.bpp],eax
        mov eax,[bpsl]
        mov [esi+.bpsl],eax
        mov eax,[bptl]
        mov [esi+.bptl],eax
        mov eax,[screensize]
        mov [esi+.size],eax
        mov dword [esi+.pixel],pixel
        mov dword [esi+.getpixel],getpixel
        mov dword [esi+.buffer],40'0000h;sys_vbuff
        mov word [esi+.frame],sys_vesa
        pop edx eax
        ret

macro Screen x,y,xl,yl,color,bpp,child,frame {
if frame eq
        dd screen,0,0,0,0,color,0,0,0,0,pixel,0,child,0,0
else
        dd screen,0,0,0,0,color,0,0,0,frame,pixel,0,child,0,0
end if
}