;transparent return
buts:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.ref=24
.size=28
.str=32
.func=36
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        push dword[esi+.c]
        mov eax,[esi+.xl]
        ;dec eax
        xor ecx,ecx
        mov edx,[esi+.c]
.next:
        movzx ebx,byte[esi+.str+ecx]
        or ebx,ebx
        je .no
.displaybutton:
        add ebx,[esi+.ref]
        mov bl,[ebx]
        add bl,dl
        shl bl,1
        movzx ebx,bl
        mov ebx,[palettes.usr+ebx*4]
        mov [esi+.c],ebx
        call box
        call border
        add dword[esi+.x],2
        add dword[esi+.y],2
        sub dword[esi+.xl],4
        sub dword[esi+.yl],4
        movzx ebx,dl
        mov ebx,[palettes.usr+ebx*4]
        mov [esi+.c],edx
        call frame
        sub dword[esi+.x],2
        sub dword[esi+.y],2
        add dword[esi+.xl],4
        add dword[esi+.yl],4
        add [esi+.x],eax
.no:
        inc ecx
        cmp ecx,[esi+.size]
        jl .next
.end:
        pop dword[esi+.c]
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret
