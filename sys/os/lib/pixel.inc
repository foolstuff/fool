;align 16
pixel:
;eax=x
;ebx=y
;ecx=color
       ; inc [.count]
        call [.pix]
        inc dword[desk.pixels]
        ret
       ; call dword[.pix]
       ; align 4
       ; inc dword[desk.pixels]
       ; ret

.direct:
        push ebx edi
        cmp eax,[desk+screen.xl]
        jae @f          ;thx fodder for this trick!
        cmp ebx,[desk+screen.yl];
        jae @f
        imul ebx,[desk+screen.xl];maybe here, we need optimized mul
        lea ebx,[ebx+eax]
        mov edi,[desk+screen.buffer]
        mov [edi+ebx*4],ecx
@@:
        pop edi ebx
        ret

.pix    dd .direct
.notalpha:
.setalpha:
        dd 0


macro Pixel {
      call pixel
}

getpixel:
;ebx=prepared coordinate
;ecx=color
        cmp ebx,[desk+screen.size]
        jae @f          ;thx fodder for this trick!
        mov edi,[desk+screen.buffer]
        mov ecx,[edi+ebx*4]
@@:
        ret
        align 4

macro GetPixel {
      call getpixel ;dword [desk+screen.getpixel]
}
