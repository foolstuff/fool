inb:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        in al,dx
        movzx eax,al
        pop edx
        ret

macro Inb a {
        dd inb,a
}


outb:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        out dx,al
        pop edx
        ret

macro Outb a {
        dd outb,a
}

inw:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        in ax,dx
        movzx eax,ax
        pop edx
        ret

outw:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        out dx,ax
        pop edx
        ret

ind:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        in eax,dx
        pop edx
        ret

outd:
.call=0
.io=4
        push edx
        mov edx,[esi+.io]
        out dx,eax
        pop edx
        ret
