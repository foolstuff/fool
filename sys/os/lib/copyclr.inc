copyclr:
.call=0
.dest=4
.src=8
.size=12
.c=16
        push fs es eax ebx ecx
        mov eax,[esi+.dest]
        mov ebx,[esi+.src]
        mov es,word [eax]
        mov fs,word [ebx]
        mov ebx,[esi+.c]
        mov ecx,[esi+.size]
        mov ecx,[ecx]
        dec ecx
        or ebx,ebx
        je .loop2
.loop:
        mov eax,[fs:ecx*4]
        mov [fs:ecx*4],ebx
        mov [es:ecx*3],ax
        shr eax,16
        mov [es:ecx*3+2],al
        dec ecx
        jnl .loop
        jmp .end
.loop2:
        mov eax,[fs:ecx*4]
        mov [es:ecx*3],ax
        shr eax,16
        mov [es:ecx*3+2],al
        dec ecx
        jnl .loop2
.end:
        pop ecx ebx eax es fs
        ret

macro Copyclr d,s,n,c {
        dd copyclr,d,s,n,c
}