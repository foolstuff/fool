;transparent return, maybe thread safe?
line:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.xinc1  equ esp+0
.xinc2  equ esp+4
.yinc1  equ esp+8
.yinc2  equ esp+12
.dmin   equ esp+16
.dmax   equ esp+20
        push eax ebx ecx edx esi edi
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        sub esp,6*4
        or edi,edi
        je @f
        call addxy
@@:
        mov ecx,1
        mov edx,1
        xor edi,edi
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
        cmp eax,edi
        jge @f
        neg eax
        neg ecx
@@:
        cmp ebx,edi
        jge @f
        neg ebx
        neg edx
@@:
        cmp eax,ebx
        jl .isy
.isx:
        mov [.xinc1],ecx
        mov [.xinc2],edi
        mov [.yinc1],edi
        mov [.yinc2],edx
        mov [.dmax],eax
        mov [.dmin],ebx
        jmp @f
.isy:
        mov [.xinc1],edi
        mov [.xinc2],ecx
        mov [.yinc1],edx
        mov [.yinc2],edi
        mov [.dmax],ebx
        mov [.dmin],eax
@@:
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        mov edx,[.dmax]
        shr edx,1
        mov ecx,[esi+.c]
        mov edi,[.dmax]
@@:
        Pixel
        dec edi
        jl @f
        add eax,[.xinc1]
        add ebx,[.yinc1]
        sub edx,[.dmin]
        jge @b
        add eax,[.xinc2]
        add ebx,[.yinc2]
        add edx,[.dmax]
        jmp @b
@@:
.end:
        add esp,6*4
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edi esi edx ecx ebx eax
        ret

macro Line x,y,xl,yl,c {
        dd line,x,y,xl,yl,c
}

