border:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.mod=4

if defined NO3DBORDER
        call frame
        ret
end if

        call box
        push eax ebx ecx edx ebp
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        mov ebp,.mod
        call .atom
        mov ebp,.mod/2
        inc dword[esi+.x]
        inc dword[esi+.y]
        sub dword[esi+.xl],2
        sub dword[esi+.yl],2
        call .atom
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop ebp edx ecx ebx eax
        ret

.atom:
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        mov ecx,[esi+.c]
        mov edx,[esi+.yl]
        add ecx,ebp
@@:
        dec edx
        jle @f
        Pixel
        inc ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
        sub ecx,ebp
        sub ecx,ebp
@@:
        dec edx
        jle @f
        Pixel
        inc eax
        jmp @b
@@:
        mov edx,[esi+.yl]
@@:
        dec edx
        jle @f
        Pixel
        dec ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
        add ecx,ebp
        add ecx,ebp
@@:
        dec edx
        jle @f
        Pixel
        dec eax
        jmp @b
@@:
        ret

macro Border x,y,xl,yl,c {
        dd border,x,y,xl,yl,c
}