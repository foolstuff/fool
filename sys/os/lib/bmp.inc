;transparent return
bmp1:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.bmp=24
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        xor edx,edx
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        add [esi+.xl],eax
        add [esi+.yl],ebx
.loop:
        bt dword[esi+.bmp],edx
        jc .ignore
        mov ecx,[esi+.c]
        Pixel
.ignore:
        inc edx
        inc eax
        cmp eax,[esi+.xl]
        jl .loop
        mov eax,[esi+.x]
        inc ebx
        cmp ebx,[esi+.yl]
        jl .loop
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

bmp:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.cinv=21
.bmp=24
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        xor edx,edx
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        add [esi+.xl],eax
        add [esi+.yl],ebx
.loop:
        xor ecx,ecx
        mov cl,[esi+.bmp+edx]
        cmp cl,[esi+.cinv]
        je .ignore
        add cl,[esi+.c]

        mov ecx,[palettes.usr+ecx*4]
        shl ecx,8
        mov cl,ch
        rol ecx,16
        ror cx,8
        Pixel
.ignore:
        inc edx
        inc eax
        cmp eax,[esi+.xl]
        jl .loop
        mov eax,[esi+.x]
        inc ebx
        cmp ebx,[esi+.yl]
        jl .loop
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

bmp32:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.cinv=20
.bmp=24
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        xor edx,edx
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        add [esi+.xl],eax
        add [esi+.yl],ebx
.loop:
        xor ecx,ecx
        mov ecx,[esi+.bmp+edx*4]
        cmp ecx,[esi+.cinv]
        je .ignore
        add ecx,[esi+.c]

        Pixel
.ignore:
        inc edx
        inc eax
        cmp eax,[esi+.xl]
        jl .loop
        mov eax,[esi+.x]
        inc ebx
        cmp ebx,[esi+.yl]
        jl .loop
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro Bmp32 x,y,xl,yl,c,[p] {
 if p eq
        dd bmp32,x,y,xl,yl,c
 else
 common
        dd bmp32,x,y,xl,yl,c
 forward
        dd p
 common
 end if
}

macro Bmp x,y,xl,yl,c,[p] {
 if p eq
        dd bmp,x,y,xl,yl,c
 else
 common
        dd bmp,x,y,xl,yl,c
 forward
        db p
 common
 end if
}

macro Bmp1 x,y,xl,yl,c,[p] {
 if p eq
        dd bmp1,x,y,xl,yl,c
 else
 common
        dd bmp1,x,y,xl,yl,c
 forward
        db p
 common
 end if
}
