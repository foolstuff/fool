; font to use for txt.inc function.
font85:
.nul	db 00000000b
       db 00000000b
       db 01000000b
       db 01000000b
       db 00000000b
       db 00000000b
.smilw	db 01111110b
       db 11011001b
       db 10111111b
       db 10111111b
       db 11011001b
       db 01111110b
.smilb	db 01111110b
       db 10000101b
       db 10100001b
       db 10100001b
       db 10000101b
       db 01111110b
.etx	db 00111000b
       db 01111100b
       db 11111000b
       db 01111100b
       db 00111000b
       db 00000000b
.eot	db 00010000b
       db 00111000b
       db 11111110b
       db 00111000b
       db 00010000b
       db 00000000b
.enq	db 00111000b
       db 10111011b
       db 11010111b
       db 10111011b
       db 00111000b
       db 00000000b
.ack	db 00011000b
       db 10111100b
       db 11011110b
       db 10111100b
       db 00011000b
       db 00000000b
.bel	db 00000000b
       db 00011000b
       db 00111100b
       db 00111100b
       db 00011000b
       db 00000000b
.bs	db 11111111b
       db 11100111b
       db 11000011b
       db 11000011b
       db 11100111b
       db 11111111b
.tab	db 00000000b
       db 00000000b
       db 01000000b
       db 01000000b
       db 00000000b
       db 00000000b
.lf	db 00000000b
       db 00000000b
       db 01000000b
       db 01000000b
       db 00000000b
       db 00000000b
.vt	db 01110000b
       db 10001000b
       db 10001010b
       db 01110110b
       db 00001110b
       db 00000000b
.np	db 00000110b
       db 01001001b
       db 11111001b
       db 01001001b
       db 00000110b
       db 00000000b
.cr	db 00000000b
       db 00000000b
       db 01000000b
       db 01000000b
       db 00000000b
       db 00000000b
.so	db 11000000b
       db 01111111b
       db 00000101b
       db 00000101b
       db 01100101b
       db 00111111b
.si	db 01100110b
       db 00011000b
       db 11100111b
       db 00011000b
       db 01100110b
       db 00000000b
.dle	db 00000000b
       db 11111111b
       db 01111110b
       db 00111100b
       db 00011000b
       db 00000000b
.dc1	db 00000000b
       db 00011000b
       db 00111100b
       db 01111110b
       db 11111111b
       db 00000000b
.dc2	db 00100100b
       db 01100110b
       db 11111111b
       db 01100110b
       db 00100100b
       db 00000000b
.dc3	db 00000000b
       db 11011111b
       db 00000000b
       db 11011111b
       db 00000000b
       db 00000000b
.dc4	db 00001110b
       db 00010001b
       db 11111111b
       db 00000001b
       db 11111111b
       db 00000000b
.nak	db 01000000b
       db 10011010b
       db 10100101b
       db 10100101b
       db 01011001b
       db 00000000b
.syn	db 00111000b
       db 01111100b
       db 01111100b
       db 01111100b
       db 01111100b
       db 00111000b
.etb	db 01000000b
       db 01010010b
       db 01111111b
       db 01010010b
       db 01000000b
       db 00000000b
.can	db 00000100b
       db 00000110b
       db 11111111b
       db 00000110b
       db 00000100b
       db 00000000b
.em	db 00100000b
       db 01100000b
       db 11111111b
       db 01100000b
       db 00100000b
       db 00000000b
.eof	db 00000000b
       db 00000000b
       db 01000000b
       db 01000000b
       db 00000000b
       db 00000000b
.esc	db 00010000b
       db 00111000b
       db 01010100b
       db 10010010b
       db 00010000b
       db 00010000b
.fs	db 00000000b
       db 00011110b
       db 00010000b
       db 00010000b
       db 00010000b
       db 00000000b
.gs	db 00010000b
       db 00111000b
       db 00010000b
       db 00010000b
       db 00111000b
       db 00010000b
.rs	db 01000000b
       db 01100000b
       db 01110000b
       db 01100000b
       db 01000000b
       db 00000000b
.us	db 00000100b
       db 00001100b
       db 00011100b
       db 00001100b
       db 00000100b
       db 00000000b
.espace   db 0
       db 0
       db 0
       db 0
       db 0
       db 0
.exclam db 00000000b
       db 00000000b
       db 10111111b
       db 00001110b
       db 00000000b
       db 0
.guill	db 00000000b
       db 00000110b
       db 00000000b
       db 00000110b
       db 00000000b
       db 0
.diese db 00101000b
      db 11111110b
      db 00101000b
      db 11111110b
      db 00101000b
      db 0
.dollar db 00100100b
       db 01001010b
       db 11111111b
       db 01010010b
       db 00100100b
       db 0
.%     db 10000000b
       db 01100010b
       db 00011000b
       db 01000110b
       db 00000001b
       db 0
.et    db 01100000b
       db 10010110b
       db 10011001b
       db 01100110b
       db 10110000b
       db 0
.apost db 00000000b
       db 00001110b
       db 00000110b
       db 00000000b
       db 00000000b
       db 0
.opar  db 00000000b
      db 00111100b
      db 01000010b
      db 10000001b
      db 00000000b
      db 0
.cpar  db 00000000b
      db 10000001b
      db 01000010b
      db 00111100b
      db 00000000b
      db 0
.fois  db 01000100b
      db 00101000b
      db 11111110b
      db 00101000b
      db 01000100b
      db 0
.plus  db 00010000b
      db 00010000b
      db 01111100b
      db 00010000b
      db 00010000b
      db 0
.virgul db 00000000b
       db 10000000b
       db 01100000b
       db 01100000b
       db 00000000b
       db 0
.moins db 00010000b
      db 00010000b
      db 00010000b
      db 00010000b
      db 00010000b
      db 0
.point db 00000000b
      db 00000000b
      db 01100000b
      db 01100000b
      db 00000000b
      db 0
.sur   db 00000000b
      db 11000000b
      db 00110000b
      db 00001100b
      db 00000011b
      db 0
.0   db 00111110b
     db 01010001b
     db 01001001b
     db 01000101b
     db 00111110b
     db 0
.1   db 00000000b
     db 01000010b
     db 01111111b
     db 01000000b
     db 00000000b
     db 0
.2   db 01000010b
     db 01100001b
     db 01010001b
     db 01001001b
     db 01100110b
     db 0
.3   db 00100010b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00110110b
     db 0
.4   db 00011000b
     db 00010100b
     db 00010010b
     db 01111111b
     db 00010000b
     db 0
.5   db 00101111b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00110011b
     db 0
.6   db 00111110b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00110010b
     db 0
.7   db 00000001b
     db 00000001b
     db 01100001b
     db 00011001b
     db 00000111b
     db 0
.8   db 00110110b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00110110b
     db 0
.9   db 00100110b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00111110b
     db 0
.twopoi db 00000000b
       db 00000000b
       db 01101100b
       db 01101100b
       db 00000000b
       db 0
.poinvir db 00000000b
       db 10000000b
       db 01101100b
       db 01101100b
       db 00000000b
       db 0
.infer db 00000000b
      db 00010000b
      db 00101000b
      db 01000100b
      db 10000010b
      db 0
.egal  db 00000000b
      db 01001000b
      db 01001000b
      db 01001000b
      db 01001000b
      db 0
.super db 00000000b
      db 10000010b
      db 01000100b
      db 00101000b
      db 00010000b
      db 0
.intero db 00000000b
       db 00000010b
       db 10110001b
       db 00001001b
       db 00000110b
       db 0
.arobaz db 01111110b
	db 10000001b
	db 10111001b
	db 10100101b
	db 10111111b
	db 0
.A    db 01111100b
     db 00010010b
     db 00010001b
     db 00010010b
     db 01111100b
     db 0
.B    db 01111111b
     db 01001001b
     db 01001001b
     db 01001110b
     db 00110000b
     db 0
.C    db 00111100b
     db 01000010b
     db 01000001b
     db 01000001b
     db 00100010b
     db 0
.D    db 01111111b
     db 01000001b
     db 01000001b
     db 01000010b
     db 00111100b
     db 0
.E    db 01111111b
     db 01001001b
     db 01001001b
     db 01001001b
     db 01000001b
     db 0
.F    db 01111111b
     db 00001001b
     db 00001001b
     db 00001001b
     db 00000001b
     db 0
.G    db 00111100b
     db 01000010b
     db 01000001b
     db 01010001b
     db 00110010b
     db 0
.H    db 01111111b
     db 00001000b
     db 00001000b
     db 00001000b
     db 01111111b
     db 0
.I    db 00000000b
     db 01000001b
     db 01111111b
     db 01000001b
     db 00000000b
     db 0
.J    db 00100000b
     db 01000001b
     db 00111111b
     db 00000001b
     db 00000000b
     db 0
.K    db 01111111b
     db 00001000b
     db 00010100b
     db 00100010b
     db 01000001b
     db 0
.L    db 01111111b
     db 01000000b
     db 01000000b
     db 01000000b
     db 01000000b
     db 0
.M    db 01111111b
     db 00000010b
     db 00000100b
     db 00000010b
     db 01111111b
     db 0
.N    db 01111111b
     db 00000110b
     db 00001000b
     db 00110000b
     db 01111111b
     db 0
.O    db 00111110b
     db 01000001b
     db 01000001b
     db 01000001b
     db 00111110b
     db 0
.P    db 01111111b
     db 00001001b
     db 00001001b
     db 00001001b
     db 00000110b
     db 0
.Q    db 00111110b
     db 01000001b
     db 01001001b
     db 01010001b
     db 00111110b
     db 0
.R    db 01111111b
     db 00001001b
     db 00011001b
     db 00101001b
     db 01000110b
     db 0
.S    db 00100110b
     db 01001001b
     db 01001001b
     db 01001001b
     db 00110010b
     db 0
.T    db 00000001b
     db 00000001b
     db 01111111b
     db 00000001b
     db 00000001b
     db 0
.U    db 00111111b
     db 01000000b
     db 01000000b
     db 01000000b
     db 00111111b
     db 0
.V    db 00000111b
     db 00011000b
     db 01100000b
     db 00011000b
     db 00000111b
     db 0
.W    db 00111111b
     db 01000000b
     db 00111000b
     db 01000000b
     db 00111111b
     db 0
.X    db 01100011b
     db 00010100b
     db 00001000b
     db 00010100b
     db 01100011b
     db 0
.Y    db 00000111b
     db 00001000b
     db 01110000b
     db 00001000b
     db 00000111b
     db 0
.Z    db 01100001b
     db 01010001b
     db 01001001b
     db 01000101b
     db 01000011b
     db 0
.obrak db 00000000b
      db 11111111b
      db 10000001b
      db 10000001b
      db 00000000b
      db 0
.down db 00000000b
      db 00000011b
      db 00001100b
      db 00110000b
      db 11000000b
      db 0
.cbrak db 00000000b
      db 10000001b
      db 10000001b
      db 11111111b
      db 00000000b
      db 0
.up   db 00010000b
      db 00001000b
      db 00000100b
      db 00001000b
      db 00010000b
      db 0
.tiret	 db 10000000b
	 db 10000000b
	 db 10000000b
	 db 10000000b
	 db 10000000b
	 db 80h
.60h	db 00000000b
       db 00000011b
       db 00000111b
       db 00001000b
       db 00000000b
       db 00000000b
.a    db 00000000b
     db 00100000b
     db 01010100b
     db 01010100b
     db 01111000b
     db 0
.b    db 00000000b
     db 01111111b
     db 01000100b
     db 01000100b
     db 00111000b
     db 0
.c    db 00000000b
     db 00111000b
     db 01000100b
     db 01000100b
     db 00101000b
     db 0
.d    db 00000000b
     db 00111000b
     db 01000100b
     db 01000100b
     db 01111111b
     db 0
.e    db 00000000b
     db 00111000b
     db 01010100b
     db 01010100b
     db 01011000b
     db 0
.f    db 00000000b
     db 00001000b
     db 01111110b
     db 00001001b
     db 00000001b
     db 0
.g    db 00000000b
     db 10011000b
     db 10100100b
     db 10100100b
     db 01111100b
     db 0
.h    db 00000000b
     db 01111111b
     db 00000100b
     db 00000100b
     db 01111000b
     db 0
.i    db 00000000b
     db 00000100b
     db 01000100b
     db 01111101b
     db 00000000b
     db 0
.j    db 00000000b
     db 10000000b
     db 10000100b
     db 01111101b
     db 00000000b
     db 0
.k    db 00000000b
     db 01111111b
     db 00010000b
     db 00101000b
     db 01000100b
     db 0
.l    db 00000000b
     db 00000001b
     db 01111111b
     db 01000000b
     db 00000000b
     db 0
.m    db 01111100b
     db 00000100b
     db 01111100b
     db 00000100b
     db 01111000b
     db 0
.n    db 00000000b
     db 01111100b
     db 00000100b
     db 00000100b
     db 01111000b
     db 0
.o    db 00000000b
     db 00111000b
     db 01000100b
     db 01000100b
     db 00111000b
     db 0
.p    db 00000000b
     db 11111100b
     db 01000100b
     db 01000100b
     db 00111000b
     db 0
.q    db 00000000b
     db 00111000b
     db 01000100b
     db 01000100b
     db 11111100b
     db 0
.r    db 00000000b
     db 01111100b
     db 00001000b
     db 00000100b
     db 00001000b
     db 0
.s    db 00000000b
     db 01001000b
     db 01010100b
     db 01010100b
     db 00100100b
     db 0
.t    db 00000000b
     db 00000100b
     db 00111111b
     db 01000100b
     db 01000100b
     db 0
.u    db 00000000b
     db 00111100b
     db 01000000b
     db 01000000b
     db 01111100b
     db 0
.v    db 00001100b
     db 00110000b
     db 01000000b
     db 00110000b
     db 00001100b
     db 0
.w    db 00111100b
     db 01000000b
     db 00111000b
     db 01000000b
     db 00111100b
     db 0
.x    db 01000100b
     db 00101000b
     db 00010000b
     db 00101000b
     db 01000100b
     db 0
.y    db 00000000b
     db 10011100b
     db 10100000b
     db 10100000b
     db 01111100b
     db 0
.z    db 00000000b
     db 01100100b
     db 01010100b
     db 01001100b
     db 01000100b
     db 0
.acco db 00000000b
      db 00011000b
      db 11100111b
      db 10000001b
      db 00000000b
      db 0
.separ	 db 00000000b
	 db 00000000b
	 db 11111111b
	 db 00000000b
	 db 00000000b
	 db 0
.cacco db 00000000b
      db 10000001b
      db 11100111b
      db 00011000b
      db 00000000b
      db 0
.enviro  db 00001000b
	 db 00000100b
	 db 00001000b
	 db 00010000b
	 db 00001000b
	 db 0
.delta	db 01100000b
       db 01011000b
       db 01000110b
       db 01011000b
       db 01100000b
       db 0
.cedmaj db 00111110b
       db 01000001b
       db 11000001b
       db 01000001b
       db 00100010b
       db 00000000b
.udep	db 00000000b
       db 01111010b
       db 10000000b
       db 10000000b
       db 11111010b
       db 00000000b
.eaceg	db 00000000b
       db 01110000b
       db 10101010b
       db 10101010b
       db 10010001b
       db 00000000b
.achap	db 00000000b
       db 01000000b
       db 10101010b
       db 10101001b
       db 11110010b
       db 00000000b
.adep  db 00000000b
       db 01000000b
       db 10101010b
       db 10101000b
       db 11110010b
       db 00000000b
.aacgr	db 00000000b
       db 01000000b
       db 10101001b
       db 10101010b
       db 11110010b
       db 00000000b
.ascad	db 00000000b
       db 01000000b
       db 10101011b
       db 10101011b
       db 11110000b
       db 00000000b
.cedmin db 00000000b
       db 00011100b
       db 10100010b
       db 11100010b
       db 00010100b
       db 00000000b
.echapo db 00000000b
       db 01110010b
       db 10101001b
       db 10101010b
       db 10010000b
       db 00000000b
.edep	db 00000000b
       db 01110010b
       db 10101000b
       db 10101010b
       db 10010000b
       db 00000000b
.eacgr	db 00000000b
       db 01110001b
       db 10101010b
       db 10101010b
       db 10010000b
       db 00000000b
.idep	db 00000000b
       db 00001000b
       db 10001010b
       db 11111000b
       db 00000010b
       db 00000000b
.ichap	db 00000000b
       db 00001000b
       db 10001010b
       db 11111001b
       db 00000010b
       db 00000000b
.iacgr	db 00000000b
       db 00001000b
       db 10001001b
       db 11111010b
       db 00000010b
       db 00000000b
.amdep	db 11110000b
       db 00101001b
       db 00100100b
       db 00101001b
       db 11110000b
       db 00000000b
.amaeg	db 11110000b
       db 00101010b
       db 00100110b
       db 00101001b
       db 11110000b
       db 00000000b
.eaegm	db 11111000b
       db 10010110b
       db 10010101b
       db 10010101b
       db 11000100b
       db 00000000b
.%inv	db 01100001b
       db 01100110b
       db 00011000b
       db 01100110b
       db 10000110b
       db 00000000b
.AE	db 11111110b
       db 00001001b
       db 11111111b
       db 10001001b
       db 11000011b
       db 00000000b
.ochap	db 01110000b
       db 10001010b
       db 10001001b
       db 10001010b
       db 01110000b
       db 00000000b
.odep	db 01110000b
       db 10001011b
       db 10001000b
       db 10001011b
       db 01110000b
       db 00000000b
.oacgr	db 01110000b
       db 10001001b
       db 10001010b
       db 10001010b
       db 01110000b
       db 00000000b
.uchap	db 00000000b
       db 01111010b
       db 10000001b
       db 10000001b
       db 11111010b
       db 00000000b
.uacgr	db 00000000b
       db 01111001b
       db 10000010b
       db 10000010b
       db 11111000b
       db 00000000b
.ydep	db 00000000b
       db 01001101b
       db 10010000b
       db 10010000b
       db 01111101b
       db 00000000b
.omdep	db 01111000b
       db 10000101b
       db 10000100b
       db 10000101b
       db 01111000b
       db 00000000b
.umdep	db 00000000b
       db 01111101b
       db 10000000b
       db 10000000b
       db 11111101b
       db 00000000b
.cbarr	db 00111100b
       db 01000010b
       db 11111111b
       db 01000010b
       db 00100100b
       db 00000000b
.livre	db 01001000b
       db 10111110b
       db 10001001b
       db 10000001b
       db 01000000b
       db 00000000b
.yen	db 00000011b
       db 01010100b
       db 11111000b
       db 01010100b
       db 00000011b
       db 00000000b
.Pt	db 11111110b
       db 00010010b
       db 00001100b
       db 01111110b
       db 10001000b
       db 00000000b
.pargrf db 01001000b
       db 10001000b
       db 01111110b
       db 00001001b
       db 00000010b
       db 00000000b




.rempli db 0ffh
       db 0ffh
       db 0ffh
       db 0ffh
       db 0ffh
       db 0ffh
.vide	db 0
       db 0
       db 0
       db 0
       db 0
       db 0
.cadre	db 11111111b
       db 10000001b
       db 10000001b
       db 10000001b
       db 10000001b
       db 11111111b
