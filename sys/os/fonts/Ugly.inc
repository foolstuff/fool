ugly:
.00 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.tab,.LF,.nul,.nul,.CR,.nul,.nul
.10 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.esc,.nul,.nul,.nul,.nul
.20 dw .spc,.!,.",.diese,.$,.%,.et,.',.opar,.cpar,.fois,.plus,.virgule,.moins,.point,.slash
.30 dw .0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.colon,.semicolon,.infer,.equ,.super,.?
.40 dw .@,.A,.B,.C,.D,.E,.F,.G,.H,.I,.J,.K,.L,.M,.N,.O
.50 dw .P,.Q,.R,.S,.T,.U,.V,.W,.X,.Y,.Z,.nul,.nul,.nul,.nul,._
.60 dw .nul,.a,.b,.c,.d,.e,.f,.g,.h,.i,.j,.k,.l,.m,.n,.o
.70 dw .p,.q,.r,.s,.t,.u,.v,.w,.x,.y,.z,.nul,.nul,.nul,.nul,.nul
.80 dw .nul,.box1,.box2,.box3,.box4,.box5,.box6,.box7,.box8,.box9,.box10,.nul,.nul,.nul,.nul,.nul
.90 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.A0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.B0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.C0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.D0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.E0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul
.F0 dw .nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul,.nul

db @f-$-1
db 0,0,0
@@:
db 0,0
.opar:
db @f-$-1
db 3,2,0
db -1,1,1
db -1,2,1
db 0,4,1
db 1,3,1
db 1,1,1
@@:
db 3,0

.cpar:
db @f-$-1
db 1,2,0
db 1,1,1
db 1,2,1
db 0,4,1
db -1,3,1
db -1,1,1
@@:
db 4,0

.fois:
db @f-$-1
db 1,6,0
db 4,4,1
db -4,0,0
db 4,-4,1
@@:
db 5,0

.plus:
db @f-$-1
db 2,6,0
db 0,4,1
db 2,-2,0
db -4,0,1
@@:
db 5,0

._:
db @f-$-1
db 0,12,0
db 6,0,1
@@:
db 7,0
.esc:
db @f-$-1
db 0,7,0
db 5,0,1
db -3,-2,0
db -2,2,1
db 2,2,1
@@:
db 6,0
.slash:
db @f-$-1
db 4,3,0
db -3,7,1
@@:
db 6,0
.colon:
db @f-$-1
db 1,5,0
db 0,0,1
db 0,5,0
db 0,0,1
@@:
db 4,0
.semicolon:
db @f-$-1
db 1,5,0
db 0,0,1
db 0,5,0
db 0,1,1
db -1,1,1
@@:
db 4,0
.super:
db @f-$-1
db 0,4,0
db 5,3,1
db -5,3,1
@@:
db 7,0
.infer:
db @f-$-1
db 5,4,0
db -5,3,1
db 5,3,1
@@:
db 7,0
.?:
db @f-$-1
db 0,0,0
@@:
db 0,0
.@:
db @f-$-1
db 0,0,0
db 0,0,1
@@:
db 8,0
.!:
db @f-$-1
db 1,0,0
db 0,7,1
db 0,3,0
db 0,0,1
@@:
db 4,0
.":
.diese:
.$:
.%:
.et:
.':
.nul:
db @f-$-1
db 0,10,0
db 0,0,1
@@:
db 3,0
.virgule:
db @f-$-1
db 1,10,0
db 0,1,1
db -1,1,1
@@:
db 3,0
.point:
db @f-$-1
db 0,10,0
db 0,0,1
@@:
db 3,0
.moins:
db @f-$-1
db 0,8,0
db 4,0,1
@@:
db 6,0
.box1:
db @f-$-1
db 1,0,1
db 0,15,1
db -1,0,1
db 0,-15,1
@@:
db 1,0
.box2:
db @f-$-1
db 2,0,1
db 0,15,1
db -2,0,1
db 0,-15,1
@@:
db 2,0
.box3:
db @f-$-1
db 3,0,1
db 0,15,1
db -3,0,1
db 0,-15,1
@@:
db 3,0
.box4:
db @f-$-1
db 4,0,1
db 0,15,1
db -4,0,1
db 0,-15,1
@@:
db 4,0
.box5:
db @f-$-1
db 5,0,1
db 0,15,1
db -5,0,1
db 0,-15,1
@@:
db 5,0
.box6:
db @f-$-1
db 6,0,1
db 0,15,1
db -6,0,1
db 0,-15,1
@@:
db 6,0
.box7:
db @f-$-1
db 7,0,1
db 0,15,1
db -7,0,1
db 0,-15,1
@@:
db 7,0
.box8:
db @f-$-1
db 8,0,1
db 0,15,1
db -8,0,1
db 0,-15,1
@@:
db 8,0
.box9:
db @f-$-1
db 9,0,1
db 0,15,1
db -9,0,1
db 0,-15,1
@@:
db 9,0
.box10:
db @f-$-1
db 10,0,1
db 0,15,1
db -10,0,1
db 0,-15,1
@@:
db 10,0

.0:
db @f-$-1
db 0,9,0
db 0,-5,1
db 1,-1,1
db 3,0,1
db 1,1,1
db 0,5,1
db -1,1,1
db -3,0,1
db -1,-1,1
db 5,-5,1
@@:
db 7,0
.1:
db @f-$-1
db 0,4,0
db 1,-1,1
db 0,7,1
db -1,0,0
db 2,0,1
@@:
db 4,0
.2:
db @f-$-1
db 0,4,0
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,1,1
db -4,4,1
db 0,1,1
db 4,0,1
db 0,-1,1
@@:
db 6,0
.3:
db @f-$-1
db 0,4,0
db 1,-1,1
db 3,0,1
db 1,1,1
db 0,1,1
db -1,1,1
db -2,0,1
db 2,0,0
db 1,1,1
db 0,2,1
db -1,1,1
db -3,0,1
db -1,-1,1
@@:
db 7,0
.4:
db @f-$-1
db 4,10,0
db 0,-7,1
db -4,4,1
db 5,0,1
@@:
db 7,0
.5:
db @f-$-1
db 4,3,0
db -3,0,1
db -1,3,1
db 3,0,1
db 1,1,1
db 0,2,1
db -1,1,1
db -2,0,1
db -1,-1,1
@@:
db 6,0
.6:
db @f-$-1
db 3,3,0
db -1,0,1
db -2,2,1
db 0,4,1
db 1,1,1
db 2,0,1
db 1,-1,1
db 0,-2,1
db -1,-1,1
db -3,0,1
@@:
db 6,0
.7:
db @f-$-1
db 0,3,0
db 4,0,1
db 0,2,1
db -2,2,1
db 0,3,1
@@:
db 6,0
.8:
db @f-$-1
db 1,6,0
db -1,-1,1
db 0,-1,1
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,1,1
db -1,1,1
db -2,0,1
db -1,1,1
db 0,2,1
db 1,1,1
db 2,0,1
db 1,-1,1
db 0,-2,1
db -1,-1,1
@@:
db 6,0
.9:
db @f-$-1
db 1,10,0
db 1,0,1
db 2,-2,1
db 0,-4,1
db -1,-1,1
db -2,0,1
db -1,1,1
db 0,2,1
db 1,1,1
db 3,0,1
@@:
db 6,0
.A:
db @f-$-1
db 0,10,0
db 0,-5,1
db 2,-2,1
db 1,0,1
db 2,2,1
db 0,5,1
db 0,-3,0
db -5,0,1
@@:
db 7,0
.B:
db @f-$-1
db 0,10,0
db 0,-7,1
db 3,0,1
db 1,1,1
db 0,2,1
db -4,0,1
db 4,0,0
db 1,1,1
db 0,2,1
db -1,1,1
db -4,0,1
@@:
db 7,0
.C:
db @f-$-1
db 5,4,0
db -1,-1,1
db -2,0,1
db -2,2,1
db 0,4,1
db 1,1,1
db 3,0,1
db 1,-1,1
@@:
db 7,0
.D:
db @f-$-1
db 0,10,0
db 0,-7,1
db 3,0,1
db 2,2,1
db 0,4,1
db -1,1,1
db -4,0,1
@@:
db 7,0
.E:
db @f-$-1
db 4,10,0
db -4,0,1
db 0,-7,1
db 4,0,1
db -4,3,0
db 3,0,1
@@:
db 6,0
.F:
db @f-$-1
db 0,10,0
db 0,-7,1
db 4,0,1
db -4,3,0
db 3,0,1
@@:
db 6,0
.G:
db @f-$-1
db 5,4,0
db -1,-1,1
db -2,0,1
db -2,2,1
db 0,4,1
db 1,1,1
db 3,0,1
db 1,-1,1
db 0,-2,1
db -1,0,1
@@:
db 7,0
.H:
db @f-$-1
db 0,3,0
db 0,7,1
db 0,-4,0
db 4,0,1
db 0,-3,0
db 0,7,1
@@:
db 6,0
.I:
db @f-$-1
db 1,3,0
db 0,7,1
db 1,0,0
db -2,0,1
db 0,-7,0
db 2,0,1
@@:
db 4,0
.J:
db @f-$-1
db 3,3,0
db 0,6,1
db -1,1,1
db -1,0,1
db -1,-1,1
db 1,-6,0
db 3,0,1
@@:
db 6,0
.K:
db @f-$-1
db 0,3,0
db 0,7,1
db 0,-4,0
db 4,4,1
db -4,-4,0
db 4,-3,1
@@:
db 6,0
.L:
db @f-$-1
db 0,3,0
db 0,7,1
db 4,0,1
@@:
db 6,0
.M:
db @f-$-1
db 0,10,0
db 0,-7,1
db 3,3,1
db 3,-3,1
db 0,7,1
@@:
db 8,0
.N:
db @f-$-1
db 0,10,0
db 0,-7,1
db 4,7,1
db 0,-7,1
@@:
db 6,0
.O:
db @f-$-1
db 0,4,0
db 0,5,1
db 1,1,1
db 3,0,1
db 1,-1,1
db 0,-5,1
db -1,-1,1
db -3,0,1
db -1,1,1
@@:
db 7,0
.P:
db @f-$-1
db 0,10,0
db 0,-7,1
db 3,0,1
db 1,1,1
db 0,2,1
db -1,1,1
db -3,0,1
@@:
db 6,0
.Q:
db @f-$-1
db 0,4,0
db 0,5,1
db 1,1,1
db 3,0,1
db 1,-1,1
db 0,-5,1
db -1,-1,1
db -3,0,1
db -1,1,1
db 3,4,0
db 2,2,1
@@:
db 7,0
.R:
db @f-$-1
db 0,10,0
db 0,-7,1
db 3,0,1
db 1,1,1
db 0,2,1
db -1,1,1
db -3,0,1
db 2,0,0
db 2,2,1
db 0,1,1
@@:
db 6,0
.S:
db @f-$-1
db 0,9,0
db 1,1,1
db 2,0,1
db 1,-1,1
db 0,-2,1
db -1,-1,1
db -2,0,1
db -1,-1,1
db 0,-1,1
db 1,-1,1
db 2,0,1
db 1,1,1
@@:
db 6,0
.T:
db @f-$-1
db 3,10,0
db 0,-7,1
db -3,0,0
db 6,0,1
@@:
db 8,0
.U:
db @f-$-1
db 0,3,0
db 0,6,1
db 1,1,1
db 3,0,1
db 1,-1,1
db 0,-6,1
@@:
db 7,0
.V:
db @f-$-1
db 0,3,0
db 3,7,1
db 3,-7,1
@@:
db 8,0
.W:
db @f-$-1
db 0,3,0
db 2,7,1
db 2,-5,1
db 2,5,1
db 2,-7,1
@@:
db 10,0
.X:
db @f-$-1
db 0,3,0
db 5,7,1
db 0,-7,0
db -5,7,1
@@:
db 7,0
.Y:
db @f-$-1
db 0,3,0
db 3,3,1
db 3,-3,1
db -3,3,0
db 0,4,1
@@:
db 8,0
.Z:
db @f-$-1
db 0,3,0
db 6,0,1
db -6,7,1
db 6,0,1
@@:
db 8,0
.a:
db @f-$-1
db 1,5,0
db 2,0,1
db 1,1,1
db 0,4,1
db 0,-1,0
db -1,1,1
db -2,0,1
db -1,-1,1
db 0,-1,1
db 1,-1,1
db 3,0,1
@@:
db 6,0
.b:
db @f-$-1
db 0,2,0
db 0,8,1
db 0,-1,0
db 1,1,1
db 2,0,1
db 1,-1,1
db 0,-3,1
db -1,-1,1
db -2,0,1
db -1,1,1
@@:
db 6,0
.c:
db @f-$-1
db 4,6,0
db -1,-1,1
db -2,0,1
db -1,1,1
db 0,3,1
db 1,1,1
db 2,0,1
db 1,-1,1
@@:
db 6,0
.d:
db @f-$-1
db 4,2,0
db 0,8,1
db 0,-1,0
db -1,1,1
db -2,0,1
db -1,-1,1
db 0,-3,1
db 1,-1,1
db 2,0,1
db 1,1,1
@@:
db 6,0
.e:
db @f-$-1
db 0,7,0
db 4,0,1
db 0,-1,1
db -1,-1,1
db -2,0,1
db -1,1,1
db 0,3,1
db 1,1,1
db 2,0,1
db 1,-1,1
@@:
db 6,0
.f:
db @f-$-1
db 1,10,0
db 0,-7,1
db 1,-1,1
db 1,0,1
db -1,4,0
db -2,0,1
@@:
db 4,0
.g:
db @f-$-1
db 4,9,0
db -1,1,1
db -2,0,1
db -1,-1,1
db 0,-3,1
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,-1,0
db 0,7,1
db -1,1,1
db -2,0,1
@@:
db 6,0
.h:
db @f-$-1
db 0,2,0
db 0,8,1
db 0,-4,0
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,4,1
@@:
db 6,0
.i:
db @f-$-1
db 0,3,0
db 0,0,1
db 0,2,0
db 0,5,1
@@:
db 2,0
.j:
db @f-$-1
db -1,13,0
db 1,0,1
db 1,-1,1
db 0,-7,1
db 0,-2,0
db 0,0,1
@@:
db 4,0
.k:
db @f-$-1
db 0,2,0
db 0,8,1
db 0,-3,0
db 1,0,1
db 1,-1,1
db -1,1,0
db 2,3,1
@@:
db 5,0
.l:
db @f-$-1
db 0,2,0
db 0,8,1
@@:
db 2,0
.m:
db @f-$-1
db 0,5,0
db 0,5,1
db 0,-4,1
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,4,1
db 0,-4,1
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,4,1
@@:
db 10,0
.n:
db @f-$-1
db 0,5,0
db 0,5,1
db 0,-4,1
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,4,1
@@:
db 6,0
.o:
db @f-$-1
db 4,6,0
db -1,-1,1
db -2,0,1
db -1,1,1
db 0,3,1
db 1,1,1
db 2,0,1
db 1,-1,1
db 0,-3,1
@@:
db 6,0
.p:
db @f-$-1
db 0,13,0
db 0,-8,1
db 0,1,0
db 1,-1,1
db 2,0,1
db 1,1,1
db 0,3,1
db -1,1,1
db -2,0,1
db -1,-1,1
@@:
db 6,0
.q:
db @f-$-1
db 4,13,0
db 0,-8,1
db 0,1,0
db -1,-1,1
db -2,0,1
db -1,1,1
db 0,3,1
db 1,1,1
db 2,0,1
db 1,-1,1
@@:
db 6,0
.r:
db @f-$-1
db 0,5,0
db 0,5,1
db 0,-3,1
db 2,-2,1
db 1,0,1
@@:
db 5,0
.s:
db @f-$-1
db 0,10,0
db 3,0,1
db 1,-1,1
db 0,-1,1
db -1,-1,1
db -2,0,1
db -1,-1,1
db 1,-1,1
db 3,0,1
@@:
db 6,0
.t:
db @f-$-1
db 3,10,0
db -1,0,1
db -1,-1,1
db 0,-7,1
db -1,3,0
db 3,0,1
@@:
db 5,0
.u:
db @f-$-1
db 0,5,0
db 0,4,1
db 1,1,1
db 3,0,1
db 0,-5,1
@@:
db 6,0
.v:
db @f-$-1
db 0,5,0
db 2,5,1
db 2,-5,1
@@:
db 6,0
.w:
db @f-$-1
db 0,5,0
db 2,5,1
db 2,-3,1
db 2,3,1
db 2,-5,1
@@:
db 10,0
.x:
db @f-$-1
db 0,5,0
db 4,5,1
db 0,-5,0
db -4,5,1
@@:
db 6,0
.y:
db @f-$-1
db 0,5,0
db 0,4,1
db 1,1,1
db 3,0,1
db 0,-5,1
db 0,7,1
db -1,1,1
db -2,0,1
@@:
db 6,0
.z:
db @f-$-1
db 0,5,0
db 4,0,1
db -4,5,1
db 4,0,1
@@:
db 6,0
.equ:
db @f-$-1
db 0,6,0
db 4,0,1
db -4,3,0
db 4,0,1
@@:
db 6,0
.LF:
db 0,0,15
.CR:
db 0,-1,0
.spc:
db 0,4,0
.tab:
db 0,30,0
