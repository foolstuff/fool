;irq0:
        cli
        push eax
        mov eax,1
        add [.cnt],eax
        add [.ticks],eax
        adc [.ticks+4],0
        adc [.ticks+8],0
        adc [.ticks+12],0
        add byte[.rdcnt],al
        jne  @f
        call .rdtsc
@@:
        mov al,20h
        out 20h,al
        pop eax
        sti
        iret

.rdtsc:
        push eax ebx ecx edx
        rdtsc
        mov ebx,eax
        mov ecx,edx
        sub eax,[.tsc]
        sbb edx,[.tsc+4]
        mov [.tsc],ebx
        mov [.tsc+4],ecx
        mov [.cpucnt],eax
        mov [.cpucnt+4],edx
        pop edx ecx ebx eax
        ret

.set:
        push eax ebx edx
        mov al,36h
        out 43h,al
        jmp $+2
        mov eax,[.clk]
        mov ebx,[.freq]
        xor edx,edx
        div ebx
        mov [.div],eax
        out 40h,al
        jmp $+2
        mov al,ah
        out 40h,al
        jmp $+2
        pop edx ebx eax
        ret

align 4
.tsc    dd 0,0
.cpucnt dd 0,0
.clk    dd 1193180
.freq   dd 1000
.div    dd 0
.cnt    dd 0
.ticks  dd 0,0,0,0
.inc    dd 0
.rdcnt  dd 0