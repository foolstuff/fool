Print:
        pusha
        mov   ah,0eh                           ; Request display
.loop:
        lodsb                                  ; load a byte into AL from DS:SI
        or   al,al                             ; Or AL
        jz   @f                                ; Jump if 0, to label NoMor2Print
        int  10h                               ; Call interrupt service
        jmp  .loop                             ; Jump to label More2Print
@@:
        popa
        ret

