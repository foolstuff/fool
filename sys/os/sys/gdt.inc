align 8
align 2                                    ; for GDTR alignment
gdtr   df @f-gdt-1:gdt                     ; GDTR in memory
gdt:                                       ; GDT entry
       dq 0                                ; (0) NULL selector
linear_sel_1:                              ; (8h) linear Data segment, read/write, expand down
       dw 0FFFFh,0
       db 0,10010010b,11001111b,0
sys_code_1:                                ; (10h) Code segment, read/execute, nonconforming
       dw 0FFFFh,0
       db 0,10011010b,11001111b,0
sys_data_1:                                ; (18h) Data segment, read/write, expand down
       dw 0FFFFh,0
       db 0,10010010b,11001111b,0
Real_code_1:                               ; (20h) Real mode code segment
       dw 0FFFFh,0
       db 0,10011010b,0,0
Real_data_1:                               ; (28h) Real mode data segment
       dw 0FFFFh,0
       db 0,10010010b,0,0
sys_vesa_1:                                ; (30h) vesa segment, read/write, expand down
       dw 0FFFFh,0
       db 0,10010010b,11001111b,0
sys_vbuff_1:                                ; (30h) vesa segment, read/write, expand down
       dw 0FFFFh,0
       db 0,10010010b,11001111b,0
@@:                                        ; Used to calculate the size of the GDT





