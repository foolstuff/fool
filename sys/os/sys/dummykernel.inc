if ~defined FOOLOS
        mov si,.str
        call prints

        jmp entrypoint

.str:   db 'something booted, and you can read the SMAP',0

prints:
        mov ah,0eh
@@:
        lodsb
        or al,al
        je @f
        int 10h
        jmp @b
@@:
        ret

entrypoint:
        jmp $
end if ;FOOLOS
