;*********************************
;*      System Faults            *
;*********************************
.isr0:
        call black_screen_of_death
        mov esi,E00
        call handle_int
        jmp $
.isr1:
        call black_screen_of_death
        mov esi,E01
        call handle_int
        jmp $
.isr2:
        call black_screen_of_death
        mov esi,E02
        call handle_int
        jmp $
.isr3:
        call black_screen_of_death
        mov esi,E03
        call handle_int
        jmp $
.isr4:
        call black_screen_of_death
        mov esi,E04
        call handle_int
        jmp $
.isr5:
        call black_screen_of_death
        mov esi,E05
        call handle_int
        jmp $
.isr6:
        call black_screen_of_death
        mov esi,E06
        call handle_int
        jmp $
.isr7:
        call black_screen_of_death
        mov esi,E07
        call handle_int
        jmp $
.isr8:
        call black_screen_of_death
        mov esi,E08
        call handle_int
        jmp $
.isr9:
        call black_screen_of_death
        mov esi,E09
        call handle_int
        jmp $
.isr10:
        call black_screen_of_death
        mov esi,E10
        call handle_int
        jmp $
.isr11:
        call black_screen_of_death
        mov esi,E11
        call handle_int
        jmp $
.isr12:
        call black_screen_of_death
        mov esi,E12
        call handle_int
        jmp $
.isr13:
        call black_screen_of_death
        mov esi,E13
        call handle_int
        jmp $
.isr14:
        call black_screen_of_death
        mov esi,E14
        call handle_int
        jmp $
.isr15:
        call black_screen_of_death
        mov esi,E15
        call handle_int
        jmp $
.isr16:
        call black_screen_of_death
        mov esi,E16
        call handle_int
        jmp $
.isr17:
        call black_screen_of_death
        mov esi,E17
        call handle_int
        jmp $
.isr18:
        jmp $
.isr19:
        jmp $
.isr20:
        jmp $
.isr21:
        jmp $
.isr22:
        jmp $
.isr23:
        jmp $
.isr24:
        jmp $
.isr25:
        jmp $
.isr26:
        jmp $
.isr27:
        jmp $
.isr28:
        jmp $
.isr29:
        jmp $
.isr30:
        jmp $
.isr31:
        jmp $
;*****************************************************************
;*               hardware interrupt handlers                     *
;* irq0 =Timer -I just used dos irq names - irq1 =keyboard etc.. *
;*****************************************************************
irq0:
        include '../irq0/irq0.inc'
;        call irq0
;        iret
irq1:
        include '../irq1/irq1.inc'
;        call irq1
;        iret
irq2:
        iret
irq3:
        iret
irq4:
        iret
irq5:
        iret
irq6:
        iret
irq7:
        iret
irq8:
        iret
irq9:
        iret
irq10:
        iret
irq11:
        iret
irq12:
        ;include '../irq12/irq12.inc'
        ;call irq12
        iret
irq13:
        iret
irq14:
        iret
irq15:
        iret





black_screen_of_death:
;        mov    [box1.x],0
;        mov    [box1.y],0
;        mov    eax,[xres]
;        mov    ebx,[yres]
;        mov    [box1.xl],eax
;        mov    [box1.xl],eax
;        mov    [box1.c],0
;        call   box1
        ret
handle_int:
;        mov    [txt1.x],300
;        mov    [txt1.y],200
;        mov    [txt1.c0],0
;        mov    [txt1.c1],0x00FF0000
;        mov    [txt1.smooth],0
;        call   txt1
        jmp    $  ;hlt system
;******************
;    Error Msgs   *
;******************
E00     db "Exception 00h - Division By Zero X/0",0
E01     db "Exception 01h - Debug .|.|.",0
E02     db "Exception 02h - Non Maskable Interrupt NMI",0
E03     db "Exception 03h - Breakpoint Exception .|.",0
E04     db "Exception 04h - Int 0 Detected Overflow >>",0
E05     db "Exception 05h - Out of Bounds <><!",0
E06     db "Exception 06h - Invalid Opcode ??",0
E07     db "Exception 07h - No Coprocessor /X87",0
E08     db "Exception 08h - Double Fault X2",0
E09     db "Exception 09h - Coprocessor Segment Overrun #X87",0
E10     db "Exception 0Ah - Bad TSS ",0
E11     db "Exception 0Bh - Segment Not Present #?",0
E12     db "Exception 0Ch - Stack Fault _-",0
E13     db "Exception 0Dh - General Protection Fault !G!",0
E14     db "Exception 0Eh - Page Fault !P!",0
E15     db "Exception 0Fh - Unknown Interrupt ?I?",0
E16     db "Exception 10h - Coprocessor Fault !X87!",0
E17     db "Exception 11h - Alignment Check |><|",0
;; added some Exceptions symbols for "real" informaticians brainfucking :lol:�
;; why? cause i want to show to "REAL" informaticians thay don't know anything to computers.

