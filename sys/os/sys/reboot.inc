rebootsystem:
        cli
        jmp   20h:@f
use16
@@:
        mov   ax,28h
        mov   ds,ax
        mov   ss,ax
        nop

        mov   bx,[realmodecs]
        push  bx
        lea   bx,[@f]
        push  bx
        mov   eax,cr0
        and   al,0feh
        mov   cr0,eax
        retf
@@:
        mov   ax,cs
        mov   ds,ax
        mov   ss,ax
        nop
        mov   es,ax
        mov   fs,ax
        mov   gs,ax
        lidt  [ridtr]
        push  cs
        pop   ds
        push  ds
        pop   es
        mov   al,0
        out   0a1h,al
        out   021h,al
        sti
        xor   ax,ax
        int   16h
        mov   ax,3
        int   10h
        mov   si,rebootmsg
        call  Print
        mov   ecx,48
@@:
        hlt
        loop  @b
        xor   ax,ax
        int   16h
        jmp far 0ffffh:0
        jmp $

use32