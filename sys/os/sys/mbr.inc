format binary as 'img'
;version 15/03/2023 04:02
;version 04/06/2019 13.54
;version 01/05/2014 01:29
;version 27/11/2007 01:48
;bootcode:

;;; BEGIN OF MBR

;BOOTRELOC:   ;uncomment to test reloc

masterbootrecord:
use16
        org 7c00h
        mov ax,cs
        mov ss,ax
        mov ds,ax
        mov [bootdrive],dl

;copy bootloader from 0000h:7c00h to 0020h:7c00h
;linear 07c00h to 07e00h
;this is used to load boot sectors from other partitions or drives.
if defined BOOTRELOC

        mov ax,20h
        mov es,ax
        mov bx,200h
        mov di,7c00h
  @@:
        dec bx
        jl @f
        mov al,[di+bx]
        mov [es:di+bx],al
        jmp @b
  @@:

;and jump to the recopy.

        jmp 20h:@f
otherdrive db 80h   ;set this to any drive n�
  @@:

;at this point, we can load another boot sector
;from another drive with code like this:
if defined otherdrive
        mov ah,0eh
        mov al,'B' ;print B each time it's reloaded
        int 10h
        mov dl,[otherdrive]
        mov ax,21h   ;load 32k
        mov cx,1
        xor dh,dh
        mov bx,7c0h
        mov es,bx
        xor bx,bx
        int 13h
        jmp 0:7c00h
end if


end if

;load the system from sector 2 to lowmem
        mov cx,secondstage.packets
@@:
        push cx
        mov ah,42h
        mov dl,[bootdrive]
        mov si,dap
        int 13h
        add [dap.fptr+2],(40h*512)/16
        add dword[dap.lba],40h
        pop cx
        loop @b

        xor ax,ax
        mov ss,ax
        mov ds,ax
        jmp 0:secondstage.org

dap:
        db 10h,0
.cnt    dw 40h ;32k
.fptr   dd secondstage.seg:0
.lba    dd 1,0

        rb 509-($-$$)
bootdrive:
        db 0
        dw 0aa55h

;;; END OF MBR

include 'secondstage.inc'
include 'dummysecondstage.inc'