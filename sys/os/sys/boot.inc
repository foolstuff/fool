format binary as 'img'
;version 04/06/2019 13.54
;version 01/05/2014 01:29
;version 27/11/2007 01:48
;bootcode:
bootsys:
ORG equ 1000h
use16
        org 7C00h

        mov ax,cs
        mov ss,ax
        mov ds,ax
        mov [.bootdrive],dl

;copy bootloader from 0000h:7c00h to 1000h:7c00h
;linear 07c00h to 17c00h
        mov ax,1000h
        mov es,ax
        mov bx,512
        mov di,7c00h
@@:
        dec bx
        jl @f
        mov al,[di+bx]
        mov [es:di+bx],al
        jmp @b
@@:

;and jump to the recopy.
        jmp 1000h:@f
@@:

;load the system from sector 2 to 0000h:1000h
;linear 01000h
        mov ax,280h   ;load 32k
        mov cx,2
        xor dh,dh
        mov bx,ORG/16
        mov es,bx
        xor bx,bx
        int 13h
        xor ax,ax
        cmp [.bootdrive],0
        jl @f
        mov dx,3f2h
        out dx,al
@@:
        mov ss,ax
        mov ds,ax

;and jump to the system at linear 1000h (ORG)
        jmp 0:ORG

.padding rb 509-($-$$)
.bootdrive db 0
.bootmark dw 0aa55h
include 'padding.inc'
kernel:
        org ORG
