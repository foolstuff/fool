lba2chs:
;
;S = (mod LBA/s0) + 1
;H = mod (LBA/s0)/h0
;C = (LBA/s0)/h0
;
; bx = heads
; cx = sectors
; ax:dx = LBA
;
; return dh = HHHH HHHH
;        cx = ClCl ClCl Ch SS SSSS

;SECTORS equ 18;byte[si+disk.s0]
;HEADS equ 2;byte[si+disk.h0]

      div cx    ;LBA/SECTORS
      inc dx    ;S = mod(LBA/SECTORS)+1
      mov cx,dx
      xor dx,dx
      div bx    ;(LBA/SECTORS)/HEADS

      mov dh,dl ;H=mod((LBA/SECTORS)/HEADS)

      and cl,3fh
      ror ax,8
      shl al,6
      or cx,ax
      ret
 
