format binary as 'img'
        org 7c00h
boot:
        mov ax,cs
        mov ds,ax
        call biosmem
        mov ax,0b800h
        mov es,ax
        mov ah,74h
        mov si,bonjour
        mov di,0
@@:
        lodsb
        or al,al
        je @f
        stosw
        jmp @b

@@:
        hlt
        jmp $
include 'biosmem.inc'


bonjour:
        db 'bonjour',0
        align 8
free =  510-(@f-$$)
@@:     rb free
        dw 0aa55h


macro dispword w,t {

d1='0'+w shr 8 and 0fh
d2='0'+w shr 4 and 0fh
d3='0'+w and 0fh
if d1>'9'
   d1=d1+7
end if
if d2>'9'
   d2=d2+7
end if
if d3>'9'
   d3=d3+7
end if

display d1,d2,d3,'h '

display t,13,10
}

dispword free,'free bytes'
dispword (510-free),'used bytes'
