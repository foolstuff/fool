match =EXITTITLE,EXITTITLE {
EXITTITLE equ 'fool os'
}

match =EXITMESSAGE,EXITMESSAGE {
EXITMESSAGE equ 'the machine will shutdown'
}
foolstart:
        jmp start
include '../vesa/vesainfo.inc'
include '../vesa/vesatest.inc'

include 'a20.inc'
include 'print.inc'

start:
        push cs
        pop ds
        mov ax,ds
        mov es,ax
;        mov [bootdrive],dl
        mov [bootdostest],dh
        call vesatest
        call a20enable

include 'pmswitch.inc'
        jmp kernelcode
IMPORTSECTION:
include 'gdt.inc'
include 'idt.inc'
include 'pic.inc'
include '../vesa/setvesamode.inc'
include '../vesa/vesaconstant.inc'
include 'reboot.inc'
include 'returndos.inc'
use32
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
kernelcode:
        call setvesamode
        call vesaconstant
        call remapirq
        call irq0.set
        sti
        call fool_entry
        cmp [bootdostest],0ffh
        jne @f
        call rebootsystem
@@:
        jmp return2dos

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
align 2
realmodecs        dw 0
ridtr             df 0:0ffffh
;bootdrive         db 0
bootdostest       db 0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;screen memory description
align 4
bpp               dd 0
bpsl              dd 0
bptl              dd 0
xres              dd 0
yres              dd 0
screensize        dd 0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
if defined BACKGROUND
colorclear        dd BACKGROUND
else
colorclear        dd 0123456h
end if
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
endmessage        db EXITTITLE,CRLF,EXITMESSAGE,0
rebootmsg         db 'Please remove floppy disk and press anykey to reboot', 13, 10, 0      ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
