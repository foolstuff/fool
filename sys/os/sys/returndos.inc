return2dos:
        jmp     Real_code:@f
use16
@@:
        cli
        mov     ax,Real_data
        mov     ds,ax
        mov     ss,ax
        nop
        mov     bx,[realmodecs]
        push    bx
        lea     bx,[@f]
        push    bx
        mov     eax,cr0
        and     al,0feh
        mov     cr0,eax
        retf
@@:
        mov     ax,cs
        mov     ds,ax
        mov     ss,ax
        nop
        mov     es,ax
        mov     fs,ax
        mov     gs,ax
        lidt    [ridtr]
        push    cs
        pop     ds
        push    ds
        pop     es
        mov     al,11h                         ; remap dos irq
        out     20h,al
        out     0a0h,al
        mov     al,8                           ; irq0 to irq7
        out     21h,al
        mov     al,70h                         ; irq8 to irq15
        out     0a1h,al
        mov     al,4
        out     21h,al
        mov     al,2
        out     0a1h,al
        mov     al,1
        out     21h,al
        out     0a1h,al
        mov     al,0                           ; unmask irq
        out     21h,al
        out     0a1h,al
        sti                                    ; re-enable interrupts
        mov     ax,3
        int     10h
        mov     ax,04c00h
        int     21h

        mov     si,endmessage
        call    Print
        mov  cx,20
@@:
        mov al,'|'
        mov   ah,0eh                           ; Request display
        int  10h
        call delay16bits
        loop @b


shutdown:
        mov ax,5301h
        xor bx,bx
        int 15h
        mov ax,530eh
        xor bx,bx
        mov cx,0102h
        int 15h
        mov ax,5307h
        mov bx,1
        mov cx,3
        int 15h
        jmp $

delay16bits:
        push cx
        mov cx,100
.loop:
        mov ax,-1
@@:
        dec ax
        jne @b
        loop .loop
        pop cx
        ret
