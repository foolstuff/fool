remapirq:
        mov al,11h     ;ICW1
        out 20h,al     ; icw1, need icw4, x86 mode
        out 0a0h,al    ; x86 mode
        mov al,32      ;ICW2
        out 21h,al     ; irq0 starts at 32
        mov al,40      ;ICW2
        out 0a1h,al    ; irq8 starts at 40
        mov al,4       ;ICW3
        out 21h,al     ; irq bit2 to slave
        mov al,2       ;ICW3
        out 0a1h,al    ; irq line2 on master
        mov al,1       ;ICW4
        out 21h,al     ; x86 mode
        out 0a1h,al

        mov al,1111'1100b  ;enable irq0,irq1 OCW1
        out 21h,al         ;on master
        mov al,1110'1111b  ;disable everybody OCW2 except irq12
        out 0a1h,al        ;on slave
        ret



