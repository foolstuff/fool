pmswitch:
        xor   ebx,ebx
        mov   bx,ds
        shl   ebx,4
        mov   eax,ebx
        mov   [sys_code_1 +  2],ax        ; Here we are setting real and pmode addressing the same
        mov   [sys_data_1 +  2],ax
        mov   [Real_code_1 + 2],ax
        mov   [Real_data_1 + 2],ax
        shr   eax,16
        mov   [sys_code_1 +  4],al
        mov   [sys_data_1 +  4],al
        mov   [Real_code_1 + 4],al
        mov   [Real_data_1 + 4],al
        mov   [sys_code_1 +  7],ah
        mov   [sys_data_1 +  7],ah
        mov   [Real_code_1 + 7],ah
        mov   [Real_data_1 + 7],ah
        add   ebx,gdt
        mov   dword[gdtr + 2],ebx
        add   ebx,idt-gdt
        mov   dword[idtr + 2],ebx
        cli
        mov   ax,cs
        mov   [realmodecs],ax
        lgdt  [gdtr]
        lidt  [idtr]
        mov   eax,cr0
        or    eax,1
        mov   cr0,eax
        jmp   sys_code:@f
use32
@@:
        mov   ax,sys_data
        mov   ss,ax
        nop
        mov   ax,linear_sel
        mov   ds,ax
        mov   es,ax
        mov   fs,ax
        mov   gs,ax
       ; mov   es,ax
