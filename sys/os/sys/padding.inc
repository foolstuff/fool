macro pad n {
      align 4
      v = (($-$$))
      times (n-v)/4 dd 0
}

macro fdpad {
      align 4
      pad 512*80*18*2-512
;      times (((512*80*18*2)-($-$$)-512)/4) dd 0
}
               ; 1 474 560