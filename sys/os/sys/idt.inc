align 4
align 2
idtr    df @f-idt-1:idt
idt:
        dw .isr0,sys_code,8e00h,0
        dw .isr1,sys_code,8e00h,0
        dw .isr2,sys_code,8e00h,0
        dw .isr3,sys_code,8e00h,0
        dw .isr4,sys_code,8e00h,0
        dw .isr5,sys_code,8e00h,0
        dw .isr6,sys_code,8e00h,0
        dw .isr7,sys_code,8e00h,0
        dw .isr8,sys_code,8e00h,0
        dw .isr9,sys_code,8e00h,0
        dw .isr10,sys_code,8e00h,0
        dw .isr11,sys_code,8e00h,0
        dw .isr12,sys_code,8e00h,0
        dw .isr13,sys_code,8e00h,0
        dw .isr14,sys_code,8e00h,0
        dw .isr15,sys_code,8e00h,0
        dw .isr16,sys_code,8e00h,0
        dw .isr17,sys_code,8e00h,0
        dw .isr18,sys_code,8e00h,0
        dw .isr19,sys_code,8e00h,0
        dw .isr20,sys_code,8e00h,0
        dw .isr21,sys_code,8e00h,0
        dw .isr22,sys_code,8e00h,0
        dw .isr23,sys_code,8e00h,0
        dw .isr24,sys_code,8e00h,0
        dw .isr25,sys_code,8e00h,0
        dw .isr26,sys_code,8e00h,0
        dw .isr27,sys_code,8e00h,0
        dw .isr28,sys_code,8e00h,0
        dw .isr29,sys_code,8e00h,0
        dw .isr30,sys_code,8e00h,0
        dw .isr31,sys_code,8e00h,0
; hardware interrupt handlers    timer,keyboard,ect...
; irq0 =Timer -just used dos irq names - irq1 =keyboard etc...
        dw irq0,sys_code,8e00h,0
        dw irq1,sys_code,8e00h,0
        dw irq2,sys_code,8e00h,0
        dw irq3,sys_code,8e00h,0
        dw irq4,sys_code,8e00h,0
        dw irq5,sys_code,8e00h,0
        dw irq6,sys_code,8e00h,0
        dw irq7,sys_code,8e00h,0
      ;  dw irq8,sys_code,8e00h,0
      ;  dw irq9,sys_code,8e00h,0
      ;  dw irq10,sys_code,8e00h,0
      ;  dw irq11,sys_code,8e00h,0
      ;  dw irq12,sys_code,8e00h,0
      ;  dw irq13,sys_code,8e00h,0
      ;  dw irq14,sys_code,8e00h,0
      ;  dw irq15,sys_code,8e00h,0
@@:
include 'interrupts.inc'

