if ~definite secondstage
secondstage:
.seg=800h
.org=.seg*16  ;starts at 32k
.packets=1

        org .org
        mov ax,ds
        mov es,ax
        mov ax,3
        int 10h

        mov si,.str
        call prints

        jmp $

.str:   db 'nothing to boot',0

prints:
        mov ah,0eh
@@:
        lodsb
        or al,al
        je @f
        int 10h
        jmp @b
@@:
        ret

end if
