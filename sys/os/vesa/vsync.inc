vsync:
           push es
           mov es,[desk+screen.frame]
           mov dx,3dah
@@:
           in al,dx
           test al,08h
           je @b
@@:
           in al,dx
           test al,08h
           jne @b

           push edi esi
           mov edi,[desk+screen.buffer]
           mov ecx,[desk+screen.size]
           xor ebx,ebx;
           mov edx,[colorclear]

           cmp [bpp],3
           je .retrace24
           cmp [bpp],4
           je .retrace32

.retrace32:
           mov eax,[edi+ebx*4]
           ;buffer is in 32bpp
           mov [es:ebx*4],eax
           ;screen is in 24bpp
           ;don't care to overlap a byte
if ~defined NOCLS
;.cls:
if defined CLSFX
           inc edx
end if
           mov [edi+ebx*4],edx
end if
           inc ebx
           loop .retrace32
           jmp .end
.retrace24:
           mov eax,[edi+ebx*4]
           ;buffer is in 32bpp
           mov [es:ebx*3],eax
           ;screen is in 24bpp
           ;don't care to overlap a byte
if ~defined NOCLS
;.cls:
if defined CLSFX
           inc edx
end if
           mov [edi+ebx*4],edx
end if
           inc ebx
           loop .retrace24
.end:
if defined CLSFX
           add [colorclear],10305h
end if
           pop esi edi

           pop es
           ret
