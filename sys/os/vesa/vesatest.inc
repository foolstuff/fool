vesatest:
        mov   dword [VESAInfo_Signature],'VBE2'
        mov   ax,4f00h                         ; Is Vesa installed ?                       
        mov   di,VESA_Info                     ; This is the address of how info block.
        int   10h
        cmp   ax,004Fh                         ; Is vesa installed ?,
        jne   near @f                          ; If not print a mesage & quit.
        mov   ax,4f01h                         ; Get Vesa Mode information.
        mov   di,Mode_Info                     ; This is the address of how info block.
        mov   cx,4000h+VESAMODE                ; 4112h = 32/24bit ; 0x4101 = 256bit ;4111h = 65535bit (640*480)
        and   cx,0fffh
        int   10h 
        cmp   dword [VESAInfo_Signature], 'VESA'
        jne   near @f
        cmp   byte [VESAInfo_Version+1], 2
        jb    @f                              ; VESA version below 2.0
        ret

@@:
        mov   si,novesa2msg
        call  Print
@@:
        hlt
        jmp   @b

novesa2msg:   db " You need vesa2 for this demo! ",0
