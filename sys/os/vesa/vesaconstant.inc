vesaconstant:
        mov   eax,[ModeInfo_PhysBasePtr]
        mov   [sys_vesa_1+2],ax             ; Here we are setting vesa direct segment selector
        shr   eax,16
        mov   [sys_vesa_1+4],al             ; just to simplify video access
        mov   [sys_vesa_1+7],ah
;        mov   eax,1000000h                    ; vesa buffer is at 16MB
;        mov   [sys_vbuff_1+2],ax            ; Here we are setting vesa direct segment selector
;        shr   eax,16
;        mov   [sys_vbuff_1+4],al            ; just to simplify video access
;        mov   [sys_vbuff_1+7],ah
        movzx eax,byte[ModeInfo_BitsPerPixel] ; set some vesa constants
        shr   eax,3
        mov   [bpp],eax                       ; bytes per pixel
        movzx eax,[ModeInfo_BytesPerScanLine] ; to use with the graphic lib
        mov   [bpsl],eax                      ; bytes per scan line
        imul  eax,9
        mov   [bptl],eax                      ; bytes per text line, for font85
        movzx eax,[ModeInfo_XResolution]      ; fast to read, fast to compare
        mov   [xres],eax
        movzx eax,[ModeInfo_YResolution]      ; 32 bit vesa info
        mov   [yres],eax
        imul  eax,[xres]                      ; screensize = xres * yres
        mov   [screensize],eax                ; screen total pixels
        ret