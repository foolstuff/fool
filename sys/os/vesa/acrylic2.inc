;    acrylic by unlord / xylem
;         256b intro for
;       Revision 2019 Party
;
; edfed 20230330 adapted for fasm
;
; fasm acrylic.asm


virtual at 0
stackframe:
      .di rw 1
      .si rw 1
      .bp rw 1
      .sp rw 1
      .bx rw 1
      .dx rw 1
      .cx rw 1
      .ax rw 1
      .size = $-stackframe
end virtual

HIGH_DEF:
;QUARTER_RES:

if defined HIGH_DEF
WXL=640
WYL=480
MODE=0x112
else
WXL=320
WYL=240
MODE=0x190
end if

STEPS=5
SP_OFFSET = -2

DOUBLE_SPEED:
FIX_COLORS:

        org 0x100
entrypoint:
        push 0xa000
        pop es

if defined IN_TD
        mov si, 0x100
end if

        mov ax, 0x4f02
        mov bx, MODE
        int 0x10

main:
        fild word [frame]
        fidiv word [si + entrypoint - $$]
        fst dword [time]

        fisub word [_2]
        fidiv word [_4]
        fld1

;if defined USE_PENTIUM_PRO
;        fcomi st0, st1
;else
        fcom st1
        fnstsw ax
        sahf
;end if

        ja above
        fxch st1
above:
        fstp st0
        fld1
        faddp
        fstp dword [dial]

        mov ax, 4
        mov dx, ax
        mov bx, tbl
        mov di, val
load1:
        fld1
        mov cl, STEPS
fill:
        fst dword [bx]
        add bx, ax
        fmul dword [di]
        loop fill
        fstp st0
        add di, ax
        dec dx
        jnz load1

        xor bx, bx
        xor di, di

if defined HIGH_DEF
        mov bp, 4
else
        mov bp, -1
end if

if ~defined HIGH_DEF
        mov dx, WYL
else
if defined QUARTER_RES
        mov dl, WYL/4
else
        mov dl, WYL - 256
end if
end if

row:
        mov cx, WXL
col:
        test di, di
        jnz no_bank
        inc bp
        pusha
        mov dx, bp
        mov ax, 0x4f05
        int 0x10
        popa
no_bank:

  ; AX = 4
  ; BX = 0
  ; CX = x
  ; DX = y
  ; DI = pixel
  ; SI = 0x100
  ; BP = bank

        pusha

        shl cx, 3
        sub cx, WXL*4
        shl dx, 3
        sub dx, WYL*4

        pusha

  ; compute position
        fild word [bx + SP_OFFSET - 2*stackframe.size + stackframe.dx]
        fidiv word [si + row + 1 - $$]
        fild word [bx + SP_OFFSET - 2*stackframe.size + stackframe.cx]
        fidiv word [si + row + 1 - $$]

        mov si, tbl

        mov cx, STEPS
steps:

prime:
        fld st1
        fmul dword [bx + si + 2*STEPS*4]
        fld dword [si - 4]
        fmul dword [bx + si + 0*STEPS*4]
        faddp
        fsin
        fmul dword [bx + si + 1*STEPS*4]
        dec ax
        jp prime

        fxch
        fldz
normal:
        faddp st3,st0
        fadd st2, st0
        fmul dword [si - 24]  ; 0.3
        inc ax
        jp normal
        fsubp st1,st0

        add bx, ax
        loop steps

        fld st0
        fadd st0,st2
        fmul dword [si - 16]

if defined FIX_COLORS
        fxch
end if

        popa

        mov cx, 3    ;one pixel
blit:
        fsin
        fld1
        faddp
        fimul word [si + entrypoint - $$]
        push ax
        fistp word [bx + SP_OFFSET - stackframe.size - 2]
        pop ax
        stosb        ;put subpixel
        loop blit

        popa

        add di, ax

        loop col

        dec dx
        jnz row

if defined DOUBLE_SPEED
        add word [frame], 2
else
        inc word [frame]
end if

        mov ah, 1
        int 0x16
        jz main

        mov ax, 0x3
        int 0x10

        ret

frame: dw 0
_2: dw 2
_4: dw 4
norm: dd 0.3
val: dd 0.869565217391304 ; 1/1.15
half:dd 0.588235294117647 ; 1/1.7

dial:rd 2
time: rd 1
tbl: rd STEPS*4
