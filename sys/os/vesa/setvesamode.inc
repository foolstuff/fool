setvesamode:
        pushad
        jmp   20h:@f
use16
@@:
        mov   ax,28H
        mov   ds,ax
        mov   ss,ax
        nop
        mov   bx,[realmodecs]
        push  bx
        lea   bx,[@f]
        push  bx
        mov   eax,cr0
        and   al,0xFE
        mov   cr0,eax
        retf
@@:
        mov   ax,cs
        mov   ds,ax
        mov   ss,ax
        nop
        mov   es,ax
        mov   fs,ax
        mov   gs,ax
        lidt  [ridtr]
        push  cs
        pop   ds
        push  ds
        pop   es
        mov   ax,0xB800
        mov   es,ax
        sti                                    ; re-enable interrupts
        mov   ax,4f02h                         ; set vesa screen mode
        mov   bx,VESAMODE+4000h                        ; 4112h = 640*480 32/24bpp, 0x4115 = 800*600 24/32bpp
        int   10h
        cli                                    ; Disable interrupts,   
        lgdt  [gdtr]                           ; Load the GDTR with the base address and limit of the GDT
        lidt  [idtr]
        mov   eax,cr0                          ; Set the PE [protected mode enable] bit in register CR0
        or    al,1
        mov   cr0,eax
        jmp   sys_code:@f                 ; jumps to do_pm
use32
@@:
        mov   ax,sys_data
        mov   ds,ax
        mov   ss,ax
        nop
        mov   es,ax
        mov   fs,ax
        mov   gs,ax
        mov   ax,8h
        mov   es,ax
        popad
        ret
