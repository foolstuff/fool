;-------------------------------- FASM VESA INFORMATION BLOCK -----------------------------------

VESA_Info:		
VESAInfo_Signature		rb	4      ; VBE Signature
VESAInfo_Version		rw	1      ; VBE Version
VESAInfo_OEMStringPtr		rd	1      ; VbeFarPtr to OEM String
VESAInfo_Capabilities		rb	4      ; Capabilities of graphics controller
VESAInfo_VideoModePtr		rd	1      ; VbeFarPtr to VideoModeList
VESAInfo_TotalMemory		rw	1      ; Number of 64kb memory blocks
VESAInfo_OEMSoftwareRev 	rw	1      ; VBE implementation Software revision
VESAInfo_OEMVendorNamePtr	rd	1      ; VbeFarPtr to Vendor Name String
VESAInfo_OEMProductNamePtr	rd	1      ; VbeFarPtr to Product Name String
VESAInfo_OEMProductRevPtr	rd	1      ; VbeFarPtr to Product Revision String
VESAInfo_Reserved		rb	222    ; Reserved for VBE implementation scratch area
VESAInfo_OEMData		rb	256    ; Data Area for OEM Strings

;============================== VESA MODE INFORMATION ===========================================

Mode_Info:		
ModeInfo_ModeAttributes 	rw	1      ; mode attributes
ModeInfo_WinAAttributes 	rb	1      ; window A attributes
ModeInfo_WinBAttributes 	rb	1      ; window B attributes
ModeInfo_WinGranularity 	rw	1      ; window granularity
ModeInfo_WinSize		rw	1      ; window size
ModeInfo_WinASegment		rw	1      ; window A start segment
ModeInfo_WinBSegment		rw	1      ; window B start segment
ModeInfo_WinFuncPtr		rd	1      ; real mode pointer to window function
ModeInfo_BytesPerScanLine	rw	1      ; bytes per scan line
ModeInfo_XResolution		rw	1      ; horizontal resolution in pixels or characters
ModeInfo_YResolution		rw	1      ; vertical resolution in pixels or characters
ModeInfo_XCharSize		rb	1      ; character cell width in pixels
ModeInfo_YCharSize		rb	1      ; character cell height in pixels
ModeInfo_NumberOfPlanes 	rb	1      ; number of memory planes
ModeInfo_BitsPerPixel		rb	1      ; bits per pixel
ModeInfo_NumberOfBanks		rb	1      ; number of banks
ModeInfo_MemoryModel		rb	1      ; memory model type
ModeInfo_BankSize		rb	1      ; bank size in KB
ModeInfo_NumberOfImagePages	rb	1      ; number of images
ModeInfo_Reserved_page		rb	1      ; reserved for page function
ModeInfo_RedMaskSize		rb	1      ; size of direct color red mask in bits
ModeInfo_RedMaskPos		rb	1      ; bit position of lsb of red mask
ModeInfo_GreenMaskSize		rb	1      ; size of direct color green mask in bits
ModeInfo_GreenMaskPos		rb	1      ; bit position of lsb of green mask
ModeInfo_BlueMaskSize		rb	1      ; size of direct color blue mask in bits
ModeInfo_BlueMaskPos		rb	1      ; bit position of lsb of blue mask
ModeInfo_ReservedMaskSize	rb	1      ; size of direct color reserved mask in bits
ModeInfo_ReservedMaskPos	rb	1      ; bit position of lsb of reserved mask
ModeInfo_DirectColorModeInfo	rb	1      ; direct color mode attributes
; VBE 2.0 extensions
ModeInfo_PhysBasePtr		rd	1      ; *physical address for flat memory frame buffer*
ModeInfo_OffScreenMemOffset	rd	1      ; Reserved - always set to 0
ModeInfo_OffScreenMemSize	rw	1      ; Reserved - always set to 0
; VBE 3.0 extensions
ModeInfo_LinBytesPerScanLine	rw	1      ; bytes per scan line for linear modes
ModeInfo_BnkNumberOfPages	rb	1      ; number of images for banked modes
ModeInfo_LinNumberOfPages	rb	1      ; number of images for linear modes
ModeInfo_LinRedMaskSize 	rb	1      ; size of direct color red mask (linear modes)
ModeInfo_LinRedFieldPos 	rb	1      ; bit position of lsb of red mask (linear modes)
ModeInfo_LinGreenMaskSize	rb	1      ; size of direct color green mask (linear modes)
ModeInfo_LinGreenFieldPos	rb	1      ; bit position of lsb of green mask (linear modes)
ModeInfo_LinBlueMaskSize	rb	1      ; size of direct color blue mask (linear modes)
ModeInfo_LinBlueFieldPos	rb	1      ; bit position of lsb of blue mask (linear modes)
ModeInfo_LinRsvdMaskSize	rb	1      ; size of direct color reserved mask (linear modes)
ModeInfo_LinRsvdFieldPos	rb	1      ; bit position of lsb of reserved mask (linear modes)
ModeInfo_MaxPixelClock		rd	1      ; maximum pixel clock (in Hz) for graphics mode
; Reserved
ModeInfo_Reserved		rb	190    ; remainder of ModeInfoBlock
;======================================= START OF PROGRAM  ======================================



