;irq12:
        cli
        push eax ebx ecx edx
        in al,64h
        and al,1
        je .end
        in al,60h
        mov ebx,[.byte]
        and ebx,3
        mov [.b0+ebx],al
        inc [.byte]
.end:
        pop edx ecx ebx eax
        sti
        ret

align 4
.byte dd ?

.b0 db ?
.b1 db ?
.b2 db ?
.b3 db ?

.x dd ?
.y dd ?
.z dd ?
.dx db ?
.dy db ?
.dz db ?
.l db ?
.r db ?
.m db ?
