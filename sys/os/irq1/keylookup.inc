.154f:	 ;scancode look up table
db 80h	 ; kbd buffer full
db 01h	 ; Esc
db 02h	 ; 1 !
db 03h	 ; 2 @
db 04h	 ; 3 #
db 05h	 ; 4 $
db 06h	 ; 5 %
db 07h	 ; 6 ^
db 08h	 ; 7 &
db 09h	 ; 8 *
db 0Ah	 ; 9 (
db 0Bh	 ; 0 )
db 0Ch	 ; - _
db 0Dh	 ; = +
db 0Eh	 ; Backspace
db 0Fh	 ; Tab
db 10h	 ; Q
db 11h	 ; W
db 12h	 ; E
db 13h	 ; R
db 14h	 ; T
db 15h	 ; Y
db 16h	 ; U
db 17h	 ; I
db 18h	 ; O
db 19h	 ; P
db 1Ah	 ; [ {
db 1Bh	 ; ] }
db 1Ch	 ; Enter
db 1Dh	 ; Ctrl
db 1Eh	 ; A
db 1Fh	 ; S
db 20h	 ; D
db 21h	 ; F
db 22h	 ; G
db 23h	 ; H
db 24h	 ; J
db 25h	 ; K
db 26h	 ; L
db 27h	 ; : ;
db 28h	 ; ' "
db 29h	 ; ` ~
db 2Ah	 ; Left Shift
db 2Bh	 ; \ |
db 2Ch	 ; Z
db 2Dh	 ; X
db 2Eh	 ; C
db 2Fh	 ; V
db 30h	 ; B
db 31h	 ; N
db 32h	 ; M
db 33h	 ; , <
db 34h	 ; . >
db 35h	 ; / ?
db 36h	 ; Right Shift
db 37h	 ; Grey*
db 38h	 ; Alt
db 39h	 ; SpaceBar
db 3Ah	 ; CapsLock
db 3Bh	 ; F1
db 3Ch	 ; F2
db 3Dh	 ; F3
db 3Eh	 ; F4
db 3Fh	 ; F5
db 40h	 ; F6
db 41h	 ; F7
db 42h	 ; F8
db 43h	 ; F9
db 44h	 ; F10
db 45h	 ; NumLock
db 46h	 ; ScrollLock
db 47h	 ; Home
db 48h	 ; UpArrow
db 49h	 ; PgUp
db 4Ah	 ; Grey-
db 4Bh	 ; LeftArrow
db 4Ch	 ; Keypad 5
db 4Dh	 ; RightArrow
db 4Eh	 ; Grey+
db 4Fh	 ; End
db 50h	 ; DownArrow
db 51h	 ; PgDn
db 52h	 ; Ins
db 53h	 ; Del
db 54h	 ; SysReq          -
db 0	 ;55h
db 56h	 ; left \| (102-key)
db 57h	 ; F11
db 58h	 ; F12
db 0	 ;59h
db 5Ah	 ; PA1
db 5Bh	 ; F13 (LWin)
db 5Ch	 ; F14 (RWin)
db 5Dh	 ; F15 (Menu)
db 0	 ;5Eh
db 0	 ;5Fh
db 0	 ;60h
db 0	 ;61h
db 0	 ;62h
db 63h	 ; F16
db 64h	 ; F17
db 65h	 ; F18
db 66h	 ; F19
db 67h	 ; F20
db 68h	 ; F21 (Fn) [*]
db 69h	 ; F22
db 6Ah	 ; F23
db 6Bh	 ; F24
db 6Ch	 ; --
db 6Dh	 ; EraseEOF
db 0	 ;6Eh
db 6Fh	 ; Copy/Play
db 0	 ;70h
db 0	 ;71h
db 72h	 ; CrSel
db 73h	 ;  [*]
db 74h	 ; ExSel
db 75h	 ; --
db 76h	 ; Clear
db 0	 ;77h
db 0	 ;78h
db 0	 ;79h
db 0	 ;7Ah
db 0	 ;7Bh
db 0	 ;7Ch
db 0	 ;7Dh
db 0	 ;7Eh
db 0	 ;7Fh
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db 0	 ;80h
db 81h	 ; ESC
db 82h	 ; 1 !
db 83h	 ; 2 @
db 84h	 ; 3 #
db 85h	 ; 4 $
db 86h	 ; 5 %
db 87h	 ; 6 ^
db 88h	 ; 7 &
db 89h	 ; 8 *
db 8Ah	 ; 9 (
db 8Bh	 ; 0 )
db 8Ch	 ; - _
db 8Dh	 ; = +
db 8Eh	 ; Backspace
db 8Fh	 ; Tab
db 90h	 ; Q
db 91h	 ; W
db 92h	 ; E
db 93h	 ; R
db 94h	 ; T
db 95h	 ; Y
db 96h	 ; U
db 97h	 ; I
db 98h	 ; O
db 99h	 ; P
db 9Ah	 ; [ {
db 9Bh	 ; ] }
db 9Ch	 ; Enter
db 9Dh	 ; Ctrl
db 9Eh	 ; A
db 9Fh	 ; S
db 0A0h  ; D
db 0A1h  ; F
db 0A2h  ; G
db 0A3h  ; H
db 0A4h  ; J
db 0A5h  ; K
db 0A6h  ; L
db 0A7h  ; : ;
db 0A8h  ; ' "
db 0A9h  ; ` ~
db 0AAh  ; Left Shift, self-test complete
db 0ABh  ; \ |
db 0ACh  ; Z
db 0ADh  ; X
db 0AEh  ; C
db 0AFh  ; V
db 0B0h  ; B
db 0B1h  ; N
db 0B2h  ; M
db 0B3h  ; , <
db 0B4h  ; . >
db 0B5h  ; / ?
db 0B6h  ; Right Shift
db 0B7h  ; Grey*
db 0B8h  ; Alt
db 0B9h  ; SpaceBar
db 0BAh  ; CapsLock
db 0BBh  ; F1
db 0BCh  ; F2
db 0BDh  ; F3
db 0BEh  ; F4
db 0BFh  ; F5
db 0C0h  ; F6
db 0C1h  ; F7
db 0C2h  ; F8
db 0C3h  ; F9
db 0C4h  ; F10
db 0C5h  ; NumLock
db 0C6h  ; ScrollLock
db 0C7h  ; Home
db 0C8h  ; UpArrow
db 0C9h  ; PgUp
db 0CAh  ; Grey-
db 0CBh  ; LeftArrow
db 0CCh  ; Keypad 5
db 0CDh  ; RightArrow
db 0CEh  ; Grey+
db 0CFh  ; End
db 0D0h  ; DownArrow
db 0D1h  ; PgDn
db 0D2h  ; Ins
db 0D3h  ; Del
db 0D4h  ; SysReq
db 0	 ;D5h
db 0D6h  ; left \| (102-key)
db 0D7h  ; F11
db 0D8h  ; F12
db 0	 ;D9h
db 0DAh  ; PA1
db 0DBh  ; F13 (LWin)
db 0DCh  ; F14 (RWin)
db 0DDh  ; F15 (Menu)
db 0	 ;DEh
db 0	 ;DFh
db 0E0h  ; prefix code
db 0E1h  ; prefix code
db 0	 ;E2h
db 0E3h  ; F16
db 0E4h  ; F17
db 0E5h  ; F18
db 0E6h  ; F19
db 0E7h  ; F20
db 0E8h  ; F21 (Fn) [*]
db 0E9h  ; F22
db 0EAh  ; F23
db 0EBh  ; F24
db 0ECh  ; --
db 0EDh  ; EraseEOF
db 0EEh  ; ECHO
db 0EFh  ; Copy/Play
db 0F0h  ; prefix code (key break)
db 0	 ;F1h
db 0F2h  ; CrSel
db 0F3h  ;  [*]
db 0F4h  ; ExSel
db 0F5h  ; --
db 0F6h  ; Clear
db 0	 ;F7h
db 0	 ;F8h
db 0	 ;F9h
db 80h	 ;0FAh  ; ACK
db 0	 ;0FBh
db 80h	 ;0FCh  ; diag failure (MF-kbd)
db 80h	 ;0FDh  ; diag failure (AT-kbd)
db 80h	 ;0FEh  ; RESEND
db 80h	 ;0FFh  ; kbd error/buffer full
