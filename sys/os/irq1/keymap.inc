align 4
key:
.map=0
.buffers=50
.cur=128
.old=129
.tmp=.cur+.buffers
.status=.tmp+1
.scrolled=1
.numled=2
.shiftled=4
.majled=.shiftled
.insflag=8
.shift?=16
.ctrl?=32
.alt?=64
.altgr?=128
rb 128
rb .buffers
rb 2
.stop:
include 'scancodes.inc'
align 8