;irq1:
         cli
         push   eax ebx ecx edx
         in     al,64h
         and    al,1
         je     .end
         in     al,60h
         mov    ebx,.154f
         xlat   [ebx]
         mov    ebx,key
         cmp    al,80h
         je     .reset
         or     al,al
         je     .tmp
         cmp    al,0e0h
         je     .tmp
         cmp    al,0e1h
         je     .tmp
         cmp    al,0f0h
         je     .tmp
         mov    cl,[ebx+key.tmp]
         cmp    cl,0e1h
         je     .pause
         call .push
         mov    ah,al
         and    ah,80h
         and    al,7fh
         cmp    al,70h
         jge    .end
         cmp    cl,0e0h
         jne    @f
         call   .extended
         jc     .end
         jmp    .normal
@@:
         cmp    al,key.home
         jb     @f
         cmp    al,key.delete
         ja     @f
         add    al,key.num7-key.home
@@:
         cmp    al,key.prtscr
         jne    @f
         mov    al,key.nummul
@@:
.normal:
         mov    edx,key.buffers-1
@@:
         mov    cl,[ebx+key.cur+edx]
         mov    [ebx+key.old+edx],cl
         dec    edx
         jnl    @b
         mov    cl,al
         or     cl,ah
         mov    [ebx+key.cur],cl
         movzx  ecx,al
         add    ecx,key.map
         or     ah,ah
         mov    ah,0
         jne    .break
.make:
;         call .push
         mov    ah,[ebx+ecx]
         inc    ah
         jne    @f
         dec    ah
@@:
.break:
         mov    [ebx+ecx],ah
         xor    ecx,ecx
         mov    ax,[ebx+key.cur]
         cmp    al,ah
         je     .status
         cmp    al,key.verrnum
         jne    @f
         or     cl,key.numled
         jmp    .status
@@:
         cmp    al,key.scrollock
         jne    @f
         or     cl,key.scrolled
         jmp    .status
@@:
         cmp    al,key.verrmaj
         jne    @f
         or     cl,key.majled
         jmp    .status
@@:
         cmp    al,key.ins
         jne    @f
         or     cl,key.insflag
@@:
.status:
         or     cl,cl
         je     .noled
         xor    [ebx+key.status],cl
         mov    al,0edh
         out    60h,al
@@:
         in     al,64h
         test   al,2
         jne    @b
         mov    al,[ebx+key.status]
         and    al,7
         out    60h,al
         in     al,60h
.noled:
         and    byte[ebx+key.status],0fh
         xor    al,al
         cmp    byte[ebx+key.shiftr],0
         je     @f
         or     al,key.shift?
@@:
         cmp    byte[ebx+key.shiftl],0
         je     @f
         or     al,key.shift?
@@:
         cmp    byte[ebx+key.ctrll],0
         je     @f
         or     al,key.ctrl?
@@:
         cmp    byte[ebx+key.ctrlr],0
         je     @f
         or     al,key.ctrl?
@@:
         cmp    byte[ebx+key.altgr],0
         je     @f
         or     al,key.alt?+key.altgr?
@@:
         cmp    byte[ebx+key.altl],0
         je     @f
         or     al,key.alt?
@@:
         or     [ebx+key.status],al
         xor    ecx,ecx
         jmp    .end
.reset:
         mov    eax,(128+4)-1
@@:
         mov    [ebx+eax],ah
         dec    eax
         jnl    @b
         jmp    .end
.pause:
         mov    al,0
.tmp:
         mov    cl,al
         jmp    .end
.end:
         mov    [ebx+key.tmp],cl
         mov    ecx,128/4
         xor    eax,eax
@@:
         dec    ecx
         jl     @f
         or     eax,[ebx+ecx*4]
         je     @b
@@:
         or     eax,eax
         je     @f
         mov    al,1
@@:
         mov    [ebx+key.none],al
.exit:
         in     al,61h
         or     al,80h
         out    61h,al
         and    al,7fh
         out    61h,al
         mov    al,20h
         out    20h,al
         sti
         pop    edx ecx ebx eax
         iret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.extended:                          ;;
         mov    cl,0                ;;
         cmp    al,key.shiftl       ;;
         je     .shift              ;;
         cmp    al,key.shiftr       ;;
         je     .shift              ;;
         cmp    al,key.entr�e       ;;
         jne    @f                  ;;
         mov    al,key.numenter     ;;
         ret                        ;;
@@:                                 ;;
         cmp    al,key.altl         ;;
         jne    @f                  ;;
         mov    al,key.altr         ;;
         ret                        ;;
@@:                                 ;;
         cmp    al,key.ctrll        ;;
         jne    @f                  ;;
         mov    al,key.ctrlr        ;;
         ret                        ;;
@@:                                 ;;
         cmp    al,key.!            ;;
         jne    @f                  ;;
         mov    al,key.numdiv       ;;
         ret                        ;;
@@:                                 ;;
         clc                        ;;
         ret                        ;;
.shift:                             ;;
         stc                        ;;
         ret                        ;;

.push:
        push ebx
        movzx ebx,byte[.last]
        mov [.fifo+ebx],al
        inc bl
        mov [.last],bl
        pop ebx
        ret

.pop:
        movzx ebx,byte[.first]
        cmp bl,[.last]
        je @f
        mov al,[.fifo+ebx]
        inc bl
        mov [.first],bl
        clc
        ret
@@:
        stc
        ret

.fifo   db 256 dup 0
.first  db 0
.last   db 0

.flush:
.call=0
.input=4
        pushad
@@:
        call irq1.pop
        jc @f
        fcall [esi+.input]
        jmp @b
@@:
        popad
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
include 'keylookup.inc'             ;;
include 'keymap.inc'                ;; AZERTY keyboard, for qwerty, make it your self!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Extended keys have now their own scancodes :D


macro KeyFlush i {
        dd irq1.flush,i
}

