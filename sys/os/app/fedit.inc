fedit:
.x=0
.y=0
.xl=200
.yl=170
        Gnode .x,.y,.xl,.yl,\
        .titlebar,\
        .menubar,\
        .editzone;,\
;        grabber
.fasmname:
        db 'fasm edit '
.fasmver:
        db '0.1',0
        align 4
.titlebar:
        Gnode 0,0,.xl,15,\
        .chgcase,\
        .upcase,\
        .lowcase,\
        .titleicon,\
        .titletxt,\
        .titlebox
.titletxt:
        Txt 40,0,0,0,Yellow,.fasmname,font85
.titleicon:
        But 1,1,15,14,0,.fasmicon,0
.titlebox:
        Box 0,0,.xl,15,Green+8
.chgcase:
        But 20,1,5,13,Green-4,0,.caseinv
.upcase:
        But 25,1,5,13,Yellow-4,0,.caseup
.lowcase:
        But 30,1,5,13,Red-4,0,.caselow

.caseinv:
;        Asm @f
;@@:
;        push eax ecx
;        mov ecx,.texte
;@@:
;        mov al,[ecx]
;        cmp al,0
;        je @f
;;        call case.inv
;        mov [ecx],al
;        inc ecx
;        jmp @b
;@@:
;        pop ecx eax
;        ret
;        align 4
.caseup:
;        Asm @f
;@@:
;        push eax ecx
;        mov ecx,.texte
;@@:
;        mov al,[ecx]
;        cmp al,0
;        je @f
;;        call case.up
;        mov [ecx],al
;        inc ecx
;        jmp @b
;@@:
;        pop ecx eax
;        ret
;        align 4
.caselow:
;        Asm @f
;@@:
;        push eax ecx
;;        mov ecx,.texte
;@@:
;        mov al,[ecx]
;        cmp al,0
;        je @f
;;        call case.low
;        mov [ecx],al
;        inc ecx
;        jmp @b
;@@:
;        pop ecx eax
;        ret
;        align 4
.menubar:
        Gnode 0,15,.xl,15,\
        .filemenu,\
        .editmenu,\
        .searchmenu,\
        .runmenu,\
        .optionsmenu,\
        .helpmenu,\
        .menubox
.menubox:
        Box 0,0,.xl,15,Silver
.filemenu:
        Txt 5,0,0,0,Black,.filetxt,font85
.editmenu:
        Txt 35,0,0,0,Black,.edittxt,font85
.searchmenu:
        Txt 65,0,0,0,Black,.searchtxt,font85
.runmenu:
        Txt 95,0,0,0,Black,.runtxt,font85
.optionsmenu:
        Txt 125,0,0,0,Black,.optionstxt,font85
.helpmenu:
        Txt 155,0,0,0,Black,.helptxt,font85
.filetxt:
        db 'File',0
.edittxt:
        db 'Edit',0
.searchtxt:
        db 'Find',0
.runtxt:
        db 'Run',0
.optionstxt:
        db 'Conf',0
.helptxt:
        db 'Help',0
.editzone:
        Gnode 0,30,.xl,.yl-30,\
        .edittext,\
        .editbox
.editbox:
        Box 0,0,.xl,.yl-30,Black
.edittext:
        Txt 0,0,0,0,Blue,.texte,font85
.texte:
db "db 'hello world'",0
;file 'fedit.inc'
db 0
.fasmicon:
include 'fasmicon.inc'
;include 'changecase.inc'
