dd keymap.name
keymap:
.Z=2
.C1=0fch
.C2=Silver
.XL=250*keymap.Z
.YL=95*keymap.Z
        Gnode 0,0,.XL,.YL,\
        .txt,\
        .map,\
        .flags,\
        .box

.box:
        Box 0*.Z,0*.Z,250*.Z,95*.Z,.C2

.txt:   Vtxt 170*.Z,75*.Z,10,10,0,.name,.Z,ugly
.name:  db 'Keymap v0.1',0
        align 4

.map:   Gnode 10*.Z,10*.Z,230*.Z,75*.Z,\
        .00,.01,    .02,    .03,    .04,\
        .10,                .11,    .12,  .13,\
        .20,.21,                    .23,  .24,  .25,\
        .30,.31,            .22,          .32,\
        .40,.41,            .42,    .43,  .44,  .45,\
        .50,   .51,    .52,      .53,     .54,.55

.flags: Gnode 10*.Z,10*.Z,230*.Z,75*.Z,\
        .60,\
        .70,\
        .80

.00  dd buts,0*.Z,0*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.esc
     @@:
.01  dd buts,19*.Z,0*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.f1,key.f2,key.f3,key.f4
     @@:
.02  dd buts,64*.Z,0*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.f5,key.f6,key.f7,key.f8
     @@:
.03  dd buts,109*.Z,0*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.f9,key.f10,key.f10,key.f12
     @@:
.04  dd buts,155*.Z,0*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.prtscr,key.scrollock,key.pause
     @@:
.10  dd buts,0*.Z,15*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.sqr,key.1,key.2,key.3,key.4,key.5,key.6,key.7,key.8,key.9,key.0,key.deg,key.equ
     @@:
.11  dd buts,130*.Z,15*.Z,19*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.bs
     @@:
.12  dd buts,155*.Z,15*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.ins,key.home,key.pgup
     @@:
.13  dd buts,190*.Z,15*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.verrnum,key.numdiv,key.nummul,key.numsub
     @@:
.20  dd buts,0*.Z,25*.Z,15*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.tab
     @@:
.21  dd buts,15*.Z,25*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.a,key.z,key.e,key.r,key.t,key.y,key.u,key.i,key.o,key.p,key.^,key.$
     @@:
.22: Put 135*.Z,25*.Z,14*.Z,20*.Z,0,next
     Node .22box,.22frame,.22asm,.22buts
.22buts:
     dd buts,0*.Z,0*.Z,14*.Z,20*.Z,.C1,key,@f-$-4,0
     db key.enter
     @@:
.22box:
     Box 0*.Z+1,10*.Z-1,2*.Z,10*.Z,.C2
.22frame:
     Frame 0*.Z+1,10*.Z-2,2*.Z+1,10*.Z,0
.22asm:
     Asm next
     push eax
     mov al,[key+key.enter]
     add al,[.22buts+buts.c]
     shl al,1
     movzx eax,al
     mov eax,[palettes.usr+eax*4]
     mov [.22box+box.c],eax
     mov eax,[.22buts+buts.c]
     mov [.22frame+frame.c],eax
     pop eax
     ret
     align 4
.23  dd buts,155*.Z,25*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.del,key.end,key.pgdn
     @@:
.24  dd buts,190*.Z,25*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.num7,key.num8,key.num9
     @@:
.25  dd buts,220*.Z,25*.Z,10*.Z,20*.Z,.C1,key,@f-$-4,0
     db key.numadd
     @@:
.30  dd buts,0*.Z,35*.Z,17*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.verrmaj
     @@:
.31  dd buts,17*.Z,35*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.q,key.s,key.d,key.f,key.g,key.h,key.j,key.k,key.l,key.m,key.�,key.�
     @@:
.32  dd buts,190*.Z,35*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.num4,key.num5,key.num6
     @@:
.40  dd buts,0*.Z,45*.Z,13*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.shiftl
     @@:
.41  dd buts,13*.Z,45*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.sup,key.w,key.x,key.c,key.v,key.b,key.n,key.coma,key.dotcom,key.2pt,key.!
     @@:
.42  dd buts,123*.Z,45*.Z,26*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.shiftr
     @@:
.43  dd buts,165*.Z,45*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.up
     @@:
.44  dd buts,190*.Z,45*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.num1,key.num2,key.num3
     @@:
.45  dd buts,220*.Z,45*.Z,10*.Z,20*.Z,.C1,key,@f-$-4,0
     db key.numenter
     @@:
.50  dd buts,0*.Z,55*.Z,13*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.ctrll,key.winl,key.altl
     @@:
.51  dd buts,39*.Z,55*.Z,58*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.spc
     @@:
.52  dd buts,97*.Z,55*.Z,13*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.altgr,key.winr,key.menu,key.ctrlr
     @@:
.53  dd buts,155*.Z,55*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.left,key.down,key.right
     @@:
.54  dd buts,190*.Z,55*.Z,20*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.num0
     @@:
.55  dd buts,210*.Z,55*.Z,10*.Z,10*.Z,.C1,key,@f-$-4,0
     db key.numdot
     @@:
.60  dd bits,200*.Z,6*.Z,2*.Z,2*.Z,.C1,key+key.status,@f-$-4
     db key.numled,0,0,0,key.shiftled,0,0,0,key.scrolled
     @@:
.70  dd boxs,0*.Z,70*.Z,3*.Z,5*.Z,.C1,key+key.cur-1,@f-$-4
     times key.buffers db %-1
     @@:
.80  dd bits,192*.Z,0*.Z,4*.Z,2*.Z,.C1,key+key.status,@f-$-4
     db key.insflag,0,key.shift?,0,key.alt?,0,key.altgr?,0,key.ctrl?
     @@:
