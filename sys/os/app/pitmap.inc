;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pitmap:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.xl=220
.yl=130
.tyl=20
.c1=Maroon
.c2=Silver
.c3=Green
.c4=Teal
        Panel 0,0,.xl,.yl,.c1,.c2,.title,.view
.view:  Gnode 1,.tyl+1,.xl-2,.yl-.tyl-2,\
        .app,@f
@@:     Box 0,0,.xl-2,.yl-.tyl-2,.c1+2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.app:   Gnode 0,0,.xl-2,.yl-.tyl-2,\
        pitcnt,\
        secs,\
        msecs,\
        cmoslow,\
        cpudt,\
        cputime,\
        stime,\
        rtcint,\
        cptime,\
        .asm

.scnt   dd 0
.mscnt  dd 0
.ctime  dq %t

.asm:   Asm next
        push eax ebx edx
        mov eax,[irq0.inc]
        add [.mscnt],eax
        mov eax,[.mscnt]
        xor edx,edx
        mov ebx,1000
        div ebx
        mov [.scnt],eax
        pop edx ebx eax
        ret
.title: db 'PITmap',0
rscs:
.str1:  db 'PIT counter    = '
.str10: db '00000000',0
.str2:  db 'seconds        = '
.str20: db '00000000',0
.str3:  db 'milliseconds   = '
.str30: db '00000000',0
.str4:  db 'cmos   low ram = '
.str40: db '00000000',0
.str5:  db 'CPU delta      = '
.str50: db '0000000000000000',0
.str6:  db 'CPU time       = '
.str60: db '0000000000000000',0
.str7:  db 'startup time   = '
.str70: db '00000000',0
.str8:  db 'RTC interrupt  = '
.str80: db '00000000',0
.str9:  db 'Compile time   = '
.str90: db '0000000000000000',0
align 4
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pitcnt: Htxt 6,15,0,0,pitmap.c2,rscs.str1,irq0.cnt,rscs.str10,8
secs:   Htxt 6,25,0,0,pitmap.c2,rscs.str2,pitmap.scnt,rscs.str20,8
msecs:  Htxt 6,35,0,0,pitmap.c2,rscs.str3,pitmap.mscnt,rscs.str30,8
cmoslow:Htxt 6,45,0,0,pitmap.c2,rscs.str4,0,rscs.str40,8
cpudt:  Htxt 6,55,0,0,pitmap.c2,rscs.str5,irq0.cpucnt,rscs.str50,16
cputime:Htxt 6,65,0,0,pitmap.c2,rscs.str6,irq0.tsc,rscs.str60,16
stime:  Htxt 6,75,0,0,pitmap.c2,rscs.str7,0,rscs.str70,8
rtcint: Htxt 6,85,0,0,pitmap.c2,rscs.str8,0,rscs.str80,8
cptime: Htxt 6,5,0,0,pitmap.c2,rscs.str9,pitmap.ctime,rscs.str90,16
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


