;if defined COMPORT
;
;struct DCB
;  DCBlength           dd ?
;  BaudRate            dd ?
;  flags               dd ?
;  wReserved           dw ?
;  XonLim              dw ?
;  XoffLim             dw ?
;  ByteSize            db ?
;  Parity              db ?
;  StopBits            db ?
;  Xondb               db ?
;  Xoffdb              db ?
;  Errordb             db ?
;  Eofdb               db ?
;  Evtdb               db ?
;  wReserved1          dw ?
;ends
;
;struct COMMTIMEOUTS
;  ReadIntervalTimeout           dd ?
;  ReadTotalTimeoutMultiplier    dd ?
;  ReadTotalTimeoutConstant      dd ?
;  WriteTotalTimeoutMultiplier   dd ?
;  WriteTotalTimeoutConstant     dd ?
;ends
;
;Serial:
;.init:
;        invoke CreateFile,com,GENERIC_READ or GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,0
;        mov [hcom],eax
;        invoke GetCommState,eax,dcb
;        mov [dcb.BaudRate],38400
;        mov [dcb.ByteSize],8
;        mov [dcb.Parity],0
;        mov [dcb.StopBits],0
;        mov [dcb.flags],$80000000
;        invoke SetCommState,[hcom],dcb
;        mov [timeout.ReadTotalTimeoutConstant],1000
;        invoke SetCommTimeouts,[hcom],timeout
;        invoke  CreateThread, NULL, 0, .receive, NULL, 0, tinput
;        mov [tinput],eax
;;        invoke  CreateThread, NULL, 0, .get, NULL, 0, tinput
;;        mov [tget],eax
;        invoke  CreateThread, NULL, 0, .send, NULL, 0, toutput
;        mov [toutput],eax
;        ret
;

;.receive:
;        invoke ReadFile,[hcom],input.dat,[input.size],input.cnt,NULL
;        cmp [input.cnt],0
;        je .receive
;        mov edi,[buffer.index]
;        mov al,[input.dat]
;        mov [buffer+edi],al
;        inc edi
;        and edi,0ffh
;        mov [buffer.index],edi
;        cmp al,1
;        jne @f
;        mov [gettime.do],1
;@@:
;        cmp al,2
;        jne @f
;        mov [gettime.do],4
;@@:
;        call gettime
;        jmp .receive
;
;.get:
;        invoke ReadFile,[hcom],buf.size,1,buf.cnt,NULL
;        movzx eax,byte[buf.size]
;        or al,al
;        je .get
;        invoke ReadFile,[hcom],buf.dat,eax,buf.cnt,NULL
;        jmp .get
;
;.give:
;        movzx eax,byte[buf.size]
;        or al,al
;        je @f
;        invoke ReadFile,[hcom],buf.dat,eax,buf.cnt,NULL
;@@:
;        ret
;

;.send:
;        invoke ReadFile,[keyboard],output.dat,[output.size],output.cnt,NULL
;        cmp [output.cnt],0
;        je @f
;        cmp [output.size],0
;        je .send
;        invoke WriteFile,[hcom],output.dat,[output.size],output.cnt,NULL
;        mov eax,[output.cnt]
;        mov [output.size],0

;@@:
;        jmp .send
      ;  ret

;;strsize:
;        mov ecx,-1
;@@:
;        inc ecx
;        cmp byte[eax+ecx],0
;        jne @b
;        ret
;
;end if



;tinput dd 0
;tget   dd 0
;toutput dd 0
;
;console dd 0
;keyboard dd 0
;kb_handle dd 0FFFFFFF6h
;con_handle dd 0FFFFFFF5h

;timeout COMMTIMEOUTS
;dcb DCB

;if defined COMPORT
;com TCHAR '\\.\',COMPORT,0
;else
;com TCHAR '\\.\COM3',0
;end if

;hcom rd 1

;input:
;.size dd 1
;.cnt  dd 1
;.dat  db 0,0,0,0
;
;buf:
;.cnt  dd 1
;.size db 0
;.dat  rb 255
;
;output:
;.size dd 1
;.cnt  dd 1
;.dat  db 'r',0,0,0
;
;bufferbmp:
;       dd bmp,400,0,32,32,0
;buffer rb 256
;.index dd 0
;println rb 256
;       db 0
