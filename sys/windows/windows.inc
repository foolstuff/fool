if defined HIDDEN
   _TOOLWINDOW = WS_EX_TOOLWINDOW
else
   _TOOLWINDOW = 0
end if

if defined ONTOP
   _TOPMOST = WS_EX_TOPMOST
else
   _TOPMOST = 0
end if

if defined ESCAPE
   _ESCAPE = ESCAPE
else
   _ESCAPE = VK_ESCAPE
end if

format PE GUI 4.0
BINARYSTART:
entry mswindowsmain

include 'win32a.inc'

ICON                equ 17
CURSOR              equ IDC_ARROW
TIMER1              equ 1
TIMER2              equ 2
DIB_RGB_COLORS      equ 0
COLOR_3DFACE        equ 15
SM_XVIRTUALSCREEN   equ 76
SM_YVIRTUALSCREEN   equ 77
SM_CXVIRTUALSCREEN  equ 78
SM_CYVIRTUALSCREEN  equ 79

section '.text' code readable executable
WINMAINSECTION:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

mswindowsmain:
        invoke GetModuleHandle,0
        mov [wc.hInstance],eax
        ;invoke LoadIcon,eax,ICON
        ;mov [wc.hIcon],eax
        invoke LoadCursor,[wc.hInstance],CURSOR
        mov [wc.hCursor],eax
        invoke ShowCursor,FALSE
        mov dword[mouse.render],0
        invoke RegisterClass,wc
        test eax,eax
        je .error

;if defined COMPORT
;        call Serial.init
;end if
        call window.resolution
        invoke CreateWindowEx,\
              WS_EX_LAYERED+_TOPMOST+_TOOLWINDOW,\
              classname,message,\
              WS_VISIBLE,\;+WS_POPUP,\
              [window.x],[window.y],\
              [desk+screen.xl],[desk+screen.yl],\
              NULL,NULL,[wc.hInstance],NULL
        or eax,eax
        je .error
        mov [hwnd_main],eax
        mov [mousetracker.dwFlags],2
        mov [mousetracker.hwndTrack],eax
        mov [wc.lpfnWndProc],mainproc
        mov [wc.lpszClassName],classname
        invoke GetStockObject,0
        mov [wc.hbrBackground],eax
        invoke  SetLayeredWindowAttributes,[hwnd_main],0,[transparency],LWA_ALPHA

.msg_loop:
        invoke GetMessage,wmsg,NULL,0,0
        cmp eax,1
        jb .end_loop
        jne .msg_loop
        invoke TranslateMessage,wmsg
        invoke DispatchMessage,wmsg
        jmp .msg_loop
.error:
        invoke MessageBox,NULL,msgerror,NULL,MB_ICONERROR+MB_OK
.end_loop:
        invoke ExitProcess,[wmsg.wParam]

proc mainproc uses ebx esi edi,hWnd,uMsg,wParam,lParam
        mov eax,[uMsg]
        cmp eax,WM_KILLFOCUS
        je .killfocus
        cmp eax,WM_TIMER
        je .timer
        cmp eax,WM_MOUSELEAVE
        je .leave
        cmp eax,WM_MOUSEMOVE
        je .cursor
        cmp eax,WM_MOUSEWHEEL
        je .wheel
        cmp eax,WM_MBUTTONDOWN
        je .mbutton
        cmp eax,WM_MBUTTONUP
        je .mbutton
        cmp eax,WM_LBUTTONDOWN
        je .mbutton
        cmp eax,WM_LBUTTONUP
        je .mbutton
        cmp eax,WM_RBUTTONDOWN
        je .mbutton
        cmp eax,WM_RBUTTONUP
        je .mbutton
        cmp eax,WM_KEYDOWN
        je .keydown
        cmp eax,WM_KEYUP
        je .keyup
        cmp eax,WM_PAINT
        je .paint
        cmp eax,WM_CREATE
        je .create
;        cmp eax,WM_SETTEXT
;        je .settext
        cmp eax,WM_ERASEBKGND
        je .true
        cmp eax,WM_CLOSE
        je .close
        cmp eax,WM_DESTROY
        je .quit
        cmp eax,WM_SYSCOMMAND
        je .syscommand
.killfocus:
.default:
        jmp .true
.syscommand:
        mov eax,[wParam]
        cmp eax,0f060h
        je .destroy
        jmp .end
.leave:
        mov dword[mouse.render],0
        jmp .end
.settext:
        invoke SetWindowText,[hWnd],[lParam]
        jmp .end
.wheel:
        mov [mouse.frozen],0
        mov eax,[wParam]
        sar eax,16+3
        mov eax,0
        jl @f
        mov eax,2
@@:
        dec eax
        mov [mouse.dz],eax
        add [mouse.z],eax
        jmp .end
.mbutton:
        mov [mouse.frozen],0
        mov eax,[wParam]
        mov ah,al
        shr al,1
        and al,1
        mov [mouse.r],al
        mov al,ah
        and al,1
        mov [mouse.l],al
        mov al,ah
        shr al,4
        and al,1
        mov [mouse.m],al
        jmp .end
.cursor:
        invoke TrackMouseEvent,mousetracker
        invoke GetCursorPos,mouse+gnode.x
        invoke ScreenToClient,[window],mouse+gnode.x
        mov dword[mouse.render],gnode
        jmp .end
.keydown:
        mov [mouse.frozen],0
if defined NOESCAPE
else
        cmp byte[wParam],_ESCAPE
        je .destroy
end if
        mov eax,[lParam]
        shr eax,16
        and al,7fh
        and ah,1
        call irq1
        jmp .end
.keyup:
        mov [mouse.frozen],0
        mov eax,[lParam]
        shr eax,16
        or al,80h
        and ah,1
        call irq1
        jmp .end
.create:
        mov eax,[hWnd]
        mov [window],eax
        invoke SetTimer,[window],TIMER1,TIMERMS,NULL
        jmp .end
.timer:
        invoke GetLocalTime,datetime
        invoke InvalidateRect,[window],NULL,NULL
        call irq0
        jmp .end

.createdib:
        ret

.deletedib:
        invoke BitBlt,[hdc],0,0,[desk+screen.xl],[desk+screen.yl],[memdc],0,0,SRCCOPY
        invoke DeleteObject,[dib]
        invoke DeleteDC,[memdc]
        invoke EndPaint,[window],ps
        ret
.paint:
        cmp [exit],0
        jne .destroy

        invoke BeginPaint,[window],ps
;        cmp eax,[hdc]
;        je @f
        mov [hdc],eax
        invoke CreateCompatibleDC,[hdc]
        mov [memdc],eax
        invoke CreateDIBSection,[hdc],bmpinfo,DIB_RGB_COLORS,desk+screen.frame,NULL,NULL
        mov [dib],eax
;@@:
        invoke SelectObject,[memdc],[dib]
        push ebp
        call fool_entry
        pop ebp
        mov [exit],al
        invoke BitBlt,[hdc],0,0,[desk+screen.xl],[desk+screen.yl],[memdc],0,0,SRCCOPY
        invoke DeleteObject,[dib]
        invoke DeleteDC,[memdc]
        invoke EndPaint,[window],ps

if defined TRANSPARENCY
        invoke  SetLayeredWindowAttributes,[hwnd_main],0,[transparency],LWA_ALPHA
end if
        jmp .end

.destroy:
        mov [exit],0
        cmp [exit+1],1
        je .end
        mov [exit+1],1
        invoke ShowCursor,TRUE
        invoke MessageBox,[window],exit.msg,exit.title,MB_YESNO+MB_SYSTEMMODAL+MB_ICONQUESTION
        cmp eax,IDYES
        je .quit
        mov [exit+1],0
.stay:
        invoke ShowCursor,FALSE
        jmp .end
.close:
.quit:
        invoke PostQuitMessage,0
.end:
.false:
        xor eax,eax
        ret
.true:
        mov eax,1
        ret
endp

struc TRACKMOUSEEVENT {
.cbSize dd 16
.dwFlags rd 1
.hwndTrack rd 1
.dwHoverTime rd 1
}


section '.rsrc' resource data readable
RESSOURCESECTION:
directory       RT_ICON,icons,RT_GROUP_ICON,group_icons
resource        icons,1,LANG_NEUTRAL,icon_data
resource        group_icons,ICON,LANG_NEUTRAL,main_icon
icon            main_icon,icon_data,MAINICON

section '.idata' import data readable writable
IMPORTSECTION:

library kernel32,'KERNEL32.DLL',\
        user32,'USER32.DLL',\
        gdi32,'GDI32.DLL',\
        shell32,'SHELL32.DLL',\
        winmm,'WINMM.DLL',\
        vlc32,'LIBVLC.dll'


include 'api\kernel32.inc'
include 'api\user32.inc'
include 'api\gdi32.inc'
include 'api\vlc32.inc'
include 'api\shell32.inc'

import winmm,\
         PlaySoundW,'PlaySoundW',\
         PlaySoundA,'PlaySoundA'
  SND_FILENAME=0x20000
  SND_LOOP=0x8
  SND_ASYNC=0x1


section '.data' data readable writeable
DATASECTION:
include 'irq0.inc'
include 'irq1.inc'
include 'vsync.inc'
include 'serial.inc'

classname       TCHAR 'FOOLG32',0
message         TCHAR 'Fool graphic',0
msgerror        TCHAR 'startup error',0
image           db 'logo.bmp',0

wc              WNDCLASS 0,mainproc,0,0,NULL,NULL,NULL,NULL,NULL,classname
wmsg            MSG
ps              PAINTSTRUCT
mousetracker    TRACKMOUSEEVENT
datetime        SYSTEMTIME

bmpinfo         BITMAPINFOHEADER sizeof.BITMAPINFOHEADER,\
                0,0,1,32,0,0,0,0,0,0
pixels          rd 1
hbmp            rd 1
hdc             rd 1
memdc           rd 1
dib             rd 1
color3dface     rd 1
colorindexes    dd COLOR_3DFACE
colorvalues     dd Blue

exit            db 0,0,0,0
.title          db EXITTITLE,0
.msg            db EXITMESSAGE,0

hwnd_main dd ?
if defined TRANSPARENCY
   transparency dd TRANSPARENCY
else
   transparency dd 255
end if

window:         rd 1
.rect           RECT
.x dd ?
.y dd ?
.grab:
        push eax ebx ecx edx
        invoke GetWindowRect,dword[window],.rect
        mov ecx,[.rect.right]
        mov edx,[.rect.bottom]
        mov eax,[.rect.left]
        mov ebx,[.rect.top]
        sub ecx,eax
        sub edx,ebx
        add eax,[mouse.dx]
        add ebx,[mouse.dy]
        invoke SetWindowPos,dword[window],NULL,eax,ebx,ecx,edx,SWP_NOZORDER
        mov eax,[mouse.dx]
        mov ebx,[mouse.dy]      ;(.)(.)
        sub [mouse.x],eax
        sub [mouse.y],ebx
        mov dword[mouse.dx],0
        mov dword[mouse.dy],0
        pop edx ecx ebx eax
        ret


.resolution:
if defined WINDOWED

        mov eax,WX
        mov [.x],eax
        mov eax,WY
        mov [.y],eax
        mov eax,WXL
        mov [desk+screen.xl],eax
        mov [bmpinfo.biWidth],eax
        mov eax,WYL
else

        invoke GetSystemMetrics,SM_XVIRTUALSCREEN
        mov [.x],eax
        invoke GetSystemMetrics,SM_YVIRTUALSCREEN
        mov [.y],eax
        invoke GetSystemMetrics,SM_CXVIRTUALSCREEN
        sub eax,[.x]
        mov [desk+screen.xl],eax
        mov [bmpinfo.biWidth],eax
        invoke GetSystemMetrics,SM_CYVIRTUALSCREEN
        sub eax,[.y]
end if

        mov [desk+screen.yl],eax
        neg eax
        mov [bmpinfo.biHeight],eax
        neg eax
        xor edx,edx
        imul eax,dword[desk+screen.xl]
        shl eax,2
        mov [desk+screen.size],eax
        mov [bmpinfo.biSizeImage],eax
if defined BACKGROUND
        mov eax,BACKGROUND
else
        invoke GetSysColor,COLOR_3DFACE
        bswap eax
        shr eax,8
end if
        mov [desk+screen.color],eax
        invoke VirtualAlloc,0,[desk+screen.size],MEM_COMMIT,PAGE_READWRITE
        mov [desk+screen.buffer],eax
        ret


;loadimage:
;        invoke LoadImage,0,image,IMAGE_BITMAP,0,0,LR_LOADFROMFILE
;        mov [hbmp],eax
;        ret

