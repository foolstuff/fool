irq1:
        push eax ebx ecx
        movzx ecx,al
        and cl,7fh
        inc byte[key+ecx]
        test al,80h
        je @f
        mov byte[key+ecx],0
@@:
        mov ecx,key.buffers-1
@@:
        mov ah,[key+key.cur+ecx]
        mov [key+key.old+ecx],ah
        dec ecx
        jnl @b
        mov [key+key.cur],al
        call .push
       ; fcall [.callback]
; maybe one day, able to launche user action for each key event
        pop ecx ebx eax
        ret

.callback dd 0
.nocallback:
        xor eax,eax
.setcallback:
        mov [.callback],eax
        ret
.push:
        movzx ebx,byte[.last]
        mov [.fifo+ebx],al
        inc bl
        mov [.last],bl
        ret

.pop:
        movzx ebx,byte[.first]
        cmp bl,[.last]
        je @f
        mov al,[.fifo+ebx]
        inc bl
        mov [.first],bl
        clc
        ret
@@:
        stc
        ret

.fifo   db 256 dup 0
.first  db 0
.last   db 0

.flush:
.call=0
.input=4
        pushad
@@:
        call irq1.pop
        jc @f
        push esi
        mov esi,[esi+.input]
        call caller
        pop esi
        jmp @b
@@:
        popad
        ret

macro KeyFlush i {
        dd irq1.flush,i
}


key:
db 256 dup 0
.map=0
.buffers=50
db .buffers dup 0
.cur=256
.old=257
.tmp=.cur+.buffers
.status=.tmp+1
db 0,0
.scrolled=1
.numled=2
.shiftled=4
.majled=.shiftled
.insflag=8
.shift?=16
.ctrl?=32
.alt?=64
.altgr?=128

include 'scancodes.inc'
