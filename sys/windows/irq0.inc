TIMERMS=5
irq0:
        push eax edx
        invoke GetTickCount
        mov edx,[.old]
        or edx,edx
        jne @f
        mov edx,eax
@@:
        mov [.old],eax
        sub eax,edx
        xor edx,edx
        mov [.inc],eax
        add [.cnt],eax
        add [.ticks],eax
        adc [.ticks+4],edx
        adc [.ticks+8],edx
        adc [.ticks+12],edx
        rdtsc
        mov [.tsc],eax
        mov [.tsc+4],edx
        pop edx eax
        ret

.old    dd 0
.tsc    dd 0,0
.div    dd 0
.cnt    dd 0
.ticks  dd 0,0,0,0
.inc    dd 0
.rdcnt  dd 0
