onmouse:
;esi = item
;ebx = mouse point
        mov eax,[esi+item.x]
        sub eax,[ebx+item.x]
        jg @f
        add eax,[esi+item.xl]
        jl @f
        mov eax,[esi+item.y]
        sub eax,[ebx+item.y]
        jg @f
        add eax,[esi+item.yl]
        jl @f
        stc
        ret
@@:
        clc
        ret

mouse:
        Gnode 0,0,0,0,0,.update

.default:
        Asm next
        mov dword[.ptr+cptr.ptr],.basic
        mov dword[.popup+_ptr.ptr],0
;        mov dword[.esi],0
;        mov dword[.edi],0
;        mov dword[.esir],0
;        mov dword[.edir],0
;        mov dword[.esim],0
;        mov dword[.edim],0
        ret

.events:
        Asm next
        push eax ebx ecx edx esi edi
        mov esi,[.esir]
        mov edi,[.edir]
        fcall
;        mov [.esir],0
        mov esi,[.esim]
        mov edi,[.edim]
        fcall
;        mov [.esim],0
        mov esi,[.esi]
        mov edi,[.edi]
        fcall
        mov [.esi],0
        pop edi esi edx ecx ebx eax
        ret

.update:
        Asm next
        push eax ebx

        mov eax,[mouse+gnode.x]
        mov ebx,[.x]
        mov [.x],eax
        sub eax,ebx
        je @f
        mov [mouse.frozen],0
@@:
        mov [.dx],eax

        mov eax,[mouse+gnode.y]
        mov ebx,[.y]
        mov [.y],eax
        sub eax,ebx
        je @f
        mov [mouse.frozen],0
@@:
        mov [.dy],eax

        mov al,[.r]
        mov ah,[.r+1]
        mov [.r+1],al
        sub al,ah
        mov [.dr],al

        mov al,[.m]
        mov ah,[.m+1]
        mov [.m+1],al
        sub al,ah
        mov [.dm],al

        mov al,[.l]
        mov ah,[.l+1]
        mov [.l+1],al
        sub al,ah
        mov [.dl],al

        pop ebx eax
        ret
align 4
.render:
        dd gnode
.x:     dd 0
.y:     dd 0
        dd 0,0,@f-$-4
        dd .default,.events,.popup,.ptr,mousefreezetimer
@@:
.z:     dd 0
.dx:    dd 0
.dy:    dd 0
.dz:    dd 0

.r:     db 0,0
.dr:    db 0,0

.l:     db 0,0
.dl:    db 0,0

.m:     db 0,0
.dm:    db 0,0

.esi    dd 0
.edi    dd 0
.esir   dd 0
.edir   dd 0
.esim   dd 0
.edim   dd 0

.frozen dd 0

.ptr:   Ptr .basic

.popup: Ptr 0

.basic: Node .pointer,mousedebug

.pointer:
        Node pointer
.hand:
        Node opened_hand
.grab:
        Node closed_hand
.finger:
        Node finger_up
.clic:
        Node finger_down

.sines: dd 0;Node .ball,.s1,.ball,.s2,.ball,.s3,.ball,.s4
.ball:  Circle 0,0,40,40,White
.s1:    dd sincircle,.ball,40,0,1,180
.s2:    dd sincircle,.ball,40,45,2,180
.s3:    dd sincircle,.ball,40,90,3,180
.s4:    dd sincircle,.ball,40,135,5,180

sincircle:
.call=0
.child=4
.d=8
.t=12
.w=16
.pi=20
        mov ecx,[esi+.child]
        finit                   ; EMPTY
        fldpi                   ; pi
        fimul dword[esi+.t]     ; pi*angle
        fidiv dword[esi+.pi]    ; /180
        fsin                    ; sin
        fimul dword[esi+.d]     ; sin*xl
        fiadd dword[esi+.d]
        fist dword[ecx+item.xl]
        fist dword[ecx+item.yl]
        mov eax,[ecx+item.xl]
        mov ebx,[ecx+item.yl]
        sar eax,1
        sar ebx,1
        neg eax
        neg ebx
        mov [ecx+item.x],eax
        mov [ecx+item.y],ebx
        mov eax,[esi+.w]
        add dword[esi+.t],eax
        ret

mousedebug:
        Gnode 15,0,0,0,.border,.xyz,.clics,pixel.notalpha,.box,pixel.setalpha
.border:Border 0,0,102,45,0
.box:   Box 0,0,102,45,Maroon

.clics: Node .ltxt,.rtxt,.mtxt
.xyz:   Node .xtxt,.ytxt,.ztxt,.xnum,.ynum,.znum

.ltxt:  Htxt 2,2,0,0,White,@f,mouse.l,.lstr,6
@@:     db 'l='
.lstr:  db 'llllll'
@@:     db 0

.rtxt:  Htxt 2,17,0,0,White,@f,mouse.r,.rstr,6
@@:     db 'r='
.rstr:  db 'rrrrrr'
@@:     db 0

.mtxt:  Htxt 2,32,0,0,White,@f,mouse.m,.mstr,6
@@:     db 'm='
.mstr:  db 'mmmmmm'
@@:     db 0

.xtxt:  Txt 52,2,0,0,White,@f,font85
@@:     db 'x='
.xstr:  db 'xxxxxx'
@@:     db 0
.xnum:  Fxdec mouse+box.x,.xstr,@b

.ytxt:  Txt 52,17,0,0,White,@f,font85
@@:     db 'y='
.ystr:  db 'yyyyyy'
@@:     db 0
.ynum:  Fxdec mouse+box.y,.ystr,@b

.ztxt:  Txt 52,32,0,0,White,@f,font85
@@:     db 'z='
.zstr:  db 'zzzzzz'
@@:     db 0
.znum:  Fxdec mouse.z,.zstr,@b


mousefreezetimer:
        Gnode 0,-10,125,5,.func,.compute
.func:  Ptr .bar;mouse.sines
.bar:   Node .frame,.box
.frame: Frame 0,0,125,5,Black
.box:   Box 0,0,125,5,Red
.compute:
        Asm next
        mov eax,[irq0.inc]
        add [mouse.frozen],eax
;        inc [mouse.frozen]
        mov eax,16000
        sub eax,[mouse.frozen]
        sar eax,7
        mov dword[.func+cptr.ptr],.bar
        jg @f
        mov dword[.func+cptr.ptr],mouse.sines
        mov dword[mouse.ptr+cptr.ptr],0
        mov eax,1
@@:
        mov [.box+box.xl],eax
        ret




include '../bmp/pointer.inc'
include '../bmp/hand.inc'