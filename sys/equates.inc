linear_sel equ linear_sel_1-gdt
sys_code equ sys_code_1-gdt
sys_data equ sys_data_1-gdt
sys_vesa equ sys_vesa_1-gdt
sys_vbuff equ sys_vbuff_1-gdt
Real_code equ Real_code_1-gdt
Real_data equ Real_data_1-gdt
NULL = 80000000h
CGA=10fh    ; 320*200*8*8*8
VGA=112h    ; 640*480*8*8*8

SVGA=115h   ; 800*600*8*8*8
XGA=118h    ; 1024*768*8*8*8
SXGA=11bh   ; 1280*1024*8*8*8
UXGA=11fh   ; 1660*1200
FullHD=11ch ; 1920*1080*32

VESA320=CGA
VESA640=VGA
VESA800=SVGA
VESA1024=XGA
VESA1280=SXGA
VESA1660=UXGA

;CGA     10F     320x200*24
;VGA     112     640x480*24
;SVGA    113     800x600*24
;XGA     118     1024x768*24
;SXGA    11B     1280x1024*24
;FullHD  11C     1920x1080*24
;WUXGA   11D     1920x1200*24
;WXGA    12D     1366x768*24
;WXGA    12E     1360x768*24
;WXGA+   153     1440x900*24
;WXGA    161     1280x800*24
;QXGA    1A0     2048x1536*24
;WQXGA   1C0     2560x1600*24
;UHD     1F5     3840x2160*24

if ~definite VESAMODE
;else
VESAMODE=VGA
end if
CR equ 13
LF equ 10
cr equ CR
lf equ LF
CRLF equ CR,LF
crlf equ CRLF
next equ $+4
NEXT equ next
