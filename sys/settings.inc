match =MAINICON,MAINICON {
   MAINICON equ 'fool.ico'
}

match =EXITTITLE,EXITTITLE {
   EXITTITLE equ 'foolg'
}

match =EXITMESSAGE,EXITMESSAGE {
   EXITMESSAGE equ 'Quit foolg?'
}

macro _SCREEN {
virtual at 0
        dd WINDOW
        load WX dword from 0
        load WY dword from 4
        load WXL dword from 8
        load WYL dword from 12
        WINDOWED = 1
end virtual
}

match =VESAMODE,VESAMODE {


}

match =WINDOW,WINDOW {
_FULLSCREEN = 1
}

if defined _FULLSCREEN
;WX=0
;WY=0
;WXL=1600
;WYL=900
else
   _SCREEN
end if

