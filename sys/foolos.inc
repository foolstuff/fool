include 'os/os.inc'
include 'equates.inc'
include 'macros.inc'
BINARYSTART:
include 'os/lib/fool.inc'
debugword $
include '../bmp/fool_logo.inc'
include '../bmp/fasmicon.inc'

desk:   Screen 0,0,0,0,0,0,system
.pixels dd 0
.total  dd 0
.frames dd 0
transparency db ?


debugword $

include 'os/fonts/ugly.inc'
include 'os/fonts/font85.inc'
include 'os/vesa/vsync.inc'
debugword $
palettes:
        dd palette.flashy,.usr
.usr:   rd 256

debugword $
include '../codepages/khdle.inc'
debugword $
include 'colors.inc'

fool_entry:
        xor edi,edi
        mov esi,desk
        call screen.init
        fcall palettes
@@:
        xor edi,edi
        mov dword[desk.pixels],0
        fcall .main
        inc dword[desk.frames]
        or eax,eax
        je @b
        ret

.main:
        Node desk,next
        Init next
        Node palettes

if defined NOMOUSE
system: Node \
        .exit?,\
        application,\
        .exitkey
else
system: Node \
        .exit?,\
        mouse,\
        application,\
        .exitkey
end if

.exit:  dd 0
.exit?: Asm next
        mov eax,[.exit]
        mov dword[.exit],0
        ret
.exit!: Asm next
        mov dword[.exit],1
        ret

.exitkey:
        Tmk key.echap,system.exit!

include 'mouse.inc'
if defined SHOWFPS
include 'showfps.inc'
end if

application:
        dd node,@f-$-4
if defined SHOWFPS
        dd showfps
end if
        dd @f
@@:
