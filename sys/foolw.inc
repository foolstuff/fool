include 'windows/windows.inc'

include 'equates.inc'
include 'macros.inc'

include '../lib/fool.inc'

include '../bmp/fool_logo.inc'
include '../bmp/fasmicon.inc'
include '../fnt/font85.inc'
include '../fnt/ugly.inc'
include '../codepages/khdle.inc'
include 'colors.inc'

codepage dd azerty
Msgthanks:
        db 'good bye',0
Msgthankstitle:
        db 'fool graphix',0

deltax dd 0
deltay dd 0

edfedcolor=165+223*256+248*65536

section '.fool' data readable writeable

fool_entry:
        push esi edi
        xor edi,edi
        mov eax,[caller.tcnt]
        mov [caller.cnt],eax
        xor eax,eax
        mov [caller.tcnt],eax
        fcall .main
        mov dword[mouse.dx],0
        mov dword[mouse.dy],0
        mov dword[mouse.dz],0
        inc dword[desk.frames]
        push dword 0 dword[desk.pixels]
        pop dword[desk.total] dword[desk.pixels]
        pop edi esi
        ret

.main:
        Node desk,next
        Init next
        Node palettes

desk:   Screen 0,0,800,600,0,4,system;edfedcolor,4,system

.pixels dd 0
.total  dd 0
.frames dd 0

palettes:
        dd palette.flashy,.usr
.usr    rd 256


grabzone:
if defined NOGRAB
        dd 0
else
 if defined WINDOWED
        Gnode 0,-TITLEBAR_YL,WXL,WYL,next
 else
        Gnode 0,0,400,30,pixel.notalpha,.box,pixel.setalpha,next
 end if
        Asm next
        mov dword[.box],0
        invoke GetCursorPos,mouse+gnode.x
        invoke ScreenToClient,[window],mouse+gnode.x
        push esi
        mov esi,edi
        mov ebx,mouse
        call onmouse
        pop esi
        jnc @f
        mov dword[.box],border
        mov dword[mouse.ptr+cptr.ptr],mouse.hand
        cmp byte[mouse.dl],1
        jne .1
        mov [.grab],edi
.1:
        cmp byte[mouse.l],1
        jne @f
        cmp [.grab],edi
        jne @f
        mov dword[mouse.ptr+cptr.ptr],mouse.grab
        call window.grab
@@:
        cmp byte[mouse.dl],-1
        jne @f
        mov dword[.grab],0
@@:
        ret
.x      dd 0
.y      dd 0
.grab:  dd 0
end if

;if ~defined WINDOWED
.box:   Border 0,0,400,30,Red
;end if
if defined NOMOUSE
system: Node \
        .exit?,\
        application,\
        mouse
else
system: Node \
        .exit?,\
        mouse.render,\
        application,\
        mouse
end if

.exit:  dd 0
.exit?: Asm next
        mov eax,[.exit]
        mov dword[.exit],0
        ret
.exit!: Asm next
        mov dword[.exit],1
        ret

include 'mouse.inc'
if defined SHOWFPS
include 'showfps.inc'
end if

application:
if defined SHOWFPS
        Node showfps,@f,grabzone
else
        Node @f,grabzone
end if
@@: