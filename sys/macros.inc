X=0
Y=0
XL=30
YL=30
TITLEBAR_YL=15
N=1


macro TEXT x,y,c,z {
local .s
        Vtxt x,y,0,0,c,.s,z,ugly
.s:
}

macro TITRE titre {
local .t
        TEXT 15,0,Yellow,1
.t:     db titre,0
        dd .t
}

macro RUBRIQUE x,y,xl,yl,titre,contenu {
.YL=TITLEBAR_YL
local .g,.b
.g:     Gnode x,y,xl,yl+.YL,titre,.b,next
        Gnode 0,.YL,xl,yl,contenu
.b:     Box 0,0,xl,.YL,024487ch;0ebebebh;Teal
.win:   Win .g,win.SHOW
}

macro APPLICATION x,y,xl,yl,title {
local b,c,t
        Node b,main.win;,.set
b:     Frame 0,0,WXL,WYL,Black
t:     TITRE TITLE;title
;.set:   Asm next
;        invoke SendMessage,[hwnd_main],WM_SETTEXT,0,[.set-4]
;        ret
WYL=WYL-TITLEBAR_YL
main:   RUBRIQUE 0,0,WXL,WYL,t,c
c:
}

macro TXT x,y,c,z {
local .s
        Txt x,y,0,0,c,.s,font85
.s:
}

macro tr {
X=0
Y=Y+YL
}

macro colspan l {
X=X+(XL*l)
}

macro te t {
local .t
      Gnode X,Y,XL,YL,.t
.t:   TEXT 10,10,Black,1
      db t,0
X=X+XL
}

macro TOOLTIP xl,yl,t {
local .f,.t,.b,.g,.fr
      Zone 0,0,xl,yl,next
      Node .g,.f
.f:   Frame 0,0,xl,yl,Blue
.g:   Gnode XL-5,YL-5,90,30,.fr,.t,.b
.t:   TEXT 10,10,Black,1
      db t,0
.b:   Box 0,0,90,30,White
.fr:  Frame 0,0,90,30,Green
}

macro td t,c,l {
local .b,.t,.f,.tn,.d,.s,.@s,.n,.z
      Gnode X+2,Y+2,XL,YL,.z,.f,.t,.tn,.d,.b
.z:   TOOLTIP XL,YL,l
.b:   Box 2,2,XL-4,YL-4,c
.f:   Frame 2,2,XL-4,YL-4,Black
.t:   TEXT 5,13,Black,1
      db t,0
.d:   Fxdec .n,.s,.@s
.tn:  TEXT 5,3,1,1
.s:   db "   "
.@s:  db 0
.n:   dd N
N=N+1
X=X+XL
}

macro LEGENDE t,c {
local .b,.f,.t
        Gnode X,Y,150,15,.t,.f,.b
.b:     Box 1,1,150,13,c
.f:     Frame 1,1,150,13,Black
.t:     TEXT 10,0,1,1
        db t,0
Y=Y+15
}


macro Htxt x,y,xl,yl,c,t,v,s,n {

        Node $+24,next
        dd num.hex,v,s,n
        Txt x,y,xl,yl,c,t,font85
}

macro Panel x,y,xl,yl,c1,c2,t,w {
        Gnode x,y,xl,yl,\
        .frm,\
        .titlebar,\
        w
.frm:   Frame 0,0,xl,yl,c2
.titlebar:
        Gnode 1,1,xl-2,20,\
        @f,.box
@@:     Vtxt 20,3,0,0,c2,t,1,ugly
.box:   Box 0,0,.xl-2,20,c1+101010h
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;macros to display infos collected on compile
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro debugword s {
;display the value of the dword "s"
d0='0'+s shr 28 and 0fh
d1='0'+s shr 24 and 0fh
d2='0'+s shr 20 and 0fh
d3='0'+s shr 16 and 0fh
d4='0'+s shr 12 and 0fh
d5='0'+s shr 8 and 0fh
d6='0'+s shr 4 and 0fh
d7='0'+s and 0fh
if d0>'9'
        d0=d0+7
end if

if d1>'9'
        d1=d1+7
end if

if d2>'9'
        d2=d2+7
end if

if d3>'9'
        d3=d3+7
end if

if d4>'9'
        d4=d4+7
end if

if d5>'9'
        d5=d5+7
end if

if d6>'9'
        d6=d6+7
end if

if d7>'9'
        d7=d7+7
end if

display d0,d1,d2,d3,d4,d5,d6,d7,'h '
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro appsize a {
;display infos about output memory repartition
debugword ($)
display 'bytes total',crlf
debugword (a)
display 'bytes for system',crlf
s=$-a
debugword s
display 'bytes for application',crlf;
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro blocksize a,b {
;used at the end of the block to tell the sizeof
display b,".size=";
s=$-a
debugword s
display crlf
}
