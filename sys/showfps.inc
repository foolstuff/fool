;WINDOWED = 1

;include "sys/fool.inc"

showfps:
        Node .border,FcallsPerLoop,PixelsPerSecond,FramePerSecond,pixel.notalpha,.box,pixel.setalpha,.asm
.box:   Box 0,0,220,100,Black
.border:Border 0,0,220,100,0
;.name:  db 'showfps',0
.asm:   Asm @f
.tmp:   dd 0
.cnt:   dd 0
.fps:   dd 0
.pps:   dd 0
@@:
        inc dword[.cnt]
        mov ecx,[irq0.inc]
        add [.tmp],ecx
        cmp dword[.tmp],500 ;update each 500ms
        jl .end

        mov ecx,[.tmp]
        mov eax,[.cnt]
        sub dword[.tmp],500
        mov dword[.cnt],0
        imul eax,1'000'00
        cdq
        idiv ecx
        mov [.fps],eax
        cdq
        mov ecx,100
        idiv ecx
        imul eax,[desk.pixels]
        mov [.pps],eax
.end:
        ret

FramePerSecond:
        Node .txt,.num
.txt:   Vtxt 0,0,0,0,White,.str,3
.num:   Fxdec showfps.fps,.str,@f
.str    db 'xxxx.xx'
@@:     db ' fps',0

PixelsPerSecond:
       Node .txt,.num,.fnum,.ppnum
.txt:  Txt 5,40,70,15,Cyan,.fstr
.fnum: Fxdec desk.frames,.fstr,.fstre
.num:  Fxdec desk.pixels,.str,@f
.fstr  db 'xxxxxxxxx'
.fstre db ' frames',CRLF
.str   db 'xxxxxxxxx'
@@:     db ' pixels',CRLF
.psstr  db 'xxxxxxxxxx'
@@:     db " per second",0
align 4
.ppnum:  Fxdec showfps.pps,.psstr,@b

FcallsPerLoop:
        Node .txt,.num
.txt:   Vtxt 0,65,0,0,Lime,.str,2
.num:   Fxdec caller.cnt,.str,@f
.str    db '0123456789'
@@:     db ' fcalls',0

