AliceBlue = 0xF0F8FF
AliceBlue.n db 'AliceBlue',0

AntiqueWhite = 0xFAEBD7
AntiqueWhite.n db 'AntiqueWhite',0

Aqua = 0x00FFFF
Aqua.n db 'Aqua',0

Aquamarine = 0x7FFFD4
Aquamarine.n db 'Aquamarine',0

Azure = 0xF0FFFF
Azure.n db 'Azure',0

Beige = 0xF5F5DC
Beige.n db 'Beige',0

Bisque = 0xFFE4C4
Bisque.n db 'Bisque',0

Black = 0x000000
Black.n db 'Black',0

BlanchedAlmond = 0xFFEBCD
BlanchedAlmond.n db 'BlanchedAlmond',0

Blue = 0x0000FF
Blue.n db 'Blue',0

BlueViolet = 0x8A2BE2
BlueViolet.n db 'BlueViolet',0

Brown = 0xA52A2A
Brown.n db 'Brown',0

BurlyWood = 0xDEB887
BurlyWood.n db 'BurlyWood',0

CadetBlue = 0x5F9EA0
CadetBlue.n db 'CadetBlue',0

Chartreuse = 0x7FFF00
Chartreuse.n db 'Chartreuse',0

Chocolate = 0xD2691E
Chocolate.n db 'Chocolate',0

Coral = 0xFF7F50
Coral.n db 'Coral',0

CornflowerBlue = 0x6495ED
CornflowerBlue.n db 'CornflowerBlue',0

Cornsilk = 0xFFF8DC
Cornsilk.n db 'Cornsilk',0

CosmicLatte = 0xFFF8E7
CosmicLatte.n db 'CosmicLatte',0

Crimson = 0xDC143C
Crimson.n db 'Crimson',0

Cyan = 0x00FFFF
Cyan.n db 'Cyan',0

DarkBlue = 0x00008B
DarkBlue.n db 'DarkBlue',0

DarkCyan = 0x008B8B
DarkCyan.n db 'DarkCyan',0

DarkGoldenRod = 0xB8860B
DarkGoldenRod.n db 'DarkGoldenRod',0

DarkGray = 0xA9A9A9
DarkGray.n db 'DarkGray',0

DarkGrey = 0xA9A9A9
DarkGrey.n db 'DarkGrey',0

DarkGreen = 0x006400
DarkGreen.n db 'DarkGreen',0

DarkKhaki = 0xBDB76B
DarkKhaki.n db 'DarkKhaki',0

DarkMagenta = 0x8B008B
DarkMagenta.n db 'DarkMagenta',0

DarkOliveGreen = 0x556B2F
DarkOliveGreen.n db 'DarkOliveGreen',0

DarkOrange = 0xFF8C00
DarkOrange.n db 'DarkOrange',0

DarkOrchid = 0x9932CC
DarkOrchid.n db 'DarkOrchid',0

DarkRed = 0x8B0000
DarkRed.n db 'DarkRed',0

DarkSalmon = 0xE9967A
DarkSalmon.n db 'DarkSalmon',0

DarkSeaGreen = 0x8FBC8F
DarkSeaGreen.n db 'DarkSeaGreen',0

DarkSlateBlue = 0x483D8B
DarkSlateBlue.n db 'DarkSlateBlue',0

DarkSlateGray = 0x2F4F4F
DarkSlateGray.n db 'DarkSlateGray',0

DarkSlateGrey = 0x2F4F4F
DarkSlateGrey.n db 'DarkSlateGrey',0

DarkTurquoise = 0x00CED1
DarkTurquoise.n db 'DarkTurquoise',0

DarkViolet = 0x9400D3
DarkViolet.n db 'DarkViolet',0

DeepPink = 0xFF1493
DeepPink.n db 'DeepPink',0

DeepSkyBlue = 0x00BFFF
DeepSkyBlue.n db 'DeepSkyBlue',0

DimGray = 0x696969
DimGray.n db 'DimGray',0

DimGrey = 0x696969
DimGrey.n db 'DimGrey',0

DodgerBlue = 0x1E90FF
DodgerBlue.n db 'DodgerBlue',0

FireBrick = 0xB22222
FireBrick.n db 'FireBrick',0

FloralWhite = 0xFFFAF0
FloralWhite.n db 'FloralWhite',0

ForestGreen = 0x228B22
ForestGreen.n db 'ForestGreen',0

Fuchsia = 0xFF00FF
Fuchsia.n db 'Fuchsia',0

Gainsboro = 0xDCDCDC
Gainsboro.n db 'Gainsboro',0

GhostWhite = 0xF8F8FF
GhostWhite.n db 'GhostWhite',0

Gold = 0xFFD700
Gold.n db 'Gold',0

GoldenRod = 0xDAA520
GoldenRod.n db 'GoldenRod',0

Gray = 0x808080
Gray.n db 'Gray',0

Grey = 0x808080
Grey.n db 'Grey',0

Green = 0x008000
Green.n db 'Green',0

GreenYellow = 0xADFF2F
GreenYellow.n db 'GreenYellow',0

HoneyDew = 0xF0FFF0
HoneyDew.n db 'HoneyDew',0

HotPink = 0xFF69B4
HotPink.n db 'HotPink',0

IndianRed = 0xCD5C5C
IndianRed.n db 'IndianRed',0

Indigo = 0x4B0082
Indigo.n db 'Indigo',0

Ivory = 0xFFFFF0
Ivory.n db 'Ivory',0

Khaki = 0xF0E68C
Khaki.n db 'Khaki',0

Lavender = 0xE6E6FA
Lavender.n db 'Lavender',0

LavenderBlush = 0xFFF0F5
LavenderBlush.n db 'LavenderBlush',0

LawnGreen = 0x7CFC00
LawnGreen.n db 'LawnGreen',0

LemonChiffon = 0xFFFACD
LemonChiffon.n db 'LemonChiffon',0

LightBlue = 0xADD8E6
LightBlue.n db 'LightBlue',0

LightCoral = 0xF08080
LightCoral.n db 'LightCoral',0

LightCyan = 0xE0FFFF
LightCyan.n db 'LightCyan',0

LightGoldenRodYellow = 0xFAFAD2
LightGoldenRodYellow.n db 'LightGoldenRodYellow',0

LightGray = 0xD3D3D3
LightGray.n db 'LightGray',0

LightGrey = 0xD3D3D3
LightGrey.n db 'LightGrey',0

LightGreen = 0x90EE90
LightGreen.n db 'LightGreen',0

LightPink = 0xFFB6C1
LightPink.n db 'LightPink',0

LightSalmon = 0xFFA07A
LightSalmon.n db 'LightSalmon',0

LightSeaGreen = 0x20B2AA
LightSeaGreen.n db 'LightSeaGreen',0

LightSkyBlue = 0x87CEFA
LightSkyBlue.n db 'LightSkyBlue',0

LightSlateGray = 0x778899
LightSlateGray.n db 'LightSlateGray',0

LightSlateGrey = 0x778899
LightSlateGrey.n db 'LightSlateGrey',0

LightSteelBlue = 0xB0C4DE
LightSteelBlue.n db 'LightSteelBlue',0

LightYellow = 0xFFFFE0
LightYellow.n db 'LightYellow',0

Lime = 0x00FF00
Lime.n db 'Lime',0

LimeGreen = 0x32CD32
LimeGreen.n db 'LimeGreen',0

Linen = 0xFAF0E6
Linen.n db 'Linen',0

Magenta = 0xFF00FF
Magenta.n db 'Magenta',0

Maroon = 0x800000
Maroon.n db 'Maroon',0

MediumAquaMarine = 0x66CDAA
MediumAquaMarine.n db 'MediumAquaMarine',0

MediumBlue = 0x0000CD
MediumBlue.n db 'MediumBlue',0

MediumOrchid = 0xBA55D3
MediumOrchid.n db 'MediumOrchid',0

MediumPurple = 0x9370DB
MediumPurple.n db 'MediumPurple',0

MediumSeaGreen = 0x3CB371
MediumSeaGreen.n db 'MediumSeaGreen',0

MediumSlateBlue = 0x7B68EE
MediumSlateBlue.n db 'MediumSlateBlue',0

MediumSpringGreen = 0x00FA9A
MediumSpringGreen.n db 'MediumSpringGreen',0

MediumTurquoise = 0x48D1CC
MediumTurquoise.n db 'MediumTurquoise',0

MediumVioletRed = 0xC71585
MediumVioletRed.n db 'MediumVioletRed',0

MidnightBlue = 0x191970
MidnightBlue.n db 'MidnightBlue',0

MintCream = 0xF5FFFA
MintCream.n db 'MintCream',0

MistyRose = 0xFFE4E1
MistyRose.n db 'MistyRose',0

Moccasin = 0xFFE4B5
Moccasin.n db 'Moccasin',0

NavajoWhite = 0xFFDEAD
NavajoWhite.n db 'NavajoWhite',0

Navy = 0x000080
Navy.n db 'Navy',0

OldLace = 0xFDF5E6
OldLace.n db 'OldLace',0

Olive = 0x808000
Olive.n db 'Olive',0

OliveDrab = 0x6B8E23
OliveDrab.n db 'OliveDrab',0

Orange = 0xFFA500
Orange.n db 'Orange',0

OrangeRed = 0xFF4500
OrangeRed.n db 'OrangeRed',0

Orchid = 0xDA70D6
Orchid.n db 'Orchid',0

PaleGoldenRod = 0xEEE8AA
PaleGoldenRod.n db 'PaleGoldenRod',0

PaleGreen = 0x98FB98
PaleGreen.n db 'PaleGreen',0

PaleTurquoise = 0xAFEEEE
PaleTurquoise.n db 'PaleTurquoise',0

PaleVioletRed = 0xDB7093
PaleVioletRed.n db 'PaleVioletRed',0

PapayaWhip = 0xFFEFD5
PapayaWhip.n db 'PapayaWhip',0

PeachPuff = 0xFFDAB9
PeachPuff.n db 'PeachPuff',0

Peru = 0xCD853F
Peru.n db 'Peru',0

Pink = 0xFFC0CB
Pink.n db 'Pink',0

Plum = 0xDDA0DD
Plum.n db 'Plum',0

PowderBlue = 0xB0E0E6
PowderBlue.n db 'PowderBlue',0

Purple = 0x800080
Purple.n db 'Purple',0

RandomColor = 0xffffffff
RandomColor.n db 'RandomColor',0

RebeccaPurple = 0x663399
RebeccaPurple.n db 'RebeccaPurple',0

Red = 0xFF0000
Red.n db 'Red',0

RosyBrown = 0xBC8F8F
RosyBrown.n db 'RosyBrown',0

RoyalBlue = 0x4169E1
RoyalBlue.n db 'RoyalBlue',0

SaddleBrown = 0x8B4513
SaddleBrown.n db 'SaddleBrown',0

Salmon = 0xFA8072
Salmon.n db 'Salmon',0

SandyBrown = 0xF4A460
SandyBrown.n db 'SandyBrown',0

SeaGreen = 0x2E8B57
SeaGreen.n db 'SeaGreen',0

SeaShell = 0xFFF5EE
SeaShell.n db 'SeaShell',0

Sienna = 0xA0522D
Sienna.n db 'Sienna',0

Silver = 0xC0C0C0
Silver.n db 'Silver',0

SkyBlue = 0x87CEEB
SkyBlue.n db 'SkyBlue',0

SlateBlue = 0x6A5ACD
SlateBlue.n db 'SlateBlue',0

SlateGray = 0x708090
SlateGray.n db 'SlateGray',0

SlateGrey = 0x708090
SlateGrey.n db 'SlateGrey',0

Snow = 0xFFFAFA
Snow.n db 'Snow',0

SpringGreen = 0x00FF7F
SpringGreen.n db 'SpringGreen',0

SteelBlue = 0x4682B4
SteelBlue.n db 'SteelBlue',0

Tan = 0xD2B48C
Tan.n db 'Tan',0

Teal = 0x008080
Teal.n db 'Teal',0

Thistle = 0xD8BFD8
Thistle.n db 'Thistle',0

Tomato = 0xFF6347
Tomato.n db 'Tomato',0

Turquoise = 0x40E0D0
Turquoise.n db 'Turquoise',0

Violet = 0xEE82EE
Violet.n db 'Violet',0

Wheat = 0xF5DEB3
Wheat.n db 'Wheat',0

White = 0xFFFFFF
White.n db 'White',0

WhiteSmoke = 0xF5F5F5
WhiteSmoke.n db 'WhiteSmoke',0

Yellow = 0xFFFF00
Yellow.n db 'Yellow',0

YellowGreen = 0x9ACD32
YellowGreen.n db 'YellowGreen',0



