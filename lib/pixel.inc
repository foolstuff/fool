pixel:
;eax=x
;ebx=y
;ecx=color
        ;push eax ebx ecx edx
        ;call .gamma
        ;call dword[.transform]
        call dword[.pix]
        inc dword[desk.pixels]
        ;pop edx ecx ebx eax
        ret
align 4
.count: dd 0
.total: dd 0

.gamma:
;ecx=color
        push eax ebx
        mov eax,ecx
        shr eax,16
        and eax,0ffh
        mov bl,[.red+eax]
        shl ebx,8
        movzx eax,ch
        mov bl,[.green+eax]
        shl ebx,8
        movzx eax,cl
        mov bl,[.blue+eax]
        mov ecx,ebx
        pop ebx eax
        ret
.red:
times 256 db %-1
.green:
times 256 db %-1
.blue:
times 256 db %-1



.mean:
;ecx=color1
;edx=color2
        mov eax,ecx
        mov ebx,edx
        and eax,00f0f0f0h
        and ebx,00f0f0f0h
        and ecx,000f0f0fh
        and edx,000f0f0fh
        call dword[.rate]
        and ecx,000f0f0fh
        add eax,ecx
        ret
.rate:  dd .50%
.50%:
        lea eax,[ebx+eax]
        lea ecx,[edx+ecx]
        shr eax,1;2
        shr ecx,1;2
        ret
.75%:
        lea eax,[eax*3]
        lea ecx,[ecx*3]
        lea eax,[ebx+eax]
        lea ecx,[edx+ecx]
        shr eax,2
        shr ecx,2
        ret
.25%:
        lea ebx,[ebx*3]
        lea edx,[edx*3]
        lea eax,[ebx+eax]
        lea ecx,[edx+ecx]
        shr eax,2
        shr ecx,2
        ret

.alpha:
        push eax ebx ecx edx edi
        cmp eax,[desk+screen.xl]
        jae @f
        cmp ebx,[desk+screen.yl]
        jae @f
        imul ebx,[desk+screen.xl]
        lea ebx,[ebx+eax]
        mov edi,[desk+screen.buffer]
        lea edi,[edi+ebx*4]
        mov edx,[edi]
        call .mean
        mov [edi],eax

@@:
        pop edi edx ecx ebx eax
        ret

.direct:
        push ebx edi
        cmp eax,[desk+screen.xl]
        jae @f
        cmp ebx,[desk+screen.yl]
        jae @f
        imul ebx,[desk+screen.xl]
        lea ebx,[ebx+eax]
        mov edi,[desk+screen.buffer]
        mov [edi+ebx*4],ecx
@@:
        pop edi ebx
        ret

.setalpha:
        dd @f
@@:
        mov dword [.pix],.alpha
        ret

.notalpha:
        dd @f
@@:
        mov dword [.pix],.direct
        ret

.pix:   dd .direct
.transform:
        dd .iso

.iso:
        shl eax,1
        sub eax,ebx
        shr eax,1
        ret
.normal:
        ret

macro Pixel {
      call pixel
}

getpixel:
;ebx=prepared coordinate
;ecx=color
        cmp ebx,[desk+screen.size]
        jae @f          ;thx fodder for this trick!
        mov edi,[desk+screen.buffer]
        mov ecx,[edi+ebx*4]
@@:
        ret
        align 4

macro GetPixel {
      call getpixel ;dword [desk+screen.getpixel]
}


