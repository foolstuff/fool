rnd:
.call=0
.rnd=4
.seed=8
	push eax edx
	mov edx,[esi+.seed]
	call crypt.rnd
	mov [esi+.rnd],eax
	pop edx eax
	ret

rndinit:
.call=0
.key=4
.seed=8
	push eax ebx ecx edx edi
	mov edi,[esi+.key]
	mov edx,[esi+.seed]
	call crypt.init
	mov dword[esi+.call],rnd
	fcall
	pop edi edx ecx ebx eax
	ret


macro Rnd k,s {
	dd rndinit,k,s
}