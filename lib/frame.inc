frame:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.screenadjust:
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        mov ecx,[esi+.c]
        mov edx,[esi+.yl]
@@:
        dec edx
        jle @f
        Pixel
        inc ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
@@:
        dec edx
        jle @f
        Pixel
        inc eax
        jmp @b
@@:
        mov edx,[esi+.yl]
@@:
        dec edx
        jle @f
        Pixel
        dec ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
@@:
        dec edx
        jle @f
        Pixel
        dec eax
        jmp @b
@@:
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro Frame x,y,xl,yl,c {
        dd frame,x,y,xl,yl,c
}

