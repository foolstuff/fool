;transparent return
;things to do:
;fill with '0'
;make negative sign at the good place
;manipulate datas to have a good appearance
;need a presentation function to do the job then...
;maybe should separate decimal parsing
;and string presentation



;this algo should do this:
;convert any 32 bits number into a decimal number
;the representation of this number is made with a fixed decimal point into the string.
;meaning we put a dot in the string where we want
;the number 3141528 can become 3.141528 just by fixing a dot in the resulting string.
;it means that the number 0.00001 should be printable, and the number 0000.000 shoud become 0.000

;the exceding 0 should disappear.

fxdec:
.call=0
.var=4
.str=8
.@str=12
        push eax ebx ecx edx
        mov ecx,10
        mov eax,[esi+.var]
        mov eax,[eax]
        cmp eax,0
        jge @f
        neg eax
@@:
        mov ebx,[esi+.@str]
.loop:
        dec ebx
        cmp byte[ebx],'.'
        je .notadigit
;        cmp byte[ebx],' '
;        je .notadigit
        jmp @f
.notadigit:
        dec ebx
@@:
        cmp ebx,[esi+.str]
        jl .end
        xor edx,edx
        div ecx
        or edx,edx
        jne @f
        or eax,eax
        jne @f
        mov dl,' '-'0'
@@:
        add dl,'0'
        mov [ebx],dl
        jmp .loop
.end:
        mov eax,[esi+.var]
        cmp dword[eax],0
        jge @f
        mov byte[ebx+1],'-'
@@:
        pop edx ecx ebx eax
        ret

macro Fxdec v,s,e {
        dd fxdec,v,s,e
}
