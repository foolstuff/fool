box:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
	push eax ebx ecx edx edi
	push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
	or edi,edi
	je @f
	call addxy
@@:
	mov eax,[esi+.x]
	mov ebx,[esi+.y]
	mov ecx,[esi+.c]
	mov edi,[esi+.xl]
	mov edx,[esi+.yl]
@@:
	Pixel
	inc eax
	dec edi
	jne @b
	mov edi,[esi+.xl]
	mov eax,[esi+.x]
	inc ebx
	dec edx
	jne @b
.end:
	pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
	pop edi edx ecx ebx eax
	ret

macro Box x,y,xl,yl,c {
      dd box,x,y,xl,yl,c
}

