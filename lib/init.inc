init:
.call=0
.func=4
        cmp dword[esi+.func],0
        je @f
        fcall dword[esi+.func]
@@:
        mov dword[esi+.call],0
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro Init f {
        dd init,f
}
