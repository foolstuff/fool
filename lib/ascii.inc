ascii:
.call=0
.ptr=4
.txt=8
.xl=12
.yl=16
.off=20
        pushad
        mov ebx,[esi+.ptr]
        mov ebx,[ebx]
        mov eax,[esi+.off]
        add ebx,[eax]
        mov ecx,[esi+.xl]
        mov edx,[esi+.yl]
        mov edi,[esi+.txt]
.line:
        mov al,[ebx]
        inc ebx
        cmp al,10h
        jae @f
        movzx eax,al
        mov al,[eax+.lut]
@@:
        mov [edi],al
        inc edi
        dec ecx
        jne .line
        mov ecx,[esi+.xl]
        mov word[edi],0d0ah
        add edi,2
        dec edx
        jne .line
        mov byte[edi-2],0
        popad
        ret
.lut:
        db 20h,001,002,003,004,005,006,007
        db 008,20h,20h,011,012,20h,014,015
