;transparent return
time:
.call=0
.second=4
.minute=5
.hour=6
        push eax
        mov al,byte[datetime.wSecond]
        aam
        shl ah,4
        or al,ah
        mov [esi+.second],al
        mov al,byte[datetime.wMinute]
        aam
        shl ah,4
        or al,ah
        mov [esi+.minute],al
        mov al,byte[datetime.wHour]
        aam
        shl ah,4
        or al,ah
        mov [esi+.hour],al
        pop eax
        ret
