;fool library
;core
include 'caller.inc'
include 'node.inc'
include 'gnode.inc'
include 'pixel.inc'
include 'box.inc'
include 'asm.inc'
include 'num.inc'

;A
include 'ascii.inc'
include 'anim.inc'
;B
include 'bits.inc'
include 'bmp.inc'
include 'boxs.inc'
include 'border.inc'
include 'buffer.inc'
include 'but.inc'
include 'buts.inc'
;C
include 'caret.inc'
include 'circle.inc'
include 'const.inc'
include 'copyclr.inc'
;include 'cptr.inc'
include 'crypt.inc'
;D
include 'date.inc'
include 'digit.inc'
include 'do.inc'
;E
include 'fill.inc'
include 'for.inc'
include 'frame.inc'
include 'fxdec.inc'
;F
include 'ife.inc'
include 'init.inc'
include 'input.inc'
include 'item.inc'
;G
;include 'gptr.inc'
include 'grab.inc'
;H
include 'hit.inc'
include 'hexa.inc'
;I
include 'io.inc'
;J
;K
;L
include 'lim.inc'
include 'line.inc'
include 'linenums.inc'
include 'list.inc'
include 'logger.inc'
;include 'lptr.inc'
;M
include 'malloc.inc'
include 'minusplus.inc'
;N
;O
include 'ops.inc'
include 'otk.inc'
;P
include 'palette.inc'
include 'plotb.inc'
include 'plotc.inc'
include 'plotd.inc'
include 'plotsb.inc'
include 'plotw.inc'
include 'pong.inc'
include 'ptr.inc'
include 'put.inc'
;Q
;R
include 'read.inc'
include 'rnd.inc'
;S
include 'screen.inc'
include 'sound.inc'
;T
include 'tex.inc'
include 'time.inc'
include 'timer.inc'
include 'tmk.inc'
include 'txt.inc'
;U
;V
include 'vectors.inc'
;include 'vsync.inc'
include 'vtxt.inc'
;W
include 'while.inc'
include 'win.inc'
;X
;Y
;Z
include 'zone.inc'

include '../shell/shell.inc'
