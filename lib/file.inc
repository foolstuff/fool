macro ComFile name,help {
local f,n,s
        dd comfile
        dd f
        dd s-f
        dd name
        dd help

f:
.bin:
include n
s:
        ret

}

macro AsmFile n {
local f,s
        dd asmfile
        dd f
        dd s-f
        db n,0

f:
.bin:
include n
s:
        ret

}

macro BinFile n {
local f,s
        dd binfile
        dd f
        dd s-f
        db n,0

f:
.bin:
file n
s:
        ret

}
