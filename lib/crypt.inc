;transparent return
crypt:
.call=0
.file=4
.key=8
.seed=12
	push eax ebx ecx edx edi
	mov edi,[esi+.key]
	mov edx,[esi+.seed]
	call .init
	mov ebx,[esi+.file]
	mov ecx,[ebx+lptr.size]
	mov ebx,[ebx+lptr.off]
@@:
	call .rnd	;get a random dword
	xor ah,al	;make it a random byte
	ror eax,8	;
	xor ah,al	;
	ror eax,8	;
	xor ah,al	;
	xor [ebx],ah	;apply the cryptography to a byte in the file
	inc ebx 	;next byte in the file
	loop @b
	pop edi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.init:				  ;
;edi=[key]			  ;
;edx=[seed]			  ;
	push ecx		  ;
	mov ecx,[edx]		  ; key size
	shr ecx,2		  ;
	dec ecx 		  ;
	jl .end 		  ;
@@:				  ;
	mov [edx+4+ecx*4],ecx	  ;
	dec ecx 		  ;
	jnl @b			  ;
	mov ecx,[edi]		  ;
	add edi,4		  ;
	or ecx,ecx		  ;
	je .end 		  ;
.loop:				  ;
	movsx eax,byte[edi]	  ;
	movzx ebx,al		  ;
	rol ebx,cl		  ;
	adc eax,ecx		  ;
	xor eax,ebx		  ;
	push ecx		  ;
	mov ecx,[edx]		  ;
	shr ecx,2		  ;
	dec ecx 		  ;
	clc			  ;
@@:				  ;
	adc [edx+ecx*4],eax	  ;
	sbb [edx+ecx*4],ecx	  ;
	loop @b 		  ;
	pop ecx 		  ;
	inc edi 		  ;
	loop .loop		  ;
.end:				  ;
	pop ecx 		  ;
	ret			  ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.rnd:				  ;
;edx=seed			  ;
;return eax = rnd		  ;
	push ebx ecx		  ;
	xor eax,eax		  ; eax=0
	mov ecx,[edx]		  ; size of seed
	shr ecx,2		  ; in dwords
	dec ecx 		  ; zero based size
	jl .not 		  ; null seed? (size <=0)
	push ecx		  ; save size, cause we reuse the size later
	stc			  ; carry flag = 1
	xor ebx,ebx		  ; ebx=0
@@:				  ;loop on seed digest operation
	sbb eax,[edx+ecx*4]	  ;   first digest iteration on seed content in eax
	xor eax,ecx		  ;   size value influence the value of this digest
	lea ebx,[eax*4+ecx+127]   ;   second part of the digest in ebx
	loop @b 		  ; do it with all the seed
	xor eax,ecx		  ; invert bits in eax
	stc			  ; carry flag = 0
	pop ecx 		  ; restore seed size value
@@:				  ;loop on seed re construct operation
	adc [edx+ecx*4],eax	  ;   make new value in seed
	adc eax,[edx+ecx*4]	  ;   re make iteration on digest
	adc ebx,eax		  ;   make on second part of the digest
	xor eax,ebx		  ;   switch digest parts
	ror eax,cl		  ;   rotate first part
	loop @b 		  ; do it with all the seed
.not:				  ;
	pop ecx ebx		  ;
	ret			  ; random value ready in eax, edx unchanged
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
