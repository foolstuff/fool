;transparent return
but:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.bmp=24
.event=28
        push eax ebx ecx edx edi
        or edi,edi
        je @f
        call addxy
@@:
        mov ebx,mouse
        call onmouse
        jnc @f
        mov dword[mouse.ptr+cptr.ptr],mouse.finger
        cmp byte[mouse.dl],1
        jne .1
        mov eax,[esi+.event]
        mov [mouse.esi],eax
        mov [mouse.edi],esi
.1:
        cmp byte[mouse.l],1
        jne @f
        mov dword[mouse.ptr+cptr.ptr],mouse.clic
@@:
        push edi
        mov edi,esi
        fcall [esi+.bmp]
        pop edi
        or edi,edi
        je @f
        call subxy
@@:
        cmp dword[esi+.bmp],0
        jne @f
        call box
@@:
.exit:
        pop edi edx ecx ebx eax
        ret

macro But x,y,xl,yl,c,b,e {
        dd but,x,y,xl,yl,c,b,e
}
