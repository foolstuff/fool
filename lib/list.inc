list:
.call=0
.size=4
.data=8
        ret

; List ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro List [element] {
 common
  local .a
        dd list,.a-$-4
 forward
        dd element
 common
  .a:
}

macro RList N {
        dd 0,N*4
        rd N-1
        dd 0
}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db 'decsel'
decsel:
.call=0
.selector=4
.list=8
        push eax ebx
        mov ebx,[esi+.list]
        mov eax,[ebx+list.size]
        sub eax,4
        mov ebx,[esi+.selector]
        sub dword[ebx],4
        jge @f
        mov [ebx],eax
@@:
        pop ebx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db 'incsel'
incsel:
.call=0
.selector=4
.list=8
        push eax ebx
        mov ebx,[esi+.list]
        mov eax,[ebx+list.size]
        mov ebx,[esi+.selector]
        add dword[ebx],4
        cmp [ebx],eax
        jl @f
        mov dword[ebx],0
@@:
        pop ebx eax
        ret
