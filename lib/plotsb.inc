;transparent return
plotsb:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.buf=24
.xoff=28
.yoff=32
.xscl=36
.yscl=40
        push eax ebx ecx edx esi ebp
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        mov ecx,[esi+.xl]
        dec ecx
        jl .end
        mov ebp,[esi+.yl]
        shr ebp,1
        add ebp,[esi+.y]
        add ebp,[esi+.yoff]
        mov edx,[esi+.buf]
        or edx,edx
        je .end
        mov eax,[esi+.xoff]
        lea edx,[edx+eax]
        movsx eax,byte[edx]
        push edx
        cdq
        idiv dword[esi+.yscl]
        pop edx
        neg eax
        add eax,ebp
        mov dword[esi+.xl],1
        mov ebx,[esi+.xscl]
        lea edx,[edx+ebx*1]
.loop:
        push eax ecx edx
        movsx eax,byte[edx]
        mov ecx,[esi+.xscl]
        dec ecx
        jle .notmulti
@@:
        movsx ebx,byte[edx+ecx*1]
        add eax,ebx
        loop @b
.notmulti:
        cdq
        idiv dword[esi+.xscl]
        cdq
        idiv dword[esi+.yscl]
        mov ebx,eax
        pop edx ecx eax
        neg ebx
        add ebx,ebp
        mov [esi+.y],eax
        mov eax,ebx
        sub ebx,[esi+.y]
        mov [esi+.yl],ebx
        call line
        inc dword[esi+.x]
        mov ebx,[esi+.xscl]
        lea edx,[edx+ebx*1]
        loop .loop
.end:
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop ebp esi edx ecx ebx eax
        ret