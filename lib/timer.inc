;run the .func item every delay elapse.
;transparent return from previous to child, and from child to next

timer:
;delay in iterations
.call=0
.cnt=4
.delay=8
.func=12
        push ebx
        dec dword[esi+.cnt]
        jg @f
        mov ebx,[esi+.delay]
        mov [esi+.cnt],ebx
        fcall [esi+.func]
        call caller
@@:
        pop ebx
        ret

macro Timer .d,.f {
        dd timer,.d,.d,.f
}

chron:
;delay in milliseconds
.call=0
.cnt=4
.delay=8
.func=12
        push ebx
        cmp dword[esi+.cnt],0
        mov ebx,[irq0.cnt]
        jne @f
        add ebx,[esi+.delay]
        mov [esi+.cnt],ebx
@@:
        cmp [esi+.cnt],ebx
        jge @f
        mov ebx,[esi+.delay]
        add [esi+.cnt],ebx
        fcall [esi+.func]
@@:
        pop ebx
        ret

macro Chron .d,.f {
        dd chron,0,.d,.f
}

delay:
;delay in milliseconds
.call=0
.cnt=4
.delay=8
.func=12
        push ebx
;        mov ebx,[irq0.cnt]
;        add ebx,[esi+.delay]
;        mov [esi+.cnt],ebx
        xor ebx,ebx
@@:
        inc ebx
        cmp ebx,[esi+.delay];irq0.cnt]
        jl @b
        fcall [esi+.func]
        pop ebx
        ret

macro Delay .d,.f {
        dd delay,.d,.d,.f
}
