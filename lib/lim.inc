;transparent return
lim:
.call=0
.var=4
.min=8
.max=12
limb:
.call=0
.var=4
.min=8
.max=12
	push eax ebx ecx edx
	mov eax,[esi+.var]
	movzx ebx,byte[eax]
	movzx ecx,byte[esi+.min]
	movzx edx,byte[esi+.max]
	cmp ecx,edx
	jl @f
	xchg ecx,edx
@@:
	cmp ebx,ecx
	jge @f
	mov ebx,ecx
@@:
	cmp ebx,edx
	jle @f
	mov ebx,edx
@@:
	mov [eax],bl
	pop edx ecx ebx eax
	ret
limw:
.call=0
.var=4
.min=8
.max=12
	push eax ebx ecx edx
	mov eax,[esi+.var]
	movzx ebx,word[eax]
	movzx ecx,word[esi+.min]
	movzx edx,word[esi+.max]
	cmp ecx,edx
	jl @f
	xchg ecx,edx
@@:
	cmp ebx,ecx
	jge @f
	mov ebx,ecx
@@:
	cmp ebx,edx
	jle @f
	mov ebx,edx
@@:
	mov [eax],bx
	pop edx ecx ebx eax
	ret
limd:
.call=0
.var=4
.min=8
.max=12
	push eax ebx ecx edx
	mov eax,[esi+.var]
	mov ebx,[eax]
	mov ecx,[esi+.min]
	mov edx,[esi+.max]
	cmp ebx,ecx
	jge @f
	mov ebx,ecx
@@:
	cmp ebx,edx
	jle @f
	mov ebx,edx
@@:
	mov [eax],ebx
	pop edx ecx ebx eax
	ret
