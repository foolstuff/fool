;_model 'buffer',_f,_size,_dataptr8
buffer:
.call=0
.size=4
.ptr=8
;free ram at ptr if size = 0
;otherwise allocate size and set ptr if size
;if ptr is still set, reset the content
;if size increase reallocate, copy, free and set ptr
;if size decrease don't change anything
      mov ecx,[esi+.size]
      mov ebx,[esi+.ptr]
@@:
      mov byte[ebx+ecx-1],0
      loop @b
      ret

macro Buffer s,p {
      dd buffer,s,p
      }
