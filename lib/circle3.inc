db "circle",0
circle:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.off=24
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y]
        mov edx,[esi+.xl]
        add edx,[esi+.yl]
        sar edx,8
        cmp edx,6
        jnl @f
        mov edx,6
@@:
        push edx
        shl edx,1
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
        sar eax,1
        sar ebx,1
        add [esi+.x],eax
        add [esi+.y],ebx
        mov ecx,[esi+.xl]
        sar ecx,1
        mov ebp,[esi+.y]
        add ecx,[esi+.x]
        finit                   ; EMPTY
@@:

        push edx
        push dword[esi+.xl] dword[esi+.yl]

.pi180  equ ss:esp+12
.angle  equ ss:esp+8
.sxl    equ ss:esp+4
.syl    equ ss:esp+0

        fldpi                   ; pi
        fimul dword[.angle]   ; pi*angle
        fiadd dword[esi+.off]
        fidiv dword[.pi180]     ; /180
        fsincos                 ; cos       ; sin
        fimul dword[.sxl]   ; cos*xl    ; sin
        fistp dword[.sxl]   ; sin
        fimul dword[.syl]   ; sin*yl
        fistp dword[.syl]   ; EMPTY

        pop ebx eax

        sar eax,1
        sar ebx,1

        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]

        mov [esi+.xl],eax
        mov [esi+.yl],ebx

        add eax,[esi+.x]
        add ebx,[esi+.y]

        push eax ebx
        call line ;ray
        mov [esi+.x],eax
        mov [esi+.y],ebx
        sub ecx,eax
        sub ebp,ebx
        mov [esi+.xl],ecx
        mov [esi+.yl],ebp
        call line ;outline
        pop ebp ecx
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edx
        dec edx
        jnl @b
        pop edx
        pop dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro Circle x,y,xl,yl,c,o {
        dd circle,x,y,xl,yl,c
       if o eq
        dd 0
       else
        dd o
       end if
}

_circle:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y]
        or edi,edi
        je @f
        call addxy
@@:
        mov edx,[esi+.xl]
        add edx,[esi+.yl]
        sar edx,3
        cmp edx,6
        jnl @f
        mov edx,6
@@:
        push edx
        shl edx,1
        mov ecx,[esi+.c]
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
        sar eax,1
        sar ebx,1
        add eax,[esi+.x]
        add ebx,[esi+.y]
        mov [esi+.x],eax
        mov [esi+.y],ebx
        finit                   ; EMPTY
@@:
        push edx
        push dword[esi+.xl] dword[esi+.yl]
        fldpi                   ; pi
        fimul dword[ss:esp+8]   ; pi*angle
        fidiv dword[ss:esp+12]  ; /180
        fsincos                 ; cos       ; sin
        fimul dword[ss:esp+4]   ; cos*xl    ; sin
        fistp dword[ss:esp+4]   ; sin
        fimul dword[ss:esp+0]   ; sin*yl
        fistp dword[ss:esp+0]   ; EMPTY
        pop ebx eax
        sar eax,1
        sar ebx,1
        add eax,[esi+.x]
        add ebx,[esi+.y]
        Pixel
        pop edx
        dec edx
        jne @b
        pop edx
        pop dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro _Circle x,y,xl,yl,c {
        dd _circle,x,y,xl,yl,c
}