_while:
.call=0
.cc=4
.do=8
        push esi
        fcall [esi+.cc]
        pop esi
        cmp eax,NULL    ;equivalent to break
        je .end
        or eax,eax      ;quit when eax=0
        je .end
        push esi
        fcall [esi+.do]
        pop esi
        cmp eax,NULL    ;equivalent to break
        jne _while
.end:
        ret

macro While c,d {
        dd _while,c
if ~ d eq
        dd d
else
        dd 0
end if
}
