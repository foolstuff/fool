ret0:
        xor eax,eax
        ret

retx:
        mov eax,[esi+asm.op1]
        ret

halt:
        hlt
        ret

cmpb:
        push ebx
        mov eax,[esi+asm.op1]
        movzx ebx,byte[esi+asm.op2]
        or eax,eax
        je @f
        movzx eax,byte[eax]
        sub eax,ebx
@@:
        pop ebx
        ret

cmpw:
        push ebx
        mov eax,[esi+asm.op1]
        movzx ebx,word[esi+asm.op2]
        or eax,eax
        je @f
        movzx eax,word[eax]
        sub eax,ebx
@@:
        pop ebx
        ret

cmpdw:
        mov eax,[esi+asm.op1]
        or eax,eax
        je @f
        push ebx
        mov ebx,[esi+asm.op2]
        mov eax,[eax]
        sub eax,ebx
        pop ebx
@@:
        ret

negdw:
        push eax
        mov eax,[esi+asm.op1]
        neg dword[eax]
        pop eax
        ret

negw:
        push eax
        mov eax,[esi+asm.op1]
        neg word[eax]
        pop eax
        ret

negb:
        push eax
        mov eax,[esi+asm.op1]
        neg byte[eax]
        pop eax
        ret

notdw:
        push eax
        mov eax,[esi+asm.op1]
        not dword[eax]
        pop eax
        ret

notw:
        push eax
        mov eax,[esi+asm.op1]
        not word[eax]
        pop eax
        ret

notb:
        push eax
        mov eax,[esi+asm.op1]
        not byte[eax]
        pop eax
        ret

bind:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[esi+asm.op2]
        mov ebx,[ebx]
        mov [eax],ebx
        pop ebx eax
        ret

addb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        add byte[eax],cl
        pop ecx eax
        ret

addw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        add word[eax],cx
        pop ecx eax
        ret

adddw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        add dword[eax],ecx
        pop ecx eax
        ret

anddw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        and dword[eax],ecx
        pop ecx eax
        ret

ordw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        or dword[eax],ecx
        pop ecx eax
        ret

subb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sub byte[eax],cl
        pop ecx eax
        ret

subw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sub word[eax],cx
        pop ecx eax
        ret

subdw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sub dword[eax],ecx
        pop ecx eax
        ret

rolb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        rol byte[eax],cl
        pop ecx eax
        ret

rolw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        rol word[eax],cl
        pop ecx eax
        ret

roldw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        rol dword[eax],cl
        pop ecx eax
        ret

rorb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        ror byte[eax],cl
        pop ecx eax
        ret

rorw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        ror word[eax],cl
        pop ecx eax
        ret

rordw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        ror dword[eax],cl
        pop ecx eax
        ret

sarb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sar byte[eax],cl
        pop ecx eax
        ret

sarw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sar word[eax],cl
        pop ecx eax
        ret

sardw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        sar dword[eax],cl
        pop ecx eax
        ret

shrb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shr byte[eax],cl
        pop ecx eax
        ret

shrw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shr word[eax],cl
        pop ecx eax
        ret

shrdw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shr dword[eax],cl
        pop ecx eax
        ret

shlb:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shl byte[eax],cl
        pop ecx eax
        ret

shlw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shl word[eax],cl
        pop ecx eax
        ret

shldw:
        push eax ecx
        mov eax,[esi+asm.op1]
        mov ecx,[esi+asm.op2]
        shl dword[eax],cl
        pop ecx eax
        ret

incb:
        push eax
        mov eax,[esi+asm.op1]
        inc byte[eax]
        jnc @f
        mov byte[eax],7fh
@@:
        pop eax
        ret

decb:
        push eax
        mov eax,[esi+asm.op1]
        dec byte[eax]
        jnc @f
        mov byte[eax],80h
@@:
        pop eax
        ret

incw:
        push eax
        mov eax,[esi+asm.op1]
        inc word[eax]
        jnc @f
        mov word[eax],7fffh
@@:
        pop eax
        ret

decw:
        push eax
        mov eax,[esi+asm.op1]
        dec word[eax]
        jnc @f
        mov word[eax],8000h
@@:
        pop eax
        ret

incdw:
        push eax
        mov eax,[esi+asm.op1]
        inc dword[eax]
        jnc @f
        mov dword[eax],7fffffffh
@@:
        pop eax
        ret

decdw:
        push eax
        mov eax,[esi+asm.op1]
        dec dword[eax]
        jnc @f
        mov dword[eax],80000000h
@@:
        pop eax
        ret

xormod:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[eax+4]
        mov eax,[eax]
        xor [eax],ebx
        pop ebx eax
        ret

xorb:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov bl,[esi+asm.op2]
        xor [eax],bl
        pop ebx eax
        ret

xorw:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov bx,[esi+asm.op2]
        xor [eax],bx
        pop ebx eax
        ret

xordw:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[esi+asm.op2]
        xor [eax],ebx
        pop ebx eax
        ret

movmod:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[eax+4]
        mov eax,[eax]
        mov [eax],ebx
        pop ebx eax
        ret

movb:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov bl,[esi+asm.op2]
        mov [eax],bl
        pop ebx eax
        ret

movw:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov bx,[esi+asm.op2]
        mov [eax],bx
        pop ebx eax
        ret

movdw:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[esi+asm.op2]
        mov [eax],ebx
        pop ebx eax
        ret

addmod:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[eax+4]
        mov eax,[eax]
        add [eax],ebx
        pop ebx eax
        ret

submod:
        push eax ebx
        mov eax,[esi+asm.op1]
        mov ebx,[eax+4]
        mov eax,[eax]
        sub [eax],ebx
        pop ebx eax
        ret

stbl:
        push eax ebx ecx
        mov ecx,[esi+asm.op3]
        mov eax,[esi+asm.op2]
        mov ebx,[esi+asm.op1]
@@:
        mov dword[ebx+ecx*4-4],eax
        loop @b
        pop ecx ebx eax
        ret