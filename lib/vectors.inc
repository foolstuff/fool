macro SCALE {
        fmul dword[esi+.z]
}

vectors:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.z=24
.str=28
.ctrl=32
.mx=36
.my=40
.n=44
.name=48

.next=4
.txt=8
.sx     equ dword[ss:esp+4]
.sy     equ dword[ss:esp+0]

        fcall [esi+.ctrl]
        push eax
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        finit
        mov edx,[esi+.str]
.loop:
        mov eax,[edx]
        call eax
        or edx,edx
        jne .loop
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop eax
        ret

.stop:
        xor edx,edx
        ret

.dot:
        push eax ebx
        fld dword[edx+.x]
        SCALE
        fistp .sx
        fld dword[edx+.y]
        SCALE
        fistp .sy
        pop ebx eax
        or edi,edi
        je @f
        add eax,[edi+.x]
        add ebx,[edi+.y]
@@:
        mov ecx,[esi+.c]
        call pixel
        add edx,4*3
        ret

.dotby:
        push eax ebx
        fld dword[edx+.x];xL,y0,x0
        SCALE
        faddp st2,st0    ;y0,x1
        fld dword[edx+.y];YL,y0,x1
        SCALE
        faddp st1,st0    ;y1,x1
        fist .sy
        fxch st1
        fist .sx
        fxch st1
        pop ebx eax
        add eax,[esi+.x]
        add ebx,[esi+.y]
        or edi,edi
        je @f
        add eax,[edi+.x]
        add ebx,[edi+.y]
@@:
        mov ecx,Red
        call pixel
        add edx,4*3
        ret

.lineby:
        push dword[esi+.c]
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
                                ;y0,x0
        fld dword[edx+.x]       ;xL,y0,x0
        SCALE
        fist dword[esi+.xl]     ;xL,y0,x0
        fxch st2                ;x0,y0,xL
        fist dword[esi+.x]      ;x0,y0,xL
        faddp st2,st0           ;y0,x1
        fld dword[edx+.y]       ;yL,y0,x1
        SCALE
        fist dword[esi+.yl]     ;yL,y0,x1
        fxch st1                ;y0,yL,x1
        fist dword[esi+.y]      ;y0,yL,x1
        faddp st1,st0           ;y1,x1
        push edx
        call line
        pop edx
        pop dword[esi+.c]
        add edx,4*3
        ret

.line:
        push dword[esi+.c]
                                ;y0,x0
        fld dword[edx+.x]       ;x1,y0,x0
        SCALE
        fxch st2                ;x0,y0,x1
        fist dword[esi+.x]      ;x0,y0,x1
        fsubr st0,st2           ;xL,y0,x1
        fistp dword[esi+.xl]    ;y0,x1
        fld dword[edx+.y]       ;y1,y0,x1
        SCALE
        fxch st1                ;y0,y1,x1
        fist dword[esi+.y]      ;y0,y1,x1
        fsubr st0,st1           ;yL,y1,x1
        fistp dword[esi+.yl]    ;y1,x1
        push edx
        call line
        mov eax,[esi+.x]
        add [esi+.mx],eax
        mov eax,[esi+.y]
        add [esi+.my],eax
        inc dword[esi+.n]
        pop edx
        pop dword[esi+.c]
        add edx,4*3
;        add  dword[esi+.c],1
        ret

.circle:
        call .moveto            ;y0,x0
        push dword -1.0
        fld dword[esp]          ;-1,y0,x0
        pop eax
        fld dword[edx]          ;r,-1,y0,x0
        SCALE                   ;r*Z,-1,y0,x0
        fist dword[esi+circle.xl]
        fist dword[esi+circle.yl]
        fscale                  ;r,-1,y,x
        fst st1                 ;r,r,y,x
        fsubr st0,st2
        fistp dword[esi+circle.y]
        fsubr st0,st2
        fistp dword[esi+circle.x]
        call circle
        add edx,4
        ret

.curve:
        add edx,4*3
        ret

.moveto:
        finit
        fld dword[edx+.x]     ;x0
        SCALE
        fld dword[edx+.y]     ;y0,x0
        SCALE
        add edx,4*3
        ret

.color:
        mov eax,[edx+4]
        mov [esi+.c],eax
        add edx,4*2
        ret

.moveby:
        finit
        fld dword[edx+.x]     ;xL,y0,x0
        SCALE
        faddp st2,st0         ;y0,x1
        fld dword[edx+.y]     ;yL,y0,x1
        SCALE
        faddp st1,st0         ;y1,x1
        add edx,4*3
        ret

.setname:
        mov eax,[edx+.txt]
        mov dword[esi+.name],eax
        mov edx,[edx+.next]
        ret

.startmean:
        mov dword[esi+.mx],0
        mov dword[esi+.my],0
        mov dword[esi+.n],0
        add edx,4
        ret

.stopmean:
        push edx
        push dword[esi+.c]
        mov dword[esi+.c],Teal
        push dword[esi+.z] dword[esi+.str] dword [esi+.ctrl]
        mov dword[esi+vtxt.font],ugly
        mov dword[esi+vtxt.zoom],1
        mov eax,[esi+.mx]
        cdq
        mov ebx,[esi+.n]
        idiv ebx
        mov [esi+.x],eax
        mov eax,[esi+.my]
        cdq
        mov ebx,[esi+.n]
        idiv ebx
        mov [esi+.y],eax
        mov eax,[esi+.name]
        mov dword[esi+vtxt.txt],eax
        call vtxt
        pop dword [esi+.ctrl] dword[esi+.str] dword[esi+.z]
        pop dword[esi+.c]
        pop edx
        add edx,4
        ret

macro STOP {
        dd vectors.stop
}

macro DOT x,y {
        dd vectors.dot,x,y
}

macro DOTBY x,y {
        dd vectors.dotby,x,y
}

macro MOVETO x,y {
        dd vectors.moveto,x,y
}

macro MOVEBY x,y {
        dd vectors.moveby,x,y
}

macro LINETO x,y {
        dd vectors.line,x,y
}

macro CIRCLE x,y,r {
        DOT x,y
        dd vectors.circle,x,y,r
}

macro LINEBY x,y {
        dd vectors.lineby,x,y
}

macro CURVETO x1,y1,x2,y2,x3,y3 {
        LINETO x1,y1
        LINETO x2,y2
        LINETO x3,y3
}

macro CHAIN [x1,y1,x2,y2,x,y] {
common
forward
;        LINEBY x1,y1
;        LINEBY x2,y2
        LINEBY x,y
common
}


macro LINES a,b,[x,y] {
        MOVETO a,b
common
forward
;        DOT x,y
        LINETO x,y
common
        LINETO a,b
}

macro CLOSEPATH {
}

macro CLIPNEWPATH {
}

macro FILL {
}

macro COLOR c {
        dd vectors.color,c
}

macro OCEAN {
        dd vectors.color,Blue
}

macro LAND {
        dd vectors.color,Lime
}

macro COAST {
        dd vectors.color,Yellow
}

macro LAKE {
        dd vectors.color,Teal
}

macro NAME n {
local .n,.s
        dd vectors.setname,.n,.s
.s:     db n,0
align 4
.n:
        dd vectors.startmean
}

macro SHOWNAME {
        dd vectors.stopmean
}

macro Vectors x,y,xl,yl,c,z,str,ctrl {
        dd vectors,x,y,xl,yl,c,z,str,ctrl,0,0,0,0
}
