tex:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.bmp=24
        push eax ebx ecx edx esi edi ebp
        or edi,edi
        je @f
        call addxy
@@:
        push esi edi
        mov edi,esi
        mov ecx,[edi+.c]
        mov esi,[edi+.bmp]
        push dword [esi+.c]
        add [esi+.c],ecx
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        mov ebp,[esi+.x]
        mov ecx,[esi+.x]
        mov edx,[esi+.y]
        add ecx,[edi+.xl]
        add edx,[edi+.yl]
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
@@:
        push eax ebx ecx edx ebp
        fcall
        push ebp edx ecx ebx eax
        add [esi+.x],eax
        cmp [esi+.x],ecx
        jl @b
        mov [esi+.x],ebp
        add [esi+.y],ebx
        cmp [esi+.y],edx
        jl @b
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop dword [esi+.c]
        pop edi esi
        or edi,edi
        je @f
        call subxy
@@:
        pop ebp edi esi edx ecx ebx eax
        ret

macro Tex x,y,xl,yl,c,b {
        dd tex,x,y,xl,yl,c,b
}