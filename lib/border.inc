border:
.call=0
.x=4
.y=8
.xl=12
.yl=16
;.c=20
.mod=White

if defined NO3DBORDER
        call frame
        ret
end if

        push eax ebx ecx edx ebp
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        push dword [pixel.pix]
        call pixel.setalpha+4
        mov ecx,.mod - 3f3f3f3fh
        call .atom
        mov ecx,.mod
        inc dword[esi+.x]
        inc dword[esi+.y]
        sub dword[esi+.xl],2
        sub dword[esi+.yl],2
        call .atom
        pop dword [pixel.pix]
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop ebp edx ecx ebx eax
        ret

.atom:
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        mov edx,[esi+.yl]
@@:
        dec edx
        jle @f
        Pixel
        inc ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
        xor ecx,0ffffffffh
@@:
        dec edx
        jle @f
        Pixel
        inc eax
        jmp @b
@@:
        mov edx,[esi+.yl]
@@:
        dec edx
        jle @f
        Pixel
        dec ebx
        jmp @b
@@:
        mov edx,[esi+.xl]
        xor ecx,0ffffffffh
@@:
        dec edx
        jle @f
        Pixel
        dec eax
        jmp @b
@@:
        ret

macro Border x,y,xl,yl,c {
        dd border,x,y,xl,yl,c
}