macro NewFile name {
}

macro ReadFile name {
}

macro AppendFile name,content {
}

macro WriteFile name,content {
}

macro NewFolder name {
}

macro ListFolder name {
}


newFile:
        invoke CreateFile,eax,GENERIC_READ+GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL
        invoke CloseHandle,eax
        ret

readFile:
        invoke CreateFile,eax,GENERIC_READ+GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL
        mov    [hrep],eax
        invoke GetFileSize,[hrep],NULL
        push   eax
        inc    eax
        mov    [bsize],eax
        invoke VirtualAlloc,0,eax,MEM_COMMIT,PAGE_READWRITE
        mov    [buffer],eax
        pop    ecx
        mov    byte [eax+ecx],0
        invoke ReadFile,[hrep],eax,ecx,readb,NULL
        invoke CloseHandle,[hrep]
        ret
FILE_APPEND_DATA=4
appendFile:
        invoke CreateFile,eax,FILE_APPEND_DATA,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL
        mov    [hrep],eax
        invoke WriteFile,[hrep],[buffer],[bsize],wroteb,NULL
        invoke CloseHandle,[hrep]
        ret

writeFile:
        invoke CreateFile,eax,GENERIC_READ+GENERIC_WRITE,FILE_SHARE_READ,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL
        mov    [hrep],eax
        invoke WriteFile,[hrep],[buffer],[bsize],wroteb,NULL
        invoke CloseHandle,[hrep]
        ret

ERROR_NO_MORE_FILES=12h
newFolder:
        invoke CreateDirectory,eax,NULL
        ret

listFolder:
        invoke FindFirstFileA,eax,filedata
        mov [shfile],eax
        mov eax,filedata.cFileName
        mov esi,eax
        call copyStr
        call crlfStr
.lsloop:
        invoke FindNextFileA,[shfile],filedata
        or eax,eax
        je .lserror
        mov eax,filedata.cFileName
        mov esi,eax
        call copyStr
        call crlfStr
        jmp .lsloop
.lserror:
        invoke GetLastError
        cmp eax,ERROR_NO_MORE_FILES
        jne .lsloop
        invoke FindClose,[shfile]
        call endStr
        ret
