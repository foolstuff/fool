;transparent return
mous:
.call=0
.x=4
.y=8
.dx=12
.dy=16
.c=20
.bmp=24
.status=28
.oldstatus=29
.rightedge=30
.leftedge=31
.wheel=32
.lclic=1
.rclic=2
.mclic=4
.esi=36
.edi=40
.esir=44
.edir=48
        push eax ebx ecx edx
        mov eax,[mouse.cursor+.x]
        mov ebx,[mouse.cursor+.y]
        xor ecx,ecx
        mov edx,ecx
        cmp byte[key+key.up],0
        je @f
        sub edx,[.mdx]
@@:
        cmp byte[key+key.down],0
        je @f
        add edx,[.mdx]
@@:
        cmp byte[key+key.left],0
        je @f
        sub ecx,[.mdy]
@@:
        cmp byte[key+key.right],0
        je @f
        add ecx,[.mdy]
@@:
        movsx edx,dl
        movsx ecx,cl
        add eax,ecx
        add ebx,edx
        cmp eax,0
        jge @f
        mov eax,0
@@:
        cmp eax,319
        jle @f
        mov eax,319
@@:
        cmp ebx,0
        jge @f
        mov ebx,0
@@:
        cmp ebx,199
        jle @f
        mov ebx,199
@@:

        mov [mouse.cursor+.x],eax
        mov [mouse.cursor+.y],ebx
;        mov edx,[pixel.mode]
;        or edx,edx
;        je @f
;        mov edx,[edx+pixel.out]
;        or edx,edx
;        je @f
;        call dx
;@@:
        mov ecx,[esi+.x]
        mov edx,[esi+.y]
        mov [esi+.x],eax
        mov [esi+.y],ebx
        sub eax,ecx
        sub ebx,edx
        mov [esi+.dx],eax
        mov [esi+.dy],ebx
        xor edx,edx
        xor eax,eax
        cmp byte[key+key.pgup],0
        je @f
        add edx,10
@@:
        cmp byte[key+key.pgdn],0
        je @f
        add edx,-10
@@:
        mov [esi+.wheel],edx
        mov ah,[esi+.status]
        mov [esi+.oldstatus],ah
        cmp byte[key+key.menu],0
        je @f
        or al,.rclic
@@:
        test byte[key+key.status],key.ctrl?
        je @f
        or al,.lclic
@@:
        test byte[key+key.status],key.shift?
        je @f
        or al,.mclic
@@:
        mov [esi+.status],al
        mov bx,ax
        shr bx,1
        and ax,0101h
        and bx,0101h
        sub bl,bh
        sub al,ah
        jge @f
        mov dword[esi+.esi],0
        mov dword[esi+.esir],0
        mov dword[esi+.edi],0
        mov dword[esi+.edir],0
@@:
        mov [esi+.leftedge],al
        mov [esi+.rightedge],bl
        mov eax,[esi+.bmp]
        mov [mouse.cursor+.bmp],eax
        mov eax,[mouse.default]
        mov [esi+.bmp],eax
        add esp,8
        pop edx ecx ebx eax
        ret
align 4
.mdx  dd 1
.mdy  dd 1

macro Mouse c,p{
        dd mous,0,0,1,1,c,p
        dd 0,0,0,0,0,0
}
