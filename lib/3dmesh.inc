;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DOTS=0
EDGES=1
TRIANGLES=2
TYPE=4
SINCOS=8
ISOMETRIC=16
;DRAWDOTS=1
;DRAWLINES=2
OUTOFVIEW=1
BEHIND=2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ds:esi->points datas  x,y,z
;gs:edi->2d coordinates and OOV flag (Out Of View) x,y,oov
dot:
.x=0
.y=4
.z=8
;edg:
;.p1=0
;.p2=4
;tri:
;.p1=0
;.p2=4
;.p3=8
;quad:
;.p1=0
;.p2=4
;.p3=8
;.p4=12
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
mesh:
.call=0
.lines=4
.size=8
.dots=12
.x=0
.y=4
.z=8
.p1=0
.p2=4
.p3=8
.p4=12
	push eax ebx ecx edx esi
	mov ecx,[esi+.size]
	mov [.count],ecx
	shr ecx,2
	sub ecx,3
	jl .end
	push esi
	call .translate
	xor edi,edi
	mov ebp,[.count]
.nextpoint:
	inc [.ndots?]
	call .rotate
	call .3dto2d
	add edi,12
	sub ebp,12
	jne .nextpoint

	movzx eax,[line32.c]
	mov [.edge+line.c],eax

	pop esi

	mov esi,[esi+.lines]
	mov ebp,[esi]
	add esi,4

	bt [.mode],EDGES
	jnc .end
.nextedge:
	cmp ebp,0
	je .end
	inc [.nlines?]
	call .drawedge
	sub ebp,8
	jmp .nextedge
.end:
	pop esi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.translate:
	push eax
	add esi,.dots
	xor edi,edi
@@:
	mov eax,[esi+ecx*4+dot.x]
	sub eax,[cam.x]
	mov [gs:edi+ecx*4+dot.x],eax
	mov eax,[esi+ecx*4+dot.y]
	sub eax,[cam.y]
	mov [gs:edi+ecx*4+dot.y],eax
	mov eax,[esi+ecx*4+dot.z]
	sub eax,[cam.z]
	mov [gs:edi+ecx*4+dot.z],eax
	sub ecx,3
	jnl @b
	pop eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.rotate:
	push eax ebx ecx edx
;rotate on y
;x=x*cosy-z*siny
;z=z*cosy+x*siny
	mov eax,[gs:edi+dot.x] ;x
	mov ebx,[gs:edi+dot.z] ;z
	mov ecx,eax	    ;x;x
	mov edx,ebx	    ;z;z
	imul eax,[cos.y]    ;xcosy
	imul ebx,[sin.y]    ;zsiny
	imul ecx,[sin.y]    ;xsiny
	imul edx,[cos.y]    ;zcosy
	sub eax,ebx	    ;xcosy-zsiny
	add ecx,edx	    ;xsiny+zcosy
	sar eax,8
	sar ecx,8
	mov [gs:edi+dot.x],eax
	mov [gs:edi+dot.z],ecx
;rotate on x
;y=y*cosx-z*sinx
;z=z*cosx+y*sinx
	mov eax,[gs:edi+dot.y] ;y
	mov ebx,[gs:edi+dot.z] ;z
	mov ecx,eax	    ;y;y
	mov edx,ebx	    ;z;z
	imul eax,[cos.x]    ;ycosx
	imul ebx,[sin.x]    ;zsinx
	imul ecx,[sin.x]    ;ysinx
	imul edx,[cos.x]    ;zcosx
	sub eax,ebx	    ;ycosx-zsinx
	add ecx,edx	    ;ysinx+zcosx
	sar eax,8
	sar ecx,8
	mov [gs:edi+dot.y],eax
	mov [gs:edi+dot.z],ecx
	pop edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.3dto2d:
	push eax ebx edx esi
	mov eax,[gs:edi+dot.x]
	mov ebx,[gs:edi+dot.z]
	cmp ebx,[.zmin]
	jle .behind
	cmp ebx,[.zmax]
	jge .behind
	imul eax,[focus]
;        jo .behind
	cdq
	idiv ebx
	add eax,[x0]
	mov [gs:edi+dot.x],eax

	mov eax,[gs:edi+dot.y]
	mov ebx,[gs:edi+dot.z]
	imul eax,[focus]
;        jo .behind
	cdq
	idiv ebx
	neg eax
	add eax,[y0]
	mov [gs:edi+dot.y],eax

	mov eax,[xmini]
	cmp dword[gs:edi+dot.x],eax
	jl .outofview
	mov eax,[ymini]
	cmp dword[gs:edi+dot.y],eax
	jl .outofview
	mov eax,[xmaxi]
	cmp dword[gs:edi+dot.x],eax
	jnl .outofview
	mov eax,[ymaxi]
	cmp dword[gs:edi+dot.y],eax
	jnl .outofview
	mov dword[gs:edi+dot.z],0

	bt [.mode],DOTS
	jnc @f
	mov esi,[gs:edi+dot.y]
	shl esi,2
	add esi,[gs:edi+dot.y]
	shl esi,6
	add esi,[gs:edi+dot.x]
	mov dl,[colorp]
	mov [fs:esi],dl
	inc [.ndots!]
	jmp @f
.behind:
	mov dword[gs:edi+dot.z],2
	jmp @f
.outofview:
	mov dword[gs:edi+dot.z],1
@@:
	pop esi edx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.drawedge:
	push eax ebx
	mov eax,[esi+.p1]
	mov ebx,[esi+.p2]
	imul eax,12
	imul ebx,12
	add esi,8
	cmp dword[gs:eax+dot.z],BEHIND
	je .ignore
	cmp dword[gs:ebx+dot.z],BEHIND
	je .ignore
	cmp dword[gs:eax+dot.z],OUTOFVIEW
	jne @f
	cmp dword[gs:ebx+dot.z],OUTOFVIEW
	je .ignore
@@:
	bt [.mode],TYPE
	jc @f
	call .drawedge1
	jmp .ignore
@@:
	call .drawedge2
.ignore:
	pop ebx eax
	ret
.drawedge1:
	mov ecx,[gs:eax+dot.x]
	mov edx,[gs:eax+dot.y]
	mov [line32.x1],ecx
	mov [line32.y1],edx
	mov ecx,[gs:ebx+dot.x]
	mov edx,[gs:ebx+dot.y]
	mov [line32.x2],ecx
	mov [line32.y2],edx
	inc [.nlines!]
	call line32
	ret
.drawedge2:
	mov ecx,[gs:eax+dot.x]
	mov [.edge+line.x],ecx
	mov edx,[gs:ebx+dot.x]
	sub edx,ecx
	mov [.edge+line.xl],edx

	mov ecx,[gs:eax+dot.y]
	mov [.edge+line.y],ecx
	mov edx,[gs:ebx+dot.y]
	sub edx,ecx
	mov [.edge+line.yl],edx
	inc [.nlines!]
	fcall .edge
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.count	 rd 1
.object  rd 1
.ndots?  rd 1
.ndots!  rd 1
.nlines? rd 1
.nlines! rd 1
.ntris?   rd 1
.ntris!   rd 1
.xmini	 rd 1
.xmaxi	 rd 1
.yminix  rd 1
.ymaxix  rd 1
.mode	 dd 7
.zmin	 dd 3
.zmax	 dd 5'000'000
.isoz	 dd 2000
.edge	 dd line,0,0,0,0,0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
