read:
.call=0
.a=4
        mov eax,[esi+.a]
        or eax,eax
        je @f
        mov eax,[eax]
@@:
        ret

macro Dword a {
        dd read,a
}

readw:
.call=0
.a=4
        mov eax,[esi+.a]
        or eax,eax
        je @f
        movzx eax,word[eax]
@@:
        ret

macro Word a {
        dd readw,a
}


readb:
.call=0
.a=4
        mov eax,[esi+.a]
        or eax,eax
        je @f
        movzx eax,byte[eax]
@@:
        ret

macro Byte a {
        dd readb
}

