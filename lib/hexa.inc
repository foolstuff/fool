hexa:
.call=0
.ptr=4
.txt=8
.xl=12
.yl=16
.off=20
        pushad
        push dword[esi+.ptr] dword[esi+.xl] dword[esi+.txt] dword[esi+.off]

        mov ebx,[esi+.ptr]
        mov ebx,[ebx]
        mov eax,[esi+.off]
        add ebx,[eax]
        mov ecx,[esi+.xl]
        mov edx,[esi+.yl]
        mov edi,[esi+.txt]
        mov [esi+.ptr],ebx
        mov ebx,ecx
        mov dword[esi+.xl],2

@@:
        call num.hex
        inc dword[esi+.ptr]
        add edi,2
        mov [esi+.txt],edi
        dec ecx
        jne @b
        mov ecx,ebx
        mov word[edi],0d0ah
        add edi,2
        mov [esi+.txt],edi
        dec edx
        jne @b
        mov byte[edi-2],0

        pop dword[esi+.off] dword[esi+.txt] dword[esi+.xl] dword[esi+.ptr]
        popad
        ret
