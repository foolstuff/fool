;transparent return from previous to child, and from child to next
zone:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.event=20
        push ebx esi
        or edi,edi
        je @f
        call addxy
@@:
        mov ebx,mouse
        call onmouse
        jnc @f
        push edi
        mov edi,esi
        mov dword[mouse.ptr+cptr.ptr],mouse.finger
        fcall [esi+.event]
        pop edi
@@:
        or edi,edi
        je @f
        call subxy
@@:
        pop esi ebx
        ret

macro Zone x,y,xl,yl,e {
        dd zone,x,y,xl,yl,e
}