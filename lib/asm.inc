asm:
.call=0
.func=4
.op0=4
.op1=8
.op2=12
.op3=16
.op4=20
.op5=24
.op6=28
        cmp dword[esi+.func],0
        je @f
        call near[esi+.func]
@@:
        ret

macro Asm [a] {
 common
        dd asm
 forward
        dd a
 common
}

macro ASM {
        Asm .asm
.asm:
}