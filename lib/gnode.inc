macro Gnode x,y,xl,yl,[a] {
;node constructor with .size saved on compile
 common
  local .a
        dd gnode,x,y,xl,yl,.a-$-4
 forward
        dd a
 common
  .a:
}

macro Gnone x,y,xl,yl,[a] {
;node constructor with .size saved on compile
 common
  local .a
        dd 0,x,y,xl,yl,.a-$-4
 forward
        dd a
 common
  .a:
}

;transparent return
gnode:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.size=20
.data=24
;edi is the parent item
;esi is the current item
        push esi edi
        or edi,edi
        je @f
        call addxy
@@:
        mov edi,esi
        add esi,16
        call node
.end:
        pop edi esi
        or edi,edi
        je @f
        call subxy
@@:
        ret

loadxy:
        mov eax,[edi+item.x]
        mov ebx,[edi+item.y]
        mov ecx,[edi+item.xl]
        mov edx,[edi+item.yl]
        ret
storexy:
        mov [esi+item.x],eax
        mov [esi+item.y],ebx
        mov [esi+item.xl],ecx
        mov [esi+item.yl],edx
        ret
lsxy:
        mov eax,[edi+item.x]
        mov ebx,[edi+item.y]
        mov ecx,[edi+item.xl]
        mov edx,[edi+item.yl]
        mov [esi+item.x],eax
        mov [esi+item.y],ebx
        mov [esi+item.xl],ecx
        mov [esi+item.yl],edx
        ret
addxy:
        push eax ebx
        mov eax,[edi+item.x]
        mov ebx,[edi+item.y]
        add [esi+item.x],eax
        add [esi+item.y],ebx
        pop ebx eax
        ret
subxy:
        push eax ebx
        mov eax,[edi+item.x]
        mov ebx,[edi+item.y]
        sub [esi+item.x],eax
        sub [esi+item.y],ebx
        pop ebx eax
        ret
