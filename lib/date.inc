date:
.call=0
.year=4
.cent=5
.month=6
.day=7
        push eax
        mov al,byte[datetime.wDay]
        aam
        shl ah,4
        or al,ah
        mov [esi+.day],al
        mov al,byte[datetime.wMonth]
        aam
        shl ah,4
        or al,ah
        mov [esi+.month],al
        movzx eax,word[datetime.wYear]
        xor edx,edx
        mov ebx,100
        div ebx
        aam
        shl ah,4
        or al,ah
        mov [esi+.cent],al
        mov ax,dx
        aam
        shl ah,4
        or al,ah
        mov [esi+.year],al
        pop eax
        ret
