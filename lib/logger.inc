logger:
.call=0
.file=4
.path=8
.hdle=12
.done=16
        call .create
        call .write
        call .close
        ret

.create:
        push esi
;        invoke CreateFile,[esi+.path],4,FILE_SHARE_READ,0,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,0
        pop esi
        mov [esi+.hdle],eax
        ret
.write:
        mov eax,[esi+.file]
        mov ebx,[esi+.done]
        push esi
;        invoke WriteFile,[esi+.hdle],[eax+0],[eax+4],ebx,0
        pop esi
        ret
.close:
        push esi
;        invoke CloseHandle,[esi+.hdle]
        pop esi
        ret

macro Logger f,p {
        dd logger,f,p,0,0
}
