circle:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y]
        or edi,edi
        je @f
        call addxy
@@:
        mov edx,180
        push edx
        shl edx,1
        mov ecx,[esi+.c]
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
        sar eax,1
        sar ebx,1
        add eax,[esi+.x]
        add ebx,[esi+.y]
        mov [esi+.x],eax
        mov [esi+.y],ebx
        Pixel
        finit                   ; EMPTY
@@:
        push edx dword[esi+.xl] dword[esi+.yl]
        fldpi                   ; pi        ;
        fimul dword[ss:esp+8]   ; pi*angle  ;
        fidiv dword[ss:esp+12]  ; /180
        fsincos                 ; cos       ; sin
        fimul dword[ss:esp+4]   ; cos*xl    ; sin
        fistp dword[ss:esp+4]   ; sin
        fimul dword[ss:esp+0]   ; sin*yl
        fistp dword[ss:esp+0]   ; EMPTY
        pop ebx eax
        sar eax,1
        sar ebx,1
        add eax,[esi+.x]
        add ebx,[esi+.y]
        Pixel
        pop edx
        dec edx
        jne @b
        pop edx
        pop dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro Circle x,y,dx,dy,c {
        dd circle,x,y,dx,dy,c
}