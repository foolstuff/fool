;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
p:
.x=0
.y=4
tri:
.call=0
.p1=4
.p2=8
.p3=12
.c=16
	push eax ebx ecx edx edi esi
	bt [mesh.mode],TRIANGLES
	jc @f
;        jnc .end
	call triline
	jmp .done ;.end
@@:
	mov eax,[esi+.p1]
	mov ebx,[esi+.p2]
	mov ecx,[esi+.p3]
	mov edx,[eax+p.x]
	mov edi,[ebx+p.x]
	mov esi,[ecx+p.x]
	mov eax,[eax+p.y]
	mov ebx,[ebx+p.y]
	mov ecx,[ecx+p.y]
	call trinormal
	jnc .end
	cmp eax,ebx
	jnl .2
	cmp eax,ecx
	jnl .3
	mov [.p1y],eax
	mov [.p1x],edx
	mov eax,ecx
	mov edx,esi
	jmp .ok
.2:
	cmp ebx,ecx
	jnl .3
	mov [.p1y],ebx
	mov [.p1x],edi
	mov ebx,ecx
	mov edi,esi
	jmp .ok
.3:
	mov [.p1y],ecx
	mov [.p1x],esi
.ok:
	cmp eax,ebx
	jnl @f
	xchg eax,ebx
	xchg edx,edi
@@:
	mov [.p2y],ebx
	mov [.p3y],eax
	mov [.p2x],edi
	mov [.p3x],edx
	call triraster
	pop esi
	push esi
	call trirender
.done:
	stc
	pop esi edi edx ecx ebx eax
	ret
.end:
	clc
	pop esi edi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.p1x dd 0
.p1y dd 0
.p2x dd 0
.p2y dd 0
.p3x dd 0
.p3y dd 0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
triline:
.call=0
.p1=4
.p2=8
.p3=12
.c=16
	push eax ebx ecx edx edi esi
;        bt [mesh.mode],TRIANGLES
;        jnc .end
	mov eax,[esi+.c]
	mov dword[line32.c],eax
	mov eax,[esi+.p1]
	mov ebx,[esi+.p2]
	mov ecx,[esi+.p3]
	mov edx,[eax+p.x]
	mov edi,[ebx+p.x]
	mov esi,[ecx+p.x]
	mov eax,[eax+p.y]
	mov ebx,[ebx+p.y]
	mov ecx,[ecx+p.y]
	call trinormal
	jnc .end
	mov [line32.x1],edx
	mov [line32.y1],eax
	mov [line32.x2],edi
	mov [line32.y2],ebx
	call line32
	mov [line32.x1],esi
	mov [line32.y1],ecx
	call line32
	mov [line32.x2],edx
	mov [line32.y2],eax
	call line32
.end:
	pop esi edi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
triraster:
	push eax ebx ecx edx esi edi ebp
	mov ecx,200
	xor edi,edi
	mov eax,edi
@@:
	mov [edi*4+.start],eax
	mov [edi*4+.stop],eax
	inc edi
	dec ecx
	jne @b
	mov eax,[tri.p1x]
	mov ebx,[tri.p1y]
	mov ecx,[tri.p3x]
	mov edx,[tri.p3y]
	mov [.x1],eax
	mov [.y1],ebx
	mov [.x2],ecx
	mov [.y2],edx
	mov ebp,.start
	call .fill
	mov eax,[tri.p1x]
	mov ebx,[tri.p1y]
	mov ecx,[tri.p2x]
	mov edx,[tri.p2y]
	mov [.x1],eax
	mov [.y1],ebx
	mov [.x2],ecx
	mov [.y2],edx
	mov ebp,.stop
	call .fill
	mov eax,[tri.p3x]
	mov ebx,[tri.p3y]
	mov ecx,[tri.p2x]
	mov edx,[tri.p2y]
	mov [.x1],eax
	mov [.y1],ebx
	mov [.x2],ecx
	mov [.y2],edx
	mov ebp,.stop
	call .fill
	mov esi,.start
	mov edi,.stop
	mov ecx,200
.next1:
	mov eax,[esi]
	mov ebx,[edi]
	cmp eax,ebx
	jle @f
	xchg eax,ebx
@@:
	cmp eax,0
	jge @f
	xor eax,eax
@@:
	sub ebx,eax
	mov [esi],eax
	mov [edi],ebx
	add esi,4
	add edi,4
	dec ecx
	jne .next1
	pop ebp edi esi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.fill:
	mov esi,1
	mov ecx,1
	mov edi,1
	mov edx,320
	mov eax,[.x2]
	sub eax,[.x1]
	jge @f
	neg eax
	neg ecx
	neg edi
@@:
	mov ebx,[.y2]
	sub ebx,[.y1]
	jge @f
	neg ebx
	neg edx
	neg esi
@@:
	mov [.dmax],ebx
	mov [.dmin],eax
	mov [.dinc1],edx
	mov [.dinc2],ecx
	mov [.xinc1],0
	mov [.xinc2],edi
	mov [.sinc1],esi
	mov [.sinc2],0
	cmp eax,ebx
	jl @f
	mov [.dmax],eax
	mov [.dmin],ebx
	mov [.dinc1],ecx
	mov [.dinc2],edx
	mov [.xinc1],edi
	mov [.xinc2],0
	mov [.sinc1],0
	mov [.sinc2],esi
@@:
	mov esi,[.y1]
	mov ebx,[.x1]
	mov ecx,[.dmax]
	mov edi,esi
	imul edi,320
	add edi,ebx
	mov eax,ecx
	shr ecx,1
	inc eax
.next:
	cmp esi,0
	jl .ignore
	cmp esi,200
	jge .ignore
	mov edx,ebx
	cmp edx,0
	jge @f
	mov edx,0
@@:
	cmp edx,320
	jl @f
	mov edx,320
@@:
	mov [esi*4+ebp],edx
.ignore:
	dec eax
	je @f
	add ebx,[.xinc1]
	add edi,[.dinc1]
	add esi,[.sinc1]
	add ecx,[.dmin]
	cmp ecx,[.dmax]
	jl .next
	sub ecx,[.dmax]
	add ebx,[.xinc2]
	add edi,[.dinc2]
	add esi,[.sinc2]
	jmp .next
@@:
	ret
.x1 rd 1
.x2 rd 1
.y1 rd 1
.y2 rd 1
.xinc1 rd 1
.xinc2 rd 1
.dinc1 rd 1
.dinc2 rd 1
.sinc1 rd 1
.sinc2 rd 1
.dmin rd 1
.dmax rd 1
.start rd 1080
.stop rd 1080
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
trirender:
	mov eax,[esi+tri.c]
	xor esi,esi
	mov ecx,200
.next:
	mov edi,esi
	imul edi,[screen.xl]
	mov ebx,[esi*4+triraster.start]
	mov edx,[esi*4+triraster.stop]
	cmp edx,0
	je @f
	add edi,ebx
.line:
	mov [fs:edi],al
	inc edi
	dec edx
	jg .line
@@:
	inc esi
	dec ecx
	jne .next
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
trinormal:
;eax=y1
;ebx=y2
;ecx=y3
;edx=x1
;edi=x2
;esi=x3
;
;return cf=0 if normal area positive or equal
	push eax ebx ecx edx esi edi
	sub edi,edx
	sub ecx,eax
	sub esi,edx
	sub ebx,eax
	imul edi,ecx
	imul esi,ebx
	sub edi,esi
	jl .not
	clc
@@:
	pop edi esi edx ecx ebx eax
	ret
.not:
	stc
	jmp @b
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
