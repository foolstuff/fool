digit:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.v=24
        push eax ecx edi esi
        or edi,edi
        je @f
        call addxy
@@:
        mov edi,esi
        xor ecx,ecx
        mov [.0+box.c],ecx
        mov [.1+box.c],ecx
        mov [.2+box.c],ecx
        mov [.3+box.c],ecx
        mov [.4+box.c],ecx
        mov [.5+box.c],ecx
        mov [.6+box.c],ecx
        mov dword[.0+box.call],frame
        mov dword[.1+box.call],frame
        mov dword[.2+box.call],frame
        mov dword[.3+box.call],frame
        mov dword[.4+box.call],frame
        mov dword[.5+box.call],frame
        mov dword[.6+box.call],frame
        mov ecx,[esi+.c]
        mov al,[esi+.v ]
        and al,0fh
        movzx eax,al
        mov al,[.lut+eax]
        shr al,1
        jnc @f
        mov [.0+box.c],ecx
        mov dword[.0+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.1+box.c],ecx
        mov dword[.1+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.2+box.c],ecx
        mov dword[.2+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.3+box.c],ecx
        mov dword[.3+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.4+box.c],ecx
        mov dword[.4+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.5+box.c],ecx
        mov dword[.5+box.call],box
@@:
        shr al,1
        jnc @f
        mov [.6+box.c],ecx
        mov dword[.6+box.call],box
@@:
        fcall .segments
        pop esi edi ecx eax
        or edi,edi
        je @f
        call subxy
@@:
        ret


;     111        0=0011'1111
;    2   0       1=0001'0001
;    2   0       2=0101'1011
;    2   0       3=0111'0011
;     666        4=0110'0101
;    3   5       5=0111'0110
;    3   5       6=0111'1110
;    3   5       7=0010'0011
;     444        8=0111'1111
;                9=0111'0111
;                A=0110'1111
;                B=0111'1100
;                C=0001'1110
;                D=0111'1001
;                E=0101'1110
;                F=0100'1110

;xl=45
;yl=80

.segments:
        Node .0,.1,.2,.3,.4,.5,.6
.0:     Box 35,0,8,35,0
.1:     Box 9,0,25,8,0
.2:     Box 0,0,8,35,0
.3:     Box 0,36,8,35,0
.4:     Box 9,63,25,8,0
.5:     Box 35,36,8,35,0
.6:     Box 9,32,25,8,0

.lut:
db 0011'1111b
db 0010'0001b
db 0101'1011b
db 0111'0011b
db 0110'0101b
db 0111'0110b
db 0111'1110b
db 0010'0011b
db 0111'1111b
db 0111'0111b
db 0110'1111b
db 0111'1100b
db 0001'1110b
db 0111'1001b
db 0101'1110b
db 0100'1110b

macro Digit x,y,c,v {
        dd digit,x,y,45,80,c,v
}

