;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
screen:
.call=0

;geometry
.x=4
.y=8
.xl=12;dd 0
.yl=16;dd 0
.color=20
;screen description
.bpp=24; dd 0
.size=28; dd 0

;memory model
.buffer=32; dd sys_vbuff
.frame=36; dd sys_vesa

;pixel methods
.pixel=40; dd pixel32
.getpixel=44; dd pixel32

.child=48
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        push edi esi
        mov eax,[esi+.color]
        mov ecx,[esi+.size]
        shr ecx,2
        mov edi,[esi+.frame]
        mov esi,[esi+.buffer]
@@:
        dec ecx
        jl @f
        mov ebx,[esi+ecx*4]
if ~defined NOCLS
        mov [esi+ecx*4],eax
end if
        mov [edi+ecx*4],ebx
        jmp @b
@@:
        pop esi
        mov edi,esi
        fcall [esi+.child]
        pop edi

        ret

macro Screen x,y,xl,yl,color,bpp,child,frame {
if frame eq
        dd screen,x,y,xl,yl,color,bpp,xl*yl*bpp,0,0,pixel,0,child
else
        dd screen,x,y,xl,yl,color,bpp,xl*yl*bpp,0,frame,pixel,0,child
end if
}
