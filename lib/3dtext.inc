;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db 'textextrextextextextex'
txt3d:
.call=0
.x=4
.y=8
.z=12
.mesh=16
.c=20
.txt=24
.font=28
	push eax ebx ecx edx esi
	push dword[esi+.x] dword[esi+.y] dword [esi+.z]
	mov eax,[esi+.x]
	mov [.xref],eax
	mov eax,[esi+.txt]
	mov ebx,[esi+.c]
	mov [line.c],bl
	mov [colorp],bl
.loadcar:
	push eax
	mov al,[eax]
	cmp al,0
	je .fin
	cmp al,0dh
	jne @f
	mov ebx,[.xref]
	mov [esi+.x],ebx
	jmp .nextcar
@@:
	cmp al,0ah
	jne @f
	sub dword[esi+.y],200
	jmp .nextcar
@@:
	and eax,0ffh
	mov [.count],6
	imul eax,[.count]
	add eax,[esi+.font]
	mov [.ray],eax
.nextray:
	mov eax,[.ray]
	mov al,[eax]
	mov [.maskb],1
	mov [.current],al
.testandprint:
	mov al,[.current]
	test al,[.maskb]
	je .ignore
	mov ebx,[esi+.x]
	sub [cam.x],ebx
	mov ebx,[esi+.y]
	sub [cam.y],ebx
	mov ebx,[esi+.z]
	sub [cam.z],ebx
	fcall [esi+.mesh]
	mov ebx,[esi+.x]
	add [cam.x],ebx
	mov ebx,[esi+.y]
	add [cam.y],ebx
	mov ebx,[esi+.z]
	add [cam.z],ebx
.ignore:
	sub dword[esi+.y],20
	shl [.maskb],1
	jne .testandprint
	add dword[esi+.x],20
	add dword[esi+.y],20*8
	inc [.ray]
	dec [.count]
	jne .nextray
.nextcar:
	pop eax
	inc eax
	jmp .loadcar
.fin:
	pop eax
	pop dword[esi+.z] dword[esi+.y] dword[esi+.x]
	pop esi edx ecx ebx eax
	ret
.maskb rb 1
.current rb 1
.ray rd 1
.count rd 1
.xref rd 1
