macro Htxt x,y,xl,yl,c,t,v,s,n {

        Node $+24,next
        dd num.hex,v,s,n
        Txt x,y,xl,yl,c,t,font85
}

macro Panel x,y,xl,yl,c1,c2,t,w {
        Gnode x,y,xl,yl,\
        .frm,\
        .titlebar,\
        w
.frm:   Frame 0,0,xl,yl,c2
.titlebar:
        Gnode 1,1,xl-2,20,\
        @f,.box
@@:     Vtxt 20,3,0,0,c2,t,1,ugly
.box:   Box 0,0,.xl-2,20,c1+101010h
}


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;macros to display infos collected on compile
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro debugword s {
;display the value of the dword "s"
d0='0'+s shr 28 and 0fh
d1='0'+s shr 24 and 0fh
d2='0'+s shr 20 and 0fh
d3='0'+s shr 16 and 0fh
d4='0'+s shr 12 and 0fh
d5='0'+s shr 8 and 0fh
d6='0'+s shr 4 and 0fh
d7='0'+s and 0fh
if d0>'9'
        d0=d0+7
end if

if d1>'9'
        d1=d1+7
end if

if d2>'9'
        d2=d2+7
end if

if d3>'9'
        d3=d3+7
end if

if d4>'9'
        d4=d4+7
end if

if d5>'9'
        d5=d5+7
end if

if d6>'9'
        d6=d6+7
end if

if d7>'9'
        d7=d7+7
end if

display d0,d1,d2,d3,d4,d5,d6,d7,'h '
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro appsize a {
;display infos about output memory repartition
debugword ($)
display 'bytes total',crlf
debugword (a)
display 'bytes for system',crlf
s=$-a
debugword s
display 'bytes for application',crlf;
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro blocksize a,b {
;used at the end of the block to tell the sizeof
display b,".size=";
s=$-a
debugword s
display crlf
}
