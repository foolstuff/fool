macro Malloc s,d {
if d eq
        dd malloc,s,0,0,0
else
        dd malloc,s,d,0,0
end if
}

;alloc mem of size, set the ptr,
;and makes the dest point to it

malloc:
.call=0
.amount=4
.dest=8
.location=12
.blocs=16
        push esi
        invoke VirtualAlloc,0,\
               [esi+.amount],\
               MEM_COMMIT,\
               PAGE_READWRITE
        pop esi
        mov [esi+.location],eax
        mov ebx,[esi+.dest]
        or ebx,ebx
        je @f
        mov [ebx],eax
@@:
        mov eax,[esi+.amount]
        mov ebx,eax
        shr eax,16
        and ebx,0ffffh
        je @f
        inc eax
@@:
        mov [esi+.blocs],eax
        ret

macro Mfree s,p{
        dd mfree,s,0,p,0
}

mfree:
.call=0
.amount=4
.dest=8
.location=12
.blocs=16
        push esi
        invoke VirtualFree,\
               [esi+.location],\
               0,\
               MEM_RELEASE
        pop esi
        ret
