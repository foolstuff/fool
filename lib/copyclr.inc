copyclr:
.call=0
.dest=4
.src=8
.size=12
.c=16
        push esi edi eax ebx ecx
        mov ecx,[esi+.size]
        mov ecx,[ecx]
        dec ecx
        mov ebx,[esi+.dest]
        mov eax,[esi+.src]
        mov edi,[ebx]
        mov ebx,[esi+.c]
        mov esi,[eax]
.loop:
        mov eax,[esi+ecx*4]
        mov [esi+ecx*4],ebx
        mov [edi+ecx*4],eax
        dec ecx
        jnl .loop
        jmp .end
.end:
        pop ecx ebx eax edi esi
        ret

macro Copyclr d,s,n,c {
        dd copyclr,d,s,n,c
}