;transparent return
grab:
.call=0
.child=4
        push esi edi
;        push eax
        mov esi,[esi+.child]
        or edi,edi
        je @f
        call addxy
@@:
        mov ebx,mouse
        call onmouse
        jnc @f

        mov dword[mouse.ptr+cptr.ptr],mouse.hand
        cmp byte[mouse.dl],0
        jle .1
        mov [.grab+asm.op1],esi
.1:

;        jge .2
;        mov dword[.grab+asm.op1],0
;        mov dword[mouse.esi],0
;        mov dword[mouse.edi],0
;.2:

        cmp [.grab+asm.op1],esi
        jne @f
        cmp byte[mouse.l],1
        jne @f
        cmp dword[mouse.edi],esi
        jne .3

if ~defined NOALPHAGRAB
        call pixel.setalpha+4
end if
.3:
if defined _WINDOWED
else
        mov dword[mouse.esi],.grab
        mov dword[mouse.edi],esi
        mov dword[mouse.ptr+cptr.ptr],mouse.grab
end if
@@:
        or edi,edi
        je @f
        call subxy
@@:
        fcall

if ~defined NOALPHAGRAB
        call pixel.notalpha+4
end if

        pop edi esi
        ret

.grab:  Asm @f,0
@@:
        push eax
        mov eax,[mouse.dx]
        add [edi+box.x],eax
        mov eax,[mouse.dy]
        add [edi+box.y],eax
        pop eax
        ret

macro Grab child {
        dd grab,child
}