;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
put3d:
.call=0
.x=4
.y=8
.z=12
.c=16
.mesh=20
	push eax ecx esi
	mov eax,[esi+.x]
	sub [cam.x],eax
	mov eax,[esi+.y]
	sub [cam.y],eax
	mov eax,[esi+.z]
	sub [cam.z],eax
	mov eax,[esi+.c]
	mov [line32.c],al
	mov [colorp],al
	fcall [esi+.mesh]
	mov eax,[esi+.x]
	add [cam.x],eax
	mov eax,[esi+.y]
	add [cam.y],eax
	mov eax,[esi+.z]
	add [cam.z],eax
	pop esi ecx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
