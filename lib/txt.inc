txt:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.txt=24
.smooth=28
.font=32
        push eax edx ebp
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        call addxy
        mov ebp,[esi+.x]
        mov edx,[esi+.txt]
@@:
        mov al,[edx]
        or al,al
        je @f
        call .putc
        inc edx
        jmp @b
@@:
        pop dword [esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop ebp edx eax
.over:
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.putc:
;al=char
;esi=item
;ebp=base_x
;edx=string
        push eax ebx ecx edx edi
        cmp al,0ah
        je .0ah
        cmp al,0dh
        je .0dh
        cmp al,9
        je .tab
        movzx edi,al
        lea edi,[edi*3]
        shl edi,1
        add edi,[esi+.font]
        mov eax,[esi+.x]
        mov dl,6
.char:
        mov ebx,[esi+.y]
        mov dh,1
.line:
        xor ecx,ecx
        test byte[edi],dh
        je @f
        mov ecx,[esi+.c]
@@:
        or ecx,ecx
        je @f
        Pixel
@@:
        inc ebx
        shl dh,1
        jne .line
        inc eax
        inc edi
        dec dl
        jne .char
        add dword[esi+.x],6
.end:
        pop edi edx ecx ebx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.0ah:
        mov [esi+.x],ebp
        jmp .end
.0dh:
        add dword[esi+.y],9
        mov [esi+.x],ebp
        jmp .end
.tab:
        add dword[esi+.x],8*6
        jmp .end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
macro Txt x,y,xl,yl,c,t,f {
        dd txt,x,y,xl,yl,c,t,0
        if f eq
        dd font85
        else
        dd f
        end if
}
