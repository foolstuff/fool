macro Input b,cp {
      dd input,b,0
if cp eq
      dd qwerty; azerty
else
      dd cp
end if
      }


input:
.call=0
.buffer=4
.cursor=8
.codepage=12
        test al,80h
        jne .nomore
        mov ecx,[esi+.cursor]
        mov edx,[esi+.buffer]
        mov edi,[edx+buffer.ptr]
.fkeys:
        cmp al,key.bs
        je .bs
        cmp al,key.left
        je .left
        cmp al,key.right
        je .right
        cmp al,key.home
        je .home
        cmp al,key.end
        je .endl
        cmp al,key.del
        je .del
        cmp al,key.ins
        je .ins
        cmp al,key.enter
        je .enter
        movzx eax,al
        call .status
        mov ebx,[esi+.codepage]
        mov eax,[ebx+eax*4]
        or eax,eax
        je .nomore
        mov al,[eax]
        mov ah,[edi+ecx]
        mov [edi+ecx],al
        inc ecx
        or ah,ah
        jne @f
        mov [edi+ecx],ah
@@:
.caret:
        cmp ecx,[edx+buffer.size]
        jge .nomore
        mov [esi+.cursor],ecx
.nomore:
        ret
.left:
        dec ecx
        jl .home
        jmp .caret
.right:
        inc ecx
        cmp ecx,[edx+buffer.size]
        jge .endl
        jmp .caret
.home:
        xor ecx,ecx
        jmp .caret
.endl:
        mov ecx,[edx+buffer.size]
        dec ecx
        jmp .caret
.del:
.ins:
.enter:
        jmp .caret
.bs:
        dec ecx
        jnl @f
        xor ecx,ecx
@@:
        mov byte [edi+ecx],0
        jmp .caret
.status:
        mov bl,[key+key.shiftl]
        or bl,[key+key.shiftr]
        je @f
        mov ah,1
@@:
        cmp byte[key+key.altgr],0
        je @f
        mov ah,2
@@:
        shl al,1
        shr eax,1
        ret




macro _Input buffer,cursor,codepage {
      dd input,buffer,cursor,codepage
}

_input:
.call=0
.buffer=4
.cursor=8
.pagecode=12

        pushad

        mov edi,[esi+.buffer]
        movzx edx,word[edi+buffer.ptr]

        mov ecx,[esi+.cursor]

        jmp .exit?

.exit?:
        test byte[key+key.status],key.alt?
        jne .alt
        cmp al,key.bs
        je .bs
        cmp al,key.del
        je .del
        cmp al,key.right
        je .right
        cmp al,key.left
        je .left
        cmp al,key.end
        je .endl
        cmp al,key.home
        je .home
        test byte[key+key.status],key.ctrl?
        jne .ctrl
        mov bl,[key+key.status]
        or al,al
        jle .end
        cmp al,key.num7
        jge .numpad
        cmp byte[key+key.altgr],0
        je .shift
        add eax,2*128
        jmp .load
.numpad:
        test bl,key.numled
        jne .shift
        add eax,1*128
        jmp .load
.shift:
        test bl,key.shiftled + key.shift?
        je .load
        mov bh,bl
        shr bl,4
        shr bh,2
        and bh,1
        cmp bl,bh
        je .load
        add eax,1*128

.load:
        mov ebx,[esi+.pagecode]
        mov ebx,[ebx]
        mov ebx,[eax*4+ebx]
        or ebx,ebx
        je .end
.put:
        mov al,[ebx]
        or al,al
        je .end
        mov [edx+ecx],al
        cmp ecx,[edi+buffer.size]
        je @f
        inc ecx
        inc ebx
        jmp .put
@@:
        mov byte[ecx],0

.ctrl:
.alt:
.end:
        mov [esi+.cursor],ecx
        popad
        ret
.bs:
        mov ah,0
        cmp [edx+ecx],ah
        je @f
        mov ah,' '
@@:
        test byte[key+key.status],key.ctrl?
        jne .linebs
        dec ecx
        cmp ecx,0
        jl .reset
        mov [edx+ecx],ah
        cmp word[edx+ecx],0ah
        je @f
        cmp word[edx+ecx],0dh
        je @f
        jmp .normalbs
@@:
        mov [edx+ecx],ah
        dec ecx
        jnl .normalbs
        mov ecx,[edi+buffer.ptr]
.normalbs:
        mov [edx+ecx],ah
        jmp .end
.linebs:
@@:
        mov al,[edx+ecx]
        mov [edx+ecx],ah
        dec ecx
        cmp ecx,0
        jl .reset
        cmp al,0
        je @b
        cmp al,' '
        je @b
        cmp al,'/'
        je @b
        cmp al,'\'
        je @b
        cmp al,','
        je @b
        cmp al,':'
        je @b
        cmp al,'!'
        je @b
        cmp al,'?'
        je @b
        cmp al,';'
        je @b
        cmp al,'.'
        je @b
@@:
        mov al,[edx+ecx]
        cmp al,','
        je @f
        cmp al,'.'
        je @f
        cmp al,'!'
        je @f
        cmp al,'?'
        je @f
        cmp al,';'
        je @f
        cmp al,':'
        je @f
        cmp al,'\'
        je @f
        cmp al,'/'
        je @f
        cmp al,' '
        je @f
        mov [edx+ecx],ah
        cmp ecx,0
        je .end
        dec ecx
        jmp @b
@@:
        inc ecx
        jmp .end
.reset:
        mov ecx,0
        jmp .end
.del:
        cmp byte[edx+ecx],0
        je .end
        mov byte[edx+ecx],' '
        jmp .end
.right:
        cmp byte[edx+ecx],0
        je .end
        inc ecx
        cmp ecx,[edi+buffer.size]
        jl .end
        mov ecx,[edi+buffer.size]
;        dec ecx
        jmp .end
.left:
        dec ecx
        cmp ecx,0
        jge .end
        mov ecx,0
        jmp .end
.endl:
        cmp byte[edx+ecx],0
        je .end
        inc ecx
        cmp ecx,[edi+buffer.size]
        jge @f
        jmp .endl
@@:
        mov ecx,[edi+buffer.size]
;        dec ecx
        jmp .end
.home:
        mov ecx,0
        jmp .end
