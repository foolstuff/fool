fill:
.call=0
.amount=4
.location=8
        push edi
        mov ecx,[esi+.amount]
        mov edi,[esi+.location]
        shr ecx,2
        mov eax,White
        rep stosd
        pop edi
        ret

macro Fill a,l {
        dd fill,a,l
}
