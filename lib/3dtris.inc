;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
tris:
.call=0
.size=4
.list=8
.p1=0
.p2=4
.p3=8
	push eax ebx ecx edx esi edi
	mov edi,[esi+.size]
	shr edi,2
	sub edi,3
	jl .end
.loop:
	inc [mesh.ntris?]
	mov eax,[esi+.list+edi*4+.p1]
	mov ebx,[esi+.list+edi*4+.p2]
	mov ecx,[esi+.list+edi*4+.p3]
	shl eax,4
	shl ebx,4
	shl ecx,4
	mov edx,[gs:eax+dots.c]
	add edx,[gs:ebx+dots.c]
	shr edx,1
	add edx,[gs:ecx+dots.c]
	shr edx,1
	mov [.tri+tri.c],edx
	cmp dword[gs:eax+dots.z],BEHIND
	je .ignore
	cmp dword[gs:ebx+dots.z],BEHIND
	je .ignore
	cmp dword[gs:ecx+dots.z],BEHIND
	je .ignore
	cmp dword[gs:eax+dots.z],OUTOFVIEW
;        je .ignore  ;to ignore any triangle with point out of view
	jne @f
	cmp dword[gs:ebx+dots.z],OUTOFVIEW
;        je .ignore  ;partial triangle render
	jne @f
	cmp dword[gs:ecx+dots.z],OUTOFVIEW
;        je .ignore
	jne @f
	jmp .ignore
@@:
	mov edx,[gs:eax+dots.x]
	mov [.1+dots.x],edx
	mov edx,[gs:eax+dots.y]
	mov [.1+dots.y],edx
	mov edx,[gs:ebx+dots.x]
	mov [.2+dots.x],edx
	mov edx,[gs:ebx+dots.y]
	mov [.2+dots.y],edx
	mov edx,[gs:ecx+dots.x]
	mov [.3+dots.x],edx
	mov edx,[gs:ecx+dots.y]
	mov [.3+dots.y],edx
	fcall .tri
	adc [mesh.ntris!],0
.ignore:
	sub edi,3
	jnl .loop
.end:
	pop edi esi edx ecx ebx eax
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.tri	dd tri,.1,.2,.3,Red
.1	dd 0,0
.2	dd 0,0
.3	dd 0,0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
