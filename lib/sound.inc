sound:
.call = 0
.sound = 4
.IDLE=0
.PLAY=1
.LOOP=2

.loop:
        cmp [.state],.LOOP
        je @f
        invoke PlaySoundA,dword[esi+.sound],NULL,SND_FILENAME+SND_LOOP+SND_ASYNC
        mov [.state],.LOOP
@@:
        ret
.play:
        cmp [.state],.PLAY
        je @f
        invoke PlaySoundA,dword[esi+.sound],NULL,SND_FILENAME+SND_ASYNC
        mov [.state],.PLAY
@@:
        ret
.stop:
        cmp [.state],.IDLE
        je @f
        invoke PlaySoundA,0,NULL,0
        mov [.state],.IDLE
@@:
        ret

.state  db 0


macro soundLoop s {
        dd sound.loop,s
}

macro soundPlay s {
        dd sound.play,s
}

macro soundStop {
        dd sound.stop
}
