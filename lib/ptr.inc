;linear pointer
lptr:
.off=0
.size=4

;fool pointer
fptr:
_ptr:
.call=0
.ptr=4
        fcall [esi+.ptr]
        ret

macro Ptr p {
        dd _ptr,p
}

macro Fptr p {
        dd fptr,p
}

;cptr
;Caller pointer
;provide a level of indirection with a modifiable pointer to any item.
;with a data in eax
;mycptr: Cptr myitem,data

cptr:
.call=0
.ptr=4
.dat=8
        mov eax,[esi+.dat]
        fcall [esi+.ptr]
        ret

macro Cptr p,d {
        dd cptr,p,d
}

;graphic pointer

gptr:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.ptr=20
        push esi edi
        or edi,edi
        je @f
        call addxy
@@:
        mov edi,esi
        fcall [esi+.ptr]
        pop edi esi
        or edi,edi
        je @f
        call subxy
@@:
        ret

macro Gptr x,y,p {
        dd gptr,x,y,0,0,p
}
