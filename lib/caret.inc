macro Caret x,y,xl,yl,c,i,t {
      dd caret,x,y,xl,yl,c,i,t
      }

caret:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.input=24
.txt=28
        push eax ebx ecx
        mov ebx,[esi+.x]
        mov ecx,[esi+.y]
        push ebx ecx
        mov eax,[esi+.input]
        mov ebx,[eax+input.cursor]
        imul ebx,6
        mov eax,[esi+.txt]
        add ebx,[eax+txt.x]
;        add ecx,[eax+txt.y]
;        mov dword[esi+.yl],8
;        mov dword[esi+.xl],6
        dec ebx
        add [esi+.x],ebx
;        add [esi+.y],ecx
        jne @f
;        add dword[esi+.y],9
;        mov dword[esi+.yl],1
@@:
        call box
        pop ecx ebx
        mov [esi+.x],ebx
        mov [esi+.y],ecx
        pop ecx ebx eax
        ret