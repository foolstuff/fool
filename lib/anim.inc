;transparent return
anim:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.ptr=20
.cnt=24
.time=28
.node=32

        push eax ebx ecx edx esi edi
        call addxy
        mov edi,esi
        mov eax,[esi+.ptr]
        mov esi,[esi+.node]
        mov ecx,[esi+node.size]
        push ecx
        or esi,esi
        je @f
        mov esi,[esi+node.list+eax]
        call caller
@@:
        pop ecx
        pop edi esi
        mov eax,[irq0.inc]
        mov ebx,[esi+.time]
        add [esi+.cnt],eax
        cmp [esi+.cnt],ebx
        jl @f
        sub [esi+.cnt],ebx
        call .next
@@:
        call subxy
        pop edx ecx ebx eax
        ret
.next:
        add dword[esi+.ptr],4
        cmp [esi+.ptr],ecx
        jl @f
        mov dword[esi+.ptr],0
@@:
        ret


macro Anim x,y,xl,yl,t,n {
      dd anim,x,y,xl,yl,0,0,t,n
}