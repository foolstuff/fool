;transparent return
hit:
.call=0
.func=4
.size=8
.data=12
.x=4
.y=8
.xl=12
.yl=16

;works only for graphics items in the same container, or at least the same fcall level.
        push eax ebx ecx edx esi edi
        mov ecx,[esi+.size]
        shr ecx,2
        jl .end
.next:
        push ecx esi edi
        mov esi,[esi+ecx*4+.data]
.xx:
        mov eax,[esi+.x]
        mov ebx,[esi+.xl]
        mov ecx,[edi+.x]
        mov edx,[edi+.xl]
        cmp edx,0
        jge @f
        add ecx,edx
        neg edx
@@:
        sub eax,ecx
        jl @f
        sub eax,edx
        jge .not
        jmp .yy
@@:
        add eax,ebx
        jle .not
.yy:
        mov eax,[esi+.y]
        mov ebx,[esi+.yl]
        mov ecx,[edi+.y]
        mov edx,[edi+.yl]
        cmp edx,0
        jge @f
        add ecx,edx
        neg edx
@@:
        sub eax,ecx
        jl @f
        sub eax,edx
        jge .not
        jmp .ok
@@:
        add eax,ebx
        jle .not
.ok:
        pop edi esi ecx
        mov esi,[esi+.func]
        call caller
        jmp .end
.not:
        pop edi esi ecx
        dec ecx
        jnl .next

.end:
        pop edi esi edx ecx ebx eax
        ret
