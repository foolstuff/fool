circle:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.res=24

.c2     equ ss:esp+16
.pi180  equ ss:esp+12
.angle  equ ss:esp+8
.sxl    equ ss:esp+4
.syl    equ ss:esp+0

        push eax ebx ecx edx
        push dword[esi+.x] dword[esi+.y]
;        mov edx,[esi+.xl]
;        add edx,[esi+.yl]
;        jnl @f
;        neg edx
;@@:
;        mov eax,edx
;        cdq
;        mov ecx,[esi+.res]
        mov edx,[esi+.res]
        cmp edx,2
        jnl @f
        mov edx,64
@@:
;        idiv ecx
;
;        mov edx,eax
;@@:
;        cmp edx,1
;        jnl @f
;        mov edx,1
;@@:

        ;shr edx,1
        push dword 2
        push edx
        shl edx,1
        ;pi/180
        mov eax,[esi+.xl]
        mov ebx,[esi+.yl]
        sar eax,1
        sar ebx,1
        add [esi+.x],eax
        add [esi+.y],ebx
        mov ecx,[esi+.xl]
        sar ecx,1
        mov ebp,[esi+.y]
        add ecx,[esi+.x]
        finit                   ; EMPTY

.iterate:

        push edx
        ;.angle
        push dword[esi+.xl] dword[esi+.yl]
        ;.sxl,.syl
        fldpi                   ; pi
        fimul dword[.c2]
        fimul dword[.angle]   ; pi*angle
        fidiv dword[.pi180]     ; /180
        fsincos                 ; cos       ; sin
        fimul dword[.sxl]   ; cos*xl    ; sin
        fistp dword[.sxl]   ; sin
        fimul dword[.syl]   ; sin*yl
        fistp dword[.syl]   ; EMPTY

        pop ebx eax

        sar eax,1
        sar ebx,1

        push dword[esi+.x] dword[esi+.y]
        push dword[esi+.xl] dword[esi+.yl]

        mov [esi+.xl],eax
        mov [esi+.yl],ebx

        add eax,[esi+.x]
        add ebx,[esi+.y]

        push eax ebx
        ;xor dword[esi+.c],0xffffff
if defined RAYS
        call line ;ray
end if
        mov [esi+.x],eax
        mov [esi+.y],ebx
        sub ecx,eax
        sub ebp,ebx
        mov [esi+.xl],ecx
        mov [esi+.yl],ebp
        ;xor dword[esi+.c],0xffffff
        call line ;outline
        pop ebp ecx
        pop dword[esi+.yl] dword[esi+.xl]
        pop dword[esi+.y] dword[esi+.x]
        pop edx
        dec edx
        jnl .iterate

        pop edx edx
        pop dword[esi+.y] dword[esi+.x]
        pop edx ecx ebx eax
        ret

macro Circle x,y,xl,yl,c,r=60 {
        dd circle,x,y,xl,yl,c
       if r eq
        dd 60
       else
        dd r
       end if
}