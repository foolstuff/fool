;transparent return asm call
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
palette:
.call=0
.pal=4
.r=0
.g=1
.b=2
.a=3
.flashy:
        push eax ebx edx edi esi
        mov edi,[esi+.pal]
        xor eax,eax
@@:
        call @f
        inc ah
        jne @b
        pop esi edi edx ebx eax
        ret
@@:
        mov al,ah
        mov bh,al
        mov bl,al
        and bh,0fh
        shl bh,1
        mov bl,0
        test al,20h
        je @f
        mov bl,bh
        test al,10h
        je @f
        add bl,20h
@@:
        mov al,bl
        shl al,2
        mov [edi+.r],al
        mov al,ah
        mov bh,al
        mov bl,al
        and bh,0fh
        shl bh,1
        mov bl,0
        test al,40h
        je @f
        mov bl,bh
        test al,10h
        je @f
        add bl,20h
@@:
        mov al,bl
        shl al,2
        mov [edi+.g],al
        mov al,ah
        mov bh,al
        mov bl,al
        and bh,0fh
        shl bh,1
        mov bl,0
        test al,80h
        je @f
        mov bl,bh
        test al,10h
        je @f
        add bl,20h
@@:
        mov al,bl
        shl al,2
        mov [edi+.b],al
        mov byte[edi+.a],0
        add edi,4
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.grey:
        push eax edx edi
        mov edi,[esi+.pal]
        xor eax,eax
@@:
        mov al,ah
        mov [edi+.r],al
        mov [edi+.g],al
        mov [edi+.b],al
        mov byte[edi+.a],0
        add edi,4
        inc ah
        jne @b
        pop edi edx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.pinky:
        push eax edx edi
        mov edi,[esi+.pal]
        xor eax,eax
@@:
        mov al,ah
        mov [edi+.g],al
        mov [edi+.b],al
        add al,30h

        mov [edi+.r],al
        mov byte[edi+.a],0
        add edi,4
        inc ah
        jne @b
        pop edi edx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.rgbw:
        push eax edx edi
        mov edi,[esi+.pal]
        xor eax,eax
.Rgbw:
        mov al,ah
        mov [edi+.r],al
        mov byte[edi+.g],0
        mov byte[edi+.b],0
        mov byte[edi+.a],0
        add edi,4
        add ah,4
        jne .Rgbw
.rGbw:
        mov al,ah
        mov byte[edi+.r],0
        mov [edi+.g],al
        mov byte[edi+.b],0
        mov byte[edi+.a],0
        add edi,4
        add ah,4
        jne .rGbw
.rgBw:
        mov al,ah
        mov byte[edi+.r],0
        mov byte[edi+.g],0
        mov [edi+.b],al
        mov byte[edi+.a],0
        add edi,4
        add ah,4
        jne .rgBw
.rgbW:
        mov al,ah
        mov [edi+.r],al
        mov [edi+.g],al
        mov [edi+.b],al
        mov byte[edi+.a],0
        add edi,4
        add ah,4
        jne .rgbW
        pop edi edx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.read:
        push eax edx edi
        xor eax,eax
        mov dx,3c7h
        out dx,al
        mov dx,3c9h
        mov edi,[esi+.pal]
@@:
        in al,dx
        shl al,2
        mov [edi+.r],al
        in al,dx
        shl al,2
        mov [edi+.g],al
        in al,dx
        shl al,2
        mov [edi+.b],al
        add edi,4
        inc ah
        jne @b
        pop edi edx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.write:
        push eax edx edi
        xor eax,eax
        mov dx,3c8h
        out dx,al
        mov dx,3c9h
        mov edi,[esi+.pal]
@@:
        mov al,ah
        mov al,[edi+.r]
        shr al,2
        out dx,al
        mov al,[edi+.g]
        shr al,2
        out dx,al
        mov al,[edi+.b]
        shr al,2
        out dx,al
        add edi,4
        inc ah
        jne @b
        pop edi edx eax
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
