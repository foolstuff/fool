;WXL, WYL, WX, WY are constants created at runtime.
;they are the size and position of the screen.
VESAMODE=XGA
;FOOLOS:
NOBORDER = 1
EXITTITLE equ 'ColorMap'
EXITMESSAGE equ 'Good bye'
TOPMOST equ 1
;SHOWFPS equ 1

WINDOW equ 100,100,colorMap.WXL,colorMap.WYL

;WINDOWED = 1
;WX = 100
;WY = 100
;WXL = colorMap.WXL
;WYL = colorMap.WYL

include "../../sys/fool.inc"

colorMap:
.CNT=150
.CXL=120
.CYL=50
.GXL=10
.GYL=15
.WXL=.CXL*.GXL
.WYL=.CYL*.GYL

        Gnode 0,0,0,0,.frame,.grid
.frame: Frame 0,0,.WXL,.WYL,Black
.cell:  Gnode 0,0,.CXL,.CYL,\
        .popup,.border,.box
.popup: Zone 0,0,.CXL,.CYL,next
        Node .name,.txt,pixel.notalpha,.back,pixel.setalpha,.hex
.border:Border 0,0,.CXL,.CYL,0
.box:   Box 0,0,.CXL,.CYL,0
.txt:   Txt 2,5,0,0,White,.str
.back:  Box 0,0,.CXL-10,30,Black
.hex:   dd num.hex,colors+list.data,.str,8
.str:   db '00000000',0
.name:  Vtxt 1,10,0,0,White,0
.grid:  Asm next
        mov eax,.WXL-.CXL
        mov ebx,.WYL-.CYL
        mov edx,[colors+list.size]
        shr edx,2
.loop:
        dec edx
        jl .end
        mov [.cell+box.y],ebx
@@:
        mov [.cell+box.x],eax
        lea ecx,[colors+list.data+edx*4]
        mov [.hex+num.ptr],ecx
        mov ecx,[ecx]
        cmp ecx,-1
        jne .notrnd
        add ecx,[irq0.cnt]
        ror ecx,cl
        xor cl,ch
        rol ecx,cl
.notrnd:
        mov [.box+box.c],ecx
        mov ecx,[colors.names+list.data+edx*4]
        mov [.name+vtxt.txt],ecx
        pushad
        fcall .cell
        popad
        sub eax,.CXL
        jge .loop
        mov eax,.WXL-.CXL
        sub ebx,.CYL
        jmp .loop
.end:
        ret
