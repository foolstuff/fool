WINDOW equ 100,100,900,600
BACKGROUND = Black

include 'sys/fool.inc'

Fptr treeview

treeview:
        Node typeview,fieldview,modelview,modellog

modelview:
.X=0
.Y=0
.XL=70
.YL=15
        Gnode .X,.Y,.XL,.YL,.frame,.title,.bar
.bar:   Box 0,0,.XL,.YL,CosmicLatte
.frame: Frame 0,0,.XL,.YL,Red
.title: Vtxt 5,0,0,0,Black,@f
@@:     db 'modelview',0

fieldview:
.X=100
.Y=0
.XL=70
.YL=15
        Gnode .X,.Y,.XL,.YL,.frame,.title,.bar
.bar:   Box 0,0,.XL,.YL,CosmicLatte
.frame: Frame 0,0,.XL,.YL,Red
.title: Vtxt 5,0,0,0,Black,@f
@@:     db 'fieldview',0

typeview:
.X=200
.Y=0
.XL=70
.YL=15
        Gnode .X,.Y,.XL,.YL,.frame,.title,.bar
.bar:   Box 0,0,.XL,.YL,CosmicLatte
.frame: Frame 0,0,.XL,.YL,Red
.title: Vtxt 5,0,0,0,Black,@f
@@:     db 'typeview',0

modellog:
.X=10
.Y=20
.XL=300
.YL=15
        Gnode .X,.Y,.XL,.YL,.txt,.frame,.title,.bar
.bar:   Box 0,0,.XL,.YL,CosmicLatte
.frame: Frame 0,0,.XL,.YL,Red
.title: Vtxt 5,0,0,0,Black,@f
@@:     db 'model.log',0
.txt:   Txt 0,.YL,0,0,Green,modelstring

modelstring:
file 'model.log'
db 0
