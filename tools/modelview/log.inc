CRLF equ 13,10

virtual as 'log'
  LOG::
end virtual

macro log str& {
  virtual LOG
   db str
  end virtual
}

macro log.bit b{
}

macro log.nibble n{
d='0'+n and 0fh
if d>'9'
d=d+7
end if
log d
}

macro log.byte b{
log.nibble ((b shr 4) and 0fh)
log.nibble (b and 0fh)
}

macro log.word w{
log.byte ((w shr 8) and 0ffh)
log.byte (w and 0ffh)
}

macro log.dword d{
log.word ((d shr 16) and 0ffffh)
log.word (d and 0ffffh)
}

macro log.txt t{
local .s,.a
load .a byte from t
while .a <> 0
log .a
load .a byte from t+%
end while
}

