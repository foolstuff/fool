macro _type length,name {
        log.dword $
        log ':'
        log name,' '
        log.dword length
        log CRLF

        dd length
        db name,0
}

_int    equ 0x0
_float  equ 0x1000
_strz   equ 0x2000        ;null ending string
_strs   equ 0x4000        ;sized string
_ptr equ 0x8000         ;pointers
_fooltype equ 0xF0000000;fool model type

_null:  _type 0,'null'
_bit:   _type 1,'bit'
_nibble:_type 4,'nibble'
_byte:  _type 8,'byte'
_word:  _type 16,'word'
_dword: _type 32,'dword'
_qword: _type 64,'qword'
_tword: _type 80,'tword'
_dqword:_type 128,'dqword'

_f16:   _type _float+16,'f16'
_f32:   _type _float+32,'f32'
_f64:   _type _float+64,'f64'
_f80:   _type _float+80,'f80'

_ptr1:    _type _ptr+1,'ptr1'
_ptr4:    _type _ptr+4,'ptr4'
_ptr8:    _type _ptr+8,'ptr8'
_ptr16:   _type _ptr+16,'ptr16'
_ptr32:   _type _ptr+32,'ptr32'
_ptr64:   _type _ptr+64,'ptr64'
_ptr128:  _type _ptr+128,'ptr128'
_ptrf16:  _type _ptr+_float+16,'ptrf16'
_ptrf32:  _type _ptr+_float+32,'ptrf32'
_ptrf64:  _type _ptr+_float+64,'ptrf64'
_ptrf80:  _type _ptr+_float+80,'ptrf80'

_string:_type _strz+8,'string'
_bits:  _type _strs+1,'bitfield'
_ustr:  _type _strz+16,'ustring'
_list:  _type _strs+32,'list'

_0 equ _null
_1 equ _bit
_4 equ _nibble
_8 equ _byte
_16 equ _word
_32 equ _dword
_64 equ _qword
_128 equ _dqword

