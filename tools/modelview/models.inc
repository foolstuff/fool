macro _model name,[field] {
common
        log.dword $
        log ':'
 local .a,.n,.s
.n:
        db name,0
        log name,'['
.a:
forward
        dd field
common
.s = $-.a
        dd .s
        dd .n
repeat (.s)/4
load a dword from .a+(%-1)*4
load s dword from a
        log.txt a+4
        log '('
        log.txt s+4
        log '),'
end repeat

        log.dword .s
        log ']',CRLF
}

_node:    _model 'node',_f,_list32
_gnode:   _model 'gnode',_f,_2d,_list32
_box:     _model 'box',_f,_2d,_color32
_frame:   _model 'frame',_f,_2d,_color32
_circle:  _model 'circle',_f,_2d,_color32
_border:  _model 'border',_f,_2d,_color32
_folder:  _model 'folder',_f,_txtptr,_list32
_file:    _model 'file',_f,_txtptr,_list8
_bmptr_32:_model 'bmptr32',_f,_graphix,_bmptr32
_txt:     _model 'txt',_f,_graphix,_txtptr,_zoom,_fontptr

