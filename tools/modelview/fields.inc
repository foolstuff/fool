macro _field type,name {
        log.dword $
        log ':'
        log name,'('
        log.txt type+4
        ;log.dword type
        log ')',CRLF

        dd type
        db name,0
}

_f:     _field _ptr32,'func'
_x:     _field _dword,'x'
_y:     _field _dword,'y'
_xl:    _field _dword,'xl'
_yl:    _field _dword,'yl'
_color8:_field _byte,'color8'
_color16:_field _word,'color16'
_color24:_field _dword,'color24'
_color32:_field _dword,'color32'
_size:  _field _dword,'size'
_data8: _field _byte,'data8'
_data32: _field _dword,'data32'
_txtptr: _field _ptr8,'txtptr'
_dataptr8:_field _ptr8,'dataptr8'
_bmptr: _field _ptr8,'bmptr'
_bmptr32: _field _ptr32,'bmptr32'
_dataptr: _field _ptr32,'dataptr'
_scale:   _field _dword,'scale'
_zoom:    _field _dword,'zoom'
_fontptr: _field _ptr32,'fontptr'

_2d equ _x,_y,_xl,_yl
_list8 equ _size,_data8
_list32 equ _size,_data32
_graphix equ _2d,_color32

