if definite __MODEL

 include 'log.inc'
 log "types:",CRLF
 include 'types.inc'
 log "fields:",CRLF
 include 'fields.inc'

end if

log "models:",CRLF
include 'models.inc'
