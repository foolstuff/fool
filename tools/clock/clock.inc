clock:
.x=0
.y=0
.xl=75
.yl=50
.scale=3
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        Gnode .x,.y,.xl*.scale+2,.yl*.scale/2+2,\
        .calendar,.clock,@f
@@:     Frame 0,0,.xl*.scale+2,.yl*.scale/2+2,Blue
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.clock: Gnode 0,0,.xl*.scale,.yl*.scale,\
        .text,.hhex,.mhex,.shex,.time
.time   dd time,00112233h
.hhex   dd num.hex,.time+6,.hasc,2
.mhex   dd num.hex,.time+5,.masc,2
.shex   dd num.hex,.time+4,.sasc,2
.hasc   db '  :'
.masc   db '  :'
.sasc   db '  '
        db 0
        align 4
.text:  Vtxt 4,3,0,0,Silver,.hasc,.scale,ugly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.calendar:
        Gnode 0,10*.scale,.xl*.scale,.yl*.scale,\
        .text1,.Dhex,.Mhex,.Yhex,.date
.date   dd date,00002000h
.Dhex   dd num.hex,.date+7,.Dasc,2
.Mhex   dd num.hex,.date+6,.Masc,2
.Yhex   dd num.hex,.date+4,.Yasc,4
.Dasc   db '  /'
.Masc   db '  /'
.Yasc   db '    '
        db 0
        align 4
.text1: Vtxt 4,3,0,0,Silver,.Dasc,.scale,ugly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
