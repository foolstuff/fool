memcopylist:
	List \
	test1,\
	test2,\
	test3,\
	test4,\
	test5
Y=100h
align 16
X:
rd Y*2

@@: db 'test1',0
align 4
dd test1.size
dd @b
test1:
     xor eax,eax
     mov edi,X
     mov ecx,Y
@@:  stosd
     loop @b
.size=$-test1
     mov eax,0
     ret
;------------------------
@@: db 'test2',0
align 4
dd test2.size
dd @b
test2:
     xor eax,eax
     movd mm0,eax
     mov edi,X
     mov ecx,Y/2
@@:  movq [es:edi],mm0
     add edi,8
     loop @b
.size= $-test2
     mov eax,0
     ret
;------------------------

@@: db 'test3',0
align 4
dd test3.size
dd @b
test3:
     xor eax,eax
     mov edi,X
     mov ecx,Y
     rep stosd
.size=$-test3
     mov eax,0
     ret
;------------------------

@@: db 'test4',0
align 4
dd test4.size
dd @b
test4:
     xor eax,eax
     mov edi,X
     mov ecx,Y
@@:  mov [es:edi],eax
     add edi,4
     loop @b
.size=$-test4
     mov eax,0
     ret
;------------------------
@@: db 'test5',0
align 4
dd test5.size
dd @b
test5:
     pxor xmm0,xmm0
     mov edi,X
     mov ecx,Y/4
@@:  movntdq [es:edi],xmm0
     add edi,16
     loop @b
.size=$-test5
     mov eax,0
     ret
