	 ror al,4
	 xor	ah,ah		       ; can we assume ah=0 ?
	 mov	dx,ax		       ; could push but the stack is technically a memory op, isn't it? 
	 mov	bx,3030h	       ; ascii offset 
	 mov	cx,110ah	       ; ch=17, cl=10 
	 shr	al,4		       ; upper hex 
	 div	cl		       ; result = 0 for 0 to 9, 1 for A to F 
	 add	bh,ah		       ; add remainder to result BX, 0 to 9 = 0 to 9, A to F = 0 to 5 
	 mul	ch		       ; for 0 to 9 = 0, for A to F add +17 ascii offset 
	 add	bh,al		       ; add to result 
	 mov	ax,dx		       ; this could have been a pop if memory op allowed 
	 and	al,0fh		       ; lower hex 
	 div	cl		       ; as above 
	 add	bl,ah		       ; 
	 mul	ch		       ; 
	 add	bl,al		       ; BX = hex ascii of AL 
	 mov ax,bx