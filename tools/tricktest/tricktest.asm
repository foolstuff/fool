WINDOWED equ 1
ONTOP equ 1
WX = 200
WY = 200
WXL = 320
WYL = 200


include '../../sys/fool.inc'

macro Sample d,t,c,z {
        dd sample,d,t,c,z
}

tricktest:
.x=0
.y=0
.xl=WXL
.yl=WYL
        Gnode .x,.y,.xl,.yl,\
        .app,\
        .fxdec1,\
        .fxdec,\
        .acquire1,\
        .acquire0,\
        .asm,\
        .keys
.number:
        dd 0
.sel:
        dd 0
.name:
        db 'TrickTest',0
.app:   Node \
        .label,\
        .time,\
        .size,\
        .txtin,\
        .txtout,\
        .scopes

.label: Txt 10,2,0,0,Blue,.name
.time:  Txt 10,15,0,0,Green,.ticks
.size:  Txt 80,15,0,0,Purple,.bytes
.txtin: Txt 10,40,0,0,Olive,.input
.txtout:Txt 10,50,0,0,Red,.hex
.scopes: Gnode 70,30,64,32,.frame,.scope0,.scope1
.frame: Frame 0,0,64,32,Navy

.scope0:Plotd 0,0,64,32,Red,.history0,0,15,1,1
.scope1:Plotd 0,0,64,32,Blue,.history1,0,15,1,1


.keys:  Node .k1,.k2,.k3,.k4
.k1:    Otk key.up,.decsel
.k2:    Otk key.down,.incsel
.k3:    Tmk key.right,.incnum
.k4:    Tmk key.left,.decnum

.incnum:
        Asm incdw,.number
.decnum:
        Asm decdw,.number
.decsel:
        Asm decsel,.sel,.list
.incsel:
        Asm incsel,.sel,.list

.fxdec: dd fxdec,.clocks+4,.ticks,.ticksz
.fxdec1:dd fxdec,.fsize,.bytes,.bytez
.asm:   Asm .monitor
.input: db 'in: '
.inputh:db '00',0
.hex:   db 'out: '
.numhex:db '00',0
.ticks: db '1234'
.ticksz:db ' clocks',0
.bytes: db '123'
.bytez: db ' bytes',0
.monitor:
        push eax esi edi
        call .dispname
        mov al,[.number]
        call edfed
        mov [.numhex],ax
        call word [.list+list.data+ebx]
        call .func
        ;fcall .acquire0
        ;mov dword[mouse.item],0   ;disable mouse
        pop edi esi eax
        ret
.func:
        push ebx
        cli
        ;push eax
        ;call .rdtsc0
        ;call edfed
        ;call .rdtsc1
        ;pop eax
        mov ebx,[.sel]
        mov al,[.number]
        call .rdtsc0
        call dword [.list+list.data+ebx]
        call .rdtsc1
        mov [.inputh],ax
        sti
        call .mean0
        pop ebx
        ret
.rdtsc0:
        push eax edx
        rdtsc
        mov [.clocks],eax
        pop edx eax
        ret
.rdtsc1:
        push eax edx
        rdtsc
        sub eax,[.clocks]
        mov [.clocks],eax
        pop edx eax
        ret
.mean0:
        push eax
        mov eax,[.clocks]
        add [.clocks+12],eax
        inc byte[.clocks+8]
        cmp byte[.clocks+8],64
        jne @f
        mov byte[.clocks+8],0
        mov eax,[.clocks+12]
        mov dword[.clocks+12],0
        shr eax,6
        mov [.clocks+4],eax
@@:
        pop eax
        ret
.clocks:
        dd 0,0,0,0
.fsize: dd 0

.history0:
        rd 64
.history1:
        rd 64
.history2:
        rd 64

.acquire0: Sample .clocks,.history0,63,4
.acquire1: Sample .clocks+4,.history1,63,4

.dispname:
        mov eax,[.sel]
        mov eax,[eax+.list+list.data]
        mov ebx,[eax-8]
        mov eax,[eax-4]
        mov [.label+txt.txt],eax
        mov [.fsize],ebx
        ret



;;;;;;;;;;;;;;;;;;;;;;
align 4
.list:
include 'b2hlist.inc'
include 'h2b-wht36.inc'

sample:
.call=0
.src=4
.dest=8
.count=12
.zoom=16
        push eax ebx ecx
        mov ebx,[esi+.dest]
        mov ecx,[esi+.count]
@@:
        mov eax,[ebx+4]
        mov [ebx],eax
        add ebx,4
        loop @b
        mov eax,[esi+.src]
        mov eax,[eax]
        mov cl,[esi+.zoom]
        call .scale
        mov [ebx],eax
        pop ecx ebx eax
        ret
.scale:
        shr eax,cl
        ret
