b2hlist:
        List \;        dd empty,\
        alphonso,\
        bitrake0,\
        bitrake,\
        bitshifter,\
        edfed,\
        loco0,\
        loco,\
        mhajduk0,\
        mhajduk
;;;;;;;;;;;;;;;;;;;;;;


@@:   db 'empty',0
align 4
dd empty.size
dd @b
empty:
;        mov ax,'xx'
.size=$-empty
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'bitRAKE',0
align 4
dd bitrake.size
dd @b
bitrake:
include 'B2H-bitrake.inc'
.size=$-bitrake
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'bitRAKE 0',0
align 4
dd bitrake0.size
dd @b
bitrake0:
include 'B2H-bitrake0.inc'
.size=$-bitrake0
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'Alphonso',0
align 4
dd alphonso.size
dd @b
alphonso:
include 'B2H-Alphonso.inc'
.size=$-alphonso
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'bitshifter',0
align 4
dd bitshifter.size
dd @b
bitshifter:
include 'B2H-Bitshifter.inc'
.size=$-bitshifter
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'LocoDelAssembly',0
align 4
dd loco.size
dd @b
loco:
include 'B2H-loco.inc'
.size=$-loco
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'LocoDelAssembly 0',0
align 4
dd loco0.size
dd @b
loco0:
include 'B2H-loco0.inc'
.size=$-loco0
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'edfed',0
align 4
dd edfed.size
dd @b
edfed:
include 'B2H-edfed.inc'
.size=$-edfed
        ret
;;;;;;;;;;;;;;;;;;;;;;

@@:   db 'MHajduk',0
align 4
dd mhajduk.size
dd @b
mhajduk:
include 'B2H-MHajduk.inc'
.size=$-mhajduk
        ret
;;;;;;;;;;;;;;;;;;;;;;
@@:   db 'MHajduk 0',0
align 4
dd mhajduk0.size
dd @b
mhajduk0:
include 'B2H-MHajduk0.inc'
.size=$-mhajduk0
        ret
;;;;;;;;;;;;;;;;;;;;;;
