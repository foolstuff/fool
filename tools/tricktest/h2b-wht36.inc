;33 bytes (28 bytes if 16 bit); converts AX hex code to AL, AH zero if valid conversion
h2b_wht36:
	xchg al,ah
	call .hex	    ;convert high nibble
	xchg al,ah
	call .hex	    ;convert low nibble
	rol al,4	    ;move low nibble next to high nibble
	ror ax,4	    ;bring both nibbles into AL and move everything else into AH
	ret
.hex:
	sub al,'9'+1	    ;Is it a number
	jb .num
	or al,'a'-'A'	    ;Coerce to lowercase
	sub al,'a'-('9'+1)  ;Is it a letter
	jb .ret
.num:
	add al,10	    ;Convert to binary
.ret:
	ret