include 'sys/fool.inc'

macro Resolution xl,yl,color,name {
local .f,.b,.t,.s
        Node .t,.f
.f:     Frame 0,0,xl,yl,color
.b:     Border 0,0,xl,yl,0
.t:     Vtxt xl-100,yl-15,0,0,color,.s
.s:     db name,0
}

screens:
        Gnode 0,0,4000,3000,pixel.notalpha,\
        .8_5,.cga,.wxga,.wsxgap,.wuxga,.wqxga,\
        .4_3,.qvga,.vga,.pal,.svga,.xga,.sxgap,.uxga,.qxga,.edfed,\
        .5_4,.sxga,.qsxga,\
        .5_3,.wvgap,.wxgap,\
        .16_9,.hd480,.hd720,.hd900,.hd1080,\
        pixel.setalpha
.name:  db 'screen resolutions',0

.8_5:   Line 0,0,3999,2499,Purple
.cga:   Resolution 320,200,Purple,'320*200 cga'
.wxga:  Resolution 1280,800,Purple,'1280*800 wxga'
.wsxgap:Resolution 1600,1000,Purple,'1600*1000 wsxgap'
.wuxga: Resolution 1920,1200,Purple,'1920*1200 wuxga'
.wqxga: Resolution 2560,1600,Purple,'2560*1600 wqxga'

.4_3:   Line 0,0,3999,2999,Red
.qvga:  Resolution 320,240,Red,'320*240 qvga'
.vga:   Resolution 640,480,Red,'640*480 vga'
.pal:   Resolution 768,576,Red,'768*576 pal'
.svga:  Resolution 800,600,Red,'800*600 svga'
.xga:   Resolution 1024,768,Red,'1024*768 xga'
.sxgap: Resolution 1400,1050,Red,'1400*1050 sxgap'
.uxga:  Resolution 1600,1200,Red,'1600*1200 uxga'
.qxga:  Resolution 2048,1536,Red,'2048*1536 qxga'
.edfed: Resolution 4000,3000,Red,'4000*3000 edfed'

.5_4:   Line 0,0,3749,2999,Olive
.sxga:  Resolution 1280,1024,Olive,'1280*1024 sxga'
.qsxga: Resolution 2560,2048,Olive,'2560*2048 qsxga'

.5_3:   Line 0,0,3999,2399,Gold
.wvgap: Resolution 800,480,Gold,'800*480 wvgap'
.wxgap: Resolution 1280,768,Gold,'1280*768 wxgap'

.16_9:  Line 0,0,3999,2249,Blue
.hd480: Resolution 854,480,Blue,'854*480 hd480'
.hd720: Resolution 1280,720,Blue,'1280*720 hd720'
.hd900: Resolution 1600,900,Blue,'1600*900 hd900'
.hd1080:Resolution 1920,1080,Blue,'1920*1080 hd1080'
