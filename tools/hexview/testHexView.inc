include 'includeTree.inc'

;FOOLOS:
;VESAMODE=11fh;WXGA;VESA800
;SHOWFPS:
BACKGROUND=RandomColor
WINDOW equ 100,100,710,490
TITLE equ 'HexView'

include 'sys/fool.inc'

APPLICATION

hexview:
.COLUMNS=32
.LINES=50
.LOWLIM=BINARYSTART-1000h
.UPLIM=FOOLEND
.PTR=IMPORTSECTION
.OFF=0

debugword .LOWLIM
debugword FOOLAPP
debugword .UPLIM

        Node .bmpview,.hexview,.bmpfocus,.scroll,.keys
.scroll:
        Asm next
        mov eax,[mouse.dz]
        imul eax,.COLUMNS
        lea eax,[eax*3]
        add [.off],eax
        ret

.keys:  Node .up,.down,.left,.right,.pgup,.pgdn

.up:    Tmk key.up,next
        Asm subdw,.off,.COLUMNS
.down:  Tmk key.down,next
        Asm adddw,.off,.COLUMNS
.left:  Tmk key.left,next
        Asm subdw,.off,1
.right: Tmk key.right,next
        Asm adddw,.off,1
.pgup:  Tmk key.pgup,next
        Asm subdw,.off,.COLUMNS*8
.pgdn:  Tmk key.pgdn,next
        Asm adddw,.off,.COLUMNS*8

.bmpview:
        Gnode 10,10,0,0,.cursor,.bmp

.bmpfocus:
;bmptrack is .lines*.columns bytes long
;cursor is centered
;if bmptracks is out of limits, it is limit
;cursor is free and go to limit.


        Asm .asm2,next
        mov eax,.LINES
        imul eax,.COLUMNS
        shl eax,2
        mov [.bmpcenter],eax

        mov eax,[.ptr]
        add eax,[.off]

        sub eax,[.bmpcenter]

;        mov ebx,eax
 ;       cmp eax,.LOWLIM
 ;       jge @f
 ;       sub eax,.LOWLIM
 ;       cdq
 ;       mov ecx,.COLUMNS
 ;      idiv ecx




;@@:
;        mov ecx,.UPLIM-.LINES*.COLUMNS
;        cmp eax,ecx
;        jle @f
;        sub eax,ecx

;@@:

        mov [.bmp+bmptr.bmp],eax
        mov eax,.LINES
        shl eax,2

        mov [.cursor+frame.y],eax


        ret

.asm2:
        mov eax,[.ptr]
        add eax,[.off]

if ~defined FOOLOS
        cmp eax,.LOWLIM
        jge @f
        mov dword[.off],.LOWLIM-.PTR
        mov eax,.LOWLIM
@@:
        mov ecx,.COLUMNS*.LINES*9
        mov ebx,.COLUMNS*.LINES
        add ebx,eax
        add ecx,eax
        sub ecx,.UPLIM
        jl @f
        sub eax,ecx
@@:
end if

        mov [.bmp+bmptr.bmp],eax

if ~defined FOOLOS
        sub ebx,.UPLIM
        jl @f
        sub [.off],ebx
@@:
        sub eax,[.ptr]
        sub eax,[.off]
        neg eax
        cdq
        mov ebx,.COLUMNS
        idiv ebx
        add [.bmp+bmptr.bmp],edx
        mov [.cursor+frame.y],eax
end if

        ret

.cursor:Gnode 0,0,.COLUMNS,.LINES,.frame,pixel.notalpha,.back,pixel.setalpha
.frame: Frame -1,-1,.COLUMNS+2,.LINES+2,Red
.back:  Box   0,0,.COLUMNS,.LINES,Black
.bmp:   Bmptr 0,0,.COLUMNS,.LINES*9,0,.PTR+.OFF
.bmpcenter dd ?

.txt:   Txt 700,0,0,0,Black+1,testfile

.hexview:
        Gnode 20+.COLUMNS,10,0,0,.Th,.Ta,.Tn,.doAscii,.doNum,.doHexa

.Tn:    Txt 0,0,0,0,Black+1,.nums
.Ta:    Txt 70+12*.COLUMNS,0,0,0,Black+1,.ascii
.Th:    Txt 60,0,0,0,Black+1,.hexa

.ascii: rb (.LINES+3)*(.COLUMNS+2)
.hexa:  rb (.LINES+3)*(.COLUMNS*2+2)
.nums:  rb (.LINES+3)*(8+2)


.doNum:
        dd linenums,.ptr,.nums,.COLUMNS,.LINES,.off
.doHexa:
        dd hexa,.ptr,.hexa,.COLUMNS,.LINES,.off
.doAscii:
        dd ascii,.ptr,.ascii,.COLUMNS,.LINES,.off

.ptr:   dd .PTR
.off:   dd .OFF

testfile:
file 'testHexView.inc'
db 0

db includeTree

FOOLEND:
