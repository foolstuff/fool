showfps:
        Gnode 0,0,150,30,.txt,.num,.asm,.box,.timer
.box:   Box 0,0,150,30,Yellow
.txt:   Vtxt 0,0,150,30,Red,.str,2
.fps:   dd 0
.num:   Fxdec .value,.str,@f
.str    db 'xxxxxx.xx'
@@:     db ' fps',0
.asm:   Asm @f
.tmp:   dd 0
.cnt:   dd 0
.value: dd 0
@@:
        push eax ebx ecx edx
        inc dword[.cnt]
        mov ecx,[irq0.inc]
        add [.tmp],ecx
        cmp dword[.tmp],500
        jl .end
        mov ecx,[.tmp]
        mov eax,[.cnt]
        mov dword[.tmp],0
        mov dword[.cnt],0
        imul eax,1'000'00
        cdq
        idiv ecx
        mov [.value],eax
.end:
        pop edx ecx ebx eax
        ret

.timer:
        Asm next
        push dword 0 dword[irq0.ticks]
        pop dword[irq0.inc] dword[irq0.ticks]
        ret

