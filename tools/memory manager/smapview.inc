
smapview:
        Gnode 10,10,0,0,.entries,.init

.entries:
        Gnode 0,5,0,0,entries,.box
.box:   Box 0,0,256,?,Red

.init:  Asm next
        mov eax,[SMAP]
        inc eax
        lea eax,[eax*5]
        lea eax,[eax*2]
        mov [.box+box.yl],eax
        add eax,20
        mov [memory.view+item.y],eax
        ret

entries:
.base=0
.length=8
.type=16
.ext=20
        Node .count,.for
.count: Htxt 2,2,0,0,White,@f,SMAP,@f,8
@@:     rb 8
        db 0
.for:   For .init,.cnt,.iterate,.entry
.init:  Asm next
        mov dword[.entry+item.y],0
        mov eax,[SMAP]
        inc eax
        mov [.cnt+const.c],eax
        mov dword[.ptr],0
        call @f
        ret
.iterate:
        Asm @f
@@:
        dec dword[.cnt+const.c]
        mov ecx,[.ptr]
        lea ebx,[ecx*3]
        lea eax,dword[SMAP+4+ebx*8+.base]
        mov [.hbase+num.ptr],eax
        lea eax,dword[SMAP+4+ebx*8+.length]
        mov [.hlength+num.ptr],eax
        lea eax,dword[SMAP+4+ebx*8+.type]
        mov [.htype+num.ptr],eax
        lea eax,dword[SMAP+4+ebx*8+.ext]
        mov [.hext+num.ptr],eax
        inc dword[.ptr]
        add dword[.entry+item.y],10
        ret
.cnt:   Const ?
.ptr:   dd 0


.entry: Gnode 0,0,0,0,.txt,.hbase,.hlength,.htype,.hext
.txt:   Txt 2,2,0,0,White,.sbase
.hbase:  dd num.hex,.base,.sbase,16
.hlength:dd num.hex,.length,.slength,16
.htype:  dd num.hex,.type,.stype,2
.hext:   dd num.hex,.ext,.sext,2
.sbase   rb 16
        db ' '
.slength rb 16
        db ' '
.stype   rb 2
        db ' '
.sext    rb 2
        db 0

include 'smap.inc'
