if definite SMAP
else
macro mentry base,length,type,ext {
      dq base,length
      dd type,ext
}

SMAP:
.size=0
.data=4
      dd (@f-$-4)/24
repeat 8
      mentry 8000h shl (%+8),4000h shl (%+8),% and 1 +1,1
end repeat
@@:
end if
