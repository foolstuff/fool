mlib:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

macro Mzone amount,dest,location,blocs {
;amount is effective number of bytes of the zone
;dest is a place where the location pointer will be copied
;location is the pointer to the first byte
;blocs is the number of 64k blocs used for this zone
        dd mzone,amount,dest,location,blocs
}

mzone:
.call=0
.dest=8
.amount=4
.location=12
.blocs=16

macro Mfind amount {
        dd memory.find,amount,0,0,0,0
}

mfind:
.call=0
.amount=4
.dest=8
.location=12
.blocs=16

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if defined FOOLOS


macro Mfree location {
        dd memory.free,,0,location,0
}

mfree:
.call=0
.amount=4
.dest=8
.location=12
.blocs=16

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

macro Malloc amount,dest {
if dest eq
        dd malloc,amount,0,0,0
else
        dd malloc,amount,dest,0,0
end if
}

malloc=memory.alloc
.call=0
.amount=4
.dest=8
.location=12
.blocs=16

end if

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

memory:
.call=0
.amount=4
.dest=8
.location=12
.blocs=16

.xl=256
.yl=256

.VOID=MAROON
.FREE=GRAY
.USED=RED

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; this part is just the view of the bitmap
; it will also show the memlists
;


.view:  Gnode 10,100,.xl+2,.yl+2,.bitmap,.frame,.box,.init
.frame: Frame -1,-1,.xl+2,.yl+2,Lime
.box:   Box 0,0,.xl,.yl,Black
.bitmap:
        Bmptr 0,0,.xl,.yl,0,.map

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; this map is used to monitor the state of memory
; absolutelly not convenient for seeking and freeing
; maybe for a fast reloc condition
; if something free after current bloc, just expand
; something like that

virtual at ENDOFCODE
.map:   db .xl*.yl dup 0
end virtual

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; theses lists are composed by entries
; each entry is a 2dwords long
; entry: linear adress, size in bytes
; theses are the stuff needed to allocate and free mem
; some other structure is needed to speed the find ram
; and the best fit algo need a sort of theses parts
;

.memlist:   RList 32*2
.freelist:  RList 32*2
.usedlist:  RList 32*2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.init:
        Init next
        Node .initbitmap,.initlists

.initlists:
        Asm next
        push edi
        mov ecx,[SMAP]
        xor edx,edx
        lea ebx,[SMAP+SMAP.data]
.looplist:
        mov edi,[ebx+entries.base]
        mov eax,[ebx+entries.length]

        cmp dword[ebx+entries.type],1
        jne @f
        mov [.freelist+list.data+edx*8],edi
        mov [.freelist+list.data+4+edx*8],eax
        mov [.memlist+list.data+edx*8],edi
        mov [.memlist+list.data+4+edx*8],eax
        inc edx
@@:
        add ebx,24
        dec ecx
        jne .looplist
        pop edi
        ret

.initbitmap:
        Asm next
        push edi
        mov ecx,[SMAP]
        lea ebx,[SMAP+SMAP.data]
.loop:
        mov edi,[ebx+entries.base+2]
        mov eax,[ebx+entries.length+2]
        mov dh,[ebx+entries.type]
        cmp dh,1
        mov dl,.FREE
        je @f
        mov dl,.VOID
@@:
        mov byte[.map+edi],dl
        inc edi
        dec eax
        jg @b
        add ebx,24
        dec cl
        jne .loop
        pop edi
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; scan freemap for the amount of memory
; best fit, the chunk with closest amount
; first fit, the first chunk with enough
; forcing, compose the memory with a list of fragments
; return 0 if impossible to find

.find:
        mov eax,[esi+.amount]
        mov ebx,eax
        shr eax,16
        and ebx,0ffffh
        je @f
        inc eax
@@:
        xor ebx,ebx
.recount:
        mov ecx,eax        ;eax is the number of 64kb blocs
@@:
        cmp byte[.map+ebx],.FREE
        je .onemore
        inc bx
        je .notfound
        jmp .recount
.onemore:
        inc bx
        je .notfound
        loop @b
        jmp @f
.notfound:
        mov eax,ebx
@@:
        sub ebx,eax
        shl ebx,16
        mov [esi+.blocs],eax
        mov [esi+.location],ebx
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.alloc:
;
        call .find
        or eax,eax
        je @f
        mov al,.USED
        call .mark
        mov ebx,[esi+.location]
        mov ecx,[esi+.amount]
        mov edx,[.usedlist+list.call]
        mov [.usedlist+list.data+edx],ebx
        mov [.usedlist+list.data+4+edx],ecx
        add edx,8
        mov [.usedlist+list.call],edx
        mov ebx,[esi+.dest]
        or ebx,ebx
        je @f
        mov eax,[esi+.location]
        mov [ebx],eax
@@:
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.free:
        mov al,.FREE
        call .mark
        mov edx,[.usedlist+list.call]
        sub edx,8
        mov dword[.usedlist+list.data+edx],0
        mov dword[.usedlist+list.data+4+edx],0
        mov [.usedlist+list.call],edx
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.mark:
;al=state
        mov ebx,[esi+.location]
        mov ecx,[esi+.blocs]
@@:
        mov [.map+ebx],al
        inc ebx
        loop @b
        ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

