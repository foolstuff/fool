memview:
        Gnode 0,0,0,0,\
        .for,\
        .legend,\
        .box,\
        .setbox

.legend:Txt 5,0,0,0,Yellow,@f
@@:     db 'amount',CRLF
        db 'dest',CRLF
        db 'location',CRLF
        db 'blocs',0

.memory dd ?

.setbox:Asm next
        mov eax,Maroon
        mov ebx,[.memory]
        cmp dword[ebx+memory.blocs],0
        je @f
        mov eax,Green
@@:
        mov [.box+box.c],eax
        ret

.box:   Box 0,0,130,36,Black

.for:   For .init,.cnt,.inc,next
        Node .txt,.hex
.init:  Asm next
        mov dword[.cnt+const.c],4
        mov eax,[.memory]
        lea eax,[eax+memory.amount]
        mov dword[.hex+num.ptr],eax
        mov dword[.txt+txt.y],1
        ret
.inc:   Asm next
        dec dword[.cnt+const.c]
        add dword[.hex+num.ptr],4
        add dword[.txt+txt.y],9
        ret
.cnt:   Const ?

.txt:   Txt 70,0,0,0,White,.str
.hex:   dd num.hex,?,.str,8
.str    rb 8
        db 0
