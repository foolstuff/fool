FOOLOS:
;CLSFX:
;SHOWFPS:
;NOCLS:
;NO3DBORDER:
;NOMOUSE:
WINDOW equ 200,100,770,540
TRANSPARENCY=200
BACKGROUND=Black
VESAMODE=SVGA

include '../../sys/fool.inc'

memorymanager:
        Node \
        .txt,\
        tryit, \
        .mem

.txt:   Txt 10,0,0,0,White,.str
.str:   db "memory manager",0

.mem:   Node \
        allocview,\
        findrams,\
        memory,\
        smapview

include 'memory.inc'
include 'memview.inc'
include 'findrams.inc'
include 'allocview.inc'
include 'smapview.inc'

tryit:
.xl=150
.yl=150

        Gnode 430,375,0,0,\
        .frees,\
        .debug,.txt,.hex,.bmp,\
        .fx,\
        .allocs,\
        .init

if defined FOOLOS

.frees:
        Node next
        Asm next
        mov dword[.memory+item.call],mfree
        ret

.allocs:
        Init .memory;,next
        Asm next
        mov dword[.memory+item.call],malloc
        ret

else ;defined FOOLOS

.frees:
        Node .memory,next
        Asm next
        mov dword[.memory+item.call],mfree
        ret

.allocs:
        Node .memory,next
        Asm next
        mov dword[.memory+item.call],malloc
        ret

end if ;defined FOOLOS

.init:  Init .lowmem
.lowmem:Malloc 90000h,.lowptr
.lowptr:dd ?
.memory:Malloc .xl*.yl*4,.bmp+bmp.bmp

.fx:    Asm @f,00F08000h,.xl,.yl
@@:
        mov ecx,(.yl*.xl)-1
        mov ebx,[.bmp+bmp.bmp]
@@:
        add eax,[esi+asm.op1]
        mov [ebx+ecx*4],eax
        loop @b

        inc dword[esi+asm.op1]
        ret

.debug: Node \
        memview,.d3,\
        memview,.d1

.d1:    Asm next
        mov [memview.memory],.lowmem
        mov dword[memview+item.x],0
        mov dword[memview+item.y],0
        ret

.d3:    Asm next
        mov [memview.memory],.memory
        add dword[memview+item.y],40
        ret

.hex:   Hex .bmp+bmp.bmp,.str,8
.txt:   Txt 150,0,0,0,White,.str
.str:   db '01234567',0
.bmp:   Bmptr32 150,10,.xl,.yl,0,?

ENDOFCODE:
if ~defined FOOLOS
times 256*256 db 0
end if
