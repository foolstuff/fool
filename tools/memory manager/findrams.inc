findrams:
        For .init,.cnt,.inc,.findram

.list:  List \
        5000'0000h,\
          43'3456h,\
        2342'3424h,\
        5000'0000h,\
          13'3456h,\
        2342'3424h,\
        4000'0010h,\
        2000'1000h,\
          10'0000h

.init:  Asm next
        mov dword[memview+item.y],10
        mov dword[memview+item.x],270
        mov eax,[.list+list.size]
        shr eax,2
        mov dword[.cnt+const.c],eax
        mov dword[.ptr],0
        mov eax,[.list+list.data]
        mov [.find+memory.amount],eax
        mov [memview.memory],.find
        ret

.inc:   Asm next
        dec dword[.cnt+const.c]
        inc dword[.ptr]
        mov eax,[.ptr]
        mov eax,[.list+list.data+eax*4]
        mov [.find+memory.amount],eax
        add dword[memview+item.y],40
        ret

.ptr:   dd ?
.cnt:   Const ?
.find:  Mfind ?

.findram:
        Node memview,.find

