allocview:
        Gnode 430,10,0,0,0,.forused,.forfree,.forsmap
.forsmap:
        For .initsmap,.cnt,.inc,.render
.forfree:
        For .initfree,.cnt,.inc,.render
.forused:
        For .initused,.cnt,.inc,.render

.initsmap:
        Asm next
        mov dword[.list],memory.memlist
        mov dword[.render+item.x],0
        mov dword[.box+box.c],Blue
        mov dword[.ttl],'smap'
        jmp .init

.initfree:
        Asm next
        mov dword[.list],memory.freelist
        mov dword[.render+item.x],110
        mov dword[.box+box.c],Green
        mov dword[.ttl],'free'
        jmp .init

.initused:
        Asm next
        mov dword[.list],memory.usedlist
        mov dword[.render+item.x],220
        mov dword[.box+box.c],Red
        mov dword[.ttl],'used'
        jmp .init

.init:
        mov dword[.back],init
        mov eax,[.list]
        mov eax,[eax+list.size]
        shr eax,3
        inc eax
        mov dword[.cnt+const.c],eax
        mov dword[.ptr],0
        mov dword[.txt+txt.y],10
        jmp @f

.inc:
        Asm @f
@@:     dec dword[.cnt+const.c]
        mov ebx,[.ptr]
        mov edx,[.list]
        lea eax,[edx+list.data+ebx*8]
        mov [.hex1+num.ptr],eax
        lea eax,[edx+list.data+4+ebx*8]
        mov [.hex2+num.ptr],eax
        add dword[.txt+txt.y],9
        inc dword[.ptr]
        ret

.list:  dd memory.freelist
.ptr:   dd 0
.cnt:   Const 1
.render:Gnode 0,0,0,0,.txt,.hex1,.hex2,.back
.back:  Init next
        Node .title,.frame,.box
.frame: Frame 0,0,105,355,Yellow
.box:   Box 0,0,105,355,Blue
.title: Txt 2,2,0,0,White,.ttl
.ttl:   db 'free ram zones',0
.txt:   Txt 2,20,0,0,White,.str
.hex1:  Hex memory.freelist+list.data,.str,8
.hex2:  Hex memory.freelist+list.data+4,.str2,8
.str    rb 8
        db ':'
.str2   rb 8
        db 0
