X=0
Y=0
XL=0
YL=0

CONTAINER:
.border=10

include "colors.inc"

macro STR s {
        Const next
        db s,0
}

macro TITLEBAR x,y,xl,yl,t,ps,logo {
local .b,.t,.ps
        Gnode x,y,xl,yl,logo,.ps,.t,.b
.b:     Box 0,0,xl,yl,COLORS.titlebar
.t:     TEXT 50,10,White,2
        db t,0
.ps:    TEXT xl-150,10,White,1
        db ps,0
}

macro HIGHLIGHT xl,yl {
local .z,.f
.z:     Zone 0,0,xl,yl,.f
.f:     Frame 0,0,xl,yl,COLORS.selector
}

macro SELECTITEM {
        mov eax,[.current]
        or eax,eax
        je @f
        mov dword[eax+but.c],COLORS.button
@@:
        mov dword[edi+but.c],COLORS.selected
        mov [.current],edi
}

macro BUTTON xl,yl,r,e {
        But 0,0,xl,yl,COLORS.button,r,e
}

macro ADHERENT nom,prenom,date,rang,noss,type {
local .np,.d,.n,.z,.t,.i,.but,.tnp,.tt,.f,.foot,.r
        Gnode .X,.Y,.XL,.YL,.z,.f,.foot,type,.d,.n,.r,.i,.np,.but
.f:     Frame 0,0,.XL,.YL,COLORS.beneficiaire
.z:     HIGHLIGHT .XL,.YL
.np:    Gnode 0,0,.XL,30,.tnp,next
        Box 0,0,.XL,30,COLORS.beneficiaire
.tnp:   TEXT 40,5,COLORS.text,2
        db nom,' ',prenom,0

.d:     TEXT 15,40,COLORS.text,1
        db 'N� le ',date,0
.n:     TEXT 15,55,COLORS.text,1
        db 'NIR: ',noss,0
.r:     TEXT 15,70,COLORS.text,1
        db 'rang: ',rang,0
.i:     TXT 5,10,1,0
        db ' 0 ',crlf
        db '/T\',0
.but:   BUTTON .XL,.YL,0,Adherents.select
.foot:  Box 0,.YL-10,.XL,10,COLORS.beneficiaire
.Y=.Y+.YL+5
}

macro GARANTIE code,droits,actes {
local .c,.f,.d,.z,.but,.ct,.dt
        Gnode .X,.Y,.XL,.YL,.z,.f,actes,droits,.c,.but
.f:     Frame 0,0,.XL,.YL,COLORS.beneficiaire
.z:     HIGHLIGHT .XL,.YL
.c:     Gnode 0,0,.XL,30,.ct,next
        Box 0,0,.XL,30,COLORS.garantie
.ct:    TEXT 10,0,COLORS.text,2
        db code,0
.but:   BUTTON .XL,.YL,0,Garanties.select
.Y=.Y+.YL+5
}

macro ACTE code,infos,taux {
local .c,.i,.t,.f,.z,.but,.n
        Gnode .X,.Y,.XL,.YL,.t,.i,.c;,.but
;.z:     HIGHLIGHT .XL,.YL
.c:     TEXT 10,0,COLORS.text,1
        db code,0
.i:     TEXT 100,0,COLORS.text,1
        db infos,0
.t:     TEXT .XL-80,0,COLORS.text,1
        db taux,0
.but:   BUTTON .XL,.YL,0,Actes.select
.Y=.Y+.YL+1
}

macro TOOLBUT x,y,xl,yl,e {
local .z,.b,.f,.n,.f2
        Gnode x,y,xl,yl,.z,.f2,.b
.z:     HIGHLIGHT xl,yl
.f2:    Frame 0,0,xl,yl,COLORS.frame
.b:     BUTTON xl,yl,.n,e
.n:
}

macro FBOX x,y,xl,yl {
local .b,.f
        Node .f,.b
.b:     Box x,y,xl,yl,Olive
.f:     Frame x,y,xl,yl,Black
}
