NOALPHAGRAB=1
NO3DBORDER=1
WINDOWED equ 1

WX=100
WY=100
WXL=800
WYL=600+main.YL


include "../../sys/fool.inc"

include "macros.inc"


cspClient:
        APPLICATION 0,0,WXL,WYL,"Ma Carte Mut"
        Node clock,WorkSpace,Navigation,.back

.back:  Box 0,0,800,600,COLORS.background


WorkSpace:
        Ptr next
        Node Tree,Titlebar

Titlebar:
        TITLEBAR 0,40,800,50,"Carte en cours de lecture","PS:123456789",CardLogo

Tree:
        Gnode 0,50,0,0,\
             .Adherents,\
             \;.Actes,\
             .Garanties

.Adherents:
        Ptr Adherents
.Garanties:
        Ptr Garanties
;.Actes:
;        Ptr Actes

Navigation:
        Node .back,.carte,.selectionner,.lecture,.f,.zone
.f:     Border 0,0,800,40,COLORS.frame
.zone:  Box 0,0,800,40,COLORS.menu
.back:
        TOOLBUT 10,5,30,30,0
        Node .precedent,next
        Border 0,0,30,30,COLORS.button
.precedent:
        TEXT 10,0,Black,2
        db "<",0

.carte:
        TOOLBUT 45,5,300,30,0
        Node .lecteur,next
        Box 0,0,300,30,COLORS.button
.lecteur:
        TEXT 10,0,Black,2
        db "carte virtuelle 0",0

.selectionner:
        TOOLBUT 344,5,21,30,0
        Node .select,next
        Border 0,0,20,30,COLORS.button
.select:
        TEXT 5,0,Black,2
        db "v",0

.lecture:
        TOOLBUT 370,5,150,30,.lirecarte
        Node .lire,next
        Border 0,0,150,30,COLORS.button
.lire:
        TEXT 10,0,Black,2
        db "Lire la carte",0

.lirecarte:
        Asm next
        xor dword[WorkSpace],cptr
        ret


Adherents:
X=10
Y=50
XL=250
YL=415
        Gnode X,Y,XL,YL,.list,.f
.list:  Node .porteur,.epouse,.enfant1,.enfant2

.t:

.f:     Box -CONTAINER.border,-CONTAINER.border,XL+CONTAINER.border*2,YL+CONTAINER.border*2,COLORS.workspace
.X=0
.Y=0
.XL=250
.YL=100
.porteur:
        ADHERENT "Michel","P�re","12/12/1960","1","123456789012",.adherent
.epouse:
        ADHERENT "Michel","M�re","12/12/1968","1","223456789012",.conjoint
.enfant1:
        ADHERENT "Michel","Fille","12/12/1988","1","123456789012",.enfant
.enfant2:
        ADHERENT "Michel","Fils","12/12/1988","2","123456789012",.enfant

.adherent:
        Gnode .XL-55,7,50,15,@f,next
        Box 0,0,50,15,COLORS.individuel
@@:     TEXT 2,0,White,1
        db "Adh�rent",0
.conjoint:
        Gnode .XL-55,7,50,15,@f,next
        Box 0,0,50,15,COLORS.surco
@@:     TEXT 2,0,White,1
        db "Conjoint",0
.enfant:
        Gnode .XL-55,7,50,15,@f,next
        Box 0,0,50,15,COLORS.base
@@:     TEXT 2,0,White,1
        db "Enfant",0

.select:
        Asm @f
.current:
        dd 0
@@:
        SELECTITEM
        mov dword[Tree.Garanties],cptr
        ret

Garanties:
X=X+XL+20
Y=50
XL=510
YL=250
        Gnode X,Y,XL,YL,.list,.f
.list:  Node .optique,.pharmacie,.hospitalisation
.f:     Box -CONTAINER.border,-CONTAINER.border,XL+CONTAINER.border*2,YL+CONTAINER.border*2,COLORS.workspace
.X=0
.Y=0
.XL=XL
.YL=80
.optique:
        GARANTIE "Optique",.ouvert,Actes.opti
.pharmacie:
        GARANTIE "Pharmacie",.ferm�,Actes.phar
.hospitalisation:
        GARANTIE "Hospitalisation",.ouvert,Actes.hosp

.ouvert:
        Gnode .XL-105,7,100,15,@f,next
        Box 0,0,100,15,COLORS.ouvert
@@:     TEXT 5,0,White,1
        db "Droits ouverts" ,0
.ferm�:
        Gnode .XL-105,7,100,15,@f,next
        Box 0,0,100,15,COLORS.ferm�
@@:     TEXT 5,0,White,1
        db "Droits ferm�s" ,0

.select:
        Asm @f
.current:
        dd 0
@@:
        SELECTITEM
;        mov dword[Tree.Actes],cptr
        ret

Actes:
X=2
Y=30
XL=XL-4
YL=45
.opti:
        Gnode X,Y,XL,YL,next
        Node .ver,.len,.mon
.X=0
.Y=0
.XL=XL
.YL=15
.ver:   ACTE "Verres","verres correctifs","100TC+400TM"
.len:   ACTE "Lentilles","Lentilles de contact","100TM"
.mon:   ACTE "Monture","Monture pour les verres correctifs","100TC"

.phar:
        Gnode X,Y,XL,YL,next
        Node .pharm,.appar
.X=0
.Y=0
.XL=XL
.YL=15
.pharm: ACTE "Pharmacie","Materiel de pharmacie","100TC+400TM"
.appar: ACTE "Appareillage","Materiel medical","100TM"

.hosp:
        Gnode X,Y,XL,YL,next
        Node .bloc,.cham,.cons
.X=0
.Y=0
.XL=XL
.YL=15
.bloc:   ACTE "Bloc","Chirurgie et bistouri","100TC+400TM"
.cham:   ACTE "Chambres","Sejour � l'hopital","100TM"
.cons:   ACTE "Consultation","Consultation en interne","100TC"

.select:
        Asm @f
.current:
        dd 0
@@:
        SELECTITEM
        ret

CardLogo:
        Gnode 10,10,25,25,.r1,.r2,.r3,.r4,.r5,.r6
.r6:    FBOX 0,0,25,25
.r5:    FBOX 8,0,17,25
.r4:    FBOX 16,6,9,19
.r3:    FBOX 0,6,9,7
.r2:    FBOX 0,18,9,7
.r1:    FBOX 16,12,9,7


clock:
.x=720
.y=4
.xl=75
.yl=60
.scale=1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        Gnode .x,.y,.xl*.scale+2,.yl*.scale/2+2,\
        .calendar,.clock,@f
@@:     Frame 0,0,.xl*.scale+2,.yl*.scale/2+2,COLORS.frame
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.name:  db 'clock',0
.clock: Gnode 0,0,.xl*.scale,.yl*.scale,\
        .text,.hhex,.mhex,.shex,.time
.time   dd time,00112233h
.hhex   dd num.hex,.time+6,.hasc,2
.mhex   dd num.hex,.time+5,.masc,2
.shex   dd num.hex,.time+4,.sasc,2
.hasc   db '  :'
.masc   db '  :'
.sasc   db '  '
        db 0
        align 4
.text:  Vtxt 4,3,0,0,COLORS.text,.hasc,.scale,ugly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.calendar:
        Gnode 0,15*.scale,.xl*.scale,.yl*.scale,\
        .text1,.Dhex,.Mhex,.Yhex,.date
.date   dd date,00000000h
.Dhex   dd num.hex,.date+7,.Dasc,2
.Mhex   dd num.hex,.date+6,.Masc,2
.Yhex   dd num.hex,.date+4,.Yasc,4
.Dasc   db '  /'
.Masc   db '  /'
.Yasc   db '    '
        db 0
        align 4
.text1: Vtxt 4,3,0,0,COLORS.text,.Dasc,.scale,ugly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


include "carte.inc"