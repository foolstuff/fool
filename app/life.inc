life_effect: Node .effect,.keyboard
.x=100
.y=10
.xl=150
.yl=100
                                   ;;;;;;;;;;;;;;;;;;;
.XL = 1024                         ;   -1     +1
.BUFFER = 0100000h                 ;+xl  7 8 9
                                   ;     4 5 6
.effect:                           ;-xl  1 2 3
        Node .monitor,next                  ;
        Asm .life                  ;

.monitor:
        Gnode .x,.y,.xl,.yl,.txt,.hexand,.hexadd,.hexinit,.hexskip,.frame
.frame: Box 0,0,.xl,.yl,White
.txt:   Txt 10,10,.xl-20,.yl-20,Red,.str,font85
.str:   db "Life effect",crlf
        db "and var = "
.andvarstr:
        db "00000000",crlf
        db "add var = "
.addvarstr:
        db "00000000",crlf
        db "init value = "
.initvarstr:
        db "00000000",crlf
        db "frameskip = "
.skipvarstr:
        db "00000000",0

.hexand:
        dd num.hex,.andvar,.andvarstr,8
.hexadd:
        dd num.hex,.addvar,.addvarstr,8
.hexinit:
        dd num.hex,.initvar,.initvarstr,8
.hexskip:
        dd num.hex,.skipvar,.skipvarstr,8


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.life:
        push eax ebx ecx edx edi
        movzx ecx,byte[.skipvar]
        cmp ecx,0
        je .cls
;        mov ecx,1
       ; mov dword[refresh.copy+copyclr.c],0
@@:
        push ecx
        call .iteration
        pop ecx
        loop @b
.end:
        pop edi edx ecx ebx eax
        ret
.cls:
       ; mov dword[refresh.copy+copyclr.c],White
        jmp .end

.iteration:
        mov edi,[desk+screen.size]
        sub edi,1
@@:                   ;
        mov edx,[.initvar]   ;
        lea ebx,[edi-(.XL+1)] ;
        GetPixel
        add edx,ecx
        lea ebx,[edi-.XL] ;
        GetPixel
        add edx,ecx
        lea ebx,[edi-(.XL-1)] ;
        GetPixel
        add edx,ecx
        lea ebx,[edi-1]   ;
        GetPixel
        add edx,ecx
        lea ebx,[edi+1]   ;
        GetPixel
        add edx,ecx
        lea ebx,[edi+(.XL-1)] ;
        GetPixel
        add edx,ecx
        lea ebx,[edi+.XL] ;
        GetPixel
        add edx,ecx
        lea ebx,[edi+(.XL+1)] ;
        GetPixel
        add edx,ecx
        shr edx,3
        and edx,[.andvar]
        add edx,[.addvar]
        mov [.BUFFER+edi*4],edx
        sub edi,1
        jnl @b
        call .copy
        ret


.copy:
        mov edi,[desk+screen.size]
        sub edi,1
@@:
        mov ebx,[.BUFFER+edi*4]
        mov [fs:edi*4],ebx
        sub edi,1
        jnl @b
        ret

.skipvar: dd 1
.initvar: dd 00h
.andvar:  dd 1
.addvar:  dd 99
dd 0,0,0,0

.keyboard:
        Node \
        .addup,\
        .adddown,\
        .andup,\
        .anddown,\
        .initup,\
        .initdown,\
        .frameskip,\
        .skipup,\
        .skipdown
.frameskip:
        dd limd,.skipvar,0,5
.skipup:
        Otk key.home,next
        Asm incdw,.skipvar
.skipdown:
        Otk key.end,next
        Asm decdw,.skipvar
.andup:
        Tmk key.right,1,next
        Asm adddw,.andvar,090301h
.anddown:
        Tmk key.left,1,next
        Asm subdw,.andvar,090301h
.addup:
        Tmk key.up,1,next
        Asm adddw,.addvar,010309h
.adddown:
        Tmk key.down,1,next
        Asm subdw,.addvar,010309h
.initup:
        Tmk key.pageup,1,next
        Asm adddw,.initvar,010703h
.initdown:
        Tmk key.pagedown,1,next
        Asm subdw,.initvar,010703h

