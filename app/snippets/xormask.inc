TRANSPARENCY equ 1
include '../../sys/foolw.inc'

macro Xorbox x,y,xl,yl,c {
      dd xorbox,x,y,xl,yl,c
}

Node boox,@f,.t

@@: Asm @f
.c db 0
@@:
        inc [.c]
        mov al,[.c]
        call colorwheel
        mov [boox+box.c],eax
        ;add dword[boox+box.c],21
        ret
.t:
        Asm @f
.a:     db 1
@@:
        mov al,[.a]
        add byte[transparency],al
        jne @f
        sub byte[transparency],al
        neg byte[.a]
@@:
        ret
colorwheel:
;al = color code
;return eax = color
        movzx eax,al
        mov eax,[palettes.usr+eax*4]
        ret

boox: Xorbox 0,0,1600,900,Red

xorbox:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
        push eax ebx ecx edx edi
        push dword[esi+.x] dword[esi+.y] dword[esi+.xl] dword[esi+.yl]
        or edi,edi
        je @f
        call addxy
@@:
        mov eax,[esi+.x]
        mov ebx,[esi+.y]
        mov edi,[esi+.xl]
        mov edx,[esi+.yl]
@@:
        mov ecx,eax
        xor ecx,ebx
        add ecx,[esi+.c]
        Pixel
        inc eax
        dec edi
        jne @b
        mov edi,[esi+.xl]
        mov eax,[esi+.x]
        inc ebx
        dec edx
        jne @b
.end:
        pop dword[esi+.yl] dword[esi+.xl] dword[esi+.y] dword[esi+.x]
        pop edi edx ecx ebx eax
        ret

