include '../../sys/foolw.inc'

macro RND n{
        movsx eax,n
        sar eax,5
        adc eax,0
}

macro RNDOT d{
        RND bl
        add [d+dot.x],eax
        RND bh
        add [d+dot.y],eax
}

Node poly,.dots,.rndots,.rnd

.dots:  Asm @f
@@:
        mov ecx,[dots]
        shr ecx,2
        dec ecx
        jl .nodots
        mov ebx,[.rnd+rnd.rnd]
@@:
        ror ebx,1
        lea edx,[dots+4+ecx*4]
        RNDOT edx
        mov eax,[edx]
        mov [.dot+box.x],eax
        mov eax,[edx+4]
        mov [.dot+box.y],eax
        fcall .dot
        dec ecx
        jnl @b

.nodots:
        ret

.dot:   Box 0,0,5,5,Green
.rndots:Asm @f
@@:
        mov ebx,[.rnd+rnd.rnd]
        RNDOT poly.p1
        ror ebx,4
        RNDOT poly.p2
        ror ebx,4
        RNDOT poly.p3
        ror ebx,4
        RNDOT poly.p4
        ret

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "polygon"
@@:
.s      dd @f-$-4
        rd 4
@@:

dot:
.x=0
.y=4

macro Dot x,y {
      dd x,y
}

polygon:
.call = 0
.x = 4
.y = 8
.xl = 12
.yl = 16
.c = 20
.p1 = 24
.p2 = 28
.p3 = 32
.p4 = 36
        push eax ebx ecx edx
.12:
        mov eax,[esi+.p1]
        mov ecx,[esi+.p2]
        call .line
.23:
        mov eax,[esi+.p2]
        mov ecx,[esi+.p3]
        call .line
.34:
        mov eax,[esi+.p3]
        mov ecx,[esi+.p4]
        call .line
.41:
        mov eax,[esi+.p4]
        mov ecx,[esi+.p1]
        call .line
.42:
        mov eax,[esi+.p4]
        mov ecx,[esi+.p2]
        call .line
.13:
        mov eax,[esi+.p1]
        mov ecx,[esi+.p3]
        call .line

        pop edx ecx ebx eax
        ret
.line:
        mov ebx,[eax+dot.y]
        mov eax,[eax+dot.x]
        mov [esi+.x],eax
        mov [esi+.y],ebx
        mov edx,[ecx+dot.y]
        mov ecx,[ecx+dot.x]
        sub ecx,eax
        sub edx,ebx
        mov [esi+.xl],ecx
        mov [esi+.yl],edx
        ;call circle
        call line
        ;call frame
        ret

macro Polygon p1,p2,p3,p4,c {
        dd polygon,0,0,0,0,c, p1,p2,p3,p4
}

poly:  Polygon .p1,.p2,.p3,.p4,Red
.p1:   Dot 300,100
.p2:   Dot 600,100
.p3:   Dot 600,400
.p4:   Dot 300,400

dots:
       dd @f-$-4
repeat 10000
       Dot (% mod 100) shl 3, (%/100) shl 3
end repeat
@@:



