;FOOLOS:
BACKGROUND = Black

include '../../sys/fool.inc'

localGroup: Node Sol,AlphaCentauri

Sol:  Gnode 100,100,0,0,.limit,.sun,.name
.limit: Circle 0,0,200,200,Yellow
.sun: Circle 100,100,0,0,White
.name: TEXT  100,100,Green,1
 db "SUN",0

AlphaCentauri: Gnode 537,100,0,0,.limit,.A,.B,.C,.name
.limit: Circle 0,0,200,200,Red
.A:     Circle 101,100,0,0,White
.B:     Circle 102,100,0,0,White
.C:     Circle 87,114,0,0,White
.name:  TEXT 100,100,Green,1
db "A-Centauri",0