;FOOLOS:
TITLE equ '2048'
WINDOWED:
ONTOP:
;SHOWFPS:

WX = 500
WY = 100
WXL = 500
WYL = 500
GXL = WXL
GYL = WYL-100
NX = 4
NY = 4
XMAX = NX-1
YMAX = NY-1

include "../../sys/fool.inc"

game2048:
Node score,cases,keys,.frame
.frame: Box 0,0,WXL,WYL,White

keys:
Node .up,.down,.left,.right,.reset,next
        Asm next
        mov [.moved],0
        ret

.up:    Tmk key.up,actions.up
.down:  Tmk key.down,actions.down
.left:  Tmk key.left,actions.left
.right: Tmk key.right,actions.right
.reset: Otk key.ctrl,actions.reset
.moved  dd ?

up:
        mov edx,0
.load:
        add edx,NX*4
        cmp edx,(NX*(NY-1)*4)
        jg .end
        mov eax,[cases.array+edx+ecx*4]
        or eax,eax
        je .load
        lea ebx,[edx]

.scanunder:
        sub ebx,NX*4
        jl .last
        cmp dword[cases.array+ebx+ecx*4],0
        je .scanunder
        cmp [cases.array+ebx+ecx*4],eax
        jne .last
.fusion:
        inc [keys.moved]
        shl eax,1
        neg eax
        sub ebx,NX*4
.last:
        add ebx,NX*4
        mov dword[cases.array+edx+ecx*4],0
        mov [cases.array+ebx+ecx*4],eax
        cmp ebx,edx
        jne .load
        inc [keys.moved]
        jmp .load
.end:
        ret

down:
        mov edx,NX*NY*4
.load:
        sub edx,NX*4
        cmp edx,0
        jl .end
        mov eax,[cases.array+edx+ecx*4]
        or eax,eax
        je .load
        lea ebx,[edx]

.scanunder:
        add ebx,NX*4
        cmp ebx,NX*NY*4
        jge .last
        cmp dword[cases.array+ebx+ecx*4],0
        je .scanunder
        cmp [cases.array+ebx+ecx*4],eax
        jne .last
.fusion:
        inc [keys.moved]
        shl eax,1
        neg eax
        add ebx,NX*4
.last:
        sub ebx,NX*4
        mov dword[cases.array+edx+ecx*4],0
        mov [cases.array+ebx+ecx*4],eax
        cmp ebx,edx
        jne .load
        inc [keys.moved]
        jmp .load
.end:
        ret


left:
        mov ecx,0
.load:
        inc ecx
        cmp ecx,XMAX
        jg .end
        mov eax,[cases.array+edx+ecx*4]
        or eax,eax
        je .load
        lea ebx,[ecx]

.scanunder:
        dec ebx
        jl .last
        cmp dword[cases.array+edx+ebx*4],0
        je .scanunder
        cmp [cases.array+edx+ebx*4],eax
        jne .last
.fusion:
        inc [keys.moved]
        shl eax,1
        neg eax
        dec ebx
.last:
        inc ebx
        mov dword[cases.array+edx+ecx*4],0
        mov [cases.array+edx+ebx*4],eax
        cmp ebx,ecx
        jne .load
        inc [keys.moved]
        jmp .load
.end:
        ret

right:
        mov ecx,XMAX
.load:
        dec ecx
        cmp ecx,0
        jl .end
        mov eax,[cases.array+edx+ecx*4]
        or eax,eax
        je .load
        lea ebx,[ecx]

.scanunder:
        inc ebx
        cmp ebx,XMAX
        jg .last
        cmp dword[cases.array+edx+ebx*4],0
        je .scanunder
        cmp [cases.array+edx+ebx*4],eax
        jne .last
.fusion:
        inc [keys.moved]
        shl eax,1
        neg eax
        inc ebx
.last:
        dec ebx
        mov dword[cases.array+edx+ecx*4],0
        mov [cases.array+edx+ebx*4],eax
        cmp ebx,ecx
        jne .load
        inc [keys.moved]
        jmp .load
.end:
        ret

actions:
.up:
        Node cases.new,next
        Asm next
        mov ecx,0
@@:
        call up
        inc ecx
        cmp ecx,NX-1
        jle @b
        call cases.abs
        ret
.down:
        Node cases.new,next
        Asm next
        mov ecx,0
@@:
        call down
        inc ecx
        cmp ecx,NX-1
        jle @b
        call cases.abs
        ret
.left:
        Node cases.new,next
        Asm next
        mov edx,0
@@:
        call left
        add edx,NX*4
        cmp edx,NX*NY*4
        jl @b
        call cases.abs
        ret
.right:
        Node cases.new,next
        Asm next
        mov edx,0
@@:
        call right
        add edx,NX*4
        cmp edx,NX*NY*4
        jl @b
        call cases.abs
        ret

.reset:
        Asm next
        ret

score:  Gnode 0,0,0,0,.txt,.num,.sum
.txt:   TEXT 0,0,Blue,3
.str:   db '12345600'
@@:     db 0
.num:   Fxdec .score,.str,@b
.score  dd ?
.sum:
        Asm next
        call cases.sum
        mov [.score],eax
        ret

cases:
.XL=GXL/NX
.YL=GYL/NY
Gnode 0,100,0,0,.render

.render:
        Asm @f
@@:
        mov eax,0
        mov ebx,0
        mov dword[.num+fxdec.var],.array
.line:
        mov [.slot+gnode.x],eax
        mov [.slot+gnode.y],ebx
        fcall .slot
        add dword[.num+fxdec.var],4
        add eax,.XL
        cmp eax,.XL*NX
        jl .line
        mov eax,0
        add ebx,.YL
        cmp ebx,.YL*NY
        jl .line
        ret

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "game2048"
@@:
.s      dd @f-$-4
        rd 4
@@:

.new:   Node @f,.rnd
@@:     Asm next
        cmp [keys.moved],0
        jne @f
        ret
@@:
        pushad
        mov ebx,0
        mov ecx,NX*NY
@@:
        dec ecx
        jl @f
        cmp dword[.array+ecx*4],0
        jne @b
        inc ebx
        jmp @b
@@:
        ;dec ebx
        mov eax,[.rnd+rnd.rnd]
        xor edx,edx
        idiv ebx
        mov ecx,NX*NY
@@:
        dec ecx
        jl @f
        cmp dword[.array+ecx*4],0
        jne @b
        dec edx
        jnl @b
        mov dword[.array+ecx*4],2
@@:
        popad
        ret

.abs:
        mov ecx,NX*NY
@@:
        dec ecx
        jl @f
        cmp dword[.array+ecx*4],0
        jge @b
        neg dword[.array+ecx*4]
        jmp @b
@@:
        ret
.sum:
        mov eax,0
        mov ecx,NX*NY
@@:
        dec ecx
        jl @f
        add eax,dword[.array+ecx*4]
        jmp @b
@@:
        ret

.slot:  Gnode 0,0,0,0,.txt,.border,.box,.color,.num
.box:   Box 2,2,.XL-4,.YL-4,Silver
.border:Border 2,2,.XL-4,.YL-4,0
.txt:   TEXT 10,.YL/2-12,Blue,1
.str:   db '001345'
@@:     db 0
.num:   Fxdec .array+4,.str,@b
.color:
        Asm next
        push eax ebx
        mov ebx,.colors
        mov eax,[.num+fxdec.var]
        mov eax,[eax]
@@:
        shr eax,1
        lea ebx,[ebx+4]
        jne @b
        mov eax,ebx
        mov ebx,[ebx]
        mov [.box+box.c],ebx
        xor ebx,-1
        mov [.txt+vtxt.c],ebx
        pop ebx eax
        ret

.colors:
        ;times 25 db %*5,%*7,%*10,0

        dd Gray,\
           Black,\
           White,\
           Silver,\
           Green,\
           Lime,\
           Olive,\
           Gold,\
           Yellow,\
           Maroon,\
           Red,\
           Purple,\
           Fuchsia,\
           Navy,\
           Blue,\
           Teal,\
           Aqua

.array:
        dd 0  ,0  ,2  ,0  ,0  ,0  ,0  ,0  ,\
           2  ,2  ,0  ,8  ,4  ,0  ,16 ,8  ,\
           256,512,0  ,128,0  ,256,64 ,0  ,\
           0  ,0  ,2  ,2  ,4  ,8  ,4  ,8  ,\
           32 ,32 ,64 ,0  ,0  ,0  ,128,0  ,\
           8  ,4  ,8  ,4  ,2  ,2  ,16 ,0  ,\
           0  ,256,2  ,2  ,4  ,8  ,4  ,8  ,\
           0  ,0  ,0  ,0  ,0  ,0  ,0  ,0



appsize game2048
