getCellColor:
;return the color under Bonhomme in eax
        ASM
        mov eax,[Bonhomme+box.x]
        add eax, Bonhomme.xl/2

        mov ebx,GXL*Z
        cdq
        idiv ebx
        cmp edx,0
        jge @f
        inc eax
        jge @f
        sub eax,2
@@:
        movzx ecx,al
        mov [.x],ecx
        mov eax,[Bonhomme+box.y]
        add eax, Bonhomme.yl
        mov ebx,GYL*Z
        cdq
        idiv ebx
        cmp edx,0
        jge @f
        inc eax
        jge @f
        sub eax,2
@@:
        movzx eax,al
        mov [.y],eax
        shl eax,8
        mov al,cl
        shl eax,2
        mov [.pixel],eax
        add eax,[World.level]
        mov eax,[eax]
        mov [.color],eax
        ret

.x      dd ?
.y      dd ?
.pixel  dd ?
.color  dd ?