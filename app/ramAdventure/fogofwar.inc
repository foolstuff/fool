warfog:
.SIGHT=25/Z
.unfog:
        Asm next
        mov ebx,[cellColor.pixel]
        shr ebx,2
        mov edx,[World.fog]
        sub ebx,(256*.SIGHT)+.SIGHT
        mov cl,.SIGHT*2+1
        mov ch,.SIGHT*2+1
@@:
        and ebx,0ffffh
        mov byte[ebx+edx],0
        inc ebx
        dec cl
        jne @b
        mov cl,.SIGHT*2+1
        add ebx,256-(.SIGHT*2+1)
        dec ch
        jne @b
        ret
.fog:   Bmptr 0,0,256,256,0,?


