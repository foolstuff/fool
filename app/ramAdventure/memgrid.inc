

Memgrid:

.cxy dd ?
.cx0 dd 0
.cx dd 0
.cy dd 0
.bx dd ?
.xl dd ?
.yl dd ?


.cell:  Gnode 0,0,Grid.xl,Grid.yl,\
        .box
.box:   Box 0,0,Grid.xl,Grid.yl,Red

.quantize:
        neg eax
        cdq
        idiv ebx
        cmp edx,0
        jge @f
        dec eax
@@:
        movzx eax,al
        ret

.init:
        Asm next
        mov eax,[World.world+gnode.x]
        neg eax
        call Grid.qx
        mov [.bx],eax
        mov [.cell+box.x],eax
        add eax,WXL+Grid.xl
        mov [.xl],eax

        mov eax,[World.world+gnode.x]

        mov ebx,Grid.xl

        call .quantize

        mov [.cx],eax
        mov [.cx0],eax

        mov eax,[World.world+gnode.y]
        neg eax
        call Grid.qy
        mov [.cell+box.y],eax
        add eax,WYL+Grid.yl
        mov [.yl],eax

        mov eax,[World.world+gnode.y]
        mov ebx,Grid.yl

        call .quantize

        mov [.cy],eax
@@:
        mov ecx,[.cy]
        shl ecx,8
        add ecx,[.cx]
        and ecx,0xffff
        mov [.cxy],ecx

        add ecx,[World.fog]
        movzx ecx,byte[ecx]
        cmp cl,0
        mov ecx,[palettes.usr+ecx*4]
        mov [.box+box.c],ecx
        jne .alpha

        mov ecx,[.cxy]
        shl ecx,2

        add ecx,[World.level]
        mov ecx,[ecx]

        mov [.box+box.c],ecx


;        mov ecx,[World.layer]
;        or ecx,ecx
;        je .alpha

;        mov eax,[.cxy]
;        mov ecx,[eax*4+ecx]
;        or ecx,ecx
;        je .alpha
;        mov [.box+box.c],ecx
.alpha:
        fcall .cell
        inc [.cx]
        mov eax,[.cell+box.x]
        add eax,Grid.xl
        mov [.cell+box.x],eax
        cmp eax,[.xl]
        jle @b
        mov eax,[.cx0]
        mov [.cx],eax
        inc [.cy]
        mov eax,[.bx]
        mov [.cell+box.x],eax
        mov eax,[.cell+box.y]
        add eax,Grid.yl
        mov [.cell+box.y],eax
        cmp eax,[.yl]
        jle @b

        ret

