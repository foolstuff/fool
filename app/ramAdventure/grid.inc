Grid:
.xl=Z*GXL
.yl=Z*GYL
        Node pixel.notalpha,@f,pixel.setalpha
@@:     Asm @f
.line:  Line 0,0,0,1080,Black
@@:

        mov eax,[World.world+gnode.y]
        neg eax
        mov dword[.line+line.y],eax
        mov dword[.line+line.yl],WYL

        mov eax,[World.world+gnode.x]
        neg eax
        call .qx
        lea ebx,[eax+WXL]
        mov dword[.line+line.xl],0
@@:
        mov [.line+line.x],eax
        fcall .line
        add eax,.xl
        cmp eax,ebx
        jle @b


        mov eax,[World.world+gnode.x]
        neg eax
        mov dword[.line+line.x],eax
        mov dword[.line+line.xl],WXL

        mov eax,[World.world+gnode.y]
        neg eax
        call .qy
        lea ebx,[eax+WYL]
        mov dword[.line+line.yl],0
@@:
        mov [.line+line.y],eax
        fcall .line
        add eax,.yl
        cmp eax,ebx
        jle @b

        ret

.qx:
        mov ebx,.xl
        call .quantize
        ret

.qy:
        mov ebx,.yl
        call .quantize
        ret

.quantize:
        cdq
        idiv ebx
        cmp edx,0
        mov edx,eax
        jge @f
        dec eax
@@:
        imul ebx
        ret

