Map:
;        Cptr .outside
;.inside:
;        Node Inside,BlackBack
;.outside:
        Node Trees,House,Ground,Sky

Trees:
        Node .1,.2,.3,.4,.5
.1:     Gptr 100*Z,30*Z,Tree
.2:     Gptr 80*Z,40*Z,Tree
.3:     Gptr 140*Z,50*Z,Tree
.4:     Gptr 20*Z,40*Z,Tree
.5:     Gptr 100*Z,60*Z,Tree

Tree:
        Node .leafs,.trunk
.leafs: Box 0,0,20*Z,10*Z,Green/2
.trunk: Box 9*Z,10*Z,2*Z,5*Z,Maroon

House:
        Gnode 30*Z,10*Z,0,0,.chem,.roof,.door,.wind,.wall
.roof:  Box 0*Z,0*Z,30*Z,5*Z,Maroon
.wall:  Box 0*Z,5*Z,30*Z,20*Z,Olive
.chem:  Box 20*Z,-2*Z,3*Z,4*Z,Maroon /2
.door:  Box 0*Z,15*Z,5*Z,10*Z,Black
.wind:  Box 10*Z,10*Z,5*Z,5*Z,Blue

;Inside:
;        Gnode 30*Z,10*Z,0,0,.door,.wind,.wall,.floor
;.floor: Box 0*Z,25*Z,30*Z,25*Z,Gray
;.wall:  Box 0*Z,5*Z,30*Z,20*Z,Olive
;.door:  Box 0*Z,15*Z,5*Z,10*Z,Silver
;.wind:  Box 10*Z,10*Z,5*Z,5*Z,White
;
;BlackBack:
;        Box 0,0,1920,1080,Black
;
Ground:
        Box 0,19*Z,1920,880,Green

Sky:
        Box 0,0,1920,19*Z,Teal
