Floorcell:
        Asm next
        mov eax,[Bonhomme+box.x]
        add eax, Bonhomme.xl/2

        call Grid.qx

        mov [.cursor+box.x], eax

        mov eax,[Bonhomme+box.y]
        add eax,Bonhomme.yl

        call Grid.qy

        mov [.cursor+box.y], eax

        fcall .cursor
        ret

.x      dd ?
.y      dd ?
.color  dd ?

.cursor:Gnode 0,0,0,0,.frame,pixel.notalpha,.box,pixel.setalpha
.box:   Box 0,0,Grid.xl,Grid.yl,Gray
.frame: Frame -1,-1,Grid.xl+2,Grid.yl+2,Red
