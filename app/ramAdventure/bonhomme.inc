Bonhomme:
.xl=6*Z
.yl=10*Z
        Gnode 10*Z,10*Z,.xl,.yl,\
        .lleg,.rleg,\
        .body,.head,.hair,\
        pixel.notalpha,.shade,pixel.setalpha,\
        .stepping,.walk;,.enter

;.enter:
;        dd hit,@f,@f-$-4,House.door
;@@:     Otk key.enter,next
;        Asm next
;        xor dword[Map+cptr.ptr],Map.inside
;        xor dword[Map+cptr.ptr],Map.outside
;        ret

.hair:  Box 0*Z,0*Z,7*Z,4*Z,Maroon
.body:  Box 1*Z,4*Z,5*Z,3*Z,Green-50
.lleg:  Box 1*Z,7*Z,2*Z,3*Z,Blue-20
.rleg:  Box 4*Z,7*Z,2*Z,3*Z,Blue-20

.head:  Gnode 1*Z,1*Z,0,0,.leye,.reye,.face
.face:  Box 0*Z,0*Z,5*Z,3*Z,Yellow
.leye:  Box 1*Z,1*Z,1*Z,1*Z,Blue
.reye:  Box 3*Z,1*Z,1*Z,1*Z,Blue

.shade: Box 0*Z,9*Z,7*Z,2*Z,Black
.speedx dd Z
.speedy dd Z
.walk:
        Node .up,.left,.right,.down,next
        Asm next
        mov [.walking],0
        ret
.walking dd 0
.up:
        Node @f,next
        Tmk key.up,next
        Asm next
        mov ebx,.speedy
        call .accelerate
        sub dword[Bonhomme+box.y],eax
        mov dword[.head+gnode.call],0
;        mov dword[.head+box.x],1*Z
        ret
@@:     Otk key.up,next
        Asm next
        mov [.speedy],Z
        ret

.down:
        Node @f,next
        Tmk key.down,next
        Asm next
        mov ebx,.speedy
        call .accelerate
        add dword[Bonhomme+box.y],eax
        mov dword[.head+gnode.call],gnode
        mov dword[.head+box.x],1*Z
        ret
@@:     Otk key.down,next
        Asm next
        mov [.speedy],Z
        ret

.left:
        Node @f,next
        Tmk key.left,next
        Asm next
        mov ebx,.speedx
        call .accelerate
        sub dword[Bonhomme+box.x],eax
        mov dword[.head+gnode.call],gnode
        mov dword[.head+box.x],0*Z
        ret
@@:     Otk key.left,next
        Asm next
        mov [.speedx],Z
        ret

.right:
        Node @f,next
        Tmk key.right,next
        Asm next
        mov ebx,.speedx
        call .accelerate
        add dword[Bonhomme+box.x],eax
        mov dword[.head+gnode.call],gnode
        mov dword[.head+box.x],2*Z
        ret
@@:     Otk key.right,next
        Asm next
        mov [.speedx],Z
        ret

.accelerate:
        mov [.walking],1
        mov eax,[ebx]
        inc eax
        cmp eax,Z*8
        jle @f
        mov eax,Z*8
@@:
        mov [ebx],eax
        ret

.stepping:
        Asm next
        cmp [.walking],0
        je @f
        call .stepeffect
        ret
@@:
        call .legsdefault
        ret

.stepeffect:
;eax is the sum of X and Y speeds.
;maybe useless.
;may use the X and Y coordinates to move legs and head.
;with xoring a bit, it would automatically adapt the speed of movement
        ;ret
        ;mov dword[.shade+box.c],Red
        ;ret
        mov eax,[Bonhomme+box.x]
        add eax,[Bonhomme+box.y]
        and eax,4
        xor [.rleg+box.yl],eax
        not eax
        and eax,4
        xor [.lleg+box.yl],eax
        ret

.legsdefault:
        ;mov dword[.shade+box.c],Black
        ;ret
        mov dword[.rleg+box.yl],Z*3
        mov dword[.lleg+box.yl],Z*3
        ret
