macro DumpInt x,y,v,s{
local .t,.st,.d

        Node .t,.d
.t:     TEXT x,y,White,1
@@:     db s
.st:    db '000000'
@@:     db 0
.d:     Fxdec v,.st,@b

}

toolbar:
.X=0
.Y=WYL-.YL-20
.XL=WXL
.YL=258
        Gnode 0,.Y,0,0,.panel,.key,.but,\
        warfog.unfog,cellColor.put,cellColor

.panel: Gnode 0,0,0,0,\
        pixel.notalpha,\
        tools,\
        mapview,\
        pixel.setalpha

.key:   Otk key.spc,.showhide
.but:   But 0,.YL,.XL,20,Red,0,.showhide
.showhide:
        Asm next
        xor dword[.panel],gnode
       ; xor dword[showfps],node
        xor dword[Grid],node
        xor dword[LimBoxes],node
        ;mov eax,[World.layermalloc+malloc.location]
        ;xor dword[World.layer],eax
        ret

Debug:
        Gnode 250,10,0,0,.wx,.wy,.gx,.gy,.bx,.by

.wx:    DumpInt 0,0,World.world+item.x,'world.x'
.wy:    DumpInt 0,10,World.world+item.y,'world.y'
.gx:    DumpInt 0,30,Memgrid.cx,'memgrid.x'
.gy:    DumpInt 0,40,Memgrid.cy,'memgrid.y'
.bx:    DumpInt 0,60,Bonhomme+item.x,'player.x'
.by:    DumpInt 0,70,Bonhomme+item.y,'player.y'

tools:
.X=258
.Y=toolbar.YL-.YL
.XL=WXL-.X
.YL=200
        Gnode .X,.Y,.XL,.YL,\
        .frame,\
        pixel.setalpha,\
        Debug,.pixel,.color,\
        .butr,.butg,.butb,.butw,.butd,\
        pixel.notalpha,\
        .box

.box:   Box 1,1,.XL-2,.YL-2,Gray
.butr:  But  10,10,30,20,Red,0,cellColor.set
.butg:  But  50,10,30,20,Green,0,cellColor.set
.butb:  But  90,10,30,20,Blue,0,cellColor.set
.butw:  But 130,10,30,20,CosmicLatte,0,cellColor.set
.butd:  But 170,10,30,20,Grey,0,cellColor.set
.pixel: Box 10,40,30,30,?
.color: Box 50,40,30,30,Pink
.frame: Frame 0,0,.XL,.YL,Black

cellColor:
        Asm next
        mov eax,[Bonhomme+box.x]
        add eax, Bonhomme.xl/2

        mov ebx,GXL*Z
        cdq
        idiv ebx
        cmp edx,0
        jge @f
        inc eax
        jge @f
        sub eax,2
@@:
        movzx ecx,al
        mov [.x],ecx
        mov eax,[Bonhomme+box.y]
        add eax, Bonhomme.yl
        mov ebx,GYL*Z
        cdq
        idiv ebx
        cmp edx,0
        jge @f
        inc eax
        jge @f
        sub eax,2
@@:
        movzx eax,al
        mov [.y],eax
        shl eax,8
        mov al,cl
        shl eax,2
        mov [.pixel],eax
        add eax,[World.level]
        mov eax,[eax]
        mov [tools.pixel+box.c],eax
        ret

.x      dd ?
.y      dd ?
.pixel  dd ?

.fade:  Asm next
        mov ebx,[.pixel]
        add ebx,[World.level]
        mov eax,[ebx]
        add eax,[edi+box.c]
        shr eax,1
        mov [ebx],eax
        ret

.set:   Asm next
        mov eax,[edi+box.c]
        mov [tools.color+box.c],eax
        ret

.put:   Asm next
        mov eax,[tools.color+box.c]
        mov ebx,[.pixel]
        add ebx,[World.layer]
        mov [ebx],eax
        ret

mapview:
.X=1
.Y=1
.XL=258
.YL=258
        Gnode .X,.Y,0,0,\
        .frame,pixel.setalpha,\
        .cursor,pixel.notalpha,\
        .window,warfog.fog,.layer,.bmp,\
        .asm

.window:Box 0,0,WXL/(GXL*Z),WYL/(GYL*Z),Red
.frame: Frame -1,-1,.XL,.YL,Black
.cursor:Box 0,0,3,3,White
.bmp:   But 0,0,256,256,0,@f,0
@@:     Bmptr32 0,0,256,256,0,testlevel
.layer: Bmptr32 0,0,256,256,0,?

.asm:   Asm next
        mov eax,[World.layermalloc+malloc.location]
        mov [.layer+bmp.bmp],eax
        mov eax,[World.fogmalloc+malloc.location]
        mov [warfog.fog+bmp.bmp],eax
        mov eax,[Memgrid.cxy]
        movzx ebx,ah
        movzx eax,al
        sub eax,[.window+frame.xl]
        sub ebx,[.window+frame.yl]
        mov [.window+frame.x],eax
        mov [.window+frame.y],ebx
        mov eax,[cellColor.x]
        dec eax
        mov [.cursor+box.x],eax
        mov eax,[cellColor.y]
        dec eax
        mov [.cursor+box.y],eax
        ret
