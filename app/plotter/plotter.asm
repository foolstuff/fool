TITLE equ 'Plotter'
WINDOW equ 100,50,500,500
NO3DBORDER:
ONTOP:

include '../../sys/fool.inc'

Plotter:
        APPLICATION 0,0,WXL,WYL+TITLEBAR_YL,TITLE

        Gnode 0,0,WXL,WYL,\
        .plots,\
        button.comp,button,.f,.b

.b:     Box 0,0,WXL,WYL,Black+100010h
.f:     Frame 0,0,WXL,WYL,White-100010h

.plots: Gnode 0,0,0,0,.s1,.s2,.s3,.s4

macro PLOT y,yl,c,s,n {
local .p,.f,.t,.s
        Gnode 0,y,0,0,.t,.p;,.f
.p:     Plotd 0,0,WXL,yl,c,s,0,0,1,1
;.f:     Frame 0,0,WXL,yl,Lime
.t:     Vtxt 10,10,0,0,c,.s,2,ugly
.s:     db n,0
}

.s1:    PLOT 100,300,Red,signal.s1,<"signal 1, input">
.s2:    PLOT 100,300,Green,signal.s2,<CRLF,"signal 2, filter">
.s3:    PLOT 100,300,Blue,signal.s3,<CRLF,CRLF,"signal 3, filter">
.s4:    PLOT 100,300,Yellow,signal.s4,<CRLF,CRLF,CRLF,"signal 4, derivation">

button:
        But 10,10,100,20,Silver,@f,.act
.act:
        Asm next
        xor dword[.comp],node
        ret
@@:
        Node .txt,.frame
.frame:
        Frame 0,0,100,20,Blue
.txt:   Txt 20,5,0,0,Pink,@f
@@:     db "COMPUTE",0

.comp:  Node signal.generate


include "signal.inc"