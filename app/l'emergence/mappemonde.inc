include '../../sys/fool.inc'

mappemonde:
        Ptr .vectors
.controls:
        Asm next
        mov dword[desk+box.c],Navy
.move:
        cmp byte[mouse.m],1
        jne @f
        mov eax,[mouse.dx]
        add [.g+vectors.x],eax
        mov eax,[mouse.dy]
        add [.g+vectors.y],eax
@@:
        mov eax,[mouse.x]
        sub eax,[.g+vectors.x]
        mov [.cursor+gnode.x],eax
        mov eax,[mouse.y]
        sub eax,[.g+vectors.y]
        mov [.cursor+gnode.y],eax
        fcall .cursor
.zoom:
        cmp dword[mouse.dz],0
        je @f
        push dword 0.1
        finit
        fild dword[mouse.dz]

        fmul dword[esp]
        fmul dword[.v+vectors.z]
        fst st1
        fadd dword[.v+vectors.z]
        fstp dword[.v+vectors.z]

        mov eax,[mouse.x]
        sub eax,[.g+vectors.x]
        push eax
        fild dword[esp]
        fmul dword[.v+vectors.z]
        pop eax
.adjust:

        mov dword[mouse.dz],0

        pop eax
@@:

macro GRADUATE axis{
local .l
        push eax ebx edx
        mov eax,[.line+axis]
        cdq
        lea ebx,[ebx*5]
        idiv ebx
        mov eax,Gray
        or edx,edx
        jne .l
        mov eax,White
.l:
        mov [.line+line.c],eax
        pop edx ebx eax
}

macro GRADUATION axis1,axis2 {
        mov dword[.line+line.c],Gray
        mov dword[.line+axis1],0
        mov dword[.line+axis1+8],0
        mov eax,[.g+axis2]
        neg eax
        mov dword[.line+axis2],eax
        mov eax,[desk+axis2+8]
        mov dword[.line+axis2+8],eax
        mov ecx,[desk+axis1+8]
        sub ecx,[.g+axis1]
@@:
        add dword[.line+axis1],ebx
        sub ecx,ebx
        jl @f
        GRADUATE axis1
        fcall .line
        jmp @b
@@:
        mov ecx,[.g+axis1]
        add ecx,ebx
        mov dword[.line+axis1],0
@@:
        sub ecx,ebx
        jl @f
        GRADUATE axis1
        fcall .line
        sub dword[.line+axis1],ebx
        jmp @b
@@:
}

.grid:
        push dword 200
        fild dword[esp]          ;200
        fmul dword[.v+vectors.z] ;inc
        fist dword[esp]          ;inc
        pop eax
.scaler:
        cmp eax,40
        jge .greater
        lea eax,[eax*5]
        jmp .scaler
.greater:
        cmp eax,1000
        jle .ok
        push edx
        cdq
        mov ebx,5
        idiv ebx
        pop edx
        jmp .scaler
.ok:
        mov ebx,eax
.horizontal:
        GRADUATION line.x,line.y
.vertical:
        GRADUATION line.y,line.x
        ret

.line:  Line 0,0,0,0,0
.cursor:Gnode 0,0,0,0,next
.frame: Frame -11,-11,21,21,Purple

.vectors:
        Gnode 0,0,0,0,.g,animation
.g:     Gptr 300,1200,.v
.v:     Vectors ?,?,?,?,White,20.0,.map,.controls
.map:
COLOR White
include "map/equirectangular.inc"
COLOR Blue
LINES 180.0f,90.0f,180.0f,-90.0f,-180.0f,-90.0f,-180.0f,90.0f,180.0f,90.0f
STOP

animation:
        Anim 400,333,2,2,1000/20,next
.node:
        Node \
        .f00,\
        .f01,\
        .f02,\
        .f03,\
        .f04,\
        .f05,\
        .f06,\
        .f05,\
        .f04,\
        .f03,\
        .f02,\
        .f01
;Circle fix Frame

.f00:   Circle -1,-1,2,2,Lime
.f01:   Circle -5,-5,10,10,Lime
.f02:   Circle -10,-10,20,20,Lime-4
.f03:   Circle -20,-20,40,40,Lime-8
.f04:   Circle -40,-40,80,80,Lime-12
.f05:   Circle -80,-80,160,160,Lime-16
.f06:   Circle -160,-160,320,320,Lime-20
