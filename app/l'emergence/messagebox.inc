macro MessageBox x,y,xl,yl,c,t,f {
        dd messagebox,x,y,xl,yl,messagebox.win,c,t,f
}

messagebox:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.p=20
.c=24
.t=28
.f=32
        mov eax,[esi+.t]
        mov [.txt+txt.txt],eax
        mov eax,[esi+.f]
        mov [.ptr+_ptr.ptr],eax
        mov eax,[esi+.c]
        mov [.box+box.c],eax
        and eax,0f0f0f0h
        shr eax,4
        or eax,040404h
        mov [.frame+box.c],eax
        mov eax,[esi+.xl]
        mov [.frame+box.xl],eax
        mov [.box+box.xl],eax
        mov [.border+box.xl],eax
        mov eax,[esi+.yl]
        mov [.box+box.yl],eax
        mov [.border+box.yl],eax
        call gptr
        ret

.win:   Node .ptr,.border,.txt,.frame,.box
.frame: Box 0,0,0,30,Navy/2
.box:   Box 0,0,0,0,Teal
.border:Border 0,0,0,0,White
.txt:   Vtxt 10,0,0,0,White,0,2
.ptr:   Ptr 0

macro Form x,y,xl,yl,c,t,f {
        dd form,x,y,xl,yl,form.win,c,t,f
}

form:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.p=20
.c=24
.t=28
.f=32
        mov eax,[esi+.t]
        mov [.value],eax
        mov eax,[esi+.c]
        mov [.back+box.c],eax
        not eax
        mov [.input+txt.c],eax
        mov eax,[esi+.xl]
        mov [.back+box.xl],eax
        mov eax,[esi+.yl]
        mov [.back+box.yl],eax
        mov eax,[esi+.f]
        mov [.do],eax
        call gptr
        ret

.win:   Node .enter,.input,.back,Kinput
.back:  Box 0,0,0,0,Yellow
.input: Vtxt 5,0,0,0,Black,Kinput.prompt,3

.enter: Otk key.enter,.try
.try:   Node Kinput.reset,next
        Asm @f
.do:    dd 0
.value: dd 0
@@:
        push esi edi
        mov edi,[.value]
        or edi,edi
        je .ko
        mov esi,Kinput.prompt
        call cmpstr
        jnc .ko
        fcall [.do]
.ko:
        pop edi esi
        ret
