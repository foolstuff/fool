macro Wtxt t,s,st,l {
        dd wtxt,t,s,st,l
}

wtxt:
.call=0
.txt=4
.size=8

.start=12
.length=16
;will window a text from start to stop before to disp it
        ;fcall [esi+.txt]
        ;ret

        pushad
        mov edx,[esi+.size]
        mov eax,[esi+.start]
        mov ecx,[esi+.length]
        cmp eax,0
        jnl @f
        xor eax,eax
@@:
        cmp eax,edx
        jl @f
        lea eax,[edx-1]
@@:
        add ecx,eax
        jnl @f
        xor ecx,ecx
@@:
        cmp ecx,edx
        jl @f
        lea ecx,[edx-1]
@@:
        mov ebx,[esi+.txt]
        mov edx,[ebx+txt.txt]
        push ebx edx
        add [ebx+txt.txt],eax
        mov al,[edx+ecx]
        mov byte[edx+ecx],0
        fcall [esi+.txt]
        mov [edx+ecx],al
        pop edx ebx
        mov [ebx+txt.txt],edx
        popad
        ret
