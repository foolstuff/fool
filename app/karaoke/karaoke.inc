EXITTITLE equ "EMERGENCE"
EXITMESSAGE equ "quitter L'emergence?"
;ESCAPE equ VK_F3
;NO3DBORDER equ 1
;NOMOUSE equ 1
;SHOWFPS equ 1
;FOOLOS:
include '../../sys/fool.inc'
        Win emergence,win.SHOW
db "messagebox"
include 'messagebox.inc'

macro PLOT x,y,xl,yl,c,s,n {
        Gnode x,y,0,0,@f,next
        Plotd 0,0,xl,yl,c,s,0,0,1,1
@@:     Vtxt 10,400,0,0,c,@f,2,ugly
@@:     db n,0
}

dd      emergence.name

emergence:
.XL=1920
.YL=1080
        Gnode 0,0,.XL,.YL,popup,background,.title

.title: Vtxt 20,0,0,0,White,.name,1,ugly
.name:  db "L'EMERGENCE",0

background:

        Node pixel.notalpha,\
        .box,\
        pixel.setalpha,\
        .scope,\
        mappemonde;\
        ;,randomMap

.box:   Box 20,20,1560,860,Black

.scope: Node .scroll,.plot,.rmap
.rmap:  Gptr 0,600,randomMap
.plot:  Plotb 0,600,1600,300,Lime,emergence,0,0,1,1
.scroll:Asm next
        inc dword[.plot+plotb.xoff]
        and dword[.plot+plotb.xoff],0xffff
        ret

popup:  Node .pop,.f9,.f8
.pop:   Ptr .chron
.f9:    Otk key.f9,.setLogin
.f8:    Otk key.f8,.forceForm
.chron: Chron 600000,.setLogin
.setLogin:
        Cptr .set,Login
.set:
        Node Kinput.reset,next
        Asm next
        mov dword[Welcome.chron+chron.cnt],0
        mov dword[.chron+chron.cnt],0
        mov dword[.pop+_ptr.ptr],eax
        ret
.forceForm:
        Asm next
        push esi edi
        mov esi,[form.value]
        or esi,esi
        je @f
        mov edi,Kinput.prompt
        call copystr
        mov dword[Kinput.input+input.cursor],eax
@@:
        pop edi esi
        ret

Login:  MessageBox 600,200,800,400,Red,.title,.form
.form:  Form 5,40,700,45,White,.pw,next
        Cptr popup.set,Welcome
.title: db "Veuillez entrer le mot de passe pour deverouiller le systeme",0
.pw:    db 'reduire',0

Welcome:MessageBox 100,100,1400,700,Green,.title,next
        Node .txt,.back,.key,.chron
.title: db "L'Emergence",0
.key:   Otk key.f8,.next
.chron: Chron 10000,.next
.next:  Cptr popup.set,Report1
.txt:   Vtxt 70,100,0,0,Yellow,.str,5
.back:  Box 50,100,1300,500,Green/2
.str:   db "        Bienvenue � bord",CRLF,CRLF,\
           "Les resultats des experiences sont incomplets,",CRLF,\
           "L'Emergence attend les resultats pour terminer",CRLF,\
           "la mission et rentrer � la base.",0

Report1:MessageBox 400,300,650,200,Teal,.title,.form
.form:  Form 5,40,400,45,Yellow,.value,next
        Cptr popup.set,Report2
.title: db "Microplastiques (par m2)",0
.value: db '1900000',0

Report2:MessageBox 500,400,650,200,Silver,.title,.form
.form:  Form 5,40,400,45,Blue,.value,next
        Cptr popup.set,Report3
.title: db "PH",0
.value: db '7.90',0

Report3:MessageBox 600,500,650,200,Gray,.title,.form
.form:  Form 5,40,400,45,Maroon,.value,next
        Cptr popup.set,Results
.title: db "Concentration E.Coli (pour 100ML)",0
.value: db '8513',0


Kinput: KeyFlush .input
.input: Input .buffer
.buffer:Buffer .end-.prompt,.prompt
.prompt rb 32
.end    db 0
.flush: KeyFlush 0
.reset: Node .buffer,.flush,next
        Asm next
        mov dword[.input+input.cursor],0
        mov byte[.prompt],0
        ret

Results:
        Node .init,next
        Cptr popup.set,.disp
.init:  Asm next
        mov dword[.chron+chron.cnt],0
        mov dword[.wtxt+wtxt.start],0
        ret

macro Wtxt t,s,st,l {
        dd wtxt,t,s,st,l
}

.disp:  Gnode 100,0,500,1100,.wtxt,.anim,.box,.chron
.chron: Chron 10000,next
        Cptr popup.set,Message
.box:   Box 0,0,500,1100,Black
.wtxt:  Wtxt .txt,.anim-.str-1,0,6000
.txt:   Vtxt 10,-20,0,0,Lime,.str,1
.str:   file "map/mercator.inc"
        db 0
.anim:  Asm adddw,.wtxt+wtxt.start,100

wtxt:
.call=0
.txt=4
.size=8
.start=12
.length=16
;will window a text from start to stop before to disp it
        ;fcall [esi+.txt]
        ;ret

        pushad
        mov edx,[esi+.size]
        mov eax,[esi+.start]
        mov ecx,[esi+.length]
        cmp eax,0
        jnl @f
        xor eax,eax
@@:
        cmp eax,edx
        jl @f
        lea eax,[edx-1]
@@:
        add ecx,eax
        jnl @f
        xor ecx,ecx
@@:
        cmp ecx,edx
        jl @f
        lea ecx,[edx-1]
@@:
        mov ebx,[esi+.txt]
        mov edx,[ebx+txt.txt]
        push ebx edx
        add [ebx+txt.txt],eax
        mov al,[edx+ecx]
        mov byte[edx+ecx],0
        fcall [esi+.txt]
        mov [edx+ecx],al
        pop edx ebx
        mov [ebx+txt.txt],edx
        popad
        ret

Message:
        Gnode 100,100,0,0,\
        .txt,\
        pixel.notalpha,\
        .box,\
        pixel.setalpha,\
        .key

.key:   Otk key.f8,next
        Cptr popup.set,popup.chron
.box:   Box 0,0,1200,700,Black
.txt:   Vtxt 10,0,0,0,White,.str,5
.str:   db "Bravo moussaillons",CRLF,\
           CRLF,\
           "Vous avez les bons resultats.",CRLF,\
           "Rendez vous sans plus tarder a la base.",CRLF,\
           "Entrez les bonnes donn�es de navigation",CRLF,\
           CRLF,\
           "L'emergence vous emmenera a bon port.",0
include "mappemonde.inc"
include "../../randomMap.inc"

