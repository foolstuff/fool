X=0
Y=0
XL=30
YL=30
TITLEBAR_YL=15
N=1

macro TEXT x,y,c,z {
local .s
        Vtxt x,y,0,0,c,.s,z,ugly
.s:
}

macro TITRE titre {
local .t
        TEXT 15,0,Yellow,1
.t:     db titre,0
        dd .t
}

macro RUBRIQUE x,y,xl,yl,titre,contenu {
.YL=TITLEBAR_YL
local .g,.b
.g:     Gnode x,y,xl,yl+.YL,titre,.b,next
        Gnode 0,.YL,xl,yl,contenu
.b:     Box 0,0,xl,.YL,024487ch;0ebebebh;Teal
.win:   Win .g,win.SHOW
}

macro APPLICATION x,y,xl,yl,title {
local .b
        Node .b,main.win;,.set
.b:     Frame 0,0,xl,yl,Black
@@:     TITRE title
;.set:   Asm next
;        invoke SendMessage,[hwnd_main],WM_SETTEXT,0,[.set-4]
;        ret
main:   RUBRIQUE x,y,xl,yl,@b,.c
.c:
}

macro TXT x,y,c,z {
local .s
        Txt x,y,0,0,c,.s,font85
.s:
}

macro tr {
X=0
Y=Y+YL
}

macro colspan l {
X=X+(XL*l)
}

macro te t {
local .t
      Gnode X,Y,XL,YL,.t
.t:   TEXT 10,10,Black,1
      db t,0
X=X+XL
}

macro TOOLTIP xl,yl,t {
local .f,.t,.b,.g,.fr
      Zone 0,0,xl,yl,next
      Node .g,.f
.f:   Frame 0,0,xl,yl,Blue
.g:   Gnode XL-5,YL-5,90,30,.fr,.t,.b
.t:   TEXT 10,10,Black,1
      db t,0
.b:   Box 0,0,90,30,White
.fr:  Frame 0,0,90,30,Green
}

macro td t,c,l,c2 {
local .b,.t,.f,.tn,.d,.s,.@s,.n,.z
      Gnode X+2,Y+2,XL,YL,.z,.f,.t,.tn,.d,.b
.z:   TOOLTIP XL,YL,l
.b:   Box 2,2,XL-4,YL-4,c
.f:   Frame 2,2,XL-4,YL-4,Black
if c2 eq
.t:   TEXT 5,13,Black,1
      db t,0
.d:   Fxdec .n,.s,.@s
.tn:  TEXT 5,3,Black,1
else
.t:   TEXT 5,13,c2,1
      db t,0
.d:   Fxdec .n,.s,.@s
.tn:  TEXT 5,3,c2,1
end if
.s:   db "   "
.@s:  db 0
.n:   dd N
N=N+1
X=X+XL
}

macro LEGENDE t,c {
local .b,.f,.t
        Gnode X,Y,150,15,.t,.f,.b
.b:     Box 1,1,150,13,c
.f:     Frame 1,1,150,13,Black
.t:     TEXT 10,0,1,1
        db t,0
Y=Y+15
}
