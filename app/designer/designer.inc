include "../../sys/fool.inc"

PEN = Red
RATE equ 10/3
X = 0
Y = 0
VXL = 0
VYL =0
macro Face x,y,xl,yl {
      Frame x,y,xl,yl,PEN
}

macro volume xl,yl,zl,name {
local .a,.b,.c,.f,.xl,.yl,.zl,.txt,.name
.xl=xl*RATE
.yl=yl*RATE
.zl=zl*RATE
      Gnode X,Y,0,0,.a,.b,.c,.f,.txt
.a:   Face 10,10,.xl,.zl
.b:   Face 10,.zl+20,.xl,.yl
.c:   Face .xl+20,.zl+20,.zl,.yl

if VYL < .yl+.zl+30
   VYL = .yl+.zl+30
end if
VXL = .xl+.zl+30
.f:   Frame 0,0,VXL,.yl+.zl+30,Black
.txt: Vtxt .xl+20,20,0,0,Black,.name,RATE,ugly
.name: db name,0
}
