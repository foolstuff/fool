order:
;here, the candles are used to tell what to do with the accounts.
;
;
;

        Asm next

        mov eax,[signal.cnt]
        cmp eax,[ltcchart]

        jl @f
        ret
@@:


        mov ecx,signal.SIZE-1
        mov edx,1
@@:
        mov eax,[signal.s5+edx*4]
        mov [signal.s5+edx*4-4],eax
        mov eax,[signal.s6+edx*4]
        mov [signal.s6+edx*4-4],eax
        mov eax,[signal.s7+edx*4]
        mov [signal.s7+edx*4-4],eax
        inc edx
        loop @b

        mov [wallet.buy],0
        mov [wallet.sell],0

        cmp [candles.order],0
        jle @f
        call .buy
        mov [candles.last],BUY
        jmp .order
@@:
        jge @f
        call .sell
        mov [candles.last],SELL
@@:

.order:

        mov eax,[wallet.ltc]
        mul [probe.rate]
        xor edx,edx
        mov ebx,100
        div ebx
        mov [wallet.total],eax
        call .scale
        mov [signal.s5+signal.SIZE*4-4],eax

        mov eax,[wallet.eur]
        add [wallet.total],eax
        call .scale
        mov [signal.s6+signal.SIZE*4-4],eax

        mov eax,[wallet.total]
        call .scale
        mov [signal.s7+signal.SIZE*4-4],eax

        ret

.scale:
@@:
        cmp eax,40h
        jb @f
        shr eax,6
        jmp @b
@@:
        ret


.sell:
;here we'll sell the currency and get euro

        cmp [candles.last],SELL
        je @f

        mov eax,[wallet.ltc]
        shr eax,1
        jle @f
        mov [wallet.sell],eax
        sub [wallet.ltc],eax
        mul dword[probe.rate]

        xor edx,edx
        mov ebx,100
        div ebx

        add [wallet.eur],eax
@@:
        ret


.buy:
;here we'll buy from euro to get currency
        cmp [candles.last],BUY
        je @f

        mov eax,[wallet.eur]
        shr eax,1
        jle @f
        xor edx,edx
        cmp [probe.rate],0
        je @f
        sub [wallet.eur],eax

        div dword[probe.rate]
        add [wallet.eur],edx

        mov ebx,100
        mul ebx

        mov [wallet.buy],eax
        add [wallet.ltc],eax
@@:
        ret


BUY = 1
SELL = -1
wallet:
.eur    dd 100
.ltc    dd 100
.total  dd ?
.sell   dd ?
.buy    dd ?

probe:
.rate   dd 0