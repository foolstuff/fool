
signal:
.SIZE = WXL
.s1: rd .SIZE
.s2: rd .SIZE
.s3: rd .SIZE
.s4: rd .SIZE
.s5: rd .SIZE
.s6: rd .SIZE
.s7: rd .SIZE

.generate:
        Node .a,.rnd
.cnt    dd 0
.a:     Asm next
        mov eax,[.cnt]
        cmp eax,[ltcchart]
        jl @f
        ret
@@:
        call .scroll
        call .filter1

;        mov ecx,10
;@@:
;        push ecx
        call .filter2
        call .filter3
;        pop ecx
;        loop @b

;        call .filter4
;        call .derivation1
        call .derivation2
;        call .filter3
;        call .filter2
.next:
;        call .rndgen
        call .chartget
        mov [signal.s1+.SIZE*4-4],eax
        ret

.chartget:

        mov eax,[.cnt]
        add [.cnt],4
        mov eax,[ltcchart+4+eax]
        mov [probe.rate],eax
        xor edx,edx
        mov ebx,100
        div ebx
        ret


.rndgen:
        mov eax,[.rnd+rnd.rnd]
        and eax,7fff'ffffh
;        call .downflow
        call .stableflow
        call .upflow
        ret

.stableflow:
        sar eax,24
        mov [probe.rate],eax
        ret

.downflow:
        sar al,4
        movsx eax,al
        add [probe.rate],eax
        mov eax,[probe.rate]
        and eax,0fffh
        mov [probe.rate],eax
        ret

.upflow:
        sar al,4
        movsx eax,al
        sub [probe.rate],eax
        mov eax,[probe.rate]
        and eax,0fffh
        mov [probe.rate],eax
        ret

.scroll:
        mov ecx,.SIZE-1
        mov edx,1
@@:
        mov eax,[signal.s1+edx*4]
        mov [signal.s1-4+edx*4],eax
        inc edx
        loop @b
        ret

.derivation1:
        mov ecx,.SIZE-1
        mov edx,1
@@:
        mov eax,[signal.s4+edx*4]
        mov ebx,[signal.s4-4+edx*4]
        sub eax,ebx
        shl eax,2
        mov [signal.s2-4+edx*4],eax
        inc edx
        loop @b
        ret

.derivation2:
        mov ecx,.SIZE-1
        mov edx,1
@@:
        mov eax,[signal.s2+edx*4]
        mov ebx,[signal.s2-4+edx*4]
        sub eax,ebx
        shl eax,4
        mov [signal.s4-4+edx*4],eax
        inc edx
        loop @b
        ret

.filter1:
        mov ecx,.SIZE-2
        mov edx,1
@@:
        mov eax,[signal.s1+edx*4]
        mov ebx,[signal.s1-8+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov ebx,[signal.s1-4+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov [signal.s2-4+edx*4],eax
        inc edx
        loop @b
        ret

.filter2:
        mov ecx,.SIZE-2
        mov edx,1
@@:
        mov eax,[signal.s2+edx*4]
        mov ebx,[signal.s2-8+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov ebx,[signal.s2-4+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov [signal.s3-4+edx*4],eax
        inc edx
        loop @b
        ret

.filter3:
        mov ecx,.SIZE-2
        mov edx,1
@@:
        mov eax,[signal.s3+edx*4]
        mov ebx,[signal.s3-8+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov ebx,[signal.s3-4+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov [signal.s2-4+edx*4],eax
        inc edx
        loop @b
        ret

.filter4:
        mov ecx,.SIZE-2
        mov edx,1
@@:
        mov eax,[signal.s3+edx*4]
        mov ebx,[signal.s3-8+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov ebx,[signal.s3-4+edx*4]
        sar eax,1
        sar ebx,1
        add eax,ebx
        mov [signal.s4-4+edx*4],eax
        inc edx
        loop @b
        ret

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "litecoin"
@@:
.s      dd @f-$-4
        rd 8
@@:

include 'ltcchart.inc'
