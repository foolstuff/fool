TITLE equ 'Curve Scope'
WINDOWED equ 1

WX = 500
WY = 100
WXL = 1000
WYL = 800

include '../../sys/fool.inc'

curvescope:

Plotter:
        APPLICATION 0,0,WXL,WYL+TITLEBAR_YL,TITLE

        Gnode 0,0,WXL,WYL,\
        .plots,\
        .transaction,.t0,.num0,.t3,.t2,.t1,.num3,.num2,.num1,candles,button.comp,button,.f,.b

.b:     Box 0,0,WXL,WYL,Black+100010h
.f:     Frame 0,0,WXL,WYL,White-100010h

.plots: Gnode 0,-300,0,0,.s1,.s2,.s3,.s4,.s5,.s6,.s7

macro PLOT y,c,s,n {
        Gnode 0,y,0,0,@f,next
        Plotd 0,0,WXL,WYL,c,s,0,0,1,1
@@:     Vtxt 10,400,0,0,c,@f,2,ugly
@@:     db n,0
}

.s1:    PLOT 0,Maroon,signal.s1,"signal 1, input"
.s2:    PLOT 100,Green,signal.s2,"signal 2, filter"
.s3:    PLOT 200,Blue,signal.s3,"signal 3, filter"
.s4:    PLOT 300,Yellow,signal.s4,"signal 4, derivation"
.s5:    PLOT 400,Green,signal.s5,"signal 5, wallet ltc"
.s6:    PLOT 500,Red,signal.s6,"signal 6 wallet eur"
.s7:    PLOT 600,White,signal.s7,"signal 7 wallet total"

.t0:    TEXT 610,10,Blue,1
@@:     db 'Rate = '
.str0   db '123456.78'
@@:     db 0
.num0:  Fxdec probe.rate,.str0,@b

.t1:    TEXT 10,10,Green,1
@@:     db 'LTC = '
.str1   db '123456.78'
@@:     db 0
.num1:  Fxdec wallet.ltc,.str1,@b

.t2:    TEXT 210,10,Red,1
@@:     db 'EUR = '
.str2   db '123456.78'
@@:     db 0
.num2:  Fxdec wallet.eur,.str2,@b

.t3:    TEXT 410,10,White,1
@@:     db 'TOTAL = '
.str3   db '123456.78'
@@:     db 0
.num3:  Fxdec wallet.total,.str3,@b


.transaction:
        Node .sell,.buy,.num4,.num5

.sell:  TEXT 10,30,White,1
@@:     db 'SELL '
.str4   db '000000.00'
@@:     db ' LTC',0
.num4:  Fxdec wallet.sell,.str4,@b

.buy:   TEXT 10,50,White,1
@@:     db 'BUY '
.str5   db '000000.00'
@@:     db ' LTC',0
.num5:  Fxdec wallet.buy,.str5,@b


button:
        But 10,70,100,20,Silver,@f,.act
.act:
        Asm next
        xor dword[.comp],node
        ret
@@:
        Node .txt,.frame
.frame:
        Frame 0,0,100,20,Blue
.txt:   Txt 20,5,0,0,Maroon,@f
@@:     db "COMPUTE",0

.comp:  Node order,signal.generate


include "candles.inc"
include "order.inc"
include "signal.inc"