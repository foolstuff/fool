candles:
        Node .c1,.c2,.c3

.c1:    Asm .candle,5
.c2:    Asm .candle,10
.c3:    Asm .candle,20

.y = 150
.lenght  = asm.op1
        dd 30
.signal dd signal.s1
.start  dd 0
.stop   dd WXL
.entry  dd ?
.exit   dd ?
.mini   dd ?
.maxi   dd ?

.box:   Frame ?,?,?,?,Red

.candle:
        mov eax,[esi+.lenght]
        mov [.box+box.xl],eax

        mov ebx,[.signal]
        mov eax,[.start]
        mov [.box+box.x],eax
.next:
        push eax ebx

        lea ebx,[ebx+eax*4]
        mov ecx,[esi+.lenght]
        dec ecx
        mov eax,[ebx+ecx*4]
        mov [.exit],eax
        mov [.mini],eax
        mov [.maxi],eax

.loop:
        mov eax,[ebx+ecx*4]
        cmp [.mini],eax
        jle @f
        mov [.mini],eax
@@:
        cmp [.maxi],eax
        jge @f
        mov [.maxi],eax
@@:
        loop .loop

        mov [.entry],eax

        call .render
        pop ebx eax
        add eax,[esi+.lenght]
        mov [.box+box.x],eax

        cmp eax,[.stop]

        jle .next

        ret

.render:

        mov eax,.y
        mov ebx,[.entry]
        sub eax,ebx
        mov [.box+box.y],eax
        sub ebx,[.exit]
        mov [.box+box.yl],ebx

        mov dword[.box+box.c],Red
        mov al,[.pos]
        mov [.act],al
        mov byte[.pos],-1
        cmp ebx,0
        jge @f
        mov dword[.box+box.c],Green
        mov byte[.pos],+1
@@:
        mov al,[.pos]
        mov ah,[.act]
        cmp al,ah
        je @f
        cmp al,ah
        mov dword[.box+box.c],Purple
        mov al,0
        jl .sell
.buy:
        mov dword[.box+box.c],White
        mov al,BUY
        jmp @f
.sell:
        mov al,SELL
@@:

        fcall .box
        add [.order],al

        mov eax,.y
        mov ebx,[.maxi]
        sub eax,ebx
        mov [.box+box.y],eax
        sub ebx,[.mini]
        mov [.box+box.yl],ebx

        fcall .box

        ret


.pos    db 0
.act    db 0
.order  db 0
.last   db 0
