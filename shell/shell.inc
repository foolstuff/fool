include 'macros.inc'
include 'equates.inc'
include 'libstr/libstr.inc'
include 'commands/commands.inc'


macro Shell c,t {
        dd shell,c
if t eq
        dd commands
else
        dd t
end if
}

db "shell.inc",0
shell:
.call=0
.command=4
.table=8

        push ebx esi edi
        mov ebx,[esi+.table]
        case ebx,0,.nocom
        mov esi,[esi+.command]
        case esi,0,.nocom
        call trim
.comid:
        mov ecx,[ebx+list.size]
        shr ecx,2
        je .nocom
        dec ecx
        jl .nocom
        mov edx,esi
.next:
        mov edi,[ebx+list.data+ecx*4]
        mov edi,[edi+command.name]
        call cmpstr
        jc .ok
        mov esi,edx
        dec ecx
        jnl .next

        jmp .nocom
.ok:                        ;it is a command, give control to function
        case al,' ',@f      ;is there a space? essential for command with arguments
        case al,TAB,@f      ;is it TAB?
        case al,CR,@f       ;is it CR of LF?
        case al,LF,@f
        casne al,0,.synerr  ;otherwise, is it null terminated?
@@:
        call trim           ;remove spaces and point to options or args or nothing more
        lea edx,[esi+1]     ;edx is the command arguments
        fcall [ebx+list.data+ecx*4]
        mov dword[.txt+txt.txt],eax
        mov dword[.txt+txt.c],Green
        stc
        jmp .end
.nocom:
        mov dword[.txt+txt.txt],.title
        mov dword[.txt+txt.c],Gold
        clc
        jmp .end
.noimpl:
        mov dword[.txt+txt.txt],.title
        mov dword[.txt+txt.c],Maroon
        clc
        jmp .end
.synerr:
        mov dword[.txt+txt.txt],.title
        mov dword[.txt+txt.c],Red
        clc
.end:
        pop edi esi ebx
        ret

.debug: Gnode 0,0,0,0,.txt
.txt:   Txt 0,0,0,0,White,.title
.title: db 'shell v0.1',0

.execute: Shell 0