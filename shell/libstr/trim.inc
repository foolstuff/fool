trim:
;esi => text
;return esi => text - spaces
;al = [esi]
@@:
        lodsb
        case al,0,@f
        case al,SPACE,@b
        case al,TAB,@b
@@:
        dec esi
        ret

