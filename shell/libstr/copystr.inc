copystr:
        push edi
@@:
        lodsb
        case al,0,@f
        stosb
        jmp @b
@@:
        pop eax
        sub eax,edi
        neg eax
        ret

copyword:
        push edi
@@:
        lodsb
        case al,' ',@f
        case al,0,@f
        case al,CR,@f
        stosb
        jmp @b
@@:
        pop eax
        sub eax,edi
        neg eax
        ret

copyline:
        push edi
@@:
        lodsb
        case al,0,@f
        case al,CR,@f
        stosb
        jmp @b
@@:
        pop eax
        sub eax,edi
        neg eax
        ret
