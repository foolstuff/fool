sizestr:
;esi = string
;return eax = size
	xor eax,eax
.loop:
	cmp byte[esi+eax],0
	lea eax,[eax+1]
	jne .loop
	ret
