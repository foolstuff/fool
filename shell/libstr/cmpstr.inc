cmpstr:
;esi=string1
;edi=string2
;return al=last char in esi
;cf set if string equals
        push edx
        xor edx,edx
.loop:
        mov al,[esi+edx]
        mov ah,[edi+edx]
        case ah,0,.yes?
        or eax,2020h
        inc edx
        case al,ah,.loop
        jmp .no
.yes?:
        casne edx,0,.yes!
        casne al,ah,.no
.yes!:
        lea esi,[esi+edx]
        stc
        jmp .end
.no:
        clc
.end:
        pop edx
        ret
