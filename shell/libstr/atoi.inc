;return value from signed decimal at [esi] in eax
;when executing a command from command line,
;esi will point directlly after the command.
;carry set if valid decimal number
atoi:
.trim:
	call trim
	dec esi
.seekend:
	lodsb
	case al,' ',@f
	case al,'.',@f
	case al,',',@f
	case al,'!',@f
	casne al,0,.seekend
@@:
	dec esi
	push esi
	dec esi
	xor ebx,ebx
	mov edx,1
.next:
	movzx eax,byte[esi]
	dec esi
	case al,'-',.neg
	case al,' ',.end
	case al,',',.end
	case al,'.',.end
	casl al,'0',@f
	casg al,'9',@f
	sub al,'0'
	imul eax,edx
	add ebx,eax
	imul edx,10
	jmp .next
@@:
	pop esi
	clc
	mov eax,ebx
	ret
.neg:
	mov al,[esi]
	casne al,' ',@b
	neg ebx
.end:
	pop esi
	stc
	mov eax,ebx
	ret
