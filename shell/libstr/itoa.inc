;transparent return
itoa:
	cmp eax,0
	mov byte[edi],' '
	jg @f
	mov byte[edi],'-'
	neg eax
@@:
	mov ecx,10
	mov ebx,edi
	push eax
@@:
	mov al,[ebx]
	inc ebx
	case al,CR,@f
	case al,LF,@f
	casne al,0,@b
@@:
	pop eax
	dec ebx
.loop:
	dec ebx
	casl ebx,edi,.end
	xor edx,edx
	div ecx
	casne edx,0,@f
	casne eax,0,@f
	mov dl,' '-'0'
@@:
	add dl,'0'
	cmp byte[ebx],'-'
	je .end
	mov [ebx],dl
	jmp .loop
.end:
	ret
