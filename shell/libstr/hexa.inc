hexBuffer:
;esi=buffer
;edi=hex buffer
;return esi=hex buffer end
	push edi esi
	mov esi,buffer
	mov edi,buffer.hex
	call hexa
	mov esi,edi
	pop esi edi
	ret
hexa:
	push ebx ecx edx esi edi
	mov ecx,[hexl]
	mov ebx,eax
.line:
	dec ebx
	jl .end
	dec ecx
	jl .crlf
	mov al,[esi]
	inc esi
;        case al,0,.end
	call hex
	mov [edi],ax
	add edi,2
	jmp .line
.crlf:
	mov word[edi],0a0dh
	add edi,2
	mov ecx,[hexl]
	inc ebx
	jmp .line
.end:
	mov byte[edi+0],13
	mov byte[edi+1],10
	mov byte[edi+2],13
	mov byte[edi+3],10
	mov byte[edi+4],0
	lea eax,[edx+4]
	pop edi esi edx ecx ebx
	retn

hex:
	push ebx
	xor ah, ah
	mov bl, 16
	div bl
	imul ebx, eax, 7
	shr bx, 6
	and bl, 1
	lea eax,[eax+8*ebx+'00'] ; 32 bit
	sub eax,ebx
	pop ebx
	ret
