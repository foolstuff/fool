macro case var,value,jmp {
        cmp var,value
        je jmp
}

macro casne var,value,jmp {
        cmp var,value
        jne jmp
}

macro casa var,value,jmp {
        cmp var,value
        ja jmp
}

macro casle var,value,jmp {
        cmp var,value
        jle jmp
}

macro casg var,value,jmp {
        cmp var,value
        jg jmp
}

macro casge var,value,jmp {
        cmp var,value
        jge jmp
}

macro casl var,value,jmp {
        cmp var,value
        jl jmp
}
