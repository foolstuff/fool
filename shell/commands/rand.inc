rand!:	;/rand (N)
;get the next (N) random byte(s)
dd .name
dd @f
dd .syntax
@@:
	call atoi
	jnc @f
	rdrand eax
	mov edi,.rdstr
	call itoa
	mov eax,edi
	ret
@@:
	mov eax,.syntax
	ret
.name db 'rand',0
.syntax:
db SYNTAX,'/rand',CRLF
db 'get the next random byte',0
.rdstr:
db '0123456789',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

