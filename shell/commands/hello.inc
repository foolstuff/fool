hello!: Command .name,.syntax

        push ebx ecx edx edi esi
        mov ebx,.end-.words
        rdrand eax
        cdq
        idiv ebx
        mov edi,.reply
        lea esi,[.words+edx]
        call copyline
        mov byte[edi],0
        mov eax,.reply
        pop esi edi edx ecx ebx
        ret

.name db 'hello',0
.syntax:
db SYNTAX,'hello',CRLF
db 'Tell you random thing',0
.words: file "hello.inc"
.end:   db 0

.reply:
db 129 dup 0
