@@:	db 'time',0
	dd @b
time!:	;/time OPT
;get the current time
;can force OPTional content bounded by two time captures
	mov eax,.syntax
	ret
.syntax:
db SYNTAX,'/time OPT',CRLF
db 'get the current time',CRLF
db 'can force OPTional content bounded by two time captures',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

