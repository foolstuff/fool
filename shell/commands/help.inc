help!:
        Command .name,.syntax
        push ebx ecx edi esi
        mov ebx,[shell.execute+shell.table]
        mov ecx,[ebx+list.size]
        shr ecx,2
        dec ecx
        mov edi,.commands
@@:
        mov esi,[ebx+list.data+ecx*4]
        mov esi,[esi+command.syntax]
        call copystr
        mov esi,.separator
        call copystr
        dec ecx
        jnl @b

        mov eax,.commands
        pop esi edi ecx ebx
        ret

.name db 'help',0
.syntax:
db SYNTAX,'help',CRLF
db 'display the list of supported commands',0
.commands:
db 1024 dup 0
.separator db CRLF,CRLF,0
