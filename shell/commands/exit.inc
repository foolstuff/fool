exit!:  Command .name,.syntax
        fcall system.exit!
        mov eax,.syntax
        ret

.name   db 'exit',0
.syntax:
db SYNTAX,'exit',CRLF
db 'exit the program',0
