send!: ;/send content
;send content to peer, used to make peer execute commands
dd .name
dd @f
dd .syntax
@@:
	mov al,[esi]
	case al,0,@f
	mov eax,esi
	ret
@@:
	mov eax,.syntax
	ret
.name db 'send',0
.syntax:
db SYNTAX,'/send content',CRLF
db 'send content to peer, used to make peer execute commands',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
