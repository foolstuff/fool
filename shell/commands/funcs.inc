Cf:
@@:	db 'code',0
	dd @b
.code: ;/code
;send the code as hexadecimal view to peer
	mov eax,Cmsg.codesyntax
	ret

@@:	db 'data',0
	dd @b
.data: ;/data
;send the data as hexadecimal view to peer
	mov eax,Cmsg.datasyntax
	ret

@@:	db 'echo',0
	dd @b
.echo:	;/echo T
;repeat the Text, usefull to launch a command in both peers
	mov eax,Cmsg.echosyntax
	ret

@@:	db 'exit',0
	dd @b
.exit:	;/exit
;exit the programm
	mov eax,Cmsg.exitsyntax
	ret

@@:	db 'quit',0
	dd @b
.quit:	;/quit
;duplicate for exit
	mov eax,Cmsg.quitsyntax
	ret

@@:	db 'serv',0
	dd @b
.serv: ;/serv P
;set server port P
	mov eax,Cmsg.servsyntax
	ret

@@:	db 'host',0
	dd @b
.host: ;/host I,P
;set ip I and port P of the host server
	mov eax,Cmsg.hostsyntax
	ret

@@:	db 'wait',0
	dd @b
.wait: ;/wait
;wait for peer to connect
	mov eax,Cmsg.waitsyntax
	ret

@@:	db 'join',0
	dd @b
.join: ;/join
;join the host server
	mov eax,Cmsg.joinsyntax
	ret

@@:	db 'help',0
	dd @b
.help: ;/help
;display the help file to peer and local
	mov eax,Cmsg.helpsyntax
	ret

@@:	db 'send',0
	dd @b
.send: ;/send content
;send content to peer, used to make peer execute commands
	mov al,[esi]
	case al,0,.sendsyntax
	mov eax,esi
	ret
.sendsyntax:
	mov eax,Cmsg.sendsyntax
	ret

@@:	db 'file',0
	dd @b
.file: ;/file F,P
;send the file F in P bytes chunks
	mov eax,Cmsg.file
	ret

@@:	db 'disk',0
	dd @b
.disk: ;/disk D,S,C
;send the C sectors S from disk D
	mov eax,Cmsg.disk
	ret

@@:	db 'disp',0
	dd @b
.disp:	;/disp T
;display the Text or else thing
	mov eax,Cmsg.dispsyntax
	ret

@@:	db 'stop',0
	dd @b
.stop:	;/stop
;stop the connection
	mov eax,Cmsg.stopsyntax
	ret

@@:	db 'time',0
	dd @b
.time:	;/time OPT
;get the current time
;can force OPTional content bounded by two time captures
	mov eax,Cmsg.timesyntax
	ret

@@:	db 'rxor',0
	dd @b
.rxor:	;/rxor OPT
;set transfert with rand xor cryptography
;can force OPTional content
	mov eax,Cmsg.rxorsyntax
	ret

@@:	db 'rand',0
	dd @b
.rand:	;/rand (N)
;get the next (N) random byte(s)
	call atoi
	jnc @f
	mov ecx,eax

@@:
	mov eax,Cmsg.randsyntax
	ret

@@:	db 'seed',0
	dd @b
.seed:	;/seed S
;set the seed S for random generator
	mov eax,Cmsg.seedsyntax
	ret

@@:	db 'user',0
	dd @b
.user:	;/user ID
;IDentify a user, need to confirm with password
	mov eax,Cmsg.usersyntax
	ret

@@:	db 'pass',0
	dd @b
.pass: ;/pass PW
;give the PassWord to verify user
	mov eax,Cmsg.passsyntax
	ret

@@:	db 'text',0
	dd @b
.text:	;/text OPT
;set the transfert mode to text, used to send raw binary
;can force OPTional content
	mov eax,Cmsg.textsyntax
	ret

@@:	db 'hexa',0
	dd @b
.hexa:	;/hexa OPT
;set the transfert mode to hexadecimal, used to send binary as ascii
;can force OPTional content
	mov eax,Cmsg.hexasyntax
	ret

@@:	db 'loop',0
	dd @b
.loop:	;/loop N(base 10)
;resend /loop N-1 if N>0, /loop N+1 if N<0, stop when N=0
	call atoi	   ;convert current decimal string
	jnc @f		   ;if not a number
	casge eax,0,@f
	inc eax
	jge .loopsyntax
@@:
	dec eax 	   ;adjust for previous inc eax
	jle .loopsyntax
	mov edi,Cmsg.loopN ;point to N argument for /loop
	call itoa	   ;convert eax to the N argument
	mov eax,Cmsg.loop  ;point to the full /loop string
	ret
.loopsyntax:
	mov eax,Cmsg.loopsyntax
	ret

@@:	db 'ping',0
	dd @b
.ping:
	mov eax,Cmsg.pong
	ret

@@:	db 'pong',0
	dd @b
.pong:
	mov eax,Cmsg.ping
	ret

@@:	db 'save',0
	dd @b
.save:
	mov eax,Cmsg.savesyntax
	ret

@@:	db 'load',0
	dd @b
.load:
	mov eax,Cmsg.loadsyntax
	ret

