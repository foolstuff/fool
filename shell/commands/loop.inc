loop!: ;/loop N(base 10)
;resend /loop N-1 if N>0, /loop N+1 if N<0, stop when N=0
dd .name
dd @f
dd .syntax
@@:
	call atoi      ;convert current decimal string
	jnc @f	       ;if not a number
	casge eax,0,@f
	inc eax
	jge .syn
@@:
	dec eax        ;adjust for previous inc eax
	jle .syn
	mov edi,.N     ;point to N argument for /loop
	call itoa      ;convert eax to the N argument
	mov eax,.loop  ;point to the full /loop string
	ret
.syn:
	mov eax,.syntax
	ret
.loop:
db '/loop '
.N:
db '0123456789',0
.name db 'loop',0
.syntax:
db SYNTAX,'/loop N',CRLF
db 'resend /loop N-1 until N=0',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
