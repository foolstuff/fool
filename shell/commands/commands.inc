command:
.call=0
.name=4
.syntax=8

macro Command n,s {
common
local .c
      dd .c,n,s
common
.c:
}

commands:
        List exit!,help!,hello!,inc!

include 'exit.inc'
include 'help.inc'
include 'hello.inc'
include 'inc.inc'

;.code   dd code!
;.data   dd data!
;.disk   dd disk!
;.disp   dd disp!
;.echo   dd echo!
;.exit   dd exit!
;.file   dd file!
;.help   dd help!
;.hexa   dd hexa!
;.host   dd host!
;.join   dd join!
;.load   dd load!
;.loop   dd loop!
;.pass   dd pass!
;.ping   dd ping!
;.pong   dd pong!
;.quit   dd quit!
;.rand   dd rand!
;.rxor   dd rxor!
;.save   dd save!
;.seed   dd seed!
;.send   dd send!
;.serv   dd serv!
;.stop   dd stop!
;.text   dd text!
;.time   dd time!
;.user   dd user!
;.wait   dd wait!
;.color  dd color!
;@@:

;include 'code.inc'
;include 'data.inc'
;include 'disk.inc'
;include 'disp.inc'
;include 'echo.inc'
;include 'file.inc'
;include 'hexa.inc'
;include 'host.inc'
;include 'join.inc'
;include 'load.inc'
;include 'loop.inc'
;include 'pass.inc'
;include 'ping.inc'
;include 'pong.inc'
;include 'quit.inc'
;include 'rand.inc'
;include 'rxor.inc'
;include 'save.inc'
;include 'seed.inc'
;include 'send.inc'
;include 'serv.inc'
;include 'stop.inc'
;include 'text.inc'
;include 'time.inc'
;include 'user.inc'
;include 'wait.inc'
;include 'color.inc'
