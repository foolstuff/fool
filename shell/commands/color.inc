color!: ;/color c1,c2,c3,c4
;set colors
;.get = c1
;.msg = c2
;.err = c3
;.send = c4
dd .name
dd @f
dd .syntax
@@:
	call atoi
	jnc .syn
	mov [color.get],eax
	mov edi,.get
	call itoa
	call atoi
	jnc .syn
	mov [color.msg],eax
	mov edi,.msg
	call itoa
	call atoi
	jnc .syn
	mov [color.err],eax
	mov edi,.err
	call itoa
	call atoi
	jnc .syn
	mov [color.send],eax
	mov edi,.send
	call itoa
	mov eax,.msgok
	ret
.syn:
	mov eax,.syntax
	ret
.name db 'color',0
.msgok:
db 'colors are redifined',CRLF
db '.get = '
.get:
db '0123456789',CRLF
db '.msg = '
.msg:
db '0123456789',CRLF
db '.err = '
.err:
db '0123456789',CRLF
db '.send = '
.send:
db '0123456789',0
.syntax:
db SYNTAX,'/color c1,c2,c3,c4',CRLF
db 'set colors',CRLF
db '.get = c1',CRLF
db '.msg = c2',CRLF
db '.err = c3',CRLF
db '.send = c4',0


