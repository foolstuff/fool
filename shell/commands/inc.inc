inc!:   Command .name,.syntax
        mov esi,edx
        call atoi
        jnc @f
        inc eax
        mov edi,.N     ;point to N argument for /loop
        call itoa
        mov eax,.N
        ret
@@:
        mov eax,.syntax
        ret
.N:
db '0123456789',0
.name db 'inc',0
.syntax:
db SYNTAX,'inc N',CRLF
db 'increment value N',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
