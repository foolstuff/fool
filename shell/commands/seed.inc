seed!:	;/seed S
;set the seed S for random generator
dd .name
dd @f
dd .syntax
@@:
	call atoi
	jnc @f
	mov [.seed],eax
	mov edi,.str
	call itoa
	mov eax,edi
	ret
@@:
	mov eax,.syntax
	ret
.seed:	dd ?
.str:	db '-1234567890',0
.name:	db 'seed',0
.syntax:
db SYNTAX,'/seed S',CRLF
db 'set the seed S for random generator',0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

