;FOOLOS:
SHOWFPS:
RAYS:
include "sys/fool.inc"
TestCircles:

RAY=80

Node circles,fore

circles:
        Gnode 0,RAY,0,0,.circle,.circle0,.circle1,.circle2,.circle3,.circle4
db "circles",0
.circle:  Circle RAY*0,0,RAY,RAY,Red,1
db "c0",0
.circle0: Circle RAY*1,0,RAY,RAY,Red,2
db "c0",0
.circle1: Circle RAY*2,0,RAY,RAY,Red,3
db "c0",0
.circle2: Circle RAY*3,0,RAY,RAY,Red,7
db "c0",0
.circle3: Circle RAY*4,0,RAY,RAY,Red,11
db "c0",0
.circle4: Circle RAY*5,0,RAY,RAY,Red,13

db "fore",0
fore:
       For .init,.stop,.inc,\
    .circle
db ".circle",0
.circle:  Circle 0,0,RAY,RAY,Red,1
db ".init",0
.init: Asm movdw,.inc+asm.op1,1
db ".stop",0
.stop: Asm cmpdw,.inc+asm.op1,17
db ".inc",0
.inc:  Asm @f,0
@@:
       mov eax,[esi+asm.op1]
       lea ebx,[eax-1]
       ;shl eax,1
       mov [.circle+circle.res],eax
       imul ebx,RAY
       mov [.circle+circle.x],ebx
       inc dword[esi+asm.op1]
       ret
db "end",0
