include 'sys/fool.inc'

Node .box,.setframe,.box,.setbox
.box:
        Box 100,100,100,100,Red
.frame:
        Frame 100,100,100,100,Green
.setframe:
        Asm next
        mov dword[.box],frame
        mov dword[.box+box.c],Black
        ret
.setbox:
        Asm next
        mov dword[.box],box
        mov dword[.box+box.c],Yellow
        ret