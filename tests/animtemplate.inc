;FOOLOS:
;MONITORING:
;BGNDCOLOR=Black

include 'sys/fool.inc'

animTemplate:
Node animation, animation2;, animation.node, animation2.node
animation:
        Anim 0,0,2,2,1000/50,next
.node:
        Node \
        .f00,\
        .f01,\
        .f02,\
        .f03,\
        .f04,\
        .f05,\
        .f06,\
        .pong

.f00:   Frame -1,-1,2,2,Red
.f01:   Frame -5,-5,10,10,Red
.f02:   Frame -10,-10,20,20,Red-4
.f03:   Frame -20,-20,40,40,Red-8
.f04:   Frame -40,-40,80,80,Red-12
.f05:   Frame -80,-80,160,160,Red-16
.f06:   Frame -160,-160,320,320,Red-20

.pong:  dd pong,13,9

animation2:
        Anim 0,0,2,2,1000/20,next
.node:
        Node \
        .f00,\
        .f01,\
        .f02,\
        .f03,\
        .f04,\
        .f05,\
        .f06,\
        .pong

.f00:   Circle -1,-1,2,2,Lime
.f01:   Circle -5,-5,10,10,Lime
.f02:   Circle -10,-10,20,20,Lime-4
.f03:   Circle -20,-20,40,40,Lime-8
.f04:   Circle -40,-40,80,80,Lime-12
.f05:   Circle -80,-80,160,160,Lime-16
.f06:   Circle -160,-160,320,320,Lime-20
.pong:  dd pong,17,-15
