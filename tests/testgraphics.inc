include "sys/fool.inc"

macro Progress x,y,xl,yl,c,p {
       dd progress,x,y,xl,yl,c,p,0,100
}

graphicTest:


Node \
ploop,\
.box,\
.border,\
.circles,\
.line,\
.frame,\
.zone,\
.put,\
.but,\
.progress

.box:    Box 100,100,50,50,Red
.border: Border 200,100,50,50,Blue
.circles:
         Gnode 600,100,0,0,next
         Asm @f
.inci    dd 9
@@:
         mov ecx,50

         mov eax,[graphicTest.progress+progress.p]
         and eax,0x1f
         jne @f
         mov eax,1
@@:
         lea ebx,[eax*2]
         mov dword[.circle+circle.x],0
         mov dword[.circle+circle.y],0
         mov dword[.circle+circle.xl],800
         mov dword[.circle+circle.yl],800
@@:
         sub [.circle+circle.xl],ebx
         sub [.circle+circle.yl],ebx
         add [.circle+circle.x],eax
         add [.circle+circle.y],eax
         add [.circle+circle.c],eax
         push ecx
         fcall .circle
         pop ecx

         loop @b
         ret

.circle: Circle 0,0,50,50,Green

.line:   Line 200,200,50,50,Black
.frame:  Frame 100,300,50,50,Purple
.zone:   Zone 200,300,50,50,.zbox
.put:    Put 300,100,50,50,Green,.zbox
.but:    But 300,200,50,50,0,.box,.zbox
.zbox:   Box 0,0,50,50,Navy
.progress:
         Progress 100,400,400,100,Red,1

ploop:   Chron 100,next
         Asm @f
.step    dd 0
.incp    dd 1
.iter    dd 0

@@:
         inc [.iter]
         mov eax,[graphicTest.progress+progress.p]
         add eax,[.incp]
         cmp eax,[graphicTest.progress+progress.max]
         jnl @f
         cmp eax,[graphicTest.progress+progress.min]
         jg .end
@@:
.inc:
         inc [.step]
         and [.step],3
         neg [.incp]
.end:
         mov [graphicTest.progress+progress.p],eax
         ret

progress:
.call=0
.x=4
.y=8
.xl=12
.yl=16
.c=20
.p=24
.min=28
.max=32
        push dword[esi+.xl]
        call frame
        mov eax,[esi+.p]
        mov ebx,[esi+.min]
        mov ecx,[esi+.max]
        cmp ebx,ecx
        jge .box
        cmp eax,ebx
        jg @f
        mov eax,ebx
        mov dword[esi+.xl],1
        jmp .box
@@:
        cmp eax,ecx
        jge .box

        sub ecx,ebx
        sub eax,ebx
        imul eax,[esi+.xl]
        cdq
        idiv ecx
        or eax,eax
        jne @f
        mov eax,1
@@:
        mov [esi+.xl],eax
.box:
        call box
        pop dword[esi+.xl]
        call border
        ret

