WINDOWED equ 1
;FIXED equ 1
;HIDDEN equ 1
ONTOP equ 1
TRANSPARENCY equ 220
NOBORDER equ 1

include '../sys/fool.inc'

WX=500
WY=100
WXL=700
WYL=700+TITLEBAR_YL

fontUtil:
        APPLICATION 0,0,WXL,WYL,"font util � edfed � 2019"
        Gnode 0,0,WXL,WYL,Buttons,dispChar,dispFont

dispFont:
        Node .t2,.t1,.b
.t1:    Vtxt 350,10,0,0,White,.txt,1,ugly
.t2:    Vtxt 10,10,0,0,White,.txt,2,ugly
.b:     Box 0,0,WXL,WYL,Black
.txt:   db '.',1,2,3,4,5,6,7
        db 8,'.','.',11,12,'.',14,15
        db CRLF
        ..n = 16
        repeat 15
         repeat 16
          db ..n+%-1
         end repeat
         db CRLF
         ..n = ..n + 16
        end repeat
        db 0

dispChar:
        Gnode 10,500,0,0,.c,.f,.cf,.txt,.but,.over,.a
.but:   But 0,0,6*16-1,9*16-1,0,.tb,.select
.txt:   Txt 0,0,0,0,Silver,dispFont.txt,font85
.tb:    Frame 0,0,6,9,Yellow
.tf:    Frame 0,0,6,9,Lime
.cf:    Box 150,0,10*8,15*8,Green
.f:     Frame 150,0,12*8,15*8,Red
.c:     Vtxt 158,8,0,0,White,.char,8,ugly

.char:  db 'F',0

.over:
        Zone 0,0,6*16-1,9*16-1,next
        Node .tf,next
        Asm next

        ;.tf.x = ((edi.x-mouse.x)/6) * 6

        mov eax,[mouse+box.x]
        sub eax,[edi+box.x]
        cdq
        mov ebx,6
        idiv ebx
        mov ecx,eax
        imul eax,ebx
        mov [.tf+frame.x],eax

        ;.tf.y = ((edi.y-mouse.y)/9) * 9

        mov eax,[mouse+box.y]
        sub eax,[edi+box.y]
        cdq
        mov ebx,9
        idiv ebx
        mov ch,al
        imul eax,ebx
        mov [.tf+frame.y],eax
        mov [.selector],ecx

        ret
.selector:
        dd ?
.select:
        Asm next
        mov eax,[.selector]
        shl ah,4
        add al,ah
        ;.char = (.selector.highbyte) << 4 + (.selector.lowbyte)
        mov [.char],al
        ret

.a:     ;Timer 10,next
        Asm next
;        inc byte [.char]

        push eax ebx
        movzx eax,byte[.char]
        mov ebx,eax
        and eax,0fh
        shr ebx,4
        imul eax,6
        imul ebx,9
        ;.tb.x = (.char && 0fh) * 6
        ;.tb.y = (.char >> 4) * 9
        mov [.tb+txt.x],eax
        mov [.tb+txt.y],ebx

        movzx eax,byte[.char]
        mov eax,[ugly+eax*4] ;char adress in list
        movzx ebx,byte[eax];size of char in char
        movzx ebx,byte[ebx+eax+1];offset of char in char
        shl ebx,3
        jg @f
        mov ebx,1
@@:
        ;.cf.xl = (ugly[char].xl)<<3
        mov [.cf+frame.xl],ebx
        pop ebx eax

        ret

Save:   Logger ugly-8,.name;Convert.mem,.name

.name:  db "uglyfont.bin",0


difool:
.call=0
.item=4
.dest=8

        ret

macro Difool i,d  {
        dd difool,i,d
}

Convert:
        Asm next
        ret

        Difool ugly-8,.mem

.mem:   dd ugly,100;dd 0

Buttons:
        Gnode 350,500,0,0,.open,.reload,.save,.new,.filename,system.exitButton

.open:  But 0,80,70,15,0,@f,next
        dd 0
@@:     Node @f,.skin
@@:     TEXT 10,0,Yellow,1
        db "open",0

.reload:But 0,100,70,15,0,@f,next
        dd 0
@@:     Node @f,.skin
@@:     TEXT 10,0,Yellow,1
        db "reload",0

.save:  But 0,120,70,15,0,@f,next
        Node Save,Convert
@@:     Node @f,.skin
@@:     TEXT 10,0,Yellow,1
        db "save",0

.new:   But 0,140,70,15,0,@f,next
        dd 0
@@:     Node @f,.skin
@@:     TEXT 10,0,Yellow,1
        db "new",0

.skin:  Node .border,.box
.border:Border 0,0,70,15,White
.box:   Box 0,0,70,15,Purple/2

.filename:
        Gnode 90,0,0,0,@f,next
        Box 0,0,90,15,Gray/2
@@:     TEXT 5,0,Yellow,1
        db "ugly.inc",0

