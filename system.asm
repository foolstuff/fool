;FOOLOS:
;WINDOWED equ 1
WX=0
WY=0
WXL=750
WYL=600
;NO3DBORDER equ 1
include 'sys/fool.inc'
        Win systeme,win.SHOW

dd      systeme.name

systeme:
        Gnode WX,WY,WXL,WYL,content,.title,.backt,\
        pixel.notalpha,.box,pixel.setalpha

.box:   Box 0,0,WXL,WYL,Black
.title: Vtxt 20,0,0,0,White,.name,1,ugly
.backt: Vtxt 21,1,0,0,Black,.name,1,ugly
.name:  db 'systeme',0

content:
        Node .txt
.txt:   Vtxt 20,40,0,0,Black,.str,1,ugly
.str:   file 'sys/foolw.inc'
        db 0
