db ' keyasm '
keyasm:
.mov	 db    'mov ',0
.xchg	 db    'xchg ',0
.push	 db    'push ',0
.pushw	 db    'pushw ',0
.pushd	 db    'pushd ',0
.pusha	 db    'pusha ',0
.pushaw  db    'pushaw ',0
.pushad  db    'pushad ',0
.pop	 db    'pop ',0
.popw	 db    'popw ',0
.popd	 db    'popd ',0
.popa	 db    'popa ',0
.popaw	 db    'popaw ',0
.popad	 db    'popad ',0
.cwd	 db    'cwd ',0
.cdq	 db    'cdq ',0
.cbw	 db    'cbw ',0
.cwde	 db    'cwde ',0
.movsx	 db    'movsx ',0
.movzx	 db    'movzx ',0
.add	 db    'add ',0
.adc	 db    'adc ',0
.inc	 db    'inc ',0
.sub	 db    'sub ',0
.sbb	 db    'sbb ',0
.dec	 db    'dec ',0
.cmp	 db    'cmp ',0
.neg	 db    'neg ',0
.xadd	 db    'xadd ',0
.mul	 db    'mul ',0
.imul	 db    'imul ',0
.div	 db    'div ',0
.idiv	 db    'idiv ',0
.daa	 db    'daa ',0
.das	 db    'das ',0
.aaa	 db    'aaa ',0
.aas	 db    'aas ',0
.aam	 db    'aam ',0
.aad	 db    'aad ',0
.not	 db    'not ',0
.and	 db    'and ',0
.or	 db    'or ',0
.xor	 db    'xor ',0
.bt	 db    'bt ',0
.bts	 db    'bts ',0
.btr	 db    'btr ',0
.btc	 db    'btc ',0
.bsf	 db    'bsf ',0
.bsr	 db    'bsr ',0
.shl	 db    'shl ',0
.sal	 db    'sal ',0
.shr	 db    'shr ',0
.sar	 db    'sar ',0
.shld	 db    'shld ',0
.shrd	 db    'shrd ',0
.rol	 db    'rol ',0
.rcl	 db    'rcl ',0
.ror	 db    'ror ',0
.rcr	 db    'rcr ',0
.test	 db    'test ',0
.bswap	 db    'bswap ',0
.jmp	 db    'jmp ',0
.jmpnear db    'jmp near ',0
.jmpfar  db    'jmp far ',0
.call	 db    'call ',0
.ret	 db    'ret ',0
.retn	 db    'retn ',0
.retf	 db    'retf ',0
.retw	 db    'retw ',0
.retnw	 db    'retnw ',0
.retfw	 db    'retfw ',0
.retd	 db    'retd ',0
.retnd	 db    'retnd ',0
.retfd	 db    'retfd ',0
.iret	 db    'iret ',0
.iretw	 db    'iretw ',0
.iretd	 db    'iretd ',0
.jo	 db    'jo ',0
.jno	 db    'jno ',0
.jnc	 db    'jnc ',0
.jae	 db    'jae ',0
.jnb	 db    'jnb ',0
.je	 db    'je ',0
.jz	 db    'jz ',0
.jne	 db    'jne ',0
.jnz	 db    'jnz ',0
.jbe	 db    'jbe ',0
.jna	 db    'jna ',0
.ja	 db    'ja ',0
.jnbe	 db    'jnbe ',0
.js	 db    'js ',0
.jns	 db    'jns ',0
.jp	 db    'jp ',0
.jpe	 db    'jpe ',0
.jnp	 db    'jnp ',0
.jpo	 db    'jpo ',0
.jl	 db    'jl ',0
.jnge	 db    'jnge ',0
.jge	 db    'jge ',0
.jnl	 db    'jnl ',0
.jle	 db    'jle ',0
.jng	 db    'jng ',0
.jg	 db    'jg ',0
.jnle	 db    'jnle ',0
.lea	 db    'lea ',0
.loop	 db    'loop ',0
.loopw	 db    'loopw ',0
.loopd	 db    'loopd ',0
.loope	 db    'loope ',0
.loopz	 db    'loopz ',0
.loopew  db    'loopew ',0
.loopzw  db    'loopzw ',0
.loopne  db    'loopne ',0
.loopnz  db    'loopnz ',0
.loopnew db    'loopnew ',0
.loopnzw db    'loopnzd ',0
.loopned db    'loopned ',0
.loopnzd db    'loopnzd ',0
.jcxz	 db    'jcxz ',0
.jecxz	 db    'jecxz ',0
.int	 db    'int ',0
.int3	 db    'int3 ',0
.into	 db    'into ',0
.bound	 db    'bound ',0
.in	 db    'in ',0
.out	 db    'out ',0
.movs	 db    'movs ',0
.movsb	 db    'movsb ',0
.movsw	 db    'movsw ',0
.movsd	 db    'movsd ',0
.cmps	 db    'cmps ',0
.cmpsb	 db    'cmpsb ',0
.cmpsw	 db    'cmpsw ',0
.cmpsd	 db    'cmpsd ',0
.scas	 db    'scas ',0
.scasb	 db    'scasb ',0
.scasw	 db    'scasw ',0
.scasd	 db    'scasd ',0
.stos	 db    'stos ',0
.stosb	 db    'stosb ',0
.stosw	 db    'stosw ',0
.stosd	 db    'stosd ',0
.lods	 db    'lods ',0
.lodsb	 db    'lodsb ',0
.lodsw	 db    'lodsw ',0
.lodsd	 db    'lodsd ',0
db ' registers '
;registers:
.al	db	'al',0
.ah	db	'ah',0
.bl	db	'bl',0
.bh	db	'bh',0
.cl	db	'cl',0
.ch	db	'ch',0
.dl	db	'dl',0
.dh	db	'dh',0
.ax	db	'ax',0
.bx	db	'bx',0
.cx	db	'cx',0
.dx	db	'dx',0
.sp	db	'sp',0
.bp	db	'bp',0
.si	db	'si',0
.di	db	'di',0
.eax	db	'eax',0
.ebx	db	'ebx',0
.ecx	db	'ecx',0
.edx	db	'edx',0
.esp	db	'esp',0
.ebp	db	'ebp',0
.esi	db	'esi',0
.edi	db	'edi',0
.cs	db	'cs',0
.ds	db	'ds',0
.es	db	'es',0
.fs	db	'fs',0
.gs	db	'gs',0
.ss	db	'ss',0
.cr0	db	'cr0',0
.cr2	db	'cr2',0
.cr3	db	'cr3',0
.cr4	db	'cr4',0
.dr0	db	'dr0',0
.dr1	db	'dr1',0
.dr3	db	'dr3',0
.dr6	db	'dr6',0
.dr7	db	'dr7',0
.st0	db	'st0',0
.st1	db	'st1',0
.st2	db	'st2',0
.st3	db	'st3',0
.st4	db	'st4',0
.st5	db	'st5',0
.st6	db	'st6',0
.st7	db	'st7',0
.mm0	db	'mm0',0
.mm1	db	'mm1',0
.mm2	db	'mm2',0
.mm3	db	'mm3',0
.mm4	db	'mm4',0
.mm5	db	'mm5',0
.mm6	db	'mm6',0
.mm7	db	'mm7',0
.xmm0	db	'xmm0',0
.xmm1	db	'xmm1',0
.xmm2	db	'xmm2',0
.xmm3	db	'xmm3',0
.xmm4	db	'xmm4',0
.xmm5	db	'xmm5',0
.xmm6	db	'xmm6',0
.xmm7	db	'xmm7',0
