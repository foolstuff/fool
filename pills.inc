include 'sys/fool.inc'
pills:
.X=1000
.Y=20
.XL=500
.YL=900
        Gnode .X,.Y,.XL,.YL,pill,basket,floor,next
        Asm @f,0,0
@@:
        xor ebx,ebx
.loopy:
        xor eax,eax
.loopx:
        add ebx,[pills+gnode.y]
        add eax,[pills+gnode.x]
        mov ecx,eax
        mov edx,ebx
        sub ecx,[pills+gnode.x]
        sub edx,[pills+gnode.y]
        xor ecx,edx
        movzx ecx,cl
        or ecx,[.red]
        Pixel
        sub ebx,[pills+gnode.y]
        sub eax,[pills+gnode.x]
        inc eax
        cmp eax,[pills+gnode.xl]
        jne .loopx
        inc ebx
        cmp ebx,[pills+gnode.yl]
        jne .loopy
        ret
.red:   dd 0
.name: db 'pills',0
basket:
        Node .box,next
        Asm next
        mov eax,[pills+box.x]
        sub eax,[mouse.x]
        jg @f
        add eax,[pills+box.xl]
        jl @f
        mov eax,[pills+box.y]
        sub eax,[mouse.y]
        jg @f
        add eax,[pills+box.yl]
        jl @f
        jmp .move
@@:
        ret
.move:
        mov dword[mouse.ptr+cptr.ptr],0
        mov eax,[.box+box.xl]
        shr eax,1
        neg eax
        add eax,[mouse+gnode.x]
        sub eax,[pills+gnode.x]
        jge @f
        mov eax,0
@@:
        mov ebx,[pills+gnode.xl]
        sub ebx,[.box+box.xl]
        cmp eax,ebx
        jl @f
        mov eax,ebx
@@:
        mov [.box+box.x],eax
        ret

.box:   Box 0,pills.YL-floor.YL-30,100,50,Red

floor:
.YL=200
        Node pixel.notalpha,.box,pixel.setalpha
.box:   Box 0,pills.YL-.YL,pills.XL,.YL,Green

pill:
        Node .box,.fall
.fall:
        Asm next
        push eax ebx ecx edx
        mov eax,[.box+box.y]
        add eax,10
        mov ebx,[basket.box+box.y]
        add ebx,[basket.box+box.yl]
        cmp eax,ebx
        jl .no
.hit:
        mov eax,[basket.box+box.x]
        mov ebx,[basket.box+box.xl]
        mov ecx,[.box+box.x]
        mov edx,[.box+box.xl]
        cmp edx,0
        jge @f
        add ecx,edx
        neg edx
@@:
        sub eax,ecx
        jl @f
        sub eax,edx
        jge .not
        jmp .yep
@@:
        add eax,ebx
        jg .yep
.not:
        mov eax,[.box+box.c]
        mov [floor.box+box.c],eax
        mov eax,10
        sub [floor.box+box.y],eax
        jnl @f
        xor eax,eax
        mov [floor.box+box.y],eax
@@:
        add [floor.box+box.yl],eax
        sub [basket.box+box.y],eax
        mov eax,[pills.red]
        add eax,0010000h
        cmp eax,0ff0000h
        jl @f
        mov eax,0ff0000h
@@:
        mov [pills.red],eax
        push esi
        fcall .new
        pop esi
        pop edx ecx ebx eax
        ret

.yep:
        mov eax,[.box+box.c]
        mov [basket.box+box.c],eax
        mov eax,[basket.box+box.xl]
        add eax,5

        cmp eax,[pills+box.xl]
        jl @f
        mov eax,[pills+box.xl]
@@:
        mov [basket.box+box.xl],eax
        mov eax,[basket.box+box.x]
        mov ebx,[pills+gnode.xl]
        sub ebx,[basket.box+box.xl]
        cmp eax,ebx
        jl @f
        mov eax,ebx
@@:
        mov [basket.box+box.x],eax

        push esi
        fcall .new
        pop esi
        pop edx ecx ebx eax
        ret
.no:
        mov [.box+box.y],eax
        pop edx ecx ebx eax
        ret

.new:   Node @f,.rnd
@@:
        Asm next
        push eax ebx edx
        mov eax,[.rnd+rnd.rnd]
        mov [.box+box.c],eax
        xor edx,edx
        mov ebx,[pills+gnode.xl]
        sub ebx,[.box+box.xl]
        sub ebx,[basket.box+box.xl]
        jne @f
        add ebx,[basket.box+box.xl]
@@:
        idiv ebx
        cmp edx,[basket.box+box.x]
        jl @f
        add edx,[basket.box+box.xl]
@@:
        cmp edx,[.box+box.xl]
        jl @f
        sub edx,[.box+box.xl]
@@:
        mov [.box+box.x],edx
        mov dword[.box+box.y],0
        pop edx ebx eax
        ret

.box:   Box 0,10,20,20,Green

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "pills"
@@:
.s      dd @f-$-4
        rd 4
@@: