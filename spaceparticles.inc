BACKGROUND = Black
WXL=1600
WYL=900
include 'sys/foolw.inc'

Polydots:

macro RND n{
        movsx eax,n
        sar eax,5
        adc eax,0
}

macro RNDOT d{
        RND bl
        add [d+dot.x],eax
        RND bh
        add [d+dot.y],eax
}
;Node pixel.notalpha,poly.frame,poly,.dots,.rndots,.rnd,pixel.setalpha

;Grab next
Gnode 0,0,WXL,WYL,next   ;here it will be position node, with up, down, left, right, center and none options.
Node polydots,pixel.notalpha, poly.frame,poly,.dots,.rndots,.rnd,pixel.setalpha

.dots:  Asm @f
@@:
        mov ecx,[dots]
        shr ecx,3
;        dec ecx
        jl .nodots
@@:
        push ecx
        mov ebx,[.rnd+rnd.rnd]
        lea edx,[dots+4+ecx*8-8]
        ;RNDOT edx
        mov eax,[edx]
        mov [.dot+box.x],eax
        mov eax,[edx+4]
        mov [.dot+box.y],eax

        mov [.frame+box.c],ebx
        sar bl,7
        adc bl,0
        rol ebx,8
        sar bl,7
        adc bl,0
        rol ebx,8
        sar bl,7
        adc bl,0

       ; add dword[.box+box.c],ebx;0x0fff0f
;        sar bl,7
;        adc bl,0
;        add [.txt],bl
        fcall .dot
        fcall .rnd
        pop ecx
        loop @b

.nodots:
        ret




.dot:
        Gnode 0,0,0,0,.frame;,.border

;.border:Border 0,0,.XL,.YL,Black
.frame: Circle -5,-5,10,10,Green


.text:  TEXT 2,0,White,1
.txt:   db 0,0

.rndots:Asm @f
@@:
        mov ebx,[.rnd+rnd.rnd]
        RNDOT poly.p1
        ror ebx,4
        RNDOT poly.p2
        ror ebx,4
        RNDOT poly.p3
        ror ebx,4
        RNDOT poly.p4
        ret

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "polygons"
@@:
.s      dd @f-$-4
        rd 16
@@:

dot:
.x=0
.y=4

macro Dot x,y {
      dd x,y
}

polygon:
.call = 0
.x = 4
.y = 8
.xl = 12
.yl = 16
.c = 20
.p1 = 24
.p2 = 28
.p3 = 32
.p4 = 36
        ;ret
        push eax ebx ecx edx
;        jmp .cross
.quad:
.h:
.12:
        mov eax,[esi+.p1]
        mov ecx,[esi+.p2]
        call .line
.34:
        mov eax,[esi+.p3]
        mov ecx,[esi+.p4]
        call .line
;        jmp .end
.v:
.42:
        mov eax,[esi+.p4]
        mov ecx,[esi+.p2]
        call .line
.13:
        mov eax,[esi+.p1]
        mov ecx,[esi+.p3]
        call .line
;        jmp .end
.cross:
.23:
        mov eax,[esi+.p2]
        mov ecx,[esi+.p3]
        call .line
.41:
        mov eax,[esi+.p4]
        mov ecx,[esi+.p1]
        call .line
.end:
        pop edx ecx ebx eax
        ret
.line:
        push dword[esi+.x] dword[esi+.y]
        mov ebx,[eax+dot.y]
        mov eax,[eax+dot.x]
        add [esi+.x],eax
        add [esi+.y],ebx
        mov edx,[ecx+dot.y]
        mov ecx,[ecx+dot.x]
        sub ecx,eax
        sub edx,ebx
        mov [esi+.xl],ecx
        mov [esi+.yl],edx
        call line
        pop dword[esi+.y] dword[esi+.x]
        ret

macro Polygon x,y,p1,p2,p3,p4,c {
        dd polygon,x,y,?,?,c,p1,p2,p3,p4
}

poly:  Polygon 0,0,.p1,.p2,.p3,.p4,Black
.p1:   Dot 0,0
.p2:   Dot WXL,0
.p3:   Dot 0,WYL
.p4:   Dot WXL,WYL

.frame: Frame 0,0,WXL,WYL,Purple

;.p1:   Dot 0,0
;.p2:   Dot 1920*2,0
;.p3:   Dot 1920*2,1080
;.p4:   Dot 0,1080
;
;.frame: Frame 400,300,300,300,Black



polydots:
        Asm next
        mov eax,dots
        mov ecx,[eax]
        shr ecx,3
        sub ecx,dots.XL + dots.YL -2
        add eax,4
        mov ebx,1
@@:
        dec ecx
        jle @f
        push ecx
        push eax

        mov [.polygon+polygon.p1],eax
        add eax,8
        mov [.polygon+polygon.p2],eax
        add eax,(dots.XL-1) * 8
        mov [.polygon+polygon.p3],eax
        add eax,8
        mov [.polygon+polygon.p4],eax

        fcall .polygon

        pop eax
        inc ebx
        cmp ebx,dots.XL
        jl .ok
        mov ebx,1
        add eax,8
.ok:
        add eax,8
        pop ecx
        jmp @b
@@:
        ret



.polygon: Polygon 0,0,0,0,0,0,Green

dots:
.XL=1920/100+1
.YL=1080/100+1
.DX=WXL/(.XL-1)
.DY=WYL/(.YL-1)
       dd @f-$-4
repeat .XL*.YL
       Dot (((%-1) mod .XL)*.DX), (((%-1)/.XL)*.DY)
end repeat
@@:
