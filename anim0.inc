FOOLOS:
include 'sys/fool.inc'
anim0: Gnode 0,0,320,200,\
      .anims,\
      .anims,\
      .anims,\
      .anims,\
      .anims,\
      .anims,\
      .anims2,\
      .anim0,\
      .i2


.anim0: Anim 200,50,320,200,1000/24,@f
@@:
       Node \
       .b01,.b02,.b03,.b04,.b05,.b06,.b07,.b08,.b09,.b10,\
       .b11,.b12,.b13,.b14,.b15,.b16,.b17,.b18,.b30,.b30,\
       .b29,.b29,.b30,.b29,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b33,.b33,.b33,.b33,.b33,.b33,\
       .b33,.b33,.b33,.b33,.b33,.b33,\
       .b33,.b33,.b33,.b33,.b33,.b33,\
       .b33,.b33,.b33,.b33,.b33,.b33,\
       .b31,.b31,.b31,.b31,.b31,.b31,\
       .b31,.b31,.b31,.b31,.b31,.b31,\
       .b32,.b32,.b32,.b32,.b32,.b32,\
       .b32,.b32,.b32,.b32,.b32,.b32,\
       .b32,.b32,.b32,.b32,.b32,.b32,\
       .b32,.b32,.b32,.b32,.b32,.b32,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0

.anim1: dd anim,0,0,320,200,0,0,100/20,@f
.anim2: dd anim,50,0,320,200,10*4,0,1000/10,@f
.anim3: dd anim,100,0,320,200,80*4,0,1000/15,@f
.anim4: dd anim,0,30,320,200,0,0,1000/50,@f
.anim5: dd anim,50,30,320,200,20*4,0,1000/3,@f
.anim6: dd anim,100,30,320,200,0,0,1000/16,@f
.anim7: dd anim,0,60,320,200,15*4,0,1000/27,@f
.anim8: dd anim,100,60,320,200,50*4,0,1000/22,@f

@@:
       Node \
       .b01,.b02,.b03,.b04,.b05,.b06,.b07,.b08,.b09,.b10,\
       .b11,.b12,.b13,.b14,.b15,.b16,.b17,.b18,.b30,.b30,\
       .b29,.b29,.b30,.b29,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,0000,.b30,.b29,0000,0000,0000,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,0000,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,0000,0000,0000,0000,0000,0000,0000,.b30,\
       0000,.b30,0000,.b30,0000,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,0000,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       0000,.b30,.b30,.b30,0000,.b30,.b30,.b30,.b30,.b30,\
       .b30,0000,.b30,.b30,.b30,.b30,.b30,.b30,.b30,.b30,\
       .b30,.b30,0000,.b30,0000,.b30,.b30,.b30,.b30,.b30,\
       .b19,.b20,.b21,.b22,.b23,.b24,\
       .b33,.b33,.b33,.b31,.b31,.b33,\
       .b31,.b33,.b33,.b31,.b33,.b31,\
       .b31,.b31,.b31,.b33,.b31,.b31,\
       .b31,.b33,.b31,.b31,.b31,.b31,\
       .b31,.b31,.b31,.b31,.b31,.b31,\
       .b31,.b31,.b31,.b31,.b31,.b31,\
       .b31,.b31,.b31,.b31,.b31,.b31,\
       .b31,.b31,.b32,.b31,.b31,.b31,\
       .b31,.b32,.b31,.b32,.b31,.b31,\
       .b32,.b32,.b31,.b32,.b31,.b32,\
       .b32,.b32,.b32,.b32,.b32,.b32,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0,\
       0,0,0,0,0,0

.anim9: Anim 73,74,0,0,1000/1,@f
.anim10: Anim 0,0,0,0,1000/1000,@f
       @@:
       Node \
       .p00,.p01,.p02,.p03,.p04,.p05,.p06,.p07,.p08,.p09,.p10,\
       .p11,.p12,.p13,.p14,.p15,.p16,.p17

.p00 dd txt,3,0,0,0,03f00h,@f,0,font85
@@   db 'X',0
align 4
.p01 dd put,0,0,18,18,White-1,bille,3
.p02 dd put,0,0,16,16,White-2,bille,4
.p03 dd put,0,0,11,18,White-3,bille,5
.p04 dd put,0,0,10,10,White-2,bille,6
.p05 dd put,0,0,18,18,White-3,bille,1
.p06 dd put,0,0,20,20,White-6,bille,2
.p07 dd put,0,0,18,18,White-12,bille,3
.p08 dd put,0,0,18,18,White-17,bille,3
.p09 dd put,0,0,16,16,White-20,bille,4
.p10 dd put,0,0,18,18,White-20,bille,5
.p11 dd frame,-1,-1,12,12,0ffffffffh,bille,2
.p12 dd put,0,0,10,10,White-10,bille,5
.p13 dd frame,0,0,10,10,0ffffffffh,bille,2
.p14 dd box,0,0,10,10,White-3,bille,5
.p15 dd frame,0,0,11,11,0ffffffffh,bille,2
.p16 dd put,0,0,20,20,White-3,bille,5
.p17 dd frame,0,0,10,10,0ffffffffh,bille,2

.b01 dd box,10,10,11,18,Lime
.b02 dd box,10,10,12,18,Lime
.b03 dd box,10,10,13,18,Lime
.b04 dd box,10,10,14,18,Lime
.b05 dd box,10,10,15,18,Lime
.b06 dd box,10,10,16,18,Lime
.b07 dd box,10,10,17,18,Lime
.b08 dd box,10,10,18,18,Lime
.b09 dd box,10,10,19,18,Lime
.b10 dd box,10,10,20,18,Lime
.b11 dd box,10,10,21,18,Lime
.b12 dd box,10,10,22,18,Lime
.b13 dd box,10,10,23,18,Lime
.b14 dd box,10,10,25,18,Lime
.b15 dd box,10,10,27,18,Lime
.b16 dd box,10,10,29,18,Lime
.b17 dd box,10,10,31,18,Lime
.b18 dd box,10,10,34,18,Lime
.b19 dd box,13,10,31,18,Lime
.b20 dd box,15,10,29,18,Lime
.b21 dd box,17,10,27,18,Lime
.b22 dd box,19,10,25,18,Lime
.b23 dd box,20,10,24,18,Lime-10
.b24 dd box,20,10,24,18,Lime-20
.b29 dd txt,10,10,0,0,0100ff00h,@f,0,font85
.b30 dd txt,10,10,0,0,Lime,@f,0,font85
@@   db 'hello',2,10,13,'wolrd',1,0
.b31: Txt 20,10,0,0,000ff00h,@f,font85
.b33: Txt 20,10,0,0,Silver,@f,font85
@@   db 'good',10,13,'bye!',0
.b32: Txt 20,10,0,0,100f800h,@f,font85
@@   db 'FOOL',10,13,'DEMO',0
.i2:  Gnode 0,0,60,20,\
     .b50,\
     .anim10,\
     @f
@@   dd pong,1,1

.b50: Txt 11,0,0,0,Red*256,@f,font85
@@   db 'this is a demo of',10,13,'thE ANIM function',0

.anims2: Anim 50,50,200,90,1000*4,.anims+item.size-4
.anims: Gnode 50,50,200,90,\
       .anim1,\
       .anim2,\
       .anim3,\
       .anim4,\
       .anim5,\
       .anim6,\
       .anim7,\
       .anim8,\
       .anim9


bille:
include "bmp/fool_logo.inc"
