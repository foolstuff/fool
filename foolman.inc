;FOOLOS:
include 'sys/fool.inc'
foolman:
        Gnode 0,0,80,80,.menu,next
        But 0,0,80,80,0,fool_logo_80,.domenu
.domenu:
        Asm next
        mov dword[.menu],gnode
        ret
.undomenu:
        Asm next
        mov dword[.menu],0
        ret

.name:  db 'foolman',0

.menu:  Gnone 0,0,80,80,.exit,.hide,.box
.box:   Box 0,0,80,80,Gray

.hide:  But 10,5,60,30,0,@f,.undomenu
@@:     Node @f,next
        Box 0,0,60,30,Black
@@:     Vtxt 10,0,0,0,Blue,@f,2
@@:     db "hide",0

.exit:  But 10,40,60,30,0,@f,system.exit!
@@:     Node @f,next
        Box 0,0,60,30,Black
@@:     Vtxt 10,0,0,0,Red,@f,2
@@:     db "exit",0
