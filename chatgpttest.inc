include 'sys/fool.inc'

Node .box, next
Asm next

mov eax, [mouse.dx]
mov ebx, [mouse.dy]
add eax, ebx
and eax,0FFFFh
or eax,1
shl eax, 3          ; Taille multipli�e par 8

; Ajuster la taille du rectangle
mov [.box + box.xl], eax  ; Mettre � jour la largeur
mov [.box + box.yl], eax  ; Mettre � jour la hauteur

; Centrer le rectangle sur la position de la souris
mov ecx, [mouse.x]  
shr eax, 1            ; Diviser la taille par 2
sub ecx, eax           ; Soustraire la moiti� de la taille pour centrer
mov [.box + box.x], ecx  ; Mettre � jour la position x

mov ecx, [mouse.y]      ; R�utiliser ecx pour la coordonn�e y
sub ecx, eax            ; Soustraire la moiti� de la taille pour centrer
mov [.box + box.y], ecx  ; Mettre � jour la position y

ret

.box: Box 0, 0, ?, ?, Red
