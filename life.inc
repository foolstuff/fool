;SHOWFPS:
;FOOLOS:
NOMOUSE:
;ONTOP:
;HIDDEN:
NOCLS:
WINDOW equ 0,0,1600,900
include 'sys/fool.inc'

life_effect:
.xl=WXL
.yl=WYL
        Node .mouse,.effect,.keyboard,.init
.init:
        Init next
        Node .offset,.setbuffer,.setframe
.mbuffer equ .bmp+bmptr32.bmp
.mframe dd ?

.buffer equ [.mbuffer]
.frame  equ [.mframe]

db "malloc them"
align 16
;adjustment needed for top and bottom of frame buffer
;due to access outside the screen during effect
.setbuffer:
        Malloc (WXL)*(WYL+2)*4+16,.mbuffer
.setframe:
        Malloc (WXL)*(WYL+2)*4+16,.mframe
.offset:
        Asm next
        add dword [.mbuffer],WXL*4+4
        add dword [.mframe],WXL*4+4
        ret



.effect:
        Node .monitor,.bmp,next
        Asm .life

.monitor:
        Gnode 0,0,.xl,.yl,\
        .txt,.hexand,.hexadd,.hexinit,.hexskip,pixel.notalpha,.box,pixel.setalpha
.box:   Box 0,0,300,70,Black
.txt:   Txt 10,10,.xl-20,.yl-20,White,.str
.str:   db "Life effect",crlf
        db "and var = "
.andvarstr:
        db "00000000  (right/left)",crlf
        db "add var = "
.addvarstr:
        db "00000000  (up/down)",crlf
        db "init value = "
.initvarstr:
        db "00000000  (pgup/pgdown)",crlf
        db "frameskip = "
.skipvarstr:
        db "00000000  (begin/end)",0

.hexand:
        dd num.hex,.andvar,.andvarstr,8
.hexadd:
        dd num.hex,.addvar,.addvarstr,8
.hexinit:
        dd num.hex,.initvar,.initvarstr,8
.hexskip:
        dd num.hex,.skipvar,.skipvarstr,8

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db "life is life"
align 16
.life:
        push eax ebx ecx edx edi
        push esi edi
        mov esi,[desk+screen.buffer]
        mov edi,.buffer
        call .copy
        pop edi esi
        movzx ecx,byte[.skipvar]
        or ecx,ecx
        je .end
@@:
        push ecx
        call .iteration
        pop ecx
        loop @b
.end:
        pop edi edx ecx ebx eax
        ret


macro pac n {
        mov eax,[ebx+edi*4+n+.xl]
        add ecx,eax
}

.iteration:
        mov ebx,.buffer
        mov edx,.frame
        mov edi,.xl*(.yl-2)
        dec edi
@@:
;        mov edx,[.initvar]

        pac (-(.xl*4+4))
        pac (-(.xl*4))
        pac (-(.xl*4-4))

        pac (+4)
        pac (-4)

        pac (+(.xl*4+4))
        pac (+(.xl*4))
        pac (+(.xl*4-4))

        shr ecx,3
        and ecx,[.andvar]
        add ecx,[.addvar]

        mov [edx+edi*4+.xl],ecx

        dec edi
        jnl @b

        push esi edi
        mov esi,.frame
        mov edi,.buffer
        call .copy
        pop edi esi

        ret

.copy:
        mov ecx,.xl*.yl-1
@@:
        mov eax,[esi+ecx*4]
        mov [edi+ecx*4],eax
        dec ecx
        jnl @b
        ret

.clearall:
        Asm next
        pushad
        mov edi,.xl*.yl-1
@@:
        mov eax,[.initvar]
        mov ebx,[desk+screen.frame]
        mov [ebx+edi*4],eax

        dec edi
        jnl @b
        popad
        ret



.skipvar: dd 1
.initvar: dd 93F1Bh
.andvar:  dd 0FFc0EAFAh
.addvar:  dd 001F5E7Ah

.keyboard:
        Node \
        .clear,\
        .addup,\
        .adddown,\
        .andup,\
        .anddown,\
        .initup,\
        .initdown,\
        .frameskip,\
        .skipup,\
        .skipdown

.frameskip:
        dd limd,.skipvar,0,9
.clear:
        Otk key.del,.clearall
.skipup:
        Otk key.home,next
        Asm incdw,.skipvar
.skipdown:
        Otk key.end,next
        Asm decdw,.skipvar
.andup:
        Tmk key.right,next
        Asm adddw,.andvar,090301h
.anddown:
        Tmk key.left,next
        Asm subdw,.andvar,090301h
.addup:
        Tmk key.up,next
        Asm adddw,.addvar,010309h
.adddown:
        Tmk key.down,next
        Asm subdw,.addvar,010309h
.initup:
        Tmk key.pageup,next
        Asm adddw,.initvar,010703h
.initdown:
        Tmk key.pagedown,next
        Asm subdw,.initvar,010703h
.mouse:
        Node .cursor,.asm
.asm:   Asm next

        mov eax,[mouse.y]
        mov [.cursor+item.y],eax
        mov ebx,[mouse.x]
        mov [.cursor+item.x],ebx
        mov ecx,.xl
        cdq
        imul ecx
        add eax,ebx
        shl eax,2
        mov ebx,[.initvar]
        mov [.mptr+box.c],ebx
        ret

.cursor:Gptr 0,0,next
.mptr:  Put -40,-40,80,80,0,fool_logo_80

.bmp:   Bmptr32 0,0,.xl,.yl,0,?;.buffer
;.buffer rd .xl*.yl; dup 0;equ .bmp+bmptr32.bmp
;.frame  rd .xl*.yl; dup 0;dd ?
;        dd 0