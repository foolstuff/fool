;FOOLOS:
BACKGROUND = OliveDrab
EXITTITLE equ "FOOL OS"
EXITMESSAGE equ "will exit soon"
WINDOW equ 100,100,640,480
VESAMODE=SVGA
;VESA32:
NOCLS:

include "sys/fool.inc"

viewram:
.view:  Gnode 50,100,530,290,.border,.bmp,.frame,.txt,.box,.pong
.pong:  Pong 1,1
.txt:   Txt 50,5,0,0,Pink,.str
.box:   Box 0,0,530,290,CosmicLatte
.border:Frame 0,0,530,290,Black
.str:   db "Finally, we've got something from the ruins",CRLF
        db "The entire low mem is ours now!",0
.frame: Frame 8,29,514,258,Black
.bmp:   Bmptr32 9,30,512,256,NULL,next

file 'bmp/ruines.bmp32',10000h*4
file 'bmp/saturne.bmp32',10000h*4
