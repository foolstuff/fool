include 'sys/fool.inc'
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
pitmap:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.xl=220
.yl=70
.tyl=20
.c1=Silver
.c2=Maroon
.c3=Green
.c4=Teal
        Panel 350,100,.xl,.yl,.c1,.c2,.name,.view
.view:  Gnode 1,.tyl+1,.xl-2,.yl-.tyl-2,\
        .app,@f
@@:     Box 0,0,.xl-2,.yl-.tyl-2,.c1+2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
.app:   Gnode 0,0,.xl-2,.yl-.tyl-2,\
        cptime,\
        tscnt,\
        secs,\
        msecs,\
        .asm

.scnt   dd 0
.mscnt  dd 0
.ctime  dq %t

.asm:   Asm next
        push eax ebx edx
        mov eax,[irq0.inc]
        add [.mscnt],eax
        mov eax,[.mscnt]
        xor edx,edx
        mov ebx,1000
        div ebx
        mov [.scnt],eax
        pop edx ebx eax
        ret
.name:  db 'PITmap',0
rscs:
.str9:  db 'Compile time = '
.str90: db '0000000000000000',0
.str1:  db 'rdtsc        = '
.str10: db '0000000000000000',0
.str2:  db 'seconds      = '
.str20: db '00000000',0
.str3:  db 'milliseconds = '
.str30: db '00000000',0
align 4
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
cptime: Htxt 6,5,0,0,pitmap.c2,rscs.str9,pitmap.ctime,rscs.str90,16
tscnt:  Htxt 6,15,0,0,pitmap.c2,rscs.str1,irq0.tsc,rscs.str10,16
secs:   Htxt 6,25,0,0,pitmap.c2,rscs.str2,pitmap.scnt,rscs.str20,8
msecs:  Htxt 6,35,0,0,pitmap.c2,rscs.str3,pitmap.mscnt,rscs.str30,8
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


