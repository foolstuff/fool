;FOOLOS:
WINDOWED=1
WX=100
WY=100
WXL=850
WYL=840


include "sys/fool.inc"

macro TITLE c,s,z {
       Vtxt 15,0,0,0,c,s,z,ugly
}

macro TXT x,y,c,z {
local .s
        Txt x,y,0,0,c,.s,font85
.s:
}

macro TEXT x,y,c,z {
local .s
        Vtxt x,y,0,0,c,.s,z,ugly
.s:
}

macro TITRE titre {
local .t
        TEXT 15,0,Yellow,1
.t:     db titre,0
        dd .t
}

macro RUBRIQUE x,y,xl,yl,titre,contenu {
local .g,.b
.win:
.g:     Gnode x,y,xl,yl+15,titre,.b,next
        Gnode 0,15,xl,yl,contenu
.b:     Box 0,0,xl,15,Green
;.win:   Win .g,win.SHOW
}

macro FORMATION x,y,ann�e,titre,description {
local .a,.t,.d,.f
        Gnode x,y,500,60,.f,.a,.t,.d
.a:
        TEXT 10,0,Blue,2
        db ann�e,0
.t:
        TEXT 80,0,Black,2
        db titre,0
.d:
        TEXT 30,30,Gray/2,1
        db description,0
.f:     Line 10,50,480,0,Blue
}

macro EXPERIENCE x,y,ann�e,entreprise,metier,description {
local .a,.e,.m,.d,.f
        Gnode x,y,500,120,.f,.a,.e,.m,.d
.a:
        TEXT 10,0,Green,2
        db ann�e,0
.e:
        TEXT 130,0,Black,2
        db entreprise,0
.m:
        TEXT 140,30,Blue,2
        db metier,0
.d:
        TEXT 10,60,Gray/2,1
        db description,0
.f:     Line 10,110,480,0,Green
}

macro PASSION x,y,nom,description {
local .n,.d
        Gnode x,y,320,70,.n,.d
.n:     TEXT 10,0,Maroon,2
        db nom,0
.d:     TEXT 5,30,Red,1
        db description,0
}

Curiculum_vitae:
        Node @f,Source.win
@@:     Win MainWindow,win.SHOW

@@:     TITRE "Curiculum vitae"

XL = 850
YL = 840

MainWindow:
;        Node @f,Source.win
        RUBRIQUE 0,0,850,840,@b,.content
.content:
        Node CV,.box
.box:   Box 0,0,850,840,White

FILENAME equ "cv.inc"
@@:     TITRE <"CODE SOURCE ",FILENAME>
Source:
        RUBRIQUE 0,0,500,YL,@b,.content
.content:
        Node .text,.box
.box:   Box 0,0,500,YL,Gray/2
.text:  TXT 10,10,Lime,1
        file FILENAME
        db 0

CV:     Node Coordonn�es.win,Formations.win,Experiences.win,Passions.win,Nuage.win,About.win

X = 10
XL = 320

@@:     TITRE "Coordonn�es"
Coordonn�es:
        RUBRIQUE X,10,XL,200,@b,.content
.content:
        Node .civilit�,.profession,.adresse_postale,.adresse_mail,.telephone,.age_permis,.box
.box:   Box 0,0,320,200,Gray

.civilit�:
        TEXT 60,5,White,3
        db "Olivier Cauet",0
.profession:
        TEXT 10,50,White,2
        db "Syst�mes embarqu�s",0
.adresse_postale:
        TEXT 40,90,White,1
        db "1 traverse Saint Bazile",crlf,"13001 Marseille",0
.adresse_mail:
        TEXT 30,120,White,2
        db "oliviercauet@gmail.com",0
.telephone:
        TEXT 40,150,White,1
        db "06 69 74 60 53",0
.age_permis:
        TEXT 40,170,White,1
        db "33 ans, Permis A et B",0

@@:     TITRE "Passions"
Passions:
        RUBRIQUE X,230,XL,490,@b,.content
.content:
        Node .sciences,.techniques,.arts,.bd,.litterature,.paysagisme,.nature

.sciences:
        PASSION 0,0,"Sciences",<"Astronomie, physique, chimie, �lectronique",crlf,"intelligence artificielle.">
.techniques:
        PASSION 0,70,"Techniques",<"Du moment qu'on l'imagine, il faut l'essayer.">
.arts:
        PASSION 0,140,"Arts",<"Dessin noir et blanc, couleur, peintures techniques diverses",crlf,"modelage, sculpture, dance, musique.">
.bd:
        PASSION 0,210,"Bande dessin�e",<"Lecture, �criture, narration, dessin.">
.litterature:
        PASSION 0,280,"Litterature",<"Astronomie, physique, chimie, �lectronique.">
.paysagisme:
        PASSION 0,350,"Paysagisme",<"Jardinage, botanique, agencement, amenagement.">
.nature:
        PASSION 0,420,"Nature",<"L'air, l'eau, la terre et la vie.">


@@:     TITRE "A propos"
About:
        RUBRIQUE X,740,830,70,@b,.content
.content:
        Node .about,.copy,.print
.about: TEXT 10,10,Purple,1
        db "Ce cv est ecrit en assembleur et compil� avec Fasm",crlf,"Il fonctionne sous windows et utilise le langage Fool pour toute la partie fonctionnelle",crlf,"Pour plus de renseignements, n'hesitez pas � me contacter",0
.copy:  TXT 600,60,Green,1
        db "copyright � 2016, Olivier Cauet",0
.print: But 700,15,70,15,0,Printit.but,Printit


Printit:  Asm next
        ret
.but:   Node .text,.border,.box
.border:Border 0,0,70,15,White
.box:   Box 0,0,70,15,Purple/2
.text:  TEXT 10,0,Yellow,1
        db "Imprimer",0



X = 340
XL = 500

@@:     TITRE "Formations"
Formations:
        RUBRIQUE X,10,XL,180,@b,.content
.content:
        Node .mecanique,.electronique,.informatique
.informatique:
        FORMATION 0,0,"2012","BTS D�veloppeur logiciel","AFPA Marseille Saint J�rome(13)"
.electronique:
        FORMATION 0,60,"2005","BTS Applications electroniques","AFPA Istres(13)"
.mecanique:
        FORMATION 0,120,"2002","BAC g�nie m�canique","lyc�e technique des Catalins, Montelimar(26)"

@@:     TITRE "Experiences"
Experiences:
        RUBRIQUE X,210,XL,360,@b,.content
.content:
        Node .litterature,.electronique,.informatique

.informatique:
        EXPERIENCE 0,0,<"2012 -",crlf,"- Present">,"Progexia Marseille","D�veloppeur logiciel",<"Maintenance, �volution et d�veloppement de la solution de carte de tiers payant",crlf,"� destination des professionnels de sant� bas�s dans les DOM.">
.electronique:
        EXPERIENCE 0,120,<"2005 -",crlf,"- 2009">,"SCLE-ERJI Marseille","Technicien en �lectronique",<"Maintenance, �talonnage, mise au point et conception d'equipements electroniques de securit�",crlf,"� destination du ferroviaire.",crlf,"">
.litterature:
        EXPERIENCE 0,240,<"2010 -",crlf,"- 2011">,"ENI �ditions","Auteur",<"R�daction du livre 'Le langage assembleur'.",crlf,"Ouvrage de la collection Epsilon couvrant le langage des micropossesseurs x86 (intel, AMD,...),",crlf,"ainsi que l'architecture des PC et leur programmation bas niveau.">

@@:     TITRE "Nuage de mots"
Nuage:
        RUBRIQUE X,590,XL,130,@b,.content
.content:
        TEXT 10,0,Black,10
        db "VOILA",0
