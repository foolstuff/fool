;WXL, WYL, WX, WY are constants created at runtime.
;they are the size and position of the screen.
FOOLOS:
EXITTITLE equ 'RandomMap'
TOPMOST equ 1
SHOWFPS equ 1

include "sys/fool.inc"


randomMap:
.PXL=10
.WXL=160
.WYL=90
.GXL=.WXL/.PXL
.GYL=.WYL/.PXL
.CNT=.GXL*.GYL

        Asm .grid
.cell:  Gnode 0,0,.PXL,.PXL,.border,.box
.border:Border 0,0,.PXL,.PXL,0
.box:   Box 0,0,.PXL,.PXL,0
.grid:
        mov eax,.WXL-.PXL
        mov ebx,.WYL-.PXL
        mov edx,.CNT
.loop:
        mov [.cell+box.y],ebx
@@:
        mov [.cell+box.x],eax
        fcall .rnd
        mov ecx,[.rnd+rnd.rnd]
        and ecx,[.init]
        add ecx,[.buffer+edx*4]
        mov [.buffer+edx*4],ecx
        and ecx,0ffffh
        mov [.box+box.c],ecx
        dec edx
        jl .end
        push eax ebx ecx edx
        fcall .cell
        pop edx ecx ebx eax
        sub eax,.PXL
        jnl @b
        mov eax,.WXL-.PXL
        sub ebx,.PXL
        jnl .loop
.end:
        mov [.init],070707h
        ret
.init   dd -1

.rnd:   Rnd .k,.s
.k:     dd @f-$-4
        db "randomMap"
@@:
.s      dd @f-$-4
        rd 16
@@:

.buffer rd .CNT
        dd 0
