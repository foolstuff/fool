return equ

CRLF equ 10,13


macro debugword w* {
;display the value of the dword "s"
s = w

d0='0'+s shr 28 and 0fh
d1='0'+s shr 24 and 0fh
d2='0'+s shr 20 and 0fh
d3='0'+s shr 16 and 0fh
d4='0'+s shr 12 and 0fh
d5='0'+s shr 8 and 0fh
d6='0'+s shr 4 and 0fh
d7='0'+s and 0fh
if d0>'9'
        d0=d0+7
end if

if d1>'9'
        d1=d1+7
end if

if d2>'9'
        d2=d2+7
end if

if d3>'9'
        d3=d3+7
end if

if d4>'9'
        d4=d4+7
end if

if d5>'9'
        d5=d5+7
end if

if d6>'9'
        d6=d6+7
end if

if d7>'9'
        d7=d7+7
end if

return equ d0,d1,d2,d3,d4,d5,d6,d7,'h '
}

virtual as 'render'
_RENDER::
end virtual

macro _render str& {
virtual _RENDER
 db str
end virtual
}

virtual as 'log'
  LOG::
end virtual

macro log str& {
  virtual LOG
    db str
  end virtual
}

log 'hello'
log 'hi'


macro _type length,name {
        dd length
        db name,0
;        _render name
;        _render CRLF
}

macro _field type,name {
        dd type
        db name,0
        _render name
        _render [type]
        _render CRLF
}

macro _model name*,[field] {
common
 local .a,.n;,.f
.n:
       db name,0
.a:
      ; debugword .f?
forward
;.f? = field
       ;debugword .f?
       _render name
       _render CRLF
       ;_render return
       dd field
       dd $-.a
       dd .n
common
}



_int equ 0x0
_float equ 0x1000
_strz equ 0x2000        ;null ending string
_strs equ 0x4000        ;sized string
_ptr equ 0x8000         ;pointers
_fooltype equ 0xF0000000;fool model type


_null:  _type 0,'null'
_bit:   _type 1,'bit'
_nibble:_type 4,'nibble'
_byte:  _type 8,'byte'
_word:  _type 16,'word'
_dword: _type 32,'dword'
_qword: _type 64,'qword'
_tword: _type 80,'tword'
_dqword:_type 128,'dqword'
_f16:   _type _float+16,'f16'
_f32:   _type _float+32,'f32'
_f64:   _type _float+64,'f64'
_f80:   _type _float+80,'f80'

_ptr1:    _type _ptr+1,'ptr1'
_ptr4:    _type _ptr+4,'ptr4'
_ptr8:    _type _ptr+8,'ptr8'
_ptr16:   _type _ptr+16,'ptr16'
_ptr32:   _type _ptr+32,'ptr32'
_ptr64:   _type _ptr+64,'ptr64'
_ptr128:  _type _ptr+128,'ptr128'
_ptrf16:  _type _ptr+_float+16,'ptrf16'
_ptrf32:  _type _ptr+_float+32,'ptrf32'
_ptrf64:  _type _ptr+_float+64,'ptrf64'
_ptrf80:  _type _ptr+_float+80,'ptrf80'


_string:_type _strz+8,'string'
_bits:  _type _strs+1,'bitfield'
_ustr:  _type _strz+16,'ustring'
_list:  _type _strs+32,'list'

_0 equ _null
_1 equ _bit
_4 equ _nibble
_8 equ _byte
_16 equ _word
_32 equ _dword
_64 equ _qword
_128 equ _dqword

_f:     _field _ptr32,'func'
_x:     _field _dword,'x'
_y:     _field _dword,'y'
_xl:    _field _dword,'xl'
_yl:    _field _dword,'yl'
_color8:_field _byte,'color8'
_color16:_field _word,'color16'
_color32:_field _dword,'color32'
_size:  _field _dword,'size'
_data:  _field _dword,'data'
_txtptr: _field _ptr8,'txtptr'
_bmptr: _field _ptr8,'bmptr'
_bmptr32: _field _ptr32,'bmptr32'

_2d equ _x,_y,_xl,_yl
_graphix equ _2d,_color32

_model 'node',_f,_list
_model 'gnode',_f,_2d,_list
_model 'box',_f,_graphix
_model 'frame',_f,_graphix
_model 'circle',_f,_graphix
_model 'border',_f,_graphix
_model 'folder',_f,_txtptr,_list
_model 'bmptr32',_f,_graphix,_ptr32


render:
.model:

;

.field:
;

.type:
.dword:
;debugword 432345h
;_render return

;
