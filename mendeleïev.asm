;FOOLOS:
WINDOWED equ 1
ONTOP equ 1
SHOWFPS equ 1
WX=100
WY=100
WXL=MainWindow.xl
WYL=MainWindow.yl+15

NO3DBORDER=1

include "sys/fool.inc"
include "sys/macros.inc"

Mendele�ev:
        Ptr MainWindow.win

@@:     TITRE "Periodic table of elements"
MainWindow:
.xl=32*XL+4+50
.yl=9*YL+4+50
        RUBRIQUE 0,0,.xl,.yl,@b,.c
.c:     Gnode 0,0,.xl,.yl,Legend,.t,L1,L0,L2,L3,L4,L5,L6,L7,L8,L9,.b
.b:     Box 0,0,.xl,.yl,White
.t:     TEXT 200,30,Black,1
@@:     db "On this periodic table, elements are ordered by number of electrons and number of layers",crlf,\
           "It makes appear a logic in the nature of elements of each lines,",crlf,\
           "alcalis are always the first elements,",crlf,\
           "halogens are always the last elements.",crlf,crlf,\
           "It tries to introduce the elements 0 and below as constants of the universe.",crlf,\
           "It also tries to predict the next lines of the table.",crlf,\
           "Here, it have the same number of electrons on the top layer, as the previous one",crlf,\
           "and there are 4 lines in total like them",crlf,\
           "in theory, it can lead to have elements",crlf,\
           "the size of black holes",0


L1:     Node L1_1,L1_2,L1_3,L1_4,L1_5,L1_6,L1_7,L1_8,L1_9,L1_10,\
             L1_11,L1_12,L1_13,L1_14,L1_15,L1_16,L1_17,L1_18,L1_19,L1_20,\
             L1_21,L1_22,L1_23,L1_24,L1_25,L1_26,L1_27,L1_28,L1_29,L1_30,\
             L1_31,L1_32
L1_1:   te "f1"
L1_2:   te "f2"
L1_3:   te "f3"
L1_4:   te "f4"
L1_5:   te "f5"
L1_6:   te "f6"
L1_7:   te "f7"
L1_8:   te "f8"
L1_9:   te "f9"
L1_10:  te "f10"
L1_11:  te "f11"
L1_12:  te "f12"
L1_13:  te "f13"
L1_14:  te "f14"
L1_15:  te "d1"
L1_16:  te "d2"
L1_17:  te "d3"
L1_18:  te "d4"
L1_19:  te "d5"
L1_20:  te "d6"
L1_21:  te "d7"
L1_22:  te "d8"
L1_23:  te "d9"
L1_24:  te "d10"
L1_25:  te "p1"
L1_26:  te "p2"
L1_27:  te "p3"
L1_28:  te "p4"
L1_29:  te "p5"

L1_30:  te "p6"
L1_31:  te "s1"
L1_32:  te "s2"
        tr

METALLOIDE=0cccc99h
NON_METAL=0a0ffa0h
HALOGENE=0ffff99h
GAZ_RARE=0c0e8ffh
METAL_ALCALIN=0ff6666h
METAL_ALCALINO_TERREUX=0f6cfa1h
METAL_DE_TRANSITION=0ffa0a0h
METAL_PAUVRE=0cccccch
LANTHANIDE=0ffbfffh
ACTINIDE=0ff99cch
SUPERACTINIDE=0ffddeeh
ELEMENT_NON_CLASSE=0eeeeeeh

L0:
        Node L0_1,L0_2,L0_3
        colspan 29
N=-2
L0_1:   td "X",White,"Black matter"
L0_2:   td "Z",Yellow,"Energy"
L0_3:   td "V",Silver,"Void"
        tr

L2:     Node L2_1,L2_2
        colspan 30
L2_1:   td "H",Aqua,"Hydrogen"
L2_2:   td "He",GAZ_RARE,"Helium"
        tr

L3:     Node L3_1,L3_2,L4_1,L4_2,L4_3,L4_4,L4_5,L4_6
        colspan 24
L3_1:   td "Li",METAL_ALCALIN,"Lithium"
L3_2:   td "Be",METAL_ALCALINO_TERREUX,"Beryllium"
L4_1:   td "B",METALLOIDE,"Boron"
L4_2:   td "C",NON_METAL,"Carbon"
L4_3:   td "N",NON_METAL,"Nitrogen"
L4_4:   td "O",NON_METAL,"Oxygen"
L4_5:   td "F",HALOGENE,"Fluorine"
L4_6:   td "Ne",GAZ_RARE,"Neon"
        tr

L4:     Node L4_7,L4_8,L5_1,L5_2,L5_3,L5_4,L5_5,L5_6
        colspan 24
L4_7:   td "Na",METAL_ALCALIN,"Sodium"
L4_8:   td "Mg",METAL_ALCALINO_TERREUX,"Magnesium"
L5_1:   td "Al",METAL_PAUVRE,"Aluminium"
L5_2:   td "Si",METALLOIDE,"Silicon"
L5_3:   td "P",NON_METAL,"Phosphorus"
L5_4:   td "S",NON_METAL,"Sulfur"
L5_5:   td "Cl",HALOGENE,"Chlorine"
L5_6:   td "Ar",GAZ_RARE,"Argon"
        tr

L5:     Node L5_7,L5_8,L6_1,L6_2,L6_3,L6_4,L6_5,L6_6,L6_7,L6_8,L6_9,L6_10,\
             L6_11,L6_12,L6_13,L6_14,L6_15,L6_16
        colspan 14
L5_7:   td "K",METAL_ALCALIN,"Potassium"
L5_8:   td "Ca",METAL_ALCALINO_TERREUX,"Calcium"
L6_1:   td "Sc",METAL_DE_TRANSITION,"Scandium"
L6_2:   td "Ti",METAL_DE_TRANSITION,"Titanium"
L6_3:   td "V",METAL_DE_TRANSITION,"Vanadium"
L6_4:   td "Cr",METAL_DE_TRANSITION,"Chromium"
L6_5:   td "Mn",METAL_DE_TRANSITION,"Manganese"
L6_6:   td "Fe",METAL_DE_TRANSITION,"Iron"
L6_7:   td "Co",METAL_DE_TRANSITION,"Cobalt"
L6_8:   td "Ni",METAL_DE_TRANSITION,"Nickel"
L6_9:   td "Cu",METAL_DE_TRANSITION,"Copper"
L6_10:  td "Zn",METAL_DE_TRANSITION,"Zinc"
L6_11:  td "Ga",METAL_PAUVRE,"Gallium"
L6_12:  td "Ge",METALLOIDE,"Germanium"
L6_13:  td "As",METALLOIDE,"Arsenic"
L6_14:  td "Se",NON_METAL,"Selenium"
L6_15:  td "Br",HALOGENE,"Bromine"
L6_16:  td "Kr",GAZ_RARE,"Krypton"
        tr

L6:     Node L6_17,L6_18,L7_1,L7_2,L7_3,L7_4,L7_5,L7_6,L7_7,L7_8,L7_9,L7_10,\
             L7_11,L7_12,L7_13,L7_14,L7_15,L7_16
        colspan 14
L6_17:  td "Rb",METAL_ALCALIN,"Rubidium"
L6_18:  td "Sr",METAL_ALCALINO_TERREUX,"Strontium"
L7_1:   td "Y",METAL_DE_TRANSITION,"Yttrium"
L7_2:   td "Zr",METAL_DE_TRANSITION,"Zirconium"
L7_3:   td "Nb",METAL_DE_TRANSITION,"Niobium"
L7_4:   td "Mo",METAL_DE_TRANSITION,"Molybdenum"
L7_5:   td "Tc",METAL_DE_TRANSITION,"Technetium"
L7_6:   td "Ru",METAL_DE_TRANSITION,"Ruthenium"
L7_7:   td "Rh",METAL_DE_TRANSITION,"Rhodium"
L7_8:   td "Pd",METAL_DE_TRANSITION,"Palladium"
L7_9:   td "Ag",METAL_DE_TRANSITION,"Silver"
L7_10:  td "Cd",METAL_DE_TRANSITION,"Cadmium"
L7_11:  td "In",METAL_PAUVRE,"Indium"
L7_12:  td "Sn",METAL_PAUVRE,"Tin"
L7_13:  td "Sb",METALLOIDE,"Antimony"
L7_14:  td "Te",METALLOIDE,"Tellurium"
L7_15:  td "I",HALOGENE,"Iodine"
L7_16:  td "Xe",GAZ_RARE,"Xenon"
        tr

L7:     Node L7_17,L7_18,L8_1,L8_2,L8_3,L8_4,L8_5,L8_6,L8_7,L8_8,L8_9,L8_10,\
             L8_11,L8_12,L8_13,L8_14,L8_15,L8_16,L8_17,L8_18,L8_19,L8_20,\
             L8_21,L8_22,L8_23,L8_24,L8_25,L8_26,L8_27,L8_28,L8_29,L8_30
L7_17:  td "Cs",METAL_ALCALIN,"Caesium"
L7_18:  td "Ba",METAL_ALCALINO_TERREUX,"Barium"
L8_1:   td "La",LANTHANIDE,"Lanthanum"
L8_2:   td "Ce",LANTHANIDE,"Cerium"
L8_3:   td "Pr",LANTHANIDE,"Praseodymium"
L8_4:   td "Nd",LANTHANIDE,"Neodymium"
L8_5:   td "Pm",LANTHANIDE,"Promethium"
L8_6:   td "Sm",LANTHANIDE,"Samarium"
L8_7:   td "Eu",LANTHANIDE,"Europium"
L8_8:   td "Gd",LANTHANIDE,"Gadolinium"
L8_9:   td "Tb",LANTHANIDE,"Terbium"
L8_10:  td "Dy",LANTHANIDE,"Dysprosium"
L8_11:  td "Ho",LANTHANIDE,"Holmium"
L8_12:  td "Er",LANTHANIDE,"Erbium"
L8_13:  td "Tm",LANTHANIDE,"Thulium"
L8_14:  td "Yb",LANTHANIDE,"Ytterbium"
L8_15:  td "Lu",LANTHANIDE,"Lutetium"
L8_16:  td "Hf",METAL_DE_TRANSITION,"Hafnium"
L8_17:  td "Ta",METAL_DE_TRANSITION,"Tantalum"
L8_18:  td "W",METAL_DE_TRANSITION,"Tungsten"
L8_19:  td "Re",METAL_DE_TRANSITION,"Rhenium"
L8_20:  td "Os",METAL_DE_TRANSITION,"Osmium"
L8_21:  td "Ir",METAL_DE_TRANSITION,"Iridium"
L8_22:  td "Pt",METAL_DE_TRANSITION,"Platinum"
L8_23:  td "Au",METAL_DE_TRANSITION,"Gold"
L8_24:  td "Hg",METAL_DE_TRANSITION,"Mercury"
L8_25:  td "Tl",METAL_PAUVRE,"Thallium"
L8_26:  td "Pb",METAL_PAUVRE,"Lead"
L8_27:  td "Bi",METAL_PAUVRE,"Bismuth"
L8_28:  td "Po",METAL_PAUVRE,"Polonium"
L8_29:  td "At",METALLOIDE,"Astatine"
L8_30:  td "Rn",GAZ_RARE,"Radon"
        tr

L8:     Node L8_31,L8_32,L9_1,L9_2,L9_3,L9_4,L9_5,L9_6,L9_7,L9_8,L9_9,L9_10,\
             L9_11,L9_12,L9_13,L9_14,L9_15,L9_16,L9_17,L9_18,L9_19,L9_20,\
             L9_21,L9_22,L9_23,L9_24,L9_25,L9_26,L9_27,L9_28,L9_29,L9_30
L8_31:  td "Fr",METAL_ALCALIN,"Francium"
L8_32:  td "Ra",METAL_ALCALINO_TERREUX,"Radium"
L9_1:   td "Ac",ACTINIDE,"Actinium"
L9_2:   td "Th",ACTINIDE,"Thorium"
L9_3:   td "Pa",ACTINIDE,"Protactinium"
L9_4:   td "U",ACTINIDE,"Uranium"
L9_5:   td "Np",ACTINIDE,"Neptunium"
L9_6:   td "Pu",ACTINIDE,"Plutonium"
L9_7:   td "Am",ACTINIDE,"Am�ricium"
L9_8:   td "Cm",ACTINIDE,"Curium"
L9_9:   td "Bk",ACTINIDE,"Berkelium"
L9_10:  td "Cf",ACTINIDE,"Californium"
L9_11:  td "Es",ACTINIDE,"Einsteinium"
L9_12:  td "Fm",ACTINIDE,"Fermium"
L9_13:  td "Md",ACTINIDE,"Mendelevium"
L9_14:  td "No",ACTINIDE,"Nobelium"
L9_15:  td "Lr",ACTINIDE,"Lawrencium"
L9_16:  td "Rf",METAL_DE_TRANSITION,"Rutherfordium"
L9_17:  td "Db",METAL_DE_TRANSITION,"Dubnium"
L9_18:  td "Sg",METAL_DE_TRANSITION,"Seaborgium"
L9_19:  td "Bh",METAL_DE_TRANSITION,"Bohrium"
L9_20:  td "Hs",METAL_DE_TRANSITION,"Hassium"
L9_21:  td "Mt",METAL_DE_TRANSITION,"Meitnerium"
L9_22:  td "Ds",METAL_DE_TRANSITION,"Darmstadtium"
L9_23:  td "Rg",METAL_DE_TRANSITION,"Roentgenium"
L9_24:  td "Cn",METAL_DE_TRANSITION,"Copernicium"
L9_25:  td "Nh",ELEMENT_NON_CLASSE,"Nihonium"
L9_26:  td "Fl",ELEMENT_NON_CLASSE,"Flerovium"
L9_27:  td "Mc",ELEMENT_NON_CLASSE,"Moscovium"
L9_28:  td "Lv",ELEMENT_NON_CLASSE,"Livermorium"
L9_29:  td "Ts",ELEMENT_NON_CLASSE,"Tennesine"
L9_30:  td "Og",ELEMENT_NON_CLASSE,"Organesson"
        tr

L9:     Node L9_31,L9_32
L9_31:  td "Uue",ELEMENT_NON_CLASSE,"Ununennium"
L9_32:  td "Ubn",ELEMENT_NON_CLASSE,"Unbinilium"
        tr

X=20
Y=25
Legend:
        Node Metalloide,\                       
             NonMetal,\                         
             Halogene,\                         
             GazRare,\                          
             MetalAlcalin,\                     
             MetalAlcalinoTerreux,\             
             MetalDeTransition,\                
             MetalPauvre,\                      
             Lanthanide,\                       
             Actinide,\                         
             Superactinide,\                    
             ElementNonClasse                   

Metalloide:
        LEGENDE "Metalloids",METALLOIDE
NonMetal:
        LEGENDE "Non Metal",NON_METAL
Halogene:
        LEGENDE "Reactive nonmetals",HALOGENE
GazRare:
        LEGENDE "Noble gases",GAZ_RARE
MetalAlcalin:
        LEGENDE "Alkali metals",METAL_ALCALIN
MetalAlcalinoTerreux:
        LEGENDE "Alkaline earth metals",METAL_ALCALINO_TERREUX
MetalDeTransition:
        LEGENDE "Ttransition metals",METAL_DE_TRANSITION
MetalPauvre:
        LEGENDE "Post transition metals",METAL_PAUVRE
Lanthanide:
        LEGENDE "Lanthanides",LANTHANIDE
Actinide:
        LEGENDE "Actinides",ACTINIDE
Superactinide:
        LEGENDE "Superactinides",SUPERACTINIDE
ElementNonClasse:
        LEGENDE "unknown elements",ELEMENT_NON_CLASSE

appsize application