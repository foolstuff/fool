FOOLOS:
BACKGROUND = OliveDrab
EXITTITLE equ "linked list"
EXITMESSAGE equ "what a link"
WINDOW equ 100,100,640,480
NOMOUSE:
;VESAMODE=SVGA
;NOCLS:

include "sys/fool.inc"

viewram:
.view:  Gnode 50,100,530,290,.txt
.txt:   Txt 50,5,0,0,Pink,.str
.str:   db "test linked list",0


testlist:
        dd chain,.001,.003
.001:   dd ?,0,.002
.002:   dd ?,.001,.003
.003:   dd ?,.002,0


;this is an adressing mode.
;then, it is more like a clib than fool

chain:
.call=0
.first=4
.last=8

;link:
.data=0
.prev=4
.next=8

        ret

.fromlist:

.tolist:

.delete:
;disconnect the esi element from the list
        mov eax,[esi+.prev]
        mov ebx,[esi+.next]
        mov [eax+.next],ebx
        mov [ebx+.prev],eax
        ret

.insert:
;insert the eax element in the list after esi element
        mov ebx,[esi+.next]
        mov [esi+.next],eax
        mov [eax+.next],ebx
        mov [eax+.prev],esi
        mov [ebx+.prev],eax
        ret
