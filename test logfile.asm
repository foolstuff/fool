virtual as 'log'
LOG::
end virtual

virtual as 'unit'
UNIT::
end virtual

virtual as 'dat'
DAT::
end virtual


macro log str& {
virtual LOG
db str
end virtual
}

macro unit str& {
virtual UNIT
db str
end virtual
}

macro dat str& {
virtual DAT
db str
end virtual
}

log 'hey'
unit 'ho!'
dat 'ramucho'