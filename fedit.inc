FOOLOS:
;TRANSPARENCY=200
VESAMODE=SVGA
VESA32:
ONTOP=1
WINDOW equ 100,100,800,500
include 'sys/fool.inc'

        Gnode 0,0,0,0,cmd,prompt,.shell,.back
.shell: Gptr 200,10,shell.debug
.back:  Box 0,0,WXL,WYL,MidnightBlue

cmd:
        Gnode 0,0,0,0,.reply,.logo,.exec,.prev,.next
.reply: Txt 30,10,0,0,Gold,.ver
.logo:  Put 10,10,0,0,0,fasmicon
.ver    db 'fedit 0.1 ',10,13,\
        'with fasm, fool, passion and waste of time',0

.prev:  Otk key.up,next
        Node history.prev,prompt.reset
.next:  Otk key.down,next
        Node history.next,prompt.reset
.exec:  Otk key.enter,next
        Node prompt.buffer,prompt.reset,history.push

history:
.sel    db 0

.stack  rd 256
.sptr   db 0

.bmp:   Bmp 670,0,128,256,0
.buffer rb 256*256
.bptr   dd 0

.push:
        Asm next
        mov al,[history.sptr]
        inc al
        mov [history.sel],al
        mov ebx,[.bptr]
        xor ecx,ecx
@@:
        mov al,[prompt.prompt+ecx]
        or al,al
        je @f
        mov [.buffer+ebx+ecx],al
        inc ecx
        jmp @b
@@:
        mov word[.buffer+ebx+ecx],0d0ah
        lea ebx,[ebx+ecx+2]
        movzx ebx,bx
        mov [.bptr],ebx
        inc [.sptr]
        movzx eax,[.sptr]
        mov [.stack+eax*4],ebx
        ret

.get:
        movzx eax,[.sel]
        mov ebx,[.stack+eax*4]
        xor ecx,ecx
@@:
        mov al,[.buffer+ebx+ecx]
        or al,al
        je @f
        cmp al,0dh
        je @f
        cmp al,0ah
        je @f
        mov [prompt.prompt+ecx],al
        inc ecx
        jmp @b
@@:
        mov byte[prompt.prompt+ecx],0
        ret
.prev:
        Asm next
        dec [.sel]
        jnl @f
        mov [.sel],0
@@:
        call history.get
        ret
.next:
        Asm next
        mov al,[.sptr]
        inc [.sel]
        cmp [.sel],al
        jle @f
        mov [.sel],al
@@:
        call history.get
        ret


.view:  Gnode 10,60,0,0,.txt,.index,next
        Asm next
        movzx eax,[.sel]
        lea eax,[eax*9]
        mov [.index+txt.y],eax
        ret

.txt:   Txt 0,0,0,0,White,.buffer
.index: Txt -7,0,0,0,White,@f
@@:     db '>',0

prompt: Gnode 10,40,0,0,\
        .vtxt,history.view,.view,.keys
.keys:  KeyFlush .input
.view:  Gnode 0,35,0,0,.txt,.caret,.frame,.box
.frame: Frame 0,0,(.end-.prompt+1)*6,10,Red
.box:   Box 0,0,(.end-.prompt+1)*6,10,Gray/4
.caret: Caret 1,9,5,2,White,.input,.txt
.txt:   Txt 1,1,0,0,Yellow,.prompt
.vtxt:  Vtxt 0,0,0,0,Yellow,.prompt

.input: Input .buffer,azerty
.buffer:Buffer .end-.prompt,.prompt
.prompt rb 127
.end    db 0
.reset: Asm next
        mov dword[prompt.input+input.cursor],0
        ret

;include 'keymap.inc'
